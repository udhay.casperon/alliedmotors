package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.NewCar.NewCar_Response;
import com.app.alliedmotor.repository.NewCarMakeList_Repository;
import com.app.alliedmotor.repository.NewCarRepository;

import java.util.HashMap;

public class NewCarviewModel extends ViewModel {

    private NewCarRepository newCarRepository = new NewCarRepository();

    private NewCarMakeList_Repository makeList_repository = new NewCarMakeList_Repository();

    private MutableLiveData<NewCar_Response> selectedMakeListMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<NewCar_Response> newCarMutableLiveData = new MutableLiveData<>();


    public LiveData<NewCar_Response> getnewCardata() {
        newCarMutableLiveData = newCarRepository.getNewCardataResponse();
        return newCarMutableLiveData;
    }

    public LiveData<NewCar_Response> getselectedMakeList(String url, HashMap<String,String> inputparam) {
        selectedMakeListMutableLiveData = makeList_repository.getselectedBrand_Data(url,inputparam);
        return selectedMakeListMutableLiveData;
    }


}
