package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.ForgotEmail.Response_ForgotEmail;
import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.repository.ForgotEmailRepository;
import com.app.alliedmotor.repository.VerifyOTPRepository;

import java.util.HashMap;

public class ForgotEmailviewModel extends ViewModel {
    private ForgotEmailRepository forgotEmailRepository = new ForgotEmailRepository();

    private MutableLiveData<Response_ForgotEmail> forgotEmailMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_ForgotEmail> getforgotemaildata( HashMap<String, String> header) {
        forgotEmailMutableLiveData = forgotEmailRepository.getforgotemailResponse(header);
        return forgotEmailMutableLiveData;
    }


    private VerifyOTPRepository verifyOTPRepository=new VerifyOTPRepository();

    private MutableLiveData<Response_VerifyOTP> verifyOTPMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_VerifyOTP> getVerifyOTpdata(HashMap<String,String>header,HashMap<String,String>request) {
        verifyOTPMutableLiveData = verifyOTPRepository.verfiyotp(header,request);
        return verifyOTPMutableLiveData;
    }


}
