package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.TermsCondition.Response_Terms;
import com.app.alliedmotor.repository.TermsConditionRepository;

public class TermsviewModel extends ViewModel {
    private TermsConditionRepository termsRepository = new TermsConditionRepository();

    private MutableLiveData<Response_Terms> termsMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_Terms> getTermsdata() {
        termsMutableLiveData = termsRepository.getTermsdataResponse();
        return termsMutableLiveData;
    }
}
