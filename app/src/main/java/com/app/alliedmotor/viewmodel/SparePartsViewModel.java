package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.model.SpareParts_Response;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.CarMakeListRepository;
import com.app.alliedmotor.repository.CarModelList_Repository;
import com.app.alliedmotor.repository.SparePartsRepository;
import com.app.alliedmotor.repository.UserProfileRepository;

import java.util.HashMap;

public class SparePartsViewModel extends ViewModel {

    private SparePartsRepository sparePartsRepository=new SparePartsRepository();

    private MutableLiveData<SpareParts_Response> spareParts_responseMutableLiveData = new MutableLiveData<>();

    public LiveData<SpareParts_Response> func_spareparts(HashMap<String,String>request) {
        spareParts_responseMutableLiveData = sparePartsRepository.sparepartsinfo(request);
        return spareParts_responseMutableLiveData;
    }

    private CarMakeListRepository makeListRepository=new CarMakeListRepository();
    private CarModelList_Repository modelList_repository=new CarModelList_Repository();

    private MutableLiveData<Response_CarMakeList>carMakeListMutableLiveData=new MutableLiveData<>();
    private MutableLiveData<Response_CarModelList>carModelListMutableLiveData=new MutableLiveData<>();


    public LiveData<Response_CarMakeList> getcarmakelist(HashMap<String,String>request) {
        carMakeListMutableLiveData = makeListRepository.getMakeListMutableLiveData(request);
        return carMakeListMutableLiveData;
    }

    public LiveData<Response_CarModelList> getCarModeldata(HashMap<String,String> request) {
        carModelListMutableLiveData = modelList_repository.getCarModelList(request);
        return carModelListMutableLiveData;
    }



}
