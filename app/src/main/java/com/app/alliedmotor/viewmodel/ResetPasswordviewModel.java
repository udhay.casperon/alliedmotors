package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.ResetPassword.Response_ResetPassword;
import com.app.alliedmotor.repository.ResetPasswordRepository;

import java.util.HashMap;

public class ResetPasswordviewModel extends ViewModel {
    private ResetPasswordRepository resetPasswordRepository = new ResetPasswordRepository();

    private MutableLiveData<Response_ResetPassword> change_passwordData = new MutableLiveData<>();

    public LiveData<Response_ResetPassword> getresetpassword(HashMap<String,String> request_changepassword) {
        change_passwordData = resetPasswordRepository.getResetpasswordResponse(request_changepassword);
        return change_passwordData;
    }
}
