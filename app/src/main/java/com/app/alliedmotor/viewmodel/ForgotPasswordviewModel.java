package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.ForgotPassword.Response_ForgotPassword;
import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.repository.ForgotPasswordRepository;
import com.app.alliedmotor.repository.VerifyOTPRepository;

import java.util.HashMap;

public class ForgotPasswordviewModel extends ViewModel {
    private ForgotPasswordRepository forgotPasswordRepository = new ForgotPasswordRepository();

    private MutableLiveData<Response_ForgotPassword> forgotPasswordMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_ForgotPassword> getforgotassworddata( HashMap<String, String> header) {
        forgotPasswordMutableLiveData = forgotPasswordRepository.getSignupResponse(header);
        return forgotPasswordMutableLiveData;
    }


    private VerifyOTPRepository verifyOTPRepository=new VerifyOTPRepository();

    private MutableLiveData<Response_VerifyOTP> verifyOTPMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_VerifyOTP> getVerifyOTpdata(HashMap<String,String>header,HashMap<String,String>request) {
        verifyOTPMutableLiveData = verifyOTPRepository.verfiyotp(header,request);
        return verifyOTPMutableLiveData;
    }




}
