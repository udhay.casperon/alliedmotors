package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.Response_SearchbyKeywords;
import com.app.alliedmotor.repository.SearchKeywordRepository;

import java.util.HashMap;

public class SearchKeywordviewModel extends ViewModel {
    private SearchKeywordRepository searchKeywordRepository = new SearchKeywordRepository();

    private MutableLiveData<Response_SearchbyKeywords> searchbyKeywordsData = new MutableLiveData<>();

    public LiveData<Response_SearchbyKeywords> searchbyKeywordsLiveData(HashMap<String, String> request) {
        searchbyKeywordsData = searchKeywordRepository.getSearchDataList(request);
        return searchbyKeywordsData;
    }
}
