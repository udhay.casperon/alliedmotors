package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.Response_OtpRequest;
import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.model.Sample.Response_UserCreation;
import com.app.alliedmotor.model.Signup.Response_SignupResponse;
import com.app.alliedmotor.repository.OTPRequest_Repository;
import com.app.alliedmotor.repository.SignupRepository;
import com.app.alliedmotor.repository.UserCreationRepository;
import com.app.alliedmotor.repository.VerifyOTPRepository;

import java.util.HashMap;

public class VerifyOTPviewModel extends ViewModel {

    private VerifyOTPRepository verifyOTPRepository=new VerifyOTPRepository();

    private MutableLiveData<Response_VerifyOTP> verifyOTPMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_VerifyOTP> getVerifyOTpdata(HashMap<String,String>header,HashMap<String,String>request) {
        verifyOTPMutableLiveData = verifyOTPRepository.verfiyotp(header,request);
        return verifyOTPMutableLiveData;
    }


    private UserCreationRepository userCreationRepository=new UserCreationRepository();

    private MutableLiveData<Response_UserCreation> userCreationMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_UserCreation> getusercreation(HashMap<String,String>request) {
        userCreationMutableLiveData = userCreationRepository.usercreation(request);
        return userCreationMutableLiveData;
    }


    private OTPRequest_Repository otpRequest_repository=new OTPRequest_Repository();

    private MutableLiveData<Response_OtpRequest> otpRequestMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_OtpRequest> getOTPRequestdata(HashMap<String,String>request) {
        otpRequestMutableLiveData = otpRequest_repository.getOtpRequest(request);
        return otpRequestMutableLiveData;
    }


    private SignupRepository signupRepository = new SignupRepository();

    private MutableLiveData<Response_SignupResponse> signupData = new MutableLiveData<>();

    public LiveData<Response_SignupResponse> getSignupData(HashMap<String, String> signup_request) {
        signupData = signupRepository.getSignupResponse(signup_request);
        return signupData;

    }

}
