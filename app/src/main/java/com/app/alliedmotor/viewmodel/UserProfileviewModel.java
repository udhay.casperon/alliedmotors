package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.Sample1.Response_UserNotificationstatuschange;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.repository.UserNotificationStatuschangeRepository;
import com.app.alliedmotor.repository.UserProfileRepository;

import java.util.HashMap;

public class UserProfileviewModel extends ViewModel {
    private UserProfileRepository userProfileRepository = new UserProfileRepository();

    private MutableLiveData<Response_UserProfile> userProfileMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_UserProfile> getuserprofiledata(HashMap<String,String>header) {
        userProfileMutableLiveData = userProfileRepository.getuserprofileResponse(header);
        return userProfileMutableLiveData;
    }



    private UserNotificationStatuschangeRepository userNotificationStatuschangeRepository = new UserNotificationStatuschangeRepository();

    private MutableLiveData<Response_UserNotificationstatuschange> notificationstatuschangeMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_UserNotificationstatuschange> getnotifysatauschangedata(HashMap<String,String>header, HashMap<String,String>request) {
        notificationstatuschangeMutableLiveData = userNotificationStatuschangeRepository.notifystatuschange(header,request);
        return notificationstatuschangeMutableLiveData;
    }






}
