package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.Signup.Response_SignupResponse;
import com.app.alliedmotor.repository.SignupRepository;

import java.util.HashMap;

public class SignupviewModel extends ViewModel {

    private SignupRepository signupRepository = new SignupRepository();

    private MutableLiveData<Response_SignupResponse> signupData = new MutableLiveData<>();

    public LiveData<Response_SignupResponse> getSignupData(HashMap<String, String> signup_request) {
        signupData = signupRepository.getSignupResponse(signup_request);
        return signupData;

    }
}
