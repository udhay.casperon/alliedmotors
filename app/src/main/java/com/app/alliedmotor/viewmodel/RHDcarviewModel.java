package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.RHDCar.Response_RHDCar;
import com.app.alliedmotor.repository.RHDCarRepository;
import com.app.alliedmotor.repository.RHdCarMakeList_Repository;

import java.util.HashMap;

public class RHDcarviewModel extends ViewModel {

    private RHDCarRepository rhdCarRepository = new RHDCarRepository();

    private MutableLiveData<Response_RHDCar> rhdCarMutableLiveData = new MutableLiveData<>();

    private RHdCarMakeList_Repository makeList_repository = new RHdCarMakeList_Repository();

    private MutableLiveData<Response_RHDCar> selectedMakeListMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_RHDCar> getRHDCardata() {
        rhdCarMutableLiveData = rhdCarRepository.getRHDCardataResponse();
        return rhdCarMutableLiveData;
    }

    public LiveData<Response_RHDCar> getselectedMakeList(String url, HashMap<String,String> inputparam) {
        selectedMakeListMutableLiveData = makeList_repository.getselectedBrand_Data(url,inputparam);
        return selectedMakeListMutableLiveData;
    }


}
