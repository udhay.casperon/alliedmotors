package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.ChangePassword.Response_ChangePassword;
import com.app.alliedmotor.repository.ChangePasswordRepository;

import java.util.HashMap;

public class ChangePasswordviewModel extends ViewModel {
    private ChangePasswordRepository changepasswordRepository = new ChangePasswordRepository();

    private MutableLiveData<Response_ChangePassword> change_passwordData = new MutableLiveData<>();

    public LiveData<Response_ChangePassword> getChangepasswordData(HashMap<String,String> request_changepassword, HashMap<String,String> header1) {
        change_passwordData = changepasswordRepository.getChangepasswordResponse(request_changepassword,header1);
        return change_passwordData;
    }
}
