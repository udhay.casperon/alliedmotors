package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.Privacy_Policy.PrivacyPolicy_Response;
import com.app.alliedmotor.repository.PrivacyPolicyRepository;

public class PrivacyPolicyviewModel extends ViewModel {
    private PrivacyPolicyRepository privacyPolicyRepository = new PrivacyPolicyRepository();

    private MutableLiveData<PrivacyPolicy_Response> privacyMutableLiveData = new MutableLiveData<>();

    public LiveData<PrivacyPolicy_Response> getprivacypolicydata() {
        privacyMutableLiveData = privacyPolicyRepository.getprivacyResponse();
        return privacyMutableLiveData;
    }
}
