package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.Statuschange_OfferNotificationRepository;
import com.app.alliedmotor.repository.UserProfileRepository;

import java.util.HashMap;

public class AppInfoviewModel extends ViewModel {

    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }


    private UserProfileRepository userProfileRepository = new UserProfileRepository();

    private MutableLiveData<Response_UserProfile> userProfileMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_UserProfile> getuserprofiledata(HashMap<String,String>header) {
        userProfileMutableLiveData = userProfileRepository.getuserprofileResponse(header);
        return userProfileMutableLiveData;
    }


    private Statuschange_OfferNotificationRepository offerNotificationRepository = new Statuschange_OfferNotificationRepository();
    private MutableLiveData<Response_OfferNotify> offerNotifyMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_OfferNotify> statuschange_offer(HashMap<String,String>header,HashMap<String,String> request) {
        offerNotifyMutableLiveData = offerNotificationRepository.offernotifystatuschange(header,request);
        return offerNotifyMutableLiveData;
    }


}
