package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.LoginRepository;

import java.util.HashMap;

public class LoginviewModel extends ViewModel {
    private LoginRepository loginRepository = new LoginRepository();

    private MutableLiveData<LoginResponse> loginData = new MutableLiveData<>();

    public LiveData<LoginResponse> getLoginData(HashMap<String, String> header) {
        loginData = loginRepository.getLoginResponse(header);
        return loginData;
    }



    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }


}
