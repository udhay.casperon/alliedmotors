package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.LuxuryCar.Response_LuxuryCar;
import com.app.alliedmotor.repository.LuxuryCarMakeList_Repository;
import com.app.alliedmotor.repository.LuxuryCarRepository;

import java.util.HashMap;

public class LuxurycarviewModel extends ViewModel {

    private LuxuryCarRepository luxuryCarRepository = new LuxuryCarRepository();

    private MutableLiveData<Response_LuxuryCar> luxuryCarMutableLiveData = new MutableLiveData<>();

    private LuxuryCarMakeList_Repository makeList_repository = new LuxuryCarMakeList_Repository();

    private MutableLiveData<Response_LuxuryCar> selectedMakeListMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_LuxuryCar> getluxuryCardata() {
        luxuryCarMutableLiveData = luxuryCarRepository.getluxuryCardataResponse();
        return luxuryCarMutableLiveData;
    }



    public LiveData<Response_LuxuryCar> getselectedMakeList(String url, HashMap<String,String> inputparam) {
        selectedMakeListMutableLiveData = makeList_repository.getselectedBrand_Data(url,inputparam);
        return selectedMakeListMutableLiveData;
    }

}
