package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.ProfileUpdate.Response_ProfileUpdate;
import com.app.alliedmotor.model.Response_OtpRequest;
import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.repository.OTPRequest_Repository;
import com.app.alliedmotor.repository.ProfileUpdateRepository;
import com.app.alliedmotor.repository.UserProfileRepository;
import com.app.alliedmotor.repository.VerifyOTPRepository;

import java.util.HashMap;

public class ProfileUpdateviewModel extends ViewModel {
    private ProfileUpdateRepository profileUpdateRepository = new ProfileUpdateRepository();

    private MutableLiveData<Response_ProfileUpdate> response_profileUpdateMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_ProfileUpdate> getProfileupdateData(HashMap<String,String>request_profileupdate, HashMap<String,String>header1) {
        response_profileUpdateMutableLiveData = profileUpdateRepository.getProfileUpdateResponse(request_profileupdate,header1);
        return response_profileUpdateMutableLiveData;
    }


    private OTPRequest_Repository otpRequest_repository = new OTPRequest_Repository();

    private MutableLiveData<Response_OtpRequest> otpRequestMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_OtpRequest> OtpRequestData(HashMap<String,String>request) {
        otpRequestMutableLiveData = otpRequest_repository.getOtpRequest(request);
        return otpRequestMutableLiveData;
    }


    //Verify OTP
    private VerifyOTPRepository verifyOTPRepository=new VerifyOTPRepository();

    private MutableLiveData<Response_VerifyOTP> verifyOTPMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_VerifyOTP> getVerifyOTpdata(HashMap<String,String>header,HashMap<String,String>request) {
        verifyOTPMutableLiveData = verifyOTPRepository.verfiyotp(header,request);
        return verifyOTPMutableLiveData;
    }


    private UserProfileRepository userProfileRepository = new UserProfileRepository();

    private MutableLiveData<Response_UserProfile> userProfileMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_UserProfile> getuserprofiledata(HashMap<String,String>header) {
        userProfileMutableLiveData = userProfileRepository.getuserprofileResponse(header);
        return userProfileMutableLiveData;
    }



}
