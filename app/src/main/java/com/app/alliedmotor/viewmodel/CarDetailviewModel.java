package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.CarDetail.Response_CarDetail;
import com.app.alliedmotor.model.DeleteFavourite.Response_DeleteFavourite;
import com.app.alliedmotor.model.FilterFuelTypeResponse;
import com.app.alliedmotor.model.FilterVarientResponse;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.Response_AddFavourite.Response_AddFavourite;
import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.model.Response_FilterModelOption;
import com.app.alliedmotor.repository.AddFavouriteRepository;
import com.app.alliedmotor.repository.AddQuoteRepository;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.CarDetail_Repository;
import com.app.alliedmotor.repository.DeleteFavouriteRepository;
import com.app.alliedmotor.repository.Filter_FuelType_Repository;
import com.app.alliedmotor.repository.Filter_ModelOption_Repository;
import com.app.alliedmotor.repository.RepositoryGetQuote;

import java.util.HashMap;

public class CarDetailviewModel extends ViewModel {
    private CarDetail_Repository carDetail_repository = new CarDetail_Repository();
    private AddFavouriteRepository addFavouriteRepository=new AddFavouriteRepository();
    private AddQuoteRepository addQuoteRepository=new AddQuoteRepository();


    private MutableLiveData<Response_CarDetail> carDetailMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Response_AddFavourite> carDetailFavouritedata = new MutableLiveData<>();
    private MutableLiveData<Response_Addquote> addquoteMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_CarDetail> getCarDetaildata(String url,HashMap<String,String>header,HashMap<String,String>inputparam) {
        carDetailMutableLiveData = carDetail_repository.getCardetail_Data(url,header,inputparam);
        return carDetailMutableLiveData;
    }


    public LiveData<Response_AddFavourite> addfavouritedata(HashMap<String,String>header, HashMap<String,String>request) {
        carDetailFavouritedata = addFavouriteRepository.addFavourite(header,request);
        return carDetailFavouritedata;
    }

    public LiveData<Response_Addquote> addquoteLiveData(HashMap<String,String>header, HashMap<String,String>request) {
        addquoteMutableLiveData = addQuoteRepository.getAddquote(header,request);
        return addquoteMutableLiveData;
    }


    private Filter_FuelType_Repository fuelType_repository=new Filter_FuelType_Repository();
    private Filter_ModelOption_Repository fuel_modeloption_repository=new Filter_ModelOption_Repository();
    private MutableLiveData<FilterFuelTypeResponse> fuelType_repositoryMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<FilterVarientResponse> fuelmodeloptiondata = new MutableLiveData<>();



    public LiveData<FilterFuelTypeResponse> FueltypeData(HashMap<String,String>request) {
        fuelType_repositoryMutableLiveData = fuelType_repository.getFilterdata(request);
        return fuelType_repositoryMutableLiveData;

    }
      /*  public LiveData<FilterVarientResponse> modeloptionLiveData(HashMap<String,String> request) {
            fuelmodeloptiondata = fuel_modeloption_repository.getFilter_varientdata(request);
            return fuelmodeloptiondata;

        }*/

    private MutableLiveData<Response_DeleteFavourite> deleteFavouriteMutableLiveData = new MutableLiveData<>();
    private DeleteFavouriteRepository deleteFavouriteRepository = new DeleteFavouriteRepository();


    public LiveData<Response_DeleteFavourite> deleteFavourite(HashMap<String,String>header, HashMap<String,String>request) {
        deleteFavouriteMutableLiveData = deleteFavouriteRepository.getDeleteFavouriteMutableLiveData(header,request);
        return deleteFavouriteMutableLiveData;
    }


    private MutableLiveData<FilterFuelTypeResponse> fuelTypeResponseMutableLiveData = new MutableLiveData<>();
    private Filter_FuelType_Repository filterFuelTypeRepository = new Filter_FuelType_Repository();


    public LiveData<FilterFuelTypeResponse> filterFuelTypedata(HashMap<String,String>request) {
        fuelTypeResponseMutableLiveData = filterFuelTypeRepository.getFilterdata(request);
        return fuelTypeResponseMutableLiveData;
    }


    private MutableLiveData<Response_FilterModelOption> filterModelOptionMutableLiveData = new MutableLiveData<>();
    private Filter_ModelOption_Repository filter_modelOption_repository = new Filter_ModelOption_Repository();


    public LiveData<Response_FilterModelOption> filtermodeloption(HashMap<String,String>request) {
        filterModelOptionMutableLiveData = filter_modelOption_repository.getFilter_varientdata(request);
        return filterModelOptionMutableLiveData;
    }



    private RepositoryGetQuote repositoryGetQuote = new RepositoryGetQuote();
    private MutableLiveData<Response_GetQuote> getQuoteMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_GetQuote> getquotedata(HashMap<String,String>header) {
        getQuoteMutableLiveData = repositoryGetQuote.getQuoteResponse(header);
        return getQuoteMutableLiveData;
    }



    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }





}
