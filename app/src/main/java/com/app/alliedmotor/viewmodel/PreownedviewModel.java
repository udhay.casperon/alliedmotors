package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.FileUpload.Response_FileUpload;
import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.model.Response_SellPreOwned;
import com.app.alliedmotor.repository.CarMakeListRepository;
import com.app.alliedmotor.repository.CarModelList_Repository;
import com.app.alliedmotor.repository.FileUpload_Repository;
import com.app.alliedmotor.repository.PreOwnedRepository;
import com.app.alliedmotor.repository.SellPreOwned_Repository;

import java.util.HashMap;

public class PreownedviewModel extends ViewModel {

    private PreOwnedRepository preOwnedRepository=new PreOwnedRepository();
    private CarMakeListRepository makeListRepository=new CarMakeListRepository();
    private CarModelList_Repository modelList_repository=new CarModelList_Repository();
    private SellPreOwned_Repository sellPreOwned_repository=new SellPreOwned_Repository();

    private FileUpload_Repository fileUpload_repository=new FileUpload_Repository();

    private MutableLiveData<Response_CarMakeList>carMakeListMutableLiveData=new MutableLiveData<>();
    private MutableLiveData<Response_CarModelList>carModelListMutableLiveData=new MutableLiveData<>();

    private MutableLiveData<Response_PreOwned> preOwnedMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<Response_FileUpload> fileUploadMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Response_SellPreOwned> sellPreOwnedMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_PreOwned> getpreowneddata(String url,HashMap<String, String> request) {
        preOwnedMutableLiveData = preOwnedRepository.getPreOwnedResponse(url,request);
        return preOwnedMutableLiveData;
    }

    public LiveData<Response_CarMakeList> getcarmakelist(HashMap<String,String>request) {
        carMakeListMutableLiveData = makeListRepository.getMakeListMutableLiveData(request);
        return carMakeListMutableLiveData;
    }

    public LiveData<Response_CarModelList> getCarModeldata(HashMap<String,String> request) {
        carModelListMutableLiveData = modelList_repository.getCarModelList(request);
        return carModelListMutableLiveData;
    }


    //Sell PreOwned car
    public LiveData<Response_SellPreOwned> sellpreowned(HashMap<String,String> request) {
        sellPreOwnedMutableLiveData = sellPreOwned_repository.data_sellPreowned(request);
        return sellPreOwnedMutableLiveData;
    }


    public LiveData<Response_FileUpload> FileImageUpload(HashMap<String,String> request) {
        fileUploadMutableLiveData = fileUpload_repository.data_fileupload(request);
        return fileUploadMutableLiveData;
    }


}
