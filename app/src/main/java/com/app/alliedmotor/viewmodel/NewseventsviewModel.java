package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.NewsEvents.Response_NewsEvents;
import com.app.alliedmotor.repository.NewsEventsRepository;

public class NewseventsviewModel extends ViewModel {
    private NewsEventsRepository newsEventsRepository = new NewsEventsRepository();

    private MutableLiveData<Response_NewsEvents> newsEventsMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_NewsEvents> getnewsEventsdata() {
        newsEventsMutableLiveData = newsEventsRepository.getprivacyResponse();
        return newsEventsMutableLiveData;
    }
}
