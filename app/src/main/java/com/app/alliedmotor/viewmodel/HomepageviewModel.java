package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.HomePage.Response_Homepage;
import com.app.alliedmotor.repository.HomePageRepository;

public class HomepageviewModel extends ViewModel {
    private HomePageRepository homepageRepository = new HomePageRepository();

    private MutableLiveData<Response_Homepage> homepageData = new MutableLiveData<>();

    public LiveData<Response_Homepage> getHomepagedata() {
        homepageData = homepageRepository.gethomepageResponse();
        return homepageData;
    }




}
