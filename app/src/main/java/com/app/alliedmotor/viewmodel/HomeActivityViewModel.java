package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.HomePage.Response_Homepage;
import com.app.alliedmotor.model.Response_SearchbyKeywords;
import com.app.alliedmotor.model.Sample1.Response_UserNotificationstatuschange;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.HomePageRepository;
import com.app.alliedmotor.repository.RepositoryGetQuote;
import com.app.alliedmotor.repository.UserNotificationStatuschangeRepository;
import com.app.alliedmotor.repository.SearchKeywordRepository;
import com.app.alliedmotor.repository.UserProfileRepository;

import java.util.HashMap;

public class HomeActivityViewModel extends ViewModel {

    private SearchKeywordRepository searchKeywordRepository = new SearchKeywordRepository();

    private MutableLiveData<Response_SearchbyKeywords> searchdata = new MutableLiveData<>();


    public LiveData<Response_SearchbyKeywords> getSearcheddata(HashMap<String,String> request) {
        searchdata = searchKeywordRepository.getSearchDataList(request);
        return searchdata;
    }



    private UserProfileRepository userProfileRepository = new UserProfileRepository();

    private MutableLiveData<Response_UserProfile> userProfileMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_UserProfile> getuserprofiledata(HashMap<String,String>header) {
        userProfileMutableLiveData = userProfileRepository.getuserprofileResponse(header);
        return userProfileMutableLiveData;
    }


    private HomePageRepository homepageRepository = new HomePageRepository();

    private MutableLiveData<Response_Homepage> homepageData = new MutableLiveData<>();

    public LiveData<Response_Homepage> getHomepagedata() {
        homepageData = homepageRepository.gethomepageResponse();
        return homepageData;
    }



    private UserNotificationStatuschangeRepository userNotificationStatuschangeRepository = new UserNotificationStatuschangeRepository();

    private MutableLiveData<Response_UserNotificationstatuschange> notificationstatuschangeMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_UserNotificationstatuschange> getnotifysatauschangedata(HashMap<String,String>header, HashMap<String,String>request) {
        notificationstatuschangeMutableLiveData = userNotificationStatuschangeRepository.notifystatuschange(header,request);
        return notificationstatuschangeMutableLiveData;
    }


    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }

    private RepositoryGetQuote repositoryGetQuote = new RepositoryGetQuote();
    private MutableLiveData<Response_GetQuote> getQuoteMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_GetQuote> getquotedata(HashMap<String,String>header) {
        getQuoteMutableLiveData = repositoryGetQuote.getQuoteResponse(header);
        return getQuoteMutableLiveData;
    }




}
