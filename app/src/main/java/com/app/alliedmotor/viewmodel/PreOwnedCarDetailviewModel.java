package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.PreOwned_CarDetail.Response_PreOwned_Detail;
import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.repository.AddQuoteRepository;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.CarMakeListRepository;
import com.app.alliedmotor.repository.CarModelList_Repository;
import com.app.alliedmotor.repository.PreOwnedCarDetail_Repository;
import com.app.alliedmotor.repository.RepositoryGetQuote;

import java.util.HashMap;

public class PreOwnedCarDetailviewModel extends ViewModel {
    private PreOwnedCarDetail_Repository carDetail_repository = new PreOwnedCarDetail_Repository();

    private AddQuoteRepository addQuoteRepository=new AddQuoteRepository();


    private MutableLiveData<Response_PreOwned_Detail> carDetailMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<Response_Addquote> addquoteMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_PreOwned_Detail> getCarDetaildata(String url) {
        carDetailMutableLiveData = carDetail_repository.getCardetail_Data(url);
        return carDetailMutableLiveData;
    }



    public LiveData<Response_Addquote> addquoteLiveData(HashMap<String,String> header, HashMap<String,String>request) {
        addquoteMutableLiveData = addQuoteRepository.getAddquote(header,request);
        return addquoteMutableLiveData;
    }

    private CarMakeListRepository makeListRepository=new CarMakeListRepository();
    private CarModelList_Repository modelList_repository=new CarModelList_Repository();

    private MutableLiveData<Response_CarMakeList>carMakeListMutableLiveData=new MutableLiveData<>();
    private MutableLiveData<Response_CarModelList>carModelListMutableLiveData=new MutableLiveData<>();


    public LiveData<Response_CarMakeList> getcarmakelist(HashMap<String,String>request) {
        carMakeListMutableLiveData = makeListRepository.getMakeListMutableLiveData(request);
        return carMakeListMutableLiveData;
    }

    public LiveData<Response_CarModelList> getCarModeldata(HashMap<String,String> request) {
        carModelListMutableLiveData = modelList_repository.getCarModelList(request);
        return carModelListMutableLiveData;
    }


    private RepositoryGetQuote repositoryGetQuote = new RepositoryGetQuote();
    private MutableLiveData<Response_GetQuote> getQuoteMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_GetQuote> getquotedata(HashMap<String,String>header) {
        getQuoteMutableLiveData = repositoryGetQuote.getQuoteResponse(header);
        return getQuoteMutableLiveData;
    }

    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }


}
