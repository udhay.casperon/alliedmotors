package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.MyOrderLatest.Response_MyOrderLatest;
import com.app.alliedmotor.model.NotificationStatusChange.Response_NotificationStatuschange;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.MyOrderRepository;
import com.app.alliedmotor.repository.Statuschange_NotificationRepository;

import java.util.HashMap;

public class MyOrderviewModel extends ViewModel {
    private MyOrderRepository myOrderRepository = new MyOrderRepository();

    private MutableLiveData<Response_MyOrderLatest> myOrderResponseMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_MyOrderLatest> getmyorderdata(HashMap<String,String>header) {
        myOrderResponseMutableLiveData = myOrderRepository.getMyOrderResponse(header);
        return myOrderResponseMutableLiveData;
    }

    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }


    private Statuschange_NotificationRepository statuschange_notificationRepository = new Statuschange_NotificationRepository();
    private MutableLiveData<Response_NotificationStatuschange> notificationStatuschangeMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_NotificationStatuschange> statuschange_notification(HashMap<String,String>header,HashMap<String,String> request) {
        notificationStatuschangeMutableLiveData = statuschange_notificationRepository.notifystatuschange(header,request);
        return notificationStatuschangeMutableLiveData;
    }


}
