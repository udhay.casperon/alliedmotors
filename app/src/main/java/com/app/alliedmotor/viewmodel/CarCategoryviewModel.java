package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.Car_CategoryList.Response_CarCategoryList;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.CarCategoryListRepository;
import com.app.alliedmotor.repository.CarMakeListRepository;
import com.app.alliedmotor.repository.CarModelList_Repository;
import com.app.alliedmotor.repository.RepositoryGetQuote;

import java.util.HashMap;

public class CarCategoryviewModel extends ViewModel {

    private CarCategoryListRepository carCategoryListRepository=new CarCategoryListRepository();

    private MutableLiveData<Response_CarCategoryList> newCarcategoryMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_CarCategoryList> getcategorycardata() {
        newCarcategoryMutableLiveData = carCategoryListRepository.getCarCategoryResponse();
        return newCarcategoryMutableLiveData;
    }


    private CarMakeListRepository makeListRepository=new CarMakeListRepository();
    private CarModelList_Repository modelList_repository=new CarModelList_Repository();

    private MutableLiveData<Response_CarMakeList>carMakeListMutableLiveData=new MutableLiveData<>();
    private MutableLiveData<Response_CarModelList>carModelListMutableLiveData=new MutableLiveData<>();


    public LiveData<Response_CarMakeList> getcarmakelist(HashMap<String,String>request) {
        carMakeListMutableLiveData = makeListRepository.getMakeListMutableLiveData(request);
        return carMakeListMutableLiveData;
    }

    public LiveData<Response_CarModelList> getCarModeldata(HashMap<String,String> request) {
        carModelListMutableLiveData = modelList_repository.getCarModelList(request);
        return carModelListMutableLiveData;
    }



    private RepositoryGetQuote repositoryGetQuote = new RepositoryGetQuote();
    private MutableLiveData<Response_GetQuote> getQuoteMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_GetQuote> getquotedata(HashMap<String,String>header) {
        getQuoteMutableLiveData = repositoryGetQuote.getQuoteResponse(header);
        return getQuoteMutableLiveData;
    }

    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }

}
