package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.FAQ_Page.Response_Faq;
import com.app.alliedmotor.repository.FAQRepository;

public class FAQviewModel extends ViewModel {
    private FAQRepository faqRepository = new FAQRepository();

    private MutableLiveData<Response_Faq> faqMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_Faq> getFaqdata() {
        faqMutableLiveData = faqRepository.getfaqdataResponse();
        return faqMutableLiveData;
    }
}
