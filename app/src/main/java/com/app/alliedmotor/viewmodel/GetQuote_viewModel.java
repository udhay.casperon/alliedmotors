package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.OrderCreation.OrderCreation_Response;
import com.app.alliedmotor.model.Response_DeleteQuote;
import com.app.alliedmotor.model.Response_GuestMakeOrder;
import com.app.alliedmotor.model.Response_UpdateQuote;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.DeleteQuoteRepository;
import com.app.alliedmotor.repository.GuestMakeOrderRepository;
import com.app.alliedmotor.repository.MakeOrderRepository;
import com.app.alliedmotor.repository.RepositoryGetQuote;
import com.app.alliedmotor.repository.UpdateQuoteRepository;

import java.util.HashMap;

public class GetQuote_viewModel extends ViewModel {
    private RepositoryGetQuote repositoryGetQuote = new RepositoryGetQuote();

    private DeleteQuoteRepository deleteQuoteRepository = new DeleteQuoteRepository();

    private UpdateQuoteRepository updateQuoteRepository=new UpdateQuoteRepository();

    private MutableLiveData<Response_UpdateQuote> updateQuoteMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<Response_GetQuote> getQuoteMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<Response_DeleteQuote> deleteQuoteMutableLiveData = new MutableLiveData<>();

    public LiveData<Response_GetQuote> getquotedata(HashMap<String,String>header) {
        getQuoteMutableLiveData = repositoryGetQuote.getQuoteResponse(header);
        return getQuoteMutableLiveData;
    }

    public LiveData<Response_DeleteQuote> deleteQuoteMutableLiveData(HashMap<String,String>header,HashMap<String, String> deletequote) {
        deleteQuoteMutableLiveData = deleteQuoteRepository.deleteQuoteMutableLiveData(header,deletequote);
        return deleteQuoteMutableLiveData;
    }


    public LiveData<Response_UpdateQuote> updateQuoteLiveData(HashMap<String,String>header,HashMap<String, String> updatequote) {
        updateQuoteMutableLiveData = updateQuoteRepository.updatequoteLiveData(header,updatequote);
        return updateQuoteMutableLiveData;
    }


    private MakeOrderRepository makeOrderRepository=new MakeOrderRepository();
    private MutableLiveData<OrderCreation_Response> orderCreationResponseMutableLiveData = new MutableLiveData<>();


    public LiveData<OrderCreation_Response> orderCreationResponseLiveData(HashMap<String,String>header,HashMap<String, String> ordercreation) {
        orderCreationResponseMutableLiveData = makeOrderRepository.orderCreationResponseMutableLiveData(header,ordercreation);
        return orderCreationResponseMutableLiveData;
    }


    private GuestMakeOrderRepository guestMakeOrderRepository=new GuestMakeOrderRepository();
    private MutableLiveData<Response_GuestMakeOrder> guestMakeOrderMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_GuestMakeOrder> guestmakeorder(HashMap<String, String> ordercreation) {
        guestMakeOrderMutableLiveData = guestMakeOrderRepository.guestmakeorder(ordercreation);
        return guestMakeOrderMutableLiveData;
    }




    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }





}
