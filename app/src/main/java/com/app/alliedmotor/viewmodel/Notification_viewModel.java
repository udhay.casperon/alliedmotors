package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.NotificationDelete.Response_NotifyDelete;
import com.app.alliedmotor.model.NotificationStatusChange.Response_NotificationStatuschange;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.model.Response_NotificationList.Response_NotificationList_folder;
import com.app.alliedmotor.repository.AppInfoRepository;
import com.app.alliedmotor.repository.DeleteNotificationRepository;
import com.app.alliedmotor.repository.NotificationListRepository;
import com.app.alliedmotor.repository.Statuschange_NotificationRepository;
import com.app.alliedmotor.repository.Statuschange_OfferNotificationRepository;

import java.util.HashMap;

public class Notification_viewModel extends ViewModel {
    private NotificationListRepository notificationListRepository = new NotificationListRepository();
    private MutableLiveData<Response_NotificationList_folder> notificationListMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_NotificationList_folder> orderCreationResponseLiveData(HashMap<String,String>header) {
        notificationListMutableLiveData = notificationListRepository.getNotificationListMutableLiveData(header);
        return notificationListMutableLiveData;
    }



    private Statuschange_NotificationRepository statuschange_notificationRepository = new Statuschange_NotificationRepository();
    private MutableLiveData<Response_NotificationStatuschange> notificationStatuschangeMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_NotificationStatuschange> statuschange_notification(HashMap<String,String>header,HashMap<String,String> request) {
        notificationStatuschangeMutableLiveData = statuschange_notificationRepository.notifystatuschange(header,request);
        return notificationStatuschangeMutableLiveData;
    }


    private AppInfoRepository appInfoRepository=new AppInfoRepository();

    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();



    public LiveData<Response_AppInfo> getAppInfodata(HashMap<String,String>header) {
        appInfoMutableLiveData = appInfoRepository.appInfo(header);
        return appInfoMutableLiveData;
    }




    private Statuschange_OfferNotificationRepository offerNotificationRepository = new Statuschange_OfferNotificationRepository();
    private MutableLiveData<Response_OfferNotify> offerNotifyMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_OfferNotify> statuschange_offer(HashMap<String,String>header,HashMap<String,String> request) {
        offerNotifyMutableLiveData = offerNotificationRepository.offernotifystatuschange(header,request);
        return offerNotifyMutableLiveData;
    }



    private DeleteNotificationRepository deleteNotificationRepository = new DeleteNotificationRepository();
    private MutableLiveData<Response_NotifyDelete> deleteMutableLiveData = new MutableLiveData<>();


    public LiveData<Response_NotifyDelete> notification_delete(HashMap<String,String>header,HashMap<String,String> request) {
        deleteMutableLiveData = deleteNotificationRepository.delete_notify(header,request);
        return deleteMutableLiveData;
    }



}
