package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.DeleteFavourite.Response_DeleteFavourite;
import com.app.alliedmotor.model.MyFavourite.Response_MyFavourite;
import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.repository.AddQuoteRepository;
import com.app.alliedmotor.repository.DeleteFavouriteRepository;
import com.app.alliedmotor.repository.MyFavouriteRepository;

import java.util.HashMap;

public class MyFavoouriteviewModel extends ViewModel {
    private MyFavouriteRepository myFavouriteRepository = new MyFavouriteRepository();
    private DeleteFavouriteRepository deleteFavouriteRepository = new DeleteFavouriteRepository();

    private MutableLiveData<Response_MyFavourite> myFavouriteMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Response_DeleteFavourite> deleteFavouriteMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<Response_Addquote> addquoteMutableLiveData = new MutableLiveData<>();
    private AddQuoteRepository addQuoteRepository = new AddQuoteRepository();

    public LiveData<Response_MyFavourite> getmyfavouritedata(HashMap<String,String>header) {
        myFavouriteMutableLiveData = myFavouriteRepository.getmyfavouriteResponse(header);
        return myFavouriteMutableLiveData;
    }


    public LiveData<Response_DeleteFavourite> deleteFavourite(HashMap<String,String>header,HashMap<String,String>request) {
        deleteFavouriteMutableLiveData = deleteFavouriteRepository.getDeleteFavouriteMutableLiveData(header,request);
        return deleteFavouriteMutableLiveData;
    }

    public LiveData<Response_Addquote> addquoteLiveData(HashMap<String,String>header,HashMap<String,String>request) {
        addquoteMutableLiveData = addQuoteRepository.getAddquote(header,request);
        return addquoteMutableLiveData;
    }

}
