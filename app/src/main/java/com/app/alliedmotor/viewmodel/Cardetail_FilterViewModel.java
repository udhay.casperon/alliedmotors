package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.FilterFuelTypeResponse;
import com.app.alliedmotor.model.Response_FilterModelOption;
import com.app.alliedmotor.repository.Filter_FuelType_Repository;
import com.app.alliedmotor.repository.Filter_ModelOption_Repository;

import java.util.HashMap;

public class Cardetail_FilterViewModel extends ViewModel {

    private MutableLiveData<FilterFuelTypeResponse> fuelTypeResponseMutableLiveData = new MutableLiveData<>();
    private Filter_FuelType_Repository filterFuelTypeRepository = new Filter_FuelType_Repository();


    public LiveData<FilterFuelTypeResponse> filterFuelTypedata(HashMap<String,String>request) {
        fuelTypeResponseMutableLiveData = filterFuelTypeRepository.getFilterdata(request);
        return fuelTypeResponseMutableLiveData;
    }


    private MutableLiveData<Response_FilterModelOption> filterModelOptionMutableLiveData = new MutableLiveData<>();
    private Filter_ModelOption_Repository filter_modelOption_repository = new Filter_ModelOption_Repository();


    public LiveData<Response_FilterModelOption> filtermodeloption(HashMap<String,String>request) {
        filterModelOptionMutableLiveData = filter_modelOption_repository.getFilter_varientdata(request);
        return filterModelOptionMutableLiveData;
    }

}
