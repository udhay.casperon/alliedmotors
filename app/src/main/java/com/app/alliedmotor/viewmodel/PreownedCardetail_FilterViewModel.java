package com.app.alliedmotor.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.repository.CarMakeListRepository;
import com.app.alliedmotor.repository.CarModelList_Repository;

import java.util.HashMap;

public class PreownedCardetail_FilterViewModel extends ViewModel {


    private CarMakeListRepository makeListRepository=new CarMakeListRepository();
    private CarModelList_Repository modelList_repository=new CarModelList_Repository();

    private MutableLiveData<Response_CarMakeList>carMakeListMutableLiveData=new MutableLiveData<>();
    private MutableLiveData<Response_CarModelList>carModelListMutableLiveData=new MutableLiveData<>();


    public LiveData<Response_CarMakeList> getcarmakelist(HashMap<String,String>request) {
        carMakeListMutableLiveData = makeListRepository.getMakeListMutableLiveData(request);
        return carMakeListMutableLiveData;
    }

    public LiveData<Response_CarModelList> getCarModeldata(HashMap<String,String> request) {
        carModelListMutableLiveData = modelList_repository.getCarModelList(request);
        return carModelListMutableLiveData;
    }


}
