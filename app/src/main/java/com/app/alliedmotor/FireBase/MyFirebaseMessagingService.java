package com.app.alliedmotor.FireBase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.FCMMessagePojo;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.view.Activity.NotificationList_Activity;
import com.app.alliedmotor.view.Activity.SplashScreen;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {



   /* public static final int NOTIFICATION_ID = 1;
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    Bitmap remote_picture_reciver = null;
    NotificationCompat.Builder builder;
    Context context;
    CharSequence title = "";
    private boolean is_driver_handicap = false;
    private SQLiteDatabase dataBase;
    private String driverID = "", driver_image = "", driverName = "", driverEmail = "", driverImage = "", driverRating = "",
            driverLat = "", driverLong = "", driverTime = "", rideID = "", driverMobile = "",
            driverCar_no = "", driverCar_model = "";
    ;
    private String key1 = "", key2 = "", key3 = "", key5 = "", key8 = "", key9 = "", message = "",cur_timestamp="", action = "", key4 = "",key11="", key6 = "", key7 = "", key10 = "", msg1, banner;
    private Boolean isInternetPresent = false;
    static SimpleDateFormat df_time = new SimpleDateFormat("hh:mm a");
    private String driver_id = "";
    String key12="";
    private boolean isAppInfoAvailable = false;

    private boolean isriderequest = false;

    String body="";
    String alliedtitle="";
    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);

        // Saving reg id to shared preferences
//        storeRegIdInPref(refreshedToken);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
        // Notify UI that registration has completed, so the progress indicator can be hidden.
//        Intent registrationComplete = new Intent(Bitmap.Config.REGISTRATION_COMPLETE);
//        registrationComplete.putExtra("token", refreshedToken);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private static void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.d(TAG, "sendRegistrationToServer: " + token);
    }

    *//*public static String getRegistrationId(Context context) {
        final SharedPreferences pref = context.getSharedPreferences(Bitmap.Config.SHARED_PREF, 0);
        String registrationId = pref.getString("regId", "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }*//*

   *//* private void storeRegIdInPref(String token) {
        final SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.apply();
    }*//*

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        context = getApplicationContext();
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                Toast.makeText(context, "remote message : "+remoteMessage.getData().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        {
            if(remoteMessage.getData().size() > 0){
                //handle the data message here
                alliedtitle=remoteMessage.getData().get("title");
                 body=remoteMessage.getData().get("message_body");

            }




            Log.e(TAG, "From: " + remoteMessage.getFrom());
            if (remoteMessage == null)
                return;            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {

                Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
                //   handleNotification(remoteMessage.getNotification().getBody());
            }
            // Check if message contains a data payload.

                Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());


            try {
                    Map<String, String> params = remoteMessage.getData();
                    JSONObject object = new JSONObject(params);
                    Log.d("JSON_OBJECT", object.toString());
                    String action = "";

                } catch (Exception e) {
                    Log.d(TAG, "Exception: " + e.getMessage());
                }


                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();

            EventBus.getDefault().postSticky(new FCMMessagePojo(title,body,"true"));
            notiifcationforlowerend(title,body);

        }
    }

    public void notiifcationforlowerend(String title,String message)
    {
        PendingIntent pendingIntent;
        Intent resultIntent = null;
        resultIntent = new Intent(getApplicationContext(), NotificationList_Activity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(this, 1251, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder;
        Resources res = this.getResources();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new Notification.Builder(this, "1");
            builder.setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.ic_launcher_alliedlogo)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher_alliedlogo))
                    .setTicker(title)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentText(message);
        } else {
            builder = new Notification.Builder(this);
            builder.setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.ic_launcher_alliedlogo)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher_alliedlogo))
                    .setTicker(title)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setContentText(message);
        }
        NotificationManager notificationmanager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    this.getResources().getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationmanager.createNotificationChannel(channel);
        }
        final int min = 20;
        final int max = 80;

        notificationmanager.notify(1, builder.build());
    }



    private boolean isMyServiceRunning(Class<?> serviceClass, AppCompatActivity activity) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }*/


    private String title_content="",message_content="",islogin="",notify_type="",post_id="",gen_notifyid="",gen_ticketid="",gen_redirect="",gen_message="";
    private String Job_status_message = "";


    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
      //  mySession = new SharedPreference(getApplicationContext());
        // Saving reg id to shared preferences
        Log.e("refreshedToken----", refreshedToken);

      //  mySession.putFCMToken(refreshedToken);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("TAG", "From: " + remoteMessage.getFrom());
        System.out.println("--------FCM Received-------" + remoteMessage);

        System.out.println("--------FCM Received----77777---" + remoteMessage.getNotification().toString());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("TAG", "Notification Body: " + remoteMessage.getNotification().getBody());
            title_content=remoteMessage.getNotification().getTitle();
            message_content=remoteMessage.getNotification().getBody();
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e("TAG", "Data Payload: " + remoteMessage.getData().toString());
                post_id=remoteMessage.getData().get("post_id");
                islogin=remoteMessage.getData().get("is_login");
                notify_type=remoteMessage.getData().get("type");
                gen_notifyid=remoteMessage.getData().get("notification_id");
            gen_ticketid=remoteMessage.getData().get("ticket_id");
            gen_message=remoteMessage.getData().get("message");
            gen_redirect=remoteMessage.getData().get("redirect");
            EventBus.getDefault().postSticky(new FCMMessagePojo(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),"true",notify_type,post_id,gen_ticketid));


            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                JSONObject data_object = json.getJSONObject("data");
                post_id = data_object.getString("post_id");
                islogin=data_object.getString("is_login");
                notify_type=data_object.getString("type");
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e("TAG", "Exception: " + e.getMessage());
            }
        }
       if(notify_type.equals("offer")) {
           handleNotification(title_content, message_content, post_id, notify_type, islogin);
       }
       else {
           handleNotification(title_content, message_content, gen_ticketid, notify_type, islogin);
       }
    }

    private void handleNotification(String title, String message,String post_id1,String type1,String islogin1) {
        CharSequence boldUsernameMessage = Html.fromHtml("<b><h3>" + title +"</h3></b> ");
        CharSequence contentMessage = message;


        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(boldUsernameMessage);
        bigTextStyle.bigText(message);

        Job_status_message = message;
        Log.e("message", Job_status_message);

        if(type1.equals("offer"))
        {
            Intent notificationIntent = new Intent(getApplicationContext(), SplashScreen.class);
            notificationIntent.putExtra("postid",post_id1);
            NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            CharSequence name = "Allied Motors";
            String desc = "this is notific";
            int imp = NotificationManager.IMPORTANCE_HIGH;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                final String ChannelID = "my_channel_01";
                final int ncode = 101;
                final NotificationManager mNotific =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                        imp);
                mChannel.setDescription(desc);
                mChannel.setLightColor(Color.CYAN);
                mChannel.canShowBadge();
                mChannel.setShowBadge(false);
                mNotific.createNotificationChannel(mChannel);
                Notification n = new Notification.Builder(this, ChannelID)
                        .setContentTitle(boldUsernameMessage)
                        .setContentText(message)
                        .setBadgeIconType(R.mipmap.ic_launcher_alliedlogo)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher_alliedlogo)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX)

                        .setStyle(new Notification.BigTextStyle().bigText(contentMessage))
                        .build();
                mNotific.notify(ncode, n);
            } else {
                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Resources res = this.getResources();
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher_alliedlogo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher_alliedlogo))
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setSound(soundUri)
                        .setContentTitle(boldUsernameMessage)
                        .setContentText(message)
                        .setLights(0xffff0000, 100, 2000)
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(contentMessage))

                        .setContentIntent(contentIntent);
                Notification n = builder.getNotification();
                n.defaults |= Notification.DEFAULT_ALL;
                nm.notify(0, n);
            }
        }
        else {

            Intent notificationIntent = new Intent(getApplicationContext(), SplashScreen.class);
            notificationIntent.putExtra("ticketid",post_id1);
            NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            CharSequence name = "Allied Motors";
            String desc = "this is notific";
            int imp = NotificationManager.IMPORTANCE_HIGH;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                final String ChannelID = "my_channel_01";
                final int ncode = 101;
                final NotificationManager mNotific =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                        imp);
                mChannel.setDescription(desc);
                mChannel.setLightColor(Color.CYAN);
                mChannel.canShowBadge();
                mChannel.setShowBadge(false);
                mNotific.createNotificationChannel(mChannel);
                Notification n = new Notification.Builder(this, ChannelID)
                        .setContentTitle(boldUsernameMessage)
                        .setContentText(message)
                        .setBadgeIconType(R.mipmap.ic_launcher_alliedlogo)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher_alliedlogo)
                        .setStyle(new Notification.BigTextStyle().bigText(contentMessage))
                        .setPriority(Notification.PRIORITY_MAX)
                        .setAutoCancel(true)
                        .build();
                mNotific.notify(ncode, n);
            } else {
                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Resources res = this.getResources();
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher_alliedlogo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher_alliedlogo))
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setSound(soundUri)
                        .setLights(0xffff0000, 100, 2000)
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentTitle(boldUsernameMessage)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(contentMessage))
                        .setContentIntent(contentIntent);
                Notification n = builder.getNotification();
                n.defaults |= Notification.DEFAULT_ALL;
                nm.notify(0, n);
            }

        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e("json", json.toString());
        System.out.println("---json----"+json.toString());



    }

}