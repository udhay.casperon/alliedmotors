package com.app.alliedmotor.utility.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewMediumLato extends TextView {
    public CustomTextViewMediumLato(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewMediumLato(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextViewMediumLato(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Sofia_Pro_Medium.otf");
        setTypeface(tf);
    }
}
