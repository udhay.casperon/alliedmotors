package com.app.alliedmotor.utility;

public interface OnItemClickListener {
    void onItemClick(int postion);
}
