package com.app.alliedmotor.utility;


import com.app.alliedmotor.model.CarDetail.Response_CarDetail;
import com.app.alliedmotor.model.FileUpload.Response_FileUpload;
import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.model.LuxuryCar.Response_LuxuryCar;
import com.app.alliedmotor.model.NewCar.NewCar_Response;
import com.app.alliedmotor.model.Notification.Response_Notification;
import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.model.RHDCar.Response_RHDCar;

public class ResponseInterface {

    public interface PReownedFilterRI {
        void onPreOwnedFilterSuccess(Response_PreOwned response_preOwned_list);
    }


    public interface CardetailInterface {
        void onCardetailSuccess(Response_CarDetail response_carDetail);    }


    public interface LoginInterface {
        void onloginsuccessSuccess(LoginResponse loginResponse);    }



    public interface PReOwnedCarListingInterface {
        void onPreownedSuccess(Response_PreOwned response_preOwned);    }



    public interface FCMNotificationInterface {
        void onFCMNotificationSuccess(Response_Notification response_notification);    }


    public interface NewCarListInterface {
        void onNewCarListSuccess(NewCar_Response newCar_response);    }

    public interface LuxuryCarListInterface {
        void onLuxuryCarListSuccess(Response_LuxuryCar response_luxuryCar);    }


    public interface RHDCarListInterface {
        void onRHDCarListSuccess(Response_RHDCar response_rhdCar);    }


    public interface FileUploadInterface {
        void onFileUploadSuccess(Response_FileUpload response_fileUpload);    }


}
