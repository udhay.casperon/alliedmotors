package com.app.alliedmotor.utility;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

public class ImageViewCustom extends AppCompatImageView {

   /* public ImageViewCustom(Context context)
    {
        super(context);
        this.setScaleType(ScaleType.FIT_XY);
    }

    public ImageViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setScaleType(ScaleType.FIT_XY);
    }

    public ImageViewCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setScaleType(ScaleType.FIT_XY);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        Drawable d = getDrawable();

        if (d != null && d.getIntrinsicHeight() > 0)
        {
            int width = getLayoutParams().width;
            int height = getLayoutParams().height;

            // تو این حالت مبنای محاسبات رو بر اساس طول در نظر می گیرم
            if (height == ViewGroup.LayoutParams.WRAP_CONTENT || (width > 0 &&  width < 10000))
            {
                if (width <= 0);
                width = MeasureSpec.getSize(widthMeasureSpec);

                if (width > 0)
                    height = width * d.getIntrinsicHeight() / d.getIntrinsicWidth();

                // ارتفاع نباید از بیشینه ارتفاع بیشتر باشه
                if (height > getMaxHeight()) {
                    height = getMaxHeight();
                    width = height * d.getIntrinsicWidth() / d.getIntrinsicHeight();
                }

            }
            else if (width == ViewGroup.LayoutParams.WRAP_CONTENT  || (height > 0 &&  height < 10000))
            {
                // اگر مقداری برای ارتفاع مشخص نشده بود ارتفاع پدر رو در نظر می گیریم
                if (height <= 0)
                    height = MeasureSpec.getSize(heightMeasureSpec);

                // اگه ارتفاع مشخص بود که می تونیم طول رو بر اساسش حساب کنیم
                if (height > 0)
                    width = height * d.getIntrinsicWidth() / d.getIntrinsicHeight();

                // طول نباید از بیشینه طول بیشتر باشه
                if (width > getMaxWidth()) {
                    width = getMaxWidth();
                    height = width * d.getIntrinsicHeight() / d.getIntrinsicWidth();
                }
            }

            // اگه محاسبات موفقیت آمیز بود
            if (width > 0 && height > 0)
                setMeasuredDimension(width, height);
                // در غیر اینصورت همه چی رو می سپریم به خود ایمیج ویو
            else
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
        else
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }*/

    public ImageViewCustom(Context context) {
        super(context);
    }

    public ImageViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
        setMeasuredDimension(width, height);
    }




}