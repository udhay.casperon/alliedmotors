package com.app.alliedmotor.utility;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.FileUpload.Response_FileUpload;
import com.app.alliedmotor.model.FilterFuelTypeResponse;
import com.app.alliedmotor.model.MyOrderLatest.Response_MyOrderLatest;
import com.app.alliedmotor.model.CarDetail.Response_CarDetail;
import com.app.alliedmotor.model.CarList.Response_CarList;


import com.app.alliedmotor.model.Car_CategoryList.Response_CarCategoryList;
import com.app.alliedmotor.model.ChangePassword.Response_ChangePassword;
import com.app.alliedmotor.model.DeleteFavourite.Response_DeleteFavourite;
import com.app.alliedmotor.model.FAQ_Page.Response_Faq;
import com.app.alliedmotor.model.ForgotEmail.Response_ForgotEmail;
import com.app.alliedmotor.model.ForgotPassword.Response_ForgotPassword;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.HomePage.Response_Homepage;
import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.model.LuxuryCar.Response_LuxuryCar;
import com.app.alliedmotor.model.MyFavourite.Response_MyFavourite;
import com.app.alliedmotor.model.NewCar.NewCar_Response;
import com.app.alliedmotor.model.NewsEvents.Response_NewsEvents;
import com.app.alliedmotor.model.Notification.Response_Notification;
import com.app.alliedmotor.model.NotificationDelete.Response_NotifyDelete;
import com.app.alliedmotor.model.NotificationStatusChange.Response_NotificationStatuschange;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.model.OrderCreation.OrderCreation_Response;
import com.app.alliedmotor.model.PreOwnedList.Request_PreownedList;
import com.app.alliedmotor.model.PreOwned_CarDetail.Response_PreOwned_Detail;
import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.model.Privacy_Policy.PrivacyPolicy_Response;
import com.app.alliedmotor.model.ProfileUpdate.Response_ProfileUpdate;
import com.app.alliedmotor.model.RHDCar.Response_RHDCar;
import com.app.alliedmotor.model.ResetPassword.Response_ResetPassword;
import com.app.alliedmotor.model.Response_AddFavourite.Response_AddFavourite;
import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.model.Response_DeleteQuote;
import com.app.alliedmotor.model.Response_FilterModelOption;
import com.app.alliedmotor.model.Response_GuestMakeOrder;
import com.app.alliedmotor.model.Response_NotificationList.Response_NotificationList_folder;
import com.app.alliedmotor.model.Response_OtpRequest;
import com.app.alliedmotor.model.Response_SearchbyKeywords;
import com.app.alliedmotor.model.Response_SellPreOwned;
import com.app.alliedmotor.model.Response_UpdateQuote;
import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.model.Sample.Response_UserCreation;
import com.app.alliedmotor.model.Sample1.Response_UserNotificationstatuschange;
import com.app.alliedmotor.model.Services_Category.Response_Services_Category;
import com.app.alliedmotor.model.Signup.Response_SignupResponse;

import com.app.alliedmotor.model.SpareParts_Response;
import com.app.alliedmotor.model.TermsCondition.Response_Terms;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ServiceInterface {

    //1. General App Info
    @POST("v1/app-info")
    Call<Response_AppInfo> AppInfo_ServiceCall(@HeaderMap HashMap<String, String> header);

    // Login - user
    @FormUrlEncoded
    @POST("v1/user-login")
    Call<LoginResponse> ResponseLoginCall(@FieldMap HashMap<String, String> header);

    //forgot password
    @FormUrlEncoded
    @POST("v1/user-forget-password")
    Call<Response_ForgotPassword> ResponseforgotpasswordCall(@FieldMap HashMap<String, String> forgotpassword);

   // Forgot email
    @FormUrlEncoded
    @POST("v1/user-forget-email")
    Call<Response_ForgotEmail> forgotemail_ServiceCall(@FieldMap HashMap<String, String> forgotpassword);

    // User Registration - signup
    @FormUrlEncoded
    @POST("v1/user-registration")
    Call<Response_SignupResponse> ResponseSignupCall(@FieldMap HashMap<String, String> signuprequest);



    //Getting user data
    @POST("v1/user-profile")
    Call<Response_UserProfile> UserProfile_ResponseCall(@HeaderMap HashMap<String, String> header);

    //Profile update
    @FormUrlEncoded
    @POST("v1/user-detail-update")
    Call<Response_ProfileUpdate> Profileupdatecall(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_profileupdate);

    // change password
    @FormUrlEncoded
    @POST("v1/user-password-update")
    Call<Response_ChangePassword> Responsechangepasswordcall(@HeaderMap HashMap<String, String> header,@FieldMap HashMap<String, String> requestpasswordupdate);

    //Privacy Policy
    @GET("v1/page/707")
    Call<PrivacyPolicy_Response> PrivacyPolicy_ResponseCall();

    //FAQ Page
    @GET("v1/page/1910")
    Call<Response_Faq> FAQ_ResponseCall();

    //News and Events
    @GET("v1/news-events")
    Call<Response_NewsEvents> NewsEvents_ResponseCall();

    // Service in Category
    @GET("/v1/page/service")
    Call<Response_Services_Category> ServicesCategory_ResponseCall();

    // Home page - Main activity
    @GET("v1/home-page")
    Call<Response_Homepage> ResponseHomePageCall();


    //CarList

    @POST("v1/car-details")
    Call<Response_CarList>CarListResponseCall();

    //Car Detail
    @FormUrlEncoded
    @POST
    Call<Response_CarDetail> CarDetail_Call(@Url String url,@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_cardetail);

 //PreOwned Car Detail
 @POST
 Call<Response_PreOwned_Detail> PreCarDeatil_Call(@Url String url);



    @POST("v1/preowned-car-details")
    Call<Response_PreOwned> PreCarList_Call_List(@Body Request_PreownedList request_preownedList);



    //add Favourite
    @FormUrlEncoded
    @POST("v1/add-favorite")
    Call<Response_AddFavourite> AddFavouritecall(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_addfavouirte);

    //My Favourite
    @POST("v1/my-favorites")
    Call<Response_MyFavourite> MyFavouriteCall(@HeaderMap HashMap<String, String> header);

    //Delete Favourite
    @FormUrlEncoded
    @POST("v1/delete-favorite")
    Call<Response_DeleteFavourite> DeleteFavouritecall(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_deltefavouirte);



    // getting terms and conditions
    @GET("v1/page/707")
    Call<Response_Terms> TermsCondition_ResponseCall();



    //Add Quote
    @FormUrlEncoded
    @POST("v1/add-quote")
    Call<Response_Addquote> AddToQuoteCall(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_addquote);


    // Car Make list or Brand List used in preowned page dropdown
    @FormUrlEncoded
    @POST("v1/car-make-list")
    Call<Response_CarMakeList> ServiceCall_MakeList(@FieldMap HashMap<String, String> requestmakelist);


    // Car Model list  used in preowned page dropdown
    @FormUrlEncoded
    @POST("v1/car-model-list")
    Call<Response_CarModelList> ServiceModelList_Call(@FieldMap HashMap<String, String> request_modellist);


   // Delete quote
    @FormUrlEncoded
    @POST("v1/update-quote")
    Call<Response_UpdateQuote> UpdateQuoteCall(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_updatequote);



    @FormUrlEncoded
    @POST("v1/delete-quote")
    Call<Response_DeleteQuote> DeleteQuoteCall(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_deletequote);




    // Search car by Keywords
    @FormUrlEncoded
    @POST("v1/car-search-by-keywords")
    Call<Response_SearchbyKeywords> SearchbyKeywordCall(@FieldMap HashMap<String, String> request_searchkeyword);

  // New car list - New Car Fragment
    @POST("v1/new-car-details")
    Call<NewCar_Response> NewCarcall();


    @GET("v1/car-category-list")
    Call<Response_CarCategoryList> CarCaetgoryList_ResponseCall();

    // Luxury car list - Luxury Car Fragment
    @GET("v1/luxury-car-details")
    Call<Response_LuxuryCar> LuxuryCar_ResponseCall();

    // RHD car list - RHD Car Fragment
    @POST("v1/rhd-car-details")
    Call<Response_RHDCar> RHDCar_ResponseCall();

    // PreOwned Car Detail
    @FormUrlEncoded
    @POST
    Call<Response_PreOwned> PreOwnedCar_Call(@Url String url,@FieldMap HashMap<String, String> request);

    //GetQuote
    @POST("v1/get-quote")
    Call<Response_GetQuote> Call_GetQuote(@HeaderMap HashMap<String, String> header);

    // Sell Preowned Car stepwise
    @FormUrlEncoded
    @POST("v1/sell-car-stepwise")
    Call<Response_SellPreOwned> SellPreOwned_Call(@FieldMap HashMap<String, String> request);


    @FormUrlEncoded
    @POST
    Call<NewCar_Response> SelectedMakeList_Call(@Url String url, @FieldMap HashMap<String, String> request_cardetail);


    @FormUrlEncoded
    @POST
    Call<Response_LuxuryCar> SelectedLuxuryMakeList_Call(@Url String url, @FieldMap HashMap<String, String> request_cardetail);


    @FormUrlEncoded
    @POST
    Call<Response_RHDCar> SelectedRHdMakeList_Call(@Url String url, @FieldMap HashMap<String, String> request_cardetail);


   // Order Creation
    @FormUrlEncoded
    @POST("v1/make-order")
    Call<OrderCreation_Response> OrderCreation_Call(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_ordercreation);



    /*-------------Above api is in order ----------------*/

    @Multipart
    @POST("v1/file-upload")
    Call<Response_FileUpload> ServiceCall_fileUpload(@Part("file_prefix") RequestBody fileprefix,
                                                     @Part MultipartBody.Part image);



   //myorder creation
    @POST("v1/myorder-latest")
    Call<Response_MyOrderLatest> MyOrderCall(@HeaderMap HashMap<String, String> header);





    // Spare Parts
    @POST("v1/spare-parts")
    Call<SpareParts_Response> Spareparts_Call(@HeaderMap HashMap<String, String> request);

    @FormUrlEncoded

    @POST("v1/user-reset-password")
    Call<Response_ResetPassword> ResetPassword_call(@FieldMap HashMap<String, String> request);





    @POST("v1/notification-list")
    Call<Response_NotificationList_folder> NotificationList_call(@HeaderMap HashMap<String, String> header);

    @FormUrlEncoded
    @POST("v1/notification-status-change")
    Call<Response_NotificationStatuschange> NotificationStatusChange_call(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_notification);


    @FormUrlEncoded
    @POST("v1/app-push-notification")
    Call<OrderCreation_Response> PushNotification_call(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_notification);


    @FormUrlEncoded
    @POST("v1/user-notification-status-change")
    Call<Response_UserNotificationstatuschange> UserNotificationStatusChange_call(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_notification);



  /*  @FormUrlEncoded
    @POST("v1/verify-otp")
    Call<OrderCreation_Response> UserNotificationStatusChange_call(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_notification);
*/

  /* Model option fuel type */
    @FormUrlEncoded
    @POST("v1/model-option-by-fuel-type")
    Call<FilterFuelTypeResponse> Call_filterFuelType(@FieldMap HashMap<String, String> request1);

    /* Vareint by model option */
    @FormUrlEncoded
    @POST("v1/variant-by-model-option")
    Call<Response_FilterModelOption> Call_filterModelOption(@FieldMap HashMap<String, String> request1);

   // Otp Request
    @FormUrlEncoded
    @POST("v1/otp-request")
    Call<Response_OtpRequest> Call_otpRequest(@FieldMap HashMap<String, String> request1);


    @FormUrlEncoded
    @POST("v1/verify-otp")
    Call<Response_VerifyOTP> Call_VerfiyOTP(@HeaderMap HashMap<String,String>header,@FieldMap HashMap<String, String> request1);


    @FormUrlEncoded
    @POST("v1/user-creation")
    Call<Response_UserCreation> Call_UserCreation(@FieldMap HashMap<String, String> request1);


    @FormUrlEncoded
    @POST("v1/guest-make-order")
    Call<Response_GuestMakeOrder> Call_GuestMakeOrder(@FieldMap HashMap<String, String> request1);




    // working fine - cardetail
    @FormUrlEncoded
    @POST
    Call<Response_CarDetail> CarDetail_Call_sample(@Url String url,@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_cardetail);


    @FormUrlEncoded
    @POST
    Call<LoginResponse> LOGIN_RESPONSE_CALL(@Url String url,@FieldMap HashMap<String, String> request);


    @POST
    Call<Response_PreOwned> ResponsePreOwnedCar_Call(@Url String url,@Body HashMap<String, String> request);




    //Offer Notification Change
    @FormUrlEncoded
    @POST("v1/user-offer-notification-status-change")
    Call<Response_OfferNotify> OfferNotifify_change(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_notification);



    // New API 10-05-2021


    @FormUrlEncoded
    @POST("v1/notification")
    Call<Response_Notification> ServiceCall_notify(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_notification);



    @FormUrlEncoded
    @POST("v1/user-notification-delete")
    Call<Response_NotifyDelete> ServiceCall_notify_delete(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> request_notification);


    @FormUrlEncoded
    @POST
    Call<NewCar_Response> NEWCAR_RESPONSE_CALL(@Url String url,@FieldMap HashMap<String, String> request);


    @FormUrlEncoded
    @POST
    Call<Response_LuxuryCar> LUXURY_RESPONSE_CALL(@Url String url,@FieldMap HashMap<String, String> request);


    @FormUrlEncoded
    @POST
    Call<Response_RHDCar> RHD_RESPONSE_CALL(@Url String url,@FieldMap HashMap<String, String> request);

    @Multipart
    @POST("v1/file-upload")
    Call<Response_FileUpload> ServiceCall_fileUpload1(@Part("file_prefix") RequestBody fileprefix,
                                                     @Part MultipartBody.Part image);




}

