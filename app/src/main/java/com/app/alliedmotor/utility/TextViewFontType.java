package com.app.alliedmotor.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class TextViewFontType {

    public Typeface typeface;

    public void fontTextView(Context mContext, final TextView textView, final String fonttype) {
        typeface = Typeface.createFromAsset(mContext.getAssets(), fonttype);
        textView.setTypeface(typeface);
    }
    public void fontButtonView(Context mContext, final Button btn, final String fonttype) {
        typeface = Typeface.createFromAsset(mContext.getAssets(), fonttype);
        btn.setTypeface(typeface);
    }

    public void fontEditView(Context mContext, final EditText edt, final String fonttype) {
        typeface = Typeface.createFromAsset(mContext.getAssets(), fonttype);
        edt.setTypeface(typeface);
    }

    public void fontRadioView(Context mContext, final RadioButton rdb, final String fonttype) {
        typeface = Typeface.createFromAsset(mContext.getAssets(), fonttype);
        rdb.setTypeface(typeface);
    }
}
