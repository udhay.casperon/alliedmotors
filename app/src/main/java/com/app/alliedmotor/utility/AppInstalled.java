package com.app.alliedmotor.utility;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class AppInstalled {

    public static boolean isAppInstalled(String package_name, Context context)
    {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo info = pm.getPackageInfo("" + package_name, PackageManager.GET_META_DATA);
          /*  Toast.makeText(getActivity().getApplicationContext(), "App installed " + app_name, Toast.LENGTH_SHORT)
                    .show();*/
            Intent sendIntent = new Intent();
            sendIntent.setPackage("com.whatsapp");
            context.startActivity(sendIntent);
            return true;

        }
        catch (PackageManager.NameNotFoundException e) {
            /*Toast.makeText(getActivity().getApplicationContext(), "Your device has not installed " + app_name, Toast.LENGTH_SHORT)
                    .show();*/
            Commons.Alert_custom(context,"Please Install WhatsApp in your phone");
            return false;
        }
    }

 // for Title case
    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }



}
