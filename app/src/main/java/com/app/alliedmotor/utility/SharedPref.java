package com.app.alliedmotor.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.alliedmotor.R;


public class SharedPref {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private static String MY_STRING_PREF = String.valueOf((R.string.app_name));
    SharedPreferences msharedPreferences;
    SharedPreferences.Editor meditor;

    public SharedPref(Context context){
        msharedPreferences =  context.getSharedPreferences(MY_STRING_PREF , Context.MODE_PRIVATE);
    }

    public void setString(String tpushauth , String value){
        meditor = msharedPreferences.edit();
        meditor.putString(tpushauth, value);
        meditor.apply();
    }

    public void mSaveString(String Stringtpushauth, String Value) {
        editor.putString(Stringtpushauth, Value);
        editor.commit();
        System.out.println("sharfffff"+Stringtpushauth+"------"+Value);
    }

/*
    public void setDouble(String key , Floatvalue){
        meditor = msharedPreferences.edit();
        meditor.putFloat(key, value);
        meditor.apply();
    }*/

    public String getString(String tpushauth) {
        return msharedPreferences.getString(tpushauth, null);
    }

    public void setInt(String user_id , int value){
        meditor = msharedPreferences.edit();
        meditor.putInt(user_id, value);
        meditor.apply();
        System.out.println("sharfffff"+user_id+"------"+value);
    }

    public int getInt(String user_id){
        return msharedPreferences.getInt(user_id , 0);
    }

    public void setBoolean(String save1, boolean value1)
    {
        meditor=msharedPreferences.edit();
        meditor.putBoolean(save1,value1);
        meditor.apply();
        System.out.println("shared pref---- boolena -----"+save1+"---------"+value1);
    }
    public void getBoolean(String save1)
    {

    }

    public void clearAll(){
        meditor = msharedPreferences.edit();
        meditor.clear().apply();
    }
}
