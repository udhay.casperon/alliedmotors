package com.app.alliedmotor.utility.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTectViewRegularItalic extends TextView {

    public CustomTectViewRegularItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTectViewRegularItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTectViewRegularItalic(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Sofia_Pro_Medium_Italic.otf");
        setTypeface(tf);
    }
}
