package com.app.alliedmotor.utility.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomRegularEditTextBold extends androidx.appcompat.widget.AppCompatEditText {

    public CustomRegularEditTextBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public CustomRegularEditTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomRegularEditTextBold(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Sofia_Pro_Semi_Bold.otf");
        setTypeface(tf);
    }
}
