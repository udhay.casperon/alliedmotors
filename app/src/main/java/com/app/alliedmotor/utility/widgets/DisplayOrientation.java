package com.app.alliedmotor.utility.widgets;

import android.content.Context;
import android.view.WindowManager;

public class DisplayOrientation {
    public static int getDisplayOrientation(Context activity) {
        return ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
    }
}
