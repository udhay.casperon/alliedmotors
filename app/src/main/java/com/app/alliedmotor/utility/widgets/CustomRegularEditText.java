package com.app.alliedmotor.utility.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomRegularEditText extends androidx.appcompat.widget.AppCompatEditText {

    public CustomRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public CustomRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomRegularEditText(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Sofia_Pro_Regular.otf");
        setTypeface(tf);
    }
}
