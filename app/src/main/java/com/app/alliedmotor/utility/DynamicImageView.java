package com.app.alliedmotor.utility;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;

public class DynamicImageView extends ImageView {
    public DynamicImageView(Context context) {
        super(context);
    }

    public DynamicImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DynamicImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

      /*  @Override public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            Drawable drawable = getDrawable();
            if (drawable == null) {
                super.onMeasure(widthSpecureSpec, heightMeasureSpec);
                return;
            }
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = MeasureSpec.getSize(heightMeasureSpec);
            int reqWidth = drawable.getIntrinsicWidth();
            int reqHeight = drawable.getIntrinsicHeight();
            // Now you have the measured width and height,
            // also the image's width and height,
            // calculate your expected width and height;
            setMeasuredDimension(targetWidth, targetHeight);
        }
    }
*/
}
