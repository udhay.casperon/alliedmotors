package com.app.alliedmotor.utility.widgets;

public interface NetworkStateReceiverListener {
    public void networkAvailable();
    public void networkUnavailable();
}
