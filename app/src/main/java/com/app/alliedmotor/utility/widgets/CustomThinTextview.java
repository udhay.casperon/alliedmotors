package com.app.alliedmotor.utility.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomThinTextview extends TextView {

    public CustomThinTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public CustomThinTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomThinTextview(Context context) {
        super(context);
        init();
    }
    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Sofia_Pro_UltraLight.otf");
        setTypeface(tf ,1);
    }
}
