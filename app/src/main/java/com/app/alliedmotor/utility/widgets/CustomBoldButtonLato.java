package com.app.alliedmotor.utility.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomBoldButtonLato extends Button {
    public CustomBoldButtonLato(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomBoldButtonLato(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBoldButtonLato(Context context) {
        super(context);
        init();
    }

    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Sofia_Pro_Bold.otf");
        setTypeface(tf, 1);
    }
}
