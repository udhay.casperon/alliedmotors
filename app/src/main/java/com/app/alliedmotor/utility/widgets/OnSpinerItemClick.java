package com.app.alliedmotor.utility.widgets;

public interface OnSpinerItemClick {
    public void onClick(String item, int position);
}
