package com.app.alliedmotor.utility;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Commons {

    static ProgressDialog progressDialog;
    static Dialog cancel_dialog;
    static ProgressDialog m_Dialog;
    static Dialog coming_soon_dialog;

    public static void ToastShort(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void ShowToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    public static void ToastLong(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }


    public static boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();
        return check;
    }

    public static void ShowNetAlert(Context context) {
        Toast.makeText(context, "Please check your internet", Toast.LENGTH_SHORT).show();
    }

    public static void snackbarNoInternet(final Context context, RelativeLayout layout) {
        Snackbar snackbar = Snackbar
                .make(layout, R.string.no_internet_message, Snackbar.LENGTH_INDEFINITE);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.subscription_light_yellow));
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));

        snackbar.show();
    }


    public static void snackbarNoInternetTwo(Context context, LinearLayout layout) {
        Snackbar snackbar = Snackbar
                .make(layout, R.string.no_internet_message, Snackbar.LENGTH_INDEFINITE);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.subscription_light_yellow));
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        snackbar.show();
    }



    public static boolean isOnline(Context mContext) {
        boolean internet = false;
        ConnectivityManager conMgr = (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null) {
            internet = false;
        } else {
            internet = true;
        }
        return internet;
    }

    public static void showProgress(Context context) {
        if(context != null) {
            progressDialog = new ProgressDialog(context);
            progressDialog = new ProgressDialog(context, R.style.progress_bar_style);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.getWindow().getAttributes().windowAnimations = R.style.progress_bar_style;
            progressDialog.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }


   /* public static void showProgress_percentage(Context context) {
        final int status=0;
        Handler handler = new Handler();

        if(context != null) {
            progressDialog = new ProgressDialog(context);
            progressDialog = new ProgressDialog(context, R.style.progress_bar_style);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.getWindow().getAttributes().windowAnimations = R.style.progress_bar_style;
            progressDialog.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                while ( status< 100) {

                    status += 1;

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    handler.post(new Runnable() {
                        @Override
                        public void run() {


                            if (status == 100) {
                                progressDialog.dismiss();
                            }
                        }
                    });
                }
            }
        }).start();


        progressDialog.show();

        Window window = progressDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }
*/



    public static void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static void showProgressDialog(Context context, String message) {
        m_Dialog = new ProgressDialog(context);
        m_Dialog.setMessage(message);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(false);
        m_Dialog.show();
    }

    public static void hideProgressDialog() {
        if (m_Dialog != null) {
            m_Dialog.dismiss();
        }
    }



    public static boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static void Alert_custom(final Context context,String message)
    {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_ios_singlebutton);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if(alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        txtInstructionDialog.setText(message);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview rlClose = alertDialog.findViewById(R.id.bt_okay);
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }






   public static void Alert_custom1(final Context context,String message)
   {
        final Dialog alertDialog;
       alertDialog = new Dialog(context);
       alertDialog.setContentView(R.layout.custom_alert_layout_ios);
       alertDialog.setCancelable(true);
       alertDialog.setCanceledOnTouchOutside(true);
       if(alertDialog.getWindow() != null) {
           alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       }

       alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
       alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
       CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
       txtInstructionDialog.setText(message);
       txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
       CustomRegularTextview rlClose = alertDialog.findViewById(R.id.bt_okay);
       rlClose.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               alertDialog.dismiss();

           }
       });
       if (!alertDialog.isShowing())
           alertDialog.show();
   }



    public static void Alert_custom_Title(final Context context,String Title,String message)
    {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_title_ios);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if(alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle=alertDialog.findViewById(R.id.txttitle);
        txttitle.setText(Title);
        txtInstructionDialog.setText(message);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview rlClose = alertDialog.findViewById(R.id.bt_okay);
        CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing())
            alertDialog.show();
    }


    public static void Alert_custom_Title1(final Context context,String Title,String message)
    {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_button_linear);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if(alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle=alertDialog.findViewById(R.id.txttitle);
        txttitle.setText(Title);
        txtInstructionDialog.setText(message);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview button1 = alertDialog.findViewById(R.id.bt_button1);
        CustomRegularTextview button2 = alertDialog.findViewById(R.id.bt_button2);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        if (!alertDialog.isShowing())
            alertDialog.show();
    }





    public static void showAlertDialog(final Context mContext, String title, String message) {

        final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);
    }

    public static void showAlertDialog1(final Context mContext, String message) {

            final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
            alertDialog.setMessage(message);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                    "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.white);


    }


    public static String getCurrentTime() {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(d);
        return currentDateTimeString;
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String base64Convertion(Bitmap mBitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();


        String data = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return data;
    }

    /*Arraylist to String */
    public static String ArraytoString(List favorites) {
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);
        return jsonFavorites;
    }


    public static void toggle(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); // hide
        } else {
            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY); // show
        }
    }//en

    public static boolean isLocationServiceEnabled(Context context) {
        Activity activity;
        if (context == null) {
            return false;
        } else {
            activity = (Activity) context;
        }
        LocationManager locationManager = null;
        boolean gps_enabled = false, network_enabled = false;

        if (locationManager == null)

            locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            //do nothing...
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            //do nothing...
        }

        return gps_enabled || network_enabled;

    }

    public static boolean isGPSEnabled(Context mContext) {
        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }




/*    public static void successDialog(final Context mContext) {
        // custom dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_alert);
        ImageView image = (ImageView) dialog.findViewById(R.id.imageView_check);
        // if button is clicked, close the custom dialog
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(mContext, "Please upload requested images", Toast.LENGTH_SHORT).show();
                Intent intentImageUpload = new Intent(mContext, HomeActivity.class);
                mContext.startActivity(intentImageUpload);
                Activity activity = (Activity) mContext;
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                activity.finish();
            }
        });

        dialog.show();
    }
*/
}
