package com.app.alliedmotor.utility;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public class ProportionalImageview extends androidx.appcompat.widget.AppCompatImageView {

        public ProportionalImageview(Context context) {
            super(context);
        }

        public ProportionalImageview(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public ProportionalImageview(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            Drawable d = getDrawable();
            if (d != null) {
                int w = MeasureSpec.getSize(widthMeasureSpec);
                int h = w * d.getIntrinsicHeight() / d.getIntrinsicWidth();
                setMeasuredDimension(w, h);
            }
            else super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

}
