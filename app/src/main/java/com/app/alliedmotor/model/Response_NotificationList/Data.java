package com.app.alliedmotor.model.Response_NotificationList;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("header")
	private String header;

	@SerializedName("offer_notification_list")
	private ArrayList<Offer_Notification_List> offer_notification_list;


	@SerializedName("notification_list")
	private ArrayList<NotificationListItem_folder> notificationList;

	public void setNotificationList(ArrayList<NotificationListItem_folder> notificationList){
		this.notificationList = notificationList;
	}

	public ArrayList<NotificationListItem_folder> getNotificationList(){
		return notificationList;
	}


	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public ArrayList<Offer_Notification_List> getOffer_notification_list() {
		return offer_notification_list;
	}

	public void setOffer_notification_list(ArrayList<Offer_Notification_List> offer_notification_list) {
		this.offer_notification_list = offer_notification_list;
	}

	@Override
	public String toString() {
		return "Data{" +
				"header='" + header + '\'' +
				", offer_notification_list=" + offer_notification_list +
				", notificationList=" + notificationList +
				'}';
	}
}