package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Response_FilterModelOption {





	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	public class Data{

		@SerializedName("varient_list")
		public ArrayList<VarientListItem> varientList;


		public class VarientListItem{

			@SerializedName("mainId")
			public String mainId;

			@SerializedName("varient_code")
			public String varientCode;

			public VarientListItem(String mainId, String varientCode) {
				this.mainId = mainId;
				this.varientCode = varientCode;
			}
		}
	}



	
}