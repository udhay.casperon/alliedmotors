package com.app.alliedmotor.model.NewCar_details;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CarOtherFeatures{

	@SerializedName("features")
	private List<String> features;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private String id;

	public void setFeatures(List<String> features){
		this.features = features;
	}

	public List<String> getFeatures(){
		return features;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"CarOtherFeatures{" + 
			"features = '" + features + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}