package com.app.alliedmotor.model.MyProfile1;

import com.google.gson.annotations.SerializedName;

public class Support{

	@SerializedName("whatsapp")
	private String whatsapp;

	@SerializedName("mobile")
	private String mobile;

	public void setWhatsapp(String whatsapp){
		this.whatsapp = whatsapp;
	}

	public String getWhatsapp(){
		return whatsapp;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}
}