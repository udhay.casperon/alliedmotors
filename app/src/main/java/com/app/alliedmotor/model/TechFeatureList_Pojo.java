package com.app.alliedmotor.model;

public class TechFeatureList_Pojo {

    public String KeyTitle;
    public String Value;
    public  int ImgRsrc;


    public TechFeatureList_Pojo(String keyTitle, String value, int imgRsrc) {
        KeyTitle = keyTitle;
        Value = value;
        ImgRsrc = imgRsrc;
    }

    public String getKeyTitle() {
        return KeyTitle;
    }

    public void setKeyTitle(String keyTitle) {
        KeyTitle = keyTitle;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public int getImgRsrc() {
        return ImgRsrc;
    }

    public void setImgRsrc(int imgRsrc) {
        ImgRsrc = imgRsrc;
    }

    @Override
    public String toString() {
        return "TechFeatureList_Pojo{" +
                "KeyTitle='" + KeyTitle + '\'' +
                ", Value='" + Value + '\'' +
                ", ImgRsrc=" + ImgRsrc +
                '}';
    }
}
