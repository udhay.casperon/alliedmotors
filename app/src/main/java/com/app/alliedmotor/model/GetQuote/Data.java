package com.app.alliedmotor.model.GetQuote;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("quoteData")
	private ArrayList<QuoteDataItem> quoteData;

	public void setQuoteData(ArrayList<QuoteDataItem> quoteData){
		this.quoteData = quoteData;
	}

	public ArrayList<QuoteDataItem> getQuoteData(){
		return quoteData;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"quoteData = '" + quoteData + '\'' + 
			"}";
		}
}