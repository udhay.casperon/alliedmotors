package com.app.alliedmotor.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class StockAutoPartResponse {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public ArrayList<DataItem> data;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;

	public class DataItem{

		@SerializedName("quantity")
		public String quantity;

		@SerializedName("parts_name")
		public String partsName;

		@SerializedName("parts_number")
		public String partsNumber;
	}
	
	
}