package com.app.alliedmotor.model.CarDetail;

import com.google.gson.annotations.SerializedName;

public class CarDetails{

	@SerializedName("pdf_file")
	private String pdfFile;

	@SerializedName("secondaryEmail")
	private String secondaryEmail;

	@SerializedName("locationName")
	private String locationName;

	@SerializedName("currency_name")
	private String currencyName;

	@SerializedName("is_favorite")
	private String isFavorite;

	@SerializedName("vehiceType")
	private String vehiceType;

	@SerializedName("mkId")
	private String mkId;

	@SerializedName("modelYear")
	private String modelYear;

	@SerializedName("makename")
	private String makename;

	@SerializedName("varient_code")
	private String varientCode;

	@SerializedName("modId")
	private String modId;

	@SerializedName("moId")
	private String moId;

	@SerializedName("modelOptions")
	private String modelOptions;

	@SerializedName("eftId")
	private String eftId;

	@SerializedName("catid")
	private String catid;

	@SerializedName("modelName")
	private String modelName;

	@SerializedName("efuelType")
	private String efuelType;

	@SerializedName("makeFolder")
	private String makeFolder;

	@SerializedName("price")
	private String price;

	@SerializedName("car_image")
	private String carImage;

	@SerializedName("mainId")
	private String mainId;

	@SerializedName("email")
	private String email;

	@SerializedName("make_mobile_number")
	private String makeMobileNumber;

	@SerializedName("colorDesc")
	private String colorDesc;

	public void setPdfFile(String pdfFile){
		this.pdfFile = pdfFile;
	}

	public String getPdfFile(){
		return pdfFile;
	}

	public void setSecondaryEmail(String secondaryEmail){
		this.secondaryEmail = secondaryEmail;
	}

	public String getSecondaryEmail(){
		return secondaryEmail;
	}

	public void setLocationName(String locationName){
		this.locationName = locationName;
	}

	public String getLocationName(){
		return locationName;
	}

	public void setCurrencyName(String currencyName){
		this.currencyName = currencyName;
	}

	public String getCurrencyName(){
		return currencyName;
	}

	public void setIsFavorite(String isFavorite){
		this.isFavorite = isFavorite;
	}

	public String getIsFavorite(){
		return isFavorite;
	}

	public void setVehiceType(String vehiceType){
		this.vehiceType = vehiceType;
	}

	public String getVehiceType(){
		return vehiceType;
	}

	public void setMkId(String mkId){
		this.mkId = mkId;
	}

	public String getMkId(){
		return mkId;
	}

	public void setModelYear(String modelYear){
		this.modelYear = modelYear;
	}

	public String getModelYear(){
		return modelYear;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	public void setVarientCode(String varientCode){
		this.varientCode = varientCode;
	}

	public String getVarientCode(){
		return varientCode;
	}

	public void setModId(String modId){
		this.modId = modId;
	}

	public String getModId(){
		return modId;
	}

	public void setMoId(String moId){
		this.moId = moId;
	}

	public String getMoId(){
		return moId;
	}

	public void setModelOptions(String modelOptions){
		this.modelOptions = modelOptions;
	}

	public String getModelOptions(){
		return modelOptions;
	}

	public void setEftId(String eftId){
		this.eftId = eftId;
	}

	public String getEftId(){
		return eftId;
	}

	public void setCatid(String catid){
		this.catid = catid;
	}

	public String getCatid(){
		return catid;
	}

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public String getModelName(){
		return modelName;
	}

	public void setEfuelType(String efuelType){
		this.efuelType = efuelType;
	}

	public String getEfuelType(){
		return efuelType;
	}

	public void setMakeFolder(String makeFolder){
		this.makeFolder = makeFolder;
	}

	public String getMakeFolder(){
		return makeFolder;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setCarImage(String carImage){
		this.carImage = carImage;
	}

	public String getCarImage(){
		return carImage;
	}

	public void setMainId(String mainId){
		this.mainId = mainId;
	}

	public String getMainId(){
		return mainId;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setMakeMobileNumber(String makeMobileNumber){
		this.makeMobileNumber = makeMobileNumber;
	}

	public String getMakeMobileNumber(){
		return makeMobileNumber;
	}

	public void setColorDesc(String colorDesc){
		this.colorDesc = colorDesc;
	}

	public String getColorDesc(){
		return colorDesc;
	}

	@Override
 	public String toString(){
		return 
			"CarDetails{" + 
			"pdf_file = '" + pdfFile + '\'' + 
			",secondaryEmail = '" + secondaryEmail + '\'' + 
			",locationName = '" + locationName + '\'' + 
			",currency_name = '" + currencyName + '\'' + 
			",is_favorite = '" + isFavorite + '\'' + 
			",vehiceType = '" + vehiceType + '\'' + 
			",mkId = '" + mkId + '\'' + 
			",modelYear = '" + modelYear + '\'' + 
			",makename = '" + makename + '\'' + 
			",varient_code = '" + varientCode + '\'' + 
			",modId = '" + modId + '\'' + 
			",moId = '" + moId + '\'' + 
			",modelOptions = '" + modelOptions + '\'' + 
			",eftId = '" + eftId + '\'' + 
			",catid = '" + catid + '\'' + 
			",modelName = '" + modelName + '\'' + 
			",efuelType = '" + efuelType + '\'' + 
			",makeFolder = '" + makeFolder + '\'' + 
			",price = '" + price + '\'' + 
			",car_image = '" + carImage + '\'' + 
			",mainId = '" + mainId + '\'' + 
			",email = '" + email + '\'' + 
			",make_mobile_number = '" + makeMobileNumber + '\'' + 
			",colorDesc = '" + colorDesc + '\'' + 
			"}";
		}
}