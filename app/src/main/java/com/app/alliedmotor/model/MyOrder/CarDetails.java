package com.app.alliedmotor.model.MyOrder;

import com.google.gson.annotations.SerializedName;

public class CarDetails{

	@SerializedName("modelName")
	private String modelName;

	@SerializedName("fueltype")
	private String fueltype;

	@SerializedName("carvarient")
	private String carvarient;

	@SerializedName("makename")
	private String makename;

	@SerializedName("modelyear")
	private String modelyear;

	@SerializedName("carId")
	private String carId;

	@SerializedName("modelOptions")
	private String modelOptions;

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public String getModelName(){
		return modelName;
	}

	public void setFueltype(String fueltype){
		this.fueltype = fueltype;
	}

	public String getFueltype(){
		return fueltype;
	}

	public void setCarvarient(String carvarient){
		this.carvarient = carvarient;
	}

	public String getCarvarient(){
		return carvarient;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	public void setModelyear(String modelyear){
		this.modelyear = modelyear;
	}

	public String getModelyear(){
		return modelyear;
	}

	public void setCarId(String carId){
		this.carId = carId;
	}

	public String getCarId(){
		return carId;
	}

	public void setModelOptions(String modelOptions){
		this.modelOptions = modelOptions;
	}

	public String getModelOptions(){
		return modelOptions;
	}

	@Override
 	public String toString(){
		return 
			"CarDetails{" + 
			"modelName = '" + modelName + '\'' + 
			",fueltype = '" + fueltype + '\'' + 
			",carvarient = '" + carvarient + '\'' + 
			",makename = '" + makename + '\'' + 
			",modelyear = '" + modelyear + '\'' + 
			",carId = '" + carId + '\'' + 
			",modelOptions = '" + modelOptions + '\'' + 
			"}";
		}
}