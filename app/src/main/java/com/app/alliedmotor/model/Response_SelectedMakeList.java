package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Response_SelectedMakeList {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;

	public class Data{

		@SerializedName("category_list")
		public ArrayList<CategoryListItem> categoryList;

		@SerializedName("make_list")
		public ArrayList<MakeListItem> makeList;

		@SerializedName("car_list")
		public ArrayList<CarListItem> carList;
	}

	public class CategoryListItem{

		@SerializedName("post_title")
		public String postTitle;

		@SerializedName("post_name")
		public String postName;
	}


	public class MakeListItem{

		@SerializedName("icon")
		public String icon;

		@SerializedName("mkid")
		public String mkid;

		@SerializedName("makename")
		public String makename;
	}

	public class CarDatasItem{

		@SerializedName("newCarStatus")
		public String newCarStatus;

		@SerializedName("secondaryEmail")
		public String secondaryEmail;

		@SerializedName("currency_name")
		public Object currencyName;

		@SerializedName("currency_flag_image")
		public String currencyFlagImage;

		@SerializedName("vehiceType")
		public String vehiceType;

		@SerializedName("promo_logo")
		public String promoLogo;

		@SerializedName("mkId")
		public String mkId;

		@SerializedName("modelYear")
		public String modelYear;

		@SerializedName("makename")
		public String makename;

		@SerializedName("modId")
		public String modId;

		@SerializedName("moId")
		public String moId;

		@SerializedName("modelOptions")
		public String modelOptions;

		@SerializedName("pdfStatus")
		public String pdfStatus;

		@SerializedName("eftId")
		public String eftId;

		@SerializedName("catid")
		public String catid;

		@SerializedName("modelName")
		public String modelName;

		@SerializedName("efuelType")
		public String efuelType;

		@SerializedName("makeFolder")
		public String makeFolder;

		@SerializedName("price")
		public String price;

		@SerializedName("car_image")
		public String carImage;

		@SerializedName("mainId")
		public String mainId;

		@SerializedName("email")
		public String email;

		@SerializedName("make_mobile_number")
		public String makeMobileNumber;

		@SerializedName("make_icon")
		public String makeIcon;
	}


	public class CarListItem{

		@SerializedName("car_datas")
		public ArrayList<CarDatasItem> carDatas;

		@SerializedName("title")
		public String title;
	}
	
}