package com.app.alliedmotor.model;

public class SampleLoginDb {


    String User_Email;
    String User_password;


    public String getUser_Email() {
        return User_Email;
    }

    public void setUser_Email(String user_Email) {
        User_Email = user_Email;
    }

    public String getUser_password() {
        return User_password;
    }

    public void setUser_password(String user_password) {
        User_password = user_password;
    }


    @Override
    public String toString() {
        return "SampleLoginDb{" +
                "User_Email='" + User_Email + '\'' +
                ", User_password='" + User_password + '\'' +
                '}';
    }
}
