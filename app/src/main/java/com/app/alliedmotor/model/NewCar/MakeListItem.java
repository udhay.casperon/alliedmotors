package com.app.alliedmotor.model.NewCar;

import com.google.gson.annotations.SerializedName;

public class MakeListItem{

	@SerializedName("icon")
	private String icon;

	@SerializedName("mkid")
	private String mkid;

	@SerializedName("makename")
	private String makename;

	public void setIcon(String icon){
		this.icon = icon;
	}

	public String getIcon(){
		return icon;
	}

	public void setMkid(String mkid){
		this.mkid = mkid;
	}

	public String getMkid(){
		return mkid;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	@Override
 	public String toString(){
		return 
			"MakeListItem{" + 
			"icon = '" + icon + '\'' + 
			",mkid = '" + mkid + '\'' + 
			",makename = '" + makename + '\'' + 
			"}";
		}
}