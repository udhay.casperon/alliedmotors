package com.app.alliedmotor.model.CarDetail;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class CarInteriorFeatures{

	@SerializedName("features")
	private ArrayList<String> features;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private String id;

	public void setFeatures(ArrayList<String> features){
		this.features = features;
	}

	public ArrayList<String> getFeatures(){
		return features;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"CarInteriorFeatures{" + 
			"features = '" + features + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}