package com.app.alliedmotor.model.Response_AddFavourite;

import com.google.gson.annotations.SerializedName;

public class Response_AddFavourite {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("commonArr")
	public CommonArr commonArr;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;

	public class Data{

		@SerializedName("car_id")
		public Integer carId;
	}


	public class CommonArr{

		@SerializedName("token")
		public String token;
	}
	
	
	
	
	
	
	
	
}