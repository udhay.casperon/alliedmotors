package com.app.alliedmotor.model.Login;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("response_code")
    public String responseCode;

    @SerializedName("commonArr")
    private CommonArr commonArr;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    @SerializedName("errors")
    private LoginError errors;

    @SerializedName("long_message")
    private String long_message;


    public String getLong_message() {
        return long_message;
    }

    public void setLong_message(String long_message) {
        this.long_message = long_message;
    }

    public LoginError getErrors() {
        return errors;
    }

    public void setErrors(LoginError errors) {
        this.errors = errors;
    }

    public void setResponseCode(String responseCode){
        this.responseCode = responseCode;
    }

    public String getResponseCode(){
        return responseCode;
    }

    public void setCommonArr(CommonArr commonArr){
        this.commonArr = commonArr;
    }

    public CommonArr getCommonArr(){
        return commonArr;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    @Override
    public String toString(){
        return
                "Response{" +
                        "response_code = '" + responseCode + '\'' +
                        ",commonArr = '" + commonArr + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}
