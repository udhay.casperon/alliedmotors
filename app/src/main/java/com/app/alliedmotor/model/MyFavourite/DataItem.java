package com.app.alliedmotor.model.MyFavourite;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	private boolean isSelected=false;

	@SerializedName("fav_id")
	private String fav_id;

	public String getFav_id() {
		return fav_id;
	}

	public void setFav_id(String fav_id) {
		this.fav_id = fav_id;
	}

	@SerializedName("car_title")
	private String carTitle;

	@SerializedName("interior_color")
	private String interiorColor;

	@SerializedName("exterior_color")
	private String exteriorColor;

	@SerializedName("car_image")
	private String carImage;

	@SerializedName("car_transmission")
	private String carTransmission;

	@SerializedName("model_year")
	private String modelYear;

	@SerializedName("varient_code")
	private String varientCode;

	@SerializedName("car_id")
	private String carId;

	@SerializedName("car_type")
	private String carType;

	public void setCarTitle(String carTitle){
		this.carTitle = carTitle;
	}

	public String getCarTitle(){
		return carTitle;
	}



	public void setCarImage(String carImage){
		this.carImage = carImage;
	}

	public String getCarImage(){
		return carImage;
	}

	public void setCarTransmission(String carTransmission){
		this.carTransmission = carTransmission;
	}

	public String getCarTransmission(){
		return carTransmission;
	}

	public String getInteriorColor() {
		return interiorColor;
	}

	public void setInteriorColor(String interiorColor) {
		this.interiorColor = interiorColor;
	}

	public String getExteriorColor() {
		return exteriorColor;
	}

	public void setExteriorColor(String exteriorColor) {
		this.exteriorColor = exteriorColor;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public void setVarientCode(String varientCode){
		this.varientCode = varientCode;
	}

	public String getVarientCode(){
		return varientCode;
	}

	public void setCarId(String carId){
		this.carId = carId;
	}

	public String getCarId(){
		return carId;
	}

	public void setCarType(String carType){
		this.carType = carType;
	}

	public String getCarType(){
		return carType;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"car_title = '" + carTitle + '\'' + 
			",interior_color = '" + interiorColor + '\'' + 
			",exterior_color = '" + exteriorColor + '\'' + 
			",car_image = '" + carImage + '\'' + 
			",car_transmission = '" + carTransmission + '\'' + 
			",model_year = '" + modelYear + '\'' + 
			",varient_code = '" + varientCode + '\'' + 
			",car_id = '" + carId + '\'' + 
			",car_type = '" + carType + '\'' + 
			"}";
		}
}