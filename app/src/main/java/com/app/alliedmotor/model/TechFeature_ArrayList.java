package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

public class TechFeature_ArrayList {




    @SerializedName("motor")
    public String motor;

    @SerializedName("hybrid System Net Output")
    public String hybridSystemNetOutput;

    @SerializedName("tyres Size")
    public String tyresSize;

    @SerializedName("torque")
    public String torque;

    @SerializedName("spare Wheel Carrier")
    public String spareWheelCarrier;

    @SerializedName("cargo Space")
    public String cargoSpace;

    @SerializedName("output")
    public String output;

    @SerializedName("electric Motor")
    public String electricMotor;

    @SerializedName("transmission")
    public String transmission;

    @SerializedName("kerb Weight")
    public String kerbWeight;

    @SerializedName("power Output")
    public String powerOutput;

    @SerializedName("engine")
    public String engine;

    @SerializedName("payload")
    public String payload;

    @SerializedName("seating Capacity")
    public String seatingCapacity;

    @SerializedName("max Torque")
    public String maxTorque;

    @SerializedName("Output Hp Combined")
    public String outputHpCombined;

    @SerializedName("emission Control")
    public String emissionControl;

    @SerializedName("Output Hp")
    public String outputHp;

    @SerializedName("rear Suspension")
    public String rearSuspension;

    @SerializedName("terrain")
    public String terrain;

    @SerializedName("ground Clearance")
    public String groundClearance;

    @SerializedName("front Suspension")
    public String frontSuspension;

    @SerializedName("torque Motor")
    public String torqueMotor;

    @SerializedName("torque Nm")
    public String torqueNm;

    @SerializedName("wheel Type")
    public String wheelType;

    @SerializedName("fuel Economy")
    public String fuelEconomy;

    @SerializedName("spec Type")
    public String specType;

    @SerializedName("doors")
    public String doors;

    @SerializedName("torque Engine")
    public String torqueEngine;

    @SerializedName("wheelbase")
    public String wheelbase;

    @SerializedName("Max Output System")
    public String maxOutputSystem;

    @SerializedName("fuel Type")
    public String fuelType;

    @SerializedName("fuel Tank Capacity")
    public String fuelTankCapacity;

    @SerializedName("max Power")
    public String maxPower;

    @SerializedName("dimensions")
    public String dimensions;

    @SerializedName("gross Weight")
    public String grossWeight;

    public void setMotor(String motor){
        this.motor = motor;
    }

    public String getMotor(){
        return motor;
    }

    public void setHybridSystemNetOutput(String hybridSystemNetOutput){
        this.hybridSystemNetOutput = hybridSystemNetOutput;
    }

    public String getHybridSystemNetOutput(){
        return hybridSystemNetOutput;
    }

    public void setTyresSize(String tyresSize){
        this.tyresSize = tyresSize;
    }

    public String getTyresSize(){
        return tyresSize;
    }

    public void setTorque(String torque){
        this.torque = torque;
    }

    public String getTorque(){
        return torque;
    }

    public void setSpareWheelCarrier(String spareWheelCarrier){
        this.spareWheelCarrier = spareWheelCarrier;
    }

    public String getSpareWheelCarrier(){
        return spareWheelCarrier;
    }

    public void setCargoSpace(String cargoSpace){
        this.cargoSpace = cargoSpace;
    }

    public String getCargoSpace(){
        return cargoSpace;
    }

    public void setOutput(String output){
        this.output = output;
    }

    public String getOutput(){
        return output;
    }

    public void setElectricMotor(String electricMotor){
        this.electricMotor = electricMotor;
    }

    public String getElectricMotor(){
        return electricMotor;
    }

    public void setTransmission(String transmission){
        this.transmission = transmission;
    }

    public String getTransmission(){
        return transmission;
    }

    public void setKerbWeight(String kerbWeight){
        this.kerbWeight = kerbWeight;
    }

    public String getKerbWeight(){
        return kerbWeight;
    }

    public void setPowerOutput(String powerOutput){
        this.powerOutput = powerOutput;
    }

    public String getPowerOutput(){
        return powerOutput;
    }

    public void setEngine(String engine){
        this.engine = engine;
    }

    public String getEngine(){
        return engine;
    }

    public void setPayload(String payload){
        this.payload = payload;
    }

    public String getPayload(){
        return payload;
    }

    public void setSeatingCapacity(String seatingCapacity){
        this.seatingCapacity = seatingCapacity;
    }

    public String getSeatingCapacity(){
        return seatingCapacity;
    }

    public void setMaxTorque(String maxTorque){
        this.maxTorque = maxTorque;
    }

    public String getMaxTorque(){
        return maxTorque;
    }

    public void setOutputHpCombined(String outputHpCombined){
        this.outputHpCombined = outputHpCombined;
    }

    public String getOutputHpCombined(){
        return outputHpCombined;
    }

    public void setEmissionControl(String emissionControl){
        this.emissionControl = emissionControl;
    }

    public String getEmissionControl(){
        return emissionControl;
    }

    public void setOutputHp(String outputHp){
        this.outputHp = outputHp;
    }

    public String getOutputHp(){
        return outputHp;
    }

    public void setRearSuspension(String rearSuspension){
        this.rearSuspension = rearSuspension;
    }

    public String getRearSuspension(){
        return rearSuspension;
    }

    public void setTerrain(String terrain){
        this.terrain = terrain;
    }

    public String getTerrain(){
        return terrain;
    }

    public void setGroundClearance(String groundClearance){
        this.groundClearance = groundClearance;
    }

    public String getGroundClearance(){
        return groundClearance;
    }

    public void setFrontSuspension(String frontSuspension){
        this.frontSuspension = frontSuspension;
    }

    public String getFrontSuspension(){
        return frontSuspension;
    }

    public void setTorqueMotor(String torqueMotor){
        this.torqueMotor = torqueMotor;
    }

    public String getTorqueMotor(){
        return torqueMotor;
    }

    public void setTorqueNm(String torqueNm){
        this.torqueNm = torqueNm;
    }

    public String getTorqueNm(){
        return torqueNm;
    }

    public void setWheelType(String wheelType){
        this.wheelType = wheelType;
    }

    public String getWheelType(){
        return wheelType;
    }

    public void setFuelEconomy(String fuelEconomy){
        this.fuelEconomy = fuelEconomy;
    }

    public String getFuelEconomy(){
        return fuelEconomy;
    }

    public void setSpecType(String specType){
        this.specType = specType;
    }

    public String getSpecType(){
        return specType;
    }

    public void setDoors(String doors){
        this.doors = doors;
    }

    public String getDoors(){
        return doors;
    }

    public void setTorqueEngine(String torqueEngine){
        this.torqueEngine = torqueEngine;
    }

    public String getTorqueEngine(){
        return torqueEngine;
    }

    public void setWheelbase(String wheelbase){
        this.wheelbase = wheelbase;
    }

    public String getWheelbase(){
        return wheelbase;
    }

    public void setMaxOutputSystem(String maxOutputSystem){
        this.maxOutputSystem = maxOutputSystem;
    }

    public String getMaxOutputSystem(){
        return maxOutputSystem;
    }

    public void setFuelType(String fuelType){
        this.fuelType = fuelType;
    }

    public String getFuelType(){
        return fuelType;
    }

    public void setFuelTankCapacity(String fuelTankCapacity){
        this.fuelTankCapacity = fuelTankCapacity;
    }

    public String getFuelTankCapacity(){
        return fuelTankCapacity;
    }

    public void setMaxPower(String maxPower){
        this.maxPower = maxPower;
    }

    public String getMaxPower(){
        return maxPower;
    }

    public void setDimensions(String dimensions){
        this.dimensions = dimensions;
    }

    public String getDimensions(){
        return dimensions;
    }

    public void setGrossWeight(String grossWeight){
        this.grossWeight = grossWeight;
    }

    public String getGrossWeight(){
        return grossWeight;
    }

    @Override
    public String toString(){
        return
                "{" +
                        "motor = '" + motor + '\'' +
                        ",hybrid System Net Output = '" + hybridSystemNetOutput + '\'' +
                        ",tyres Size = '" + tyresSize + '\'' +
                        ",torque = '" + torque + '\'' +
                        ",spare Wheel Carrier = '" + spareWheelCarrier + '\'' +
                        ",cargo Space = '" + cargoSpace + '\'' +
                        ",output = '" + output + '\'' +
                        ",electric Motor = '" + electricMotor + '\'' +
                        ",transmission = '" + transmission + '\'' +
                        ",kerb Weight = '" + kerbWeight + '\'' +
                        ",power Output = '" + powerOutput + '\'' +
                        ",engine = '" + engine + '\'' +
                        ",payload = '" + payload + '\'' +
                        ",seating Capacity = '" + seatingCapacity + '\'' +
                        ",max Torque = '" + maxTorque + '\'' +
                        ",output Hp Combined = '" + outputHpCombined + '\'' +
                        ",emission Control = '" + emissionControl + '\'' +
                        ",output Hp = '" + outputHp + '\'' +
                        ",rear Suspension = '" + rearSuspension + '\'' +
                        ",terrain = '" + terrain + '\'' +
                        ",ground Clearance = '" + groundClearance + '\'' +
                        ",front Suspension = '" + frontSuspension + '\'' +
                        ",torque Motor = '" + torqueMotor + '\'' +
                        ",torque Nm = '" + torqueNm + '\'' +
                        ",wheel Type = '" + wheelType + '\'' +
                        ",fuel Economy = '" + fuelEconomy + '\'' +
                        ",spec Type = '" + specType + '\'' +
                        ",doors = '" + doors + '\'' +
                        ",torque Engine = '" + torqueEngine + '\'' +
                        ",wheelbase = '" + wheelbase + '\'' +
                        ",max Output System = '" + maxOutputSystem + '\'' +
                        ",fuel Type = '" + fuelType + '\'' +
                        ",fuel Tank Capacity = '" + fuelTankCapacity + '\'' +
                        ",max Power = '" + maxPower + '\'' +
                        ",dimensions = '" + dimensions + '\'' +
                        ",gross Weight = '" + grossWeight + '\'' +
                        "}";
    }

}
