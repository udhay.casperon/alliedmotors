package com.app.alliedmotor.model.Services_Category;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("title")
	private String title;

	@SerializedName("banner_details")
	private ArrayList<BannerDetailsItem> bannerDetails;

	@SerializedName("page_description")
	private PageDescription pageDescription;

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setBannerDetails(ArrayList<BannerDetailsItem> bannerDetails){
		this.bannerDetails = bannerDetails;
	}

	public ArrayList<BannerDetailsItem> getBannerDetails(){
		return bannerDetails;
	}

	public void setPageDescription(PageDescription pageDescription){
		this.pageDescription = pageDescription;
	}

	public PageDescription getPageDescription(){
		return pageDescription;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"title = '" + title + '\'' + 
			",banner_details = '" + bannerDetails + '\'' + 
			",page_description = '" + pageDescription + '\'' + 
			"}";
		}
}