package com.app.alliedmotor.model.Signup;

import com.google.gson.annotations.SerializedName;

public class OtpData{

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("code")
	private String code;

	@SerializedName("dev_mode")
	private Integer devMode;

	@SerializedName("otp_type")
	private String otpType;

	@SerializedName("mobile_number")
	private String mobileNumber;

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setDevMode(Integer devMode){
		this.devMode = devMode;
	}

	public Integer getDevMode(){
		return devMode;
	}

	public void setOtpType(String otpType){
		this.otpType = otpType;
	}

	public String getOtpType(){
		return otpType;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}
}