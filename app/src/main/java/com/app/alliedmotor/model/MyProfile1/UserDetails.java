package com.app.alliedmotor.model.MyProfile1;

import com.google.gson.annotations.SerializedName;

public class UserDetails{

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("is_phone_verified")
	private Integer isPhoneVerified;

	@SerializedName("full_name")
	private String fullName;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("user_login")
	private String userLogin;

	@SerializedName("notification_status")
	private Integer notificationStatus;

	@SerializedName("user_nicename")
	private String userNicename;

	@SerializedName("ID")
	private String iD;

	@SerializedName("display_name")
	private String displayName;

	@SerializedName("mobile_number")
	private String mobileNumber;

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setIsPhoneVerified(Integer isPhoneVerified){
		this.isPhoneVerified = isPhoneVerified;
	}

	public Integer getIsPhoneVerified(){
		return isPhoneVerified;
	}

	public void setFullName(String fullName){
		this.fullName = fullName;
	}

	public String getFullName(){
		return fullName;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setUserLogin(String userLogin){
		this.userLogin = userLogin;
	}

	public String getUserLogin(){
		return userLogin;
	}

	public void setNotificationStatus(Integer notificationStatus){
		this.notificationStatus = notificationStatus;
	}

	public Integer getNotificationStatus(){
		return notificationStatus;
	}

	public void setUserNicename(String userNicename){
		this.userNicename = userNicename;
	}

	public String getUserNicename(){
		return userNicename;
	}

	public void setID(String iD){
		this.iD = iD;
	}

	public String getID(){
		return iD;
	}

	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}

	public String getDisplayName(){
		return displayName;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}
}