package com.app.alliedmotor.model.HomePage;

import com.google.gson.annotations.SerializedName;

public class BrandsItem{

	@SerializedName("icon")
	private String icon;

	@SerializedName("mkid")
	private String mkid;

	@SerializedName("makename")
	private String makename;



	@SerializedName("cat_name")
	private String cat_name;

	public String getCat_name() {
		return cat_name;
	}

	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}

	public void setIcon(String icon){
		this.icon = icon;
	}

	public String getIcon(){
		return icon;
	}

	public void setMkid(String mkid){
		this.mkid = mkid;
	}

	public String getMkid(){
		return mkid;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	@Override
	public String toString() {
		return "BrandsItem{" +
				"icon='" + icon + '\'' +
				", mkid='" + mkid + '\'' +
				", makename='" + makename + '\'' +
				", cat_name='" + cat_name + '\'' +
				'}';
	}
}