package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

public class SpareParts_Response {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("message")
	public String message;


	@SerializedName("long_message")
	public String long_message;


	@SerializedName("errors")
	public Errors errors;

	@SerializedName("status")
	public String status;

	public class Errors{

		@SerializedName("email")
		public String email;

		@SerializedName("make_id")
		public String make_id;

		@SerializedName("model_id")
		public String model_id;

		@SerializedName("name")
		public String name;

		@SerializedName("company_name")
		public String company_name;

		@SerializedName("contact")
		public String contact;

		@SerializedName("year")
		public String year;


		@SerializedName("part_no")
		public String part_no;
		@SerializedName("part_name")
		public String part_name;
		@SerializedName("description")
		public String description;
		@SerializedName("remarks")
		public String remarks;


		@SerializedName("country_code_part")
		public String country_code_part;

	}


}