package com.app.alliedmotor.model.CarDetail12;

import com.app.alliedmotor.model.CarDetail.Response_CarDetail;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.ServiceInterface;
import com.app.alliedmotor.utility.WebServiceData_sample;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Service_Cardetail {

    public Service_Cardetail(String url, HashMap<String,String>header, HashMap<String,String>request, final ResponseInterface.CardetailInterface cardetailInterface) {

        final WebServiceData_sample data = new WebServiceData_sample();
        ServiceInterface serviceInterface = data.retrofit().create(ServiceInterface.class);
        serviceInterface.CarDetail_Call_sample(url,header, request).enqueue(new Callback<Response_CarDetail>() {
            @Override
            public void onResponse(Call<Response_CarDetail> call, Response<Response_CarDetail> response) {
                try {
                    System.out.println(";;;--link---" + response);
                    if (response.body() != null) {
                        System.out.println("22222222--Trial list-" + response.body().getStatus());
                        cardetailInterface.onCardetailSuccess(response.body());

                    }
                } catch (Exception e) {
                    System.out.println("e-----Trial list--1----" + e);
                }
            }

            @Override
            public void onFailure(Call<Response_CarDetail> call, Throwable t) {
                Commons.hideProgress();
                System.out.println("111111111111--Trial list-- -onfailure----");
            }
        });


    }


}
