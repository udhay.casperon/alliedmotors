package com.app.alliedmotor.model.NotificationList;

import com.google.gson.annotations.SerializedName;

public class Response_Pushnotification {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("commonArr")
	public CommonArr commonArr;

	@SerializedName("message")
	public String message;

	@SerializedName("errors")
	public Errors errors;

	@SerializedName("status")
	public String status;

	public class Errors{

		@SerializedName("redirect_page")
		public String redirectPage;
	}

	public class CommonArr{

		@SerializedName("token")
		public String token;
	}




}