package com.app.alliedmotor.model.UserProfile;

import com.google.gson.annotations.SerializedName;

public class UserDetails{


	@SerializedName("ID")
	private String iD;

	@SerializedName("user_login")
	private String userLogin;

	@SerializedName("user_nicename")
	private String userNicename;

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("display_name")
	private String displayName;

	@SerializedName("is_phone_verified")
	private Integer isPhoneVerified;


	@SerializedName("work_phone_number")
	private String work_phone_number;


	@SerializedName("first_name")
	private String first_name;

	@SerializedName("company_name")
	private String companyName;


	@SerializedName("user_type")
	private String user_type;

	@SerializedName("mobile_number")
	private String mobileNumber;

	@SerializedName("last_name")
	private String last_name;

	@SerializedName("notification_status")
	private String notificationStatus;


	@SerializedName("locale")
	private String locale;

	@SerializedName("company_role")
	private String company_role;
	@SerializedName("full_name")
	private String full_name;
	@SerializedName("user_role")
	private String user_role;
	@SerializedName("company_user_role")
	private String company_user_role;

	public String getNotificationStatus() {
		return notificationStatus;
	}

	public void setNotificationStatus(String notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCompany_role() {
		return company_role;
	}

	public void setCompany_role(String company_role) {
		this.company_role = company_role;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getUser_role() {
		return user_role;
	}

	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	public String getCompany_user_role() {
		return company_user_role;
	}

	public void setCompany_user_role(String company_user_role) {
		this.company_user_role = company_user_role;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public Integer getIsPhoneVerified() {
		return isPhoneVerified;
	}

	public void setIsPhoneVerified(Integer isPhoneVerified) {
		this.isPhoneVerified = isPhoneVerified;
	}


	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}


	public String getUserNicename() {
		return userNicename;
	}

	public void setUserNicename(String userNicename) {
		this.userNicename = userNicename;
	}

	public String getiD() {
		return iD;
	}

	public void setiD(String iD) {
		this.iD = iD;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getWork_phone_number() {
		return work_phone_number;
	}

	public void setWork_phone_number(String work_phone_number) {
		this.work_phone_number = work_phone_number;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	@Override
	public String toString() {
		return "UserDetails{" +
				"iD='" + iD + '\'' +
				", userLogin='" + userLogin + '\'' +
				", userNicename='" + userNicename + '\'' +
				", userEmail='" + userEmail + '\'' +
				", displayName='" + displayName + '\'' +
				", isPhoneVerified=" + isPhoneVerified +
				", companyName='" + companyName + '\'' +
				", notificationStatus=" + notificationStatus +
				", mobileNumber='" + mobileNumber + '\'' +
				", work_phone_number='" + work_phone_number + '\'' +
				", first_name='" + first_name + '\'' +
				", user_type='" + user_type + '\'' +
				", last_name='" + last_name + '\'' +
				'}';
	}
}