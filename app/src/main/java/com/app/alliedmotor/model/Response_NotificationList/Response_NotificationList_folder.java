package com.app.alliedmotor.model.Response_NotificationList;

import com.google.gson.annotations.SerializedName;

public class Response_NotificationList_folder {

	@SerializedName("response_code")
	private String responseCode;

	@SerializedName("data")
	private Data data;

	@SerializedName("commonArr")
	private CommonArr commonArr;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setCommonArr(CommonArr commonArr){
		this.commonArr = commonArr;
	}

	public CommonArr getCommonArr(){
		return commonArr;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"response_code = '" + responseCode + '\'' + 
			",data = '" + data + '\'' + 
			",commonArr = '" + commonArr + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}