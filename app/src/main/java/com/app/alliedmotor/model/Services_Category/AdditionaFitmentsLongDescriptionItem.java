package com.app.alliedmotor.model.Services_Category;

import com.google.gson.annotations.SerializedName;

public class AdditionaFitmentsLongDescriptionItem{

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("image")
	private String image;

	@SerializedName("ID")
	private String iD;

	public void setPostTitle(String postTitle){
		this.postTitle = postTitle;
	}

	public String getPostTitle(){
		return postTitle;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setID(String iD){
		this.iD = iD;
	}

	public String getID(){
		return iD;
	}

	@Override
 	public String toString(){
		return 
			"AdditionaFitmentsLongDescriptionItem{" + 
			"post_title = '" + postTitle + '\'' + 
			",image = '" + image + '\'' + 
			",iD = '" + iD + '\'' + 
			"}";
		}
}