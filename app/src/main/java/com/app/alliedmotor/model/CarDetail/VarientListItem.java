package com.app.alliedmotor.model.CarDetail;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VarientListItem implements Serializable {

	public VarientListItem(String mainId, String varientCode) {
		this.mainId = mainId;
		this.varientCode = varientCode;
	}

	@SerializedName("mainId")
	private String mainId;

	@SerializedName("varient_code")
	private String varientCode;

	public void setMainId(String mainId){
		this.mainId = mainId;
	}

	public String getMainId(){
		return mainId;
	}

	public void setVarientCode(String varientCode){
		this.varientCode = varientCode;
	}

	public String getVarientCode(){
		return varientCode;
	}

	@Override
 	public String toString(){
		return 
			"VarientListItem{" + 
			"mainId = '" + mainId + '\'' + 
			",varient_code = '" + varientCode + '\'' + 
			"}";
		}
}