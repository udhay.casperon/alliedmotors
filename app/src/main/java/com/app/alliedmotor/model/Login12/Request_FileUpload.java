package com.app.alliedmotor.model.Login12;

public class Request_FileUpload {

    private String image;
    private String file_prefix;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFile_prefix() {
        return file_prefix;
    }

    public void setFile_prefix(String file_prefix) {
        this.file_prefix = file_prefix;
    }

    @Override
    public String toString() {
        return "Request_FileUpload{" +
                "image='" + image + '\'' +
                ", file_prefix='" + file_prefix + '\'' +
                '}';
    }
}
