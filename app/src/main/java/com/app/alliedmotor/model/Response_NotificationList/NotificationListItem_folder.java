package com.app.alliedmotor.model.Response_NotificationList;

import com.google.gson.annotations.SerializedName;

public class NotificationListItem_folder {

	@SerializedName("created_time")
	private String createdTime;

	@SerializedName("body_html")
	private String bodyHtml;

	@SerializedName("title_html")
	private String titleHtml;

	@SerializedName("is_unread")
	private String isUnread;

	@SerializedName("id")
	private String id;

	@SerializedName("href")
	private String href;

	@SerializedName("ticket_id")
	private String ticketId;

	@SerializedName("sender_id")
	private String senderId;

	@SerializedName("recipient_id")
	private String recipientId;

	@SerializedName("type_of_notification")
	private String typeOfNotification;

	public void setCreatedTime(String createdTime){
		this.createdTime = createdTime;
	}

	public String getCreatedTime(){
		return createdTime;
	}

	public void setBodyHtml(String bodyHtml){
		this.bodyHtml = bodyHtml;
	}

	public String getBodyHtml(){
		return bodyHtml;
	}

	public void setTitleHtml(String titleHtml){
		this.titleHtml = titleHtml;
	}

	public String getTitleHtml(){
		return titleHtml;
	}

	public void setIsUnread(String isUnread){
		this.isUnread = isUnread;
	}

	public String getIsUnread(){
		return isUnread;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setHref(String href){
		this.href = href;
	}

	public String getHref(){
		return href;
	}

	public void setTicketId(String ticketId){
		this.ticketId = ticketId;
	}

	public Object getTicketId(){
		return ticketId;
	}

	public void setSenderId(String senderId){
		this.senderId = senderId;
	}

	public String getSenderId(){
		return senderId;
	}

	public void setRecipientId(String recipientId){
		this.recipientId = recipientId;
	}

	public String getRecipientId(){
		return recipientId;
	}

	public void setTypeOfNotification(String typeOfNotification){
		this.typeOfNotification = typeOfNotification;
	}

	public String getTypeOfNotification(){
		return typeOfNotification;
	}

	@Override
 	public String toString(){
		return 
			"NotificationListItem{" + 
			"created_time = '" + createdTime + '\'' + 
			",body_html = '" + bodyHtml + '\'' + 
			",title_html = '" + titleHtml + '\'' + 
			",is_unread = '" + isUnread + '\'' + 
			",id = '" + id + '\'' + 
			",href = '" + href + '\'' + 
			",ticket_id = '" + ticketId + '\'' + 
			",sender_id = '" + senderId + '\'' + 
			",recipient_id = '" + recipientId + '\'' + 
			",type_of_notification = '" + typeOfNotification + '\'' + 
			"}";
		}
}