package com.app.alliedmotor.model.ForgotEmail;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("code")
	private String code;

	@SerializedName("email_address")
	private String emailAddress;

	@SerializedName("dev_mode")
	private Integer devMode;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setEmailAddress(String emailAddress){
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress(){
		return emailAddress;
	}

	public void setDevMode(Integer devMode){
		this.devMode = devMode;
	}

	public Integer getDevMode(){
		return devMode;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"code = '" + code + '\'' + 
			",email_address = '" + emailAddress + '\'' + 
			",dev_mode = '" + devMode + '\'' + 
			"}";
		}
}