package com.app.alliedmotor.model.MyOrderLatest;

import com.google.gson.annotations.SerializedName;

public class OrdersItem{

	@SerializedName("modelName")
	private String modelName;

	@SerializedName("fueltype")
	private String fueltype;

	@SerializedName("carvarient")
	private String carvarient;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("enq_status")
	private String enqStatus;

	@SerializedName("makename")
	private String makename;

	@SerializedName("enq_id")
	private String enqId;

	@SerializedName("modelyear")
	private String modelyear;

	@SerializedName("carId")
	private String carId;

	@SerializedName("modelOptions")
	private String modelOptions;

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public String getModelName(){
		return modelName;
	}

	public void setFueltype(String fueltype){
		this.fueltype = fueltype;
	}

	public String getFueltype(){
		return fueltype;
	}

	public void setCarvarient(String carvarient){
		this.carvarient = carvarient;
	}

	public String getCarvarient(){
		return carvarient;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setEnqStatus(String enqStatus){
		this.enqStatus = enqStatus;
	}

	public String getEnqStatus(){
		return enqStatus;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	public void setEnqId(String enqId){
		this.enqId = enqId;
	}

	public String getEnqId(){
		return enqId;
	}

	public void setModelyear(String modelyear){
		this.modelyear = modelyear;
	}

	public String getModelyear(){
		return modelyear;
	}

	public void setCarId(String carId){
		this.carId = carId;
	}

	public String getCarId(){
		return carId;
	}

	public void setModelOptions(String modelOptions){
		this.modelOptions = modelOptions;
	}

	public String getModelOptions(){
		return modelOptions;
	}

	@Override
 	public String toString(){
		return
			"OrdersItem{" +
			"modelName = '" + modelName + '\'' +
			",fueltype = '" + fueltype + '\'' +
			",carvarient = '" + carvarient + '\'' +
			",quantity = '" + quantity + '\'' +
			",enq_status = '" + enqStatus + '\'' +
			",makename = '" + makename + '\'' +
			",enq_id = '" + enqId + '\'' +
			",modelyear = '" + modelyear + '\'' +
			",carId = '" + carId + '\'' +
			",modelOptions = '" + modelOptions + '\'' +
			"}";
		}
}