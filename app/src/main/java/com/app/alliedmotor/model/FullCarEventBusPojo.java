package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FullCarEventBusPojo implements Serializable {

    @SerializedName("newCarStatus")
    private String newCarStatus;

    @SerializedName("secondaryEmail")
    private Object secondaryEmail;

    @SerializedName("currency_name")
    private String currencyName;

    @SerializedName("currency_flag_image")
    private String currencyFlagImage;

    @SerializedName("vehiceType")
    private String vehiceType;

    @SerializedName("mkId")
    private String mkId;

    @SerializedName("modelYear")
    private String modelYear;

    @SerializedName("makename")
    private String makename;

    @SerializedName("modId")
    private String modId;

    @SerializedName("moId")
    private String moId;

    @SerializedName("modelOptions")
    private String modelOptions;

    @SerializedName("pdfStatus")
    private String pdfStatus;

    @SerializedName("eftId")
    private String eftId;

    @SerializedName("catid")
    private String catid;

    @SerializedName("modelName")
    private String modelName;

    @SerializedName("efuelType")
    private String efuelType;

    @SerializedName("makeFolder")
    private String makeFolder;

    @SerializedName("price")
    private String price;

    @SerializedName("car_image")
    private String carImage;

    @SerializedName("mainId")
    private String mainId;

    @SerializedName("email")
    private String email;

    @SerializedName("make_icon")
    private String makeIcon;

    @SerializedName("make_mobile_number")
    private String make_mobile_number;


    @SerializedName("promo_logo")
    private String promo_logo;


    public String getMake_mobile_number() {
        return make_mobile_number;
    }

    public void setMake_mobile_number(String make_mobile_number) {
        this.make_mobile_number = make_mobile_number;
    }

    public String getPromo_logo() {
        return promo_logo;
    }

    public void setPromo_logo(String promo_logo) {
        this.promo_logo = promo_logo;
    }


    public void setNewCarStatus(String newCarStatus) {
        this.newCarStatus = newCarStatus;
    }

    public String getNewCarStatus() {
        return newCarStatus;
    }

    public void setSecondaryEmail(Object secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public Object getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyFlagImage(String currencyFlagImage) {
        this.currencyFlagImage = currencyFlagImage;
    }

    public String getCurrencyFlagImage() {
        return currencyFlagImage;
    }

    public void setVehiceType(String vehiceType) {
        this.vehiceType = vehiceType;
    }

    public String getVehiceType() {
        return vehiceType;
    }

    public void setMkId(String mkId) {
        this.mkId = mkId;
    }

    public String getMkId() {
        return mkId;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setMakename(String makename) {
        this.makename = makename;
    }

    public String getMakename() {
        return makename;
    }

    public void setModId(String modId) {
        this.modId = modId;
    }

    public String getModId() {
        return modId;
    }

    public void setMoId(String moId) {
        this.moId = moId;
    }

    public String getMoId() {
        return moId;
    }

    public void setModelOptions(String modelOptions) {
        this.modelOptions = modelOptions;
    }

    public String getModelOptions() {
        return modelOptions;
    }

    public void setPdfStatus(String pdfStatus) {
        this.pdfStatus = pdfStatus;
    }

    public String getPdfStatus() {
        return pdfStatus;
    }

    public void setEftId(String eftId) {
        this.eftId = eftId;
    }

    public String getEftId() {
        return eftId;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatid() {
        return catid;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setEfuelType(String efuelType) {
        this.efuelType = efuelType;
    }

    public String getEfuelType() {
        return efuelType;
    }

    public void setMakeFolder(String makeFolder) {
        this.makeFolder = makeFolder;
    }

    public String getMakeFolder() {
        return makeFolder;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setCarImage(String carImage) {
        this.carImage = carImage;
    }

    public String getCarImage() {
        return carImage;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getMainId() {
        return mainId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setMakeIcon(String makeIcon) {
        this.makeIcon = makeIcon;
    }

    public String getMakeIcon() {
        return makeIcon;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "newCarStatus = '" + newCarStatus + '\'' +
                        ",secondaryEmail = '" + secondaryEmail + '\'' +
                        ",currency_name = '" + currencyName + '\'' +
                        ",currency_flag_image = '" + currencyFlagImage + '\'' +
                        ",vehiceType = '" + vehiceType + '\'' +
                        ",mkId = '" + mkId + '\'' +
                        ",modelYear = '" + modelYear + '\'' +
                        ",makename = '" + makename + '\'' +
                        ",modId = '" + modId + '\'' +
                        ",moId = '" + moId + '\'' +
                        ",modelOptions = '" + modelOptions + '\'' +
                        ",pdfStatus = '" + pdfStatus + '\'' +
                        ",eftId = '" + eftId + '\'' +
                        ",catid = '" + catid + '\'' +
                        ",modelName = '" + modelName + '\'' +
                        ",efuelType = '" + efuelType + '\'' +
                        ",makeFolder = '" + makeFolder + '\'' +
                        ",price = '" + price + '\'' +
                        ",car_image = '" + carImage + '\'' +
                        ",mainId = '" + mainId + '\'' +
                        ",email = '" + email + '\'' +
                        ",make_icon = '" + makeIcon + '\'' +
                        "}";
    }
}