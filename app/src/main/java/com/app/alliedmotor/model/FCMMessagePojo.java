package com.app.alliedmotor.model;

public class FCMMessagePojo {

    private String fcmtitle,fcmcontent,fcm_isread,type_notification="",postid="",ticketid="";

    public FCMMessagePojo(String fcmtitle, String fcmcontent, String fcm_isread, String type_notification, String postid, String ticketid) {
        this.fcmtitle = fcmtitle;
        this.fcmcontent = fcmcontent;
        this.fcm_isread = fcm_isread;
        this.type_notification = type_notification;
        this.postid = postid;
        this.ticketid = ticketid;
    }

    public String getFcmtitle() {
        return fcmtitle;
    }

    public void setFcmtitle(String fcmtitle) {
        this.fcmtitle = fcmtitle;
    }

    public String getFcmcontent() {
        return fcmcontent;
    }

    public void setFcmcontent(String fcmcontent) {
        this.fcmcontent = fcmcontent;
    }

    public String getFcm_isread() {
        return fcm_isread;
    }

    public void setFcm_isread(String fcm_isread) {
        this.fcm_isread = fcm_isread;
    }

    public String getType_notification() {
        return type_notification;
    }

    public void setType_notification(String type_notification) {
        this.type_notification = type_notification;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public String getTicketid() {
        return ticketid;
    }

    public void setTicketid(String ticketid) {
        this.ticketid = ticketid;
    }
}
