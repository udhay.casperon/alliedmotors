package com.app.alliedmotor.model.GetQuote;

import com.google.gson.annotations.SerializedName;

public class QuoteDataItem{

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("interior_color")
	private String interiorColor;

	@SerializedName("car_year")
	private String carYear;

	@SerializedName("car_url")
	private String carUrl;

	@SerializedName("car_transmission")
	private String carTransmission;

	@SerializedName("car_cat")
	private String carCat;

	@SerializedName("model_year")
	private String modelYear;

	@SerializedName("car_fuel_type")
	private String carFuelType;

	@SerializedName("secondary_email")
	private Object secondaryEmail;

	@SerializedName("varient_code")
	private String varientCode;

	@SerializedName("make_email")
	private String makeEmail;

	@SerializedName("car_type")
	private String carType;

	@SerializedName("car_title")
	private String carTitle;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("exterior_color")
	private String exteriorColor;

	@SerializedName("car_image")
	private String carImage;

	@SerializedName("gallery_id")
	private Object galleryId;

	@SerializedName("id")
	private String id;

	@SerializedName("car_info")
	private String carInfo;

	@SerializedName("car_id")
	private String carId;

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setInteriorColor(String interiorColor){
		this.interiorColor = interiorColor;
	}

	public String getInteriorColor(){
		return interiorColor;
	}

	public void setCarYear(String carYear){
		this.carYear = carYear;
	}

	public String getCarYear(){
		return carYear;
	}

	public void setCarUrl(String carUrl){
		this.carUrl = carUrl;
	}

	public String getCarUrl(){
		return carUrl;
	}

	public void setCarTransmission(String carTransmission){
		this.carTransmission = carTransmission;
	}

	public String getCarTransmission(){
		return carTransmission;
	}

	public void setCarCat(String carCat){
		this.carCat = carCat;
	}

	public String getCarCat(){
		return carCat;
	}

	public void setModelYear(String modelYear){
		this.modelYear = modelYear;
	}

	public String getModelYear(){
		return modelYear;
	}

	public void setCarFuelType(String carFuelType){
		this.carFuelType = carFuelType;
	}

	public String getCarFuelType(){
		return carFuelType;
	}

	public void setSecondaryEmail(Object secondaryEmail){
		this.secondaryEmail = secondaryEmail;
	}

	public Object getSecondaryEmail(){
		return secondaryEmail;
	}

	public void setVarientCode(String varientCode){
		this.varientCode = varientCode;
	}

	public String getVarientCode(){
		return varientCode;
	}

	public void setMakeEmail(String makeEmail){
		this.makeEmail = makeEmail;
	}

	public String getMakeEmail(){
		return makeEmail;
	}

	public void setCarType(String carType){
		this.carType = carType;
	}

	public String getCarType(){
		return carType;
	}

	public void setCarTitle(String carTitle){
		this.carTitle = carTitle;
	}

	public String getCarTitle(){
		return carTitle;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setExteriorColor(String exteriorColor){
		this.exteriorColor = exteriorColor;
	}

	public String getExteriorColor(){
		return exteriorColor;
	}

	public void setCarImage(String carImage){
		this.carImage = carImage;
	}

	public String getCarImage(){
		return carImage;
	}

	public void setGalleryId(Object galleryId){
		this.galleryId = galleryId;
	}

	public Object getGalleryId(){
		return galleryId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCarInfo(String carInfo){
		this.carInfo = carInfo;
	}

	public String getCarInfo(){
		return carInfo;
	}

	public void setCarId(String carId){
		this.carId = carId;
	}

	public String getCarId(){
		return carId;
	}

	@Override
 	public String toString(){
		return 
			"QuoteDataItem{" + 
			"quantity = '" + quantity + '\'' + 
			",interior_color = '" + interiorColor + '\'' + 
			",car_year = '" + carYear + '\'' + 
			",car_url = '" + carUrl + '\'' + 
			",car_transmission = '" + carTransmission + '\'' + 
			",car_cat = '" + carCat + '\'' + 
			",model_year = '" + modelYear + '\'' + 
			",car_fuel_type = '" + carFuelType + '\'' + 
			",secondary_email = '" + secondaryEmail + '\'' + 
			",varient_code = '" + varientCode + '\'' + 
			",make_email = '" + makeEmail + '\'' + 
			",car_type = '" + carType + '\'' + 
			",car_title = '" + carTitle + '\'' + 
			",user_id = '" + userId + '\'' + 
			",exterior_color = '" + exteriorColor + '\'' + 
			",car_image = '" + carImage + '\'' + 
			",gallery_id = '" + galleryId + '\'' + 
			",id = '" + id + '\'' + 
			",car_info = '" + carInfo + '\'' + 
			",car_id = '" + carId + '\'' + 
			"}";
		}
}