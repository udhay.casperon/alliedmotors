package com.app.alliedmotor.model.UserProfile;

import com.google.gson.annotations.SerializedName;

public class Support{

	@SerializedName("whatsapp")
	private String whatsapp;

	@SerializedName("mobile")
	private String mobile;

	public void setWhatsapp(String whatsapp){
		this.whatsapp = whatsapp;
	}

	public String getWhatsapp(){
		return whatsapp;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	@Override
 	public String toString(){
		return 
			"Support{" + 
			"whatsapp = '" + whatsapp + '\'' + 
			",mobile = '" + mobile + '\'' + 
			"}";
		}
}