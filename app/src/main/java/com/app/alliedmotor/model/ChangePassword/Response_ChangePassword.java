package com.app.alliedmotor.model.ChangePassword;

import com.google.gson.annotations.SerializedName;

public class Response_ChangePassword {

    @SerializedName("response_code")
    public String responseCode;

    @SerializedName("long_message")
    public String longMessage;

    @SerializedName("message")
    public String message;

    @SerializedName("status")
    public String status;



    @SerializedName("errors")
    public ErrArr errors;


  public   class ErrArr {

        @SerializedName("user_password")
        public String user_password;



    }


    @Override
    public String toString() {
        return "Response_ChangePassword{" +
                "responseCode='" + responseCode + '\'' +
                ", longMessage='" + longMessage + '\'' +
                ", message='" + message + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
