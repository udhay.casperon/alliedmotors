package com.app.alliedmotor.model.Login12;

import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.model.NewCar.NewCar_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.ServiceInterface;
import com.app.alliedmotor.utility.WebServiceData_sample;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Service_NewcarList {



    public Service_NewcarList(String url, HashMap<String,String> request, final ResponseInterface.NewCarListInterface newCarListInterface) {

        final WebServiceData_sample data = new WebServiceData_sample();
        ServiceInterface serviceInterface = data.retrofit().create(ServiceInterface.class);
        serviceInterface.NEWCAR_RESPONSE_CALL(url, request).enqueue(new Callback<NewCar_Response>() {
            @Override
            public void onResponse(Call<NewCar_Response> call, Response<NewCar_Response> response) {
                try {
                    System.out.println(";;;--link---" + response);
                    if (response.body() != null) {
                        System.out.println("22222222--Trial list-" + response.body().getStatus());
                        newCarListInterface.onNewCarListSuccess(response.body());

                    }
                } catch (Exception e) {
                    System.out.println("e-----Trial list--1----" + e);
                }
            }

            @Override
            public void onFailure(Call<NewCar_Response> call, Throwable t) {
                Commons.hideProgress();
                // Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show();
                System.out.println("111111111111--Trial list-- -onfailure----");
            }
        });


    }


}
