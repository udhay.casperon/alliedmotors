package com.app.alliedmotor.model.NewCar_details;

import com.google.gson.annotations.SerializedName;

public class CarDetails{

	@SerializedName("pdf_file")
	private String pdfFile;

	@SerializedName("secondaryEmail")
	private Object secondaryEmail;

	@SerializedName("locationName")
	private String locationName;

	@SerializedName("currency_name")
	private String currencyName;

	@SerializedName("vehiceType")
	private String vehiceType;

	@SerializedName("modelYear")
	private String modelYear;

	@SerializedName("makename")
	private String makename;

	@SerializedName("varient_code")
	private String varientCode;

	@SerializedName("modelOptions")
	private String modelOptions;

	@SerializedName("modelName")
	private String modelName;

	@SerializedName("efuelType")
	private String efuelType;

	@SerializedName("makeFolder")
	private String makeFolder;

	@SerializedName("price")
	private String price;

	@SerializedName("car_image")
	private String carImage;

	@SerializedName("mainId")
	private String mainId;

	@SerializedName("email")
	private String email;

	@SerializedName("colorDesc")
	private String colorDesc;

	public void setPdfFile(String pdfFile){
		this.pdfFile = pdfFile;
	}

	public String getPdfFile(){
		return pdfFile;
	}

	public void setSecondaryEmail(Object secondaryEmail){
		this.secondaryEmail = secondaryEmail;
	}

	public Object getSecondaryEmail(){
		return secondaryEmail;
	}

	public void setLocationName(String locationName){
		this.locationName = locationName;
	}

	public String getLocationName(){
		return locationName;
	}

	public void setCurrencyName(String currencyName){
		this.currencyName = currencyName;
	}

	public String getCurrencyName(){
		return currencyName;
	}

	public void setVehiceType(String vehiceType){
		this.vehiceType = vehiceType;
	}

	public String getVehiceType(){
		return vehiceType;
	}

	public void setModelYear(String modelYear){
		this.modelYear = modelYear;
	}

	public String getModelYear(){
		return modelYear;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	public void setVarientCode(String varientCode){
		this.varientCode = varientCode;
	}

	public String getVarientCode(){
		return varientCode;
	}

	public void setModelOptions(String modelOptions){
		this.modelOptions = modelOptions;
	}

	public String getModelOptions(){
		return modelOptions;
	}

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public String getModelName(){
		return modelName;
	}

	public void setEfuelType(String efuelType){
		this.efuelType = efuelType;
	}

	public String getEfuelType(){
		return efuelType;
	}

	public void setMakeFolder(String makeFolder){
		this.makeFolder = makeFolder;
	}

	public String getMakeFolder(){
		return makeFolder;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setCarImage(String carImage){
		this.carImage = carImage;
	}

	public String getCarImage(){
		return carImage;
	}

	public void setMainId(String mainId){
		this.mainId = mainId;
	}

	public String getMainId(){
		return mainId;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setColorDesc(String colorDesc){
		this.colorDesc = colorDesc;
	}

	public String getColorDesc(){
		return colorDesc;
	}

	@Override
 	public String toString(){
		return 
			"CarDetails{" + 
			"pdf_file = '" + pdfFile + '\'' + 
			",secondaryEmail = '" + secondaryEmail + '\'' + 
			",locationName = '" + locationName + '\'' + 
			",currency_name = '" + currencyName + '\'' + 
			",vehiceType = '" + vehiceType + '\'' + 
			",modelYear = '" + modelYear + '\'' + 
			",makename = '" + makename + '\'' + 
			",varient_code = '" + varientCode + '\'' + 
			",modelOptions = '" + modelOptions + '\'' + 
			",modelName = '" + modelName + '\'' + 
			",efuelType = '" + efuelType + '\'' + 
			",makeFolder = '" + makeFolder + '\'' + 
			",price = '" + price + '\'' + 
			",car_image = '" + carImage + '\'' + 
			",mainId = '" + mainId + '\'' + 
			",email = '" + email + '\'' + 
			",colorDesc = '" + colorDesc + '\'' + 
			"}";
		}
}