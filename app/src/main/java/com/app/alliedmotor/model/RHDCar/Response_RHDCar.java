package com.app.alliedmotor.model.RHDCar;

import com.google.gson.annotations.SerializedName;

public class Response_RHDCar {

	@SerializedName("response_code")
	private String responseCode;

	@SerializedName("data")
	private Data data;

	@SerializedName("message")
	private String message;


	@SerializedName("long_message")
	private String long_message;

	@SerializedName("status")
	private String status;


	public String getLong_message() {
		return long_message;
	}

	public void setLong_message(String long_message) {
		this.long_message = long_message;
	}

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"response_code = '" + responseCode + '\'' + 
			",data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}