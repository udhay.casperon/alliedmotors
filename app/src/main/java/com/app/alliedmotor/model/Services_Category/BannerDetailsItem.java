package com.app.alliedmotor.model.Services_Category;

import com.google.gson.annotations.SerializedName;

public class BannerDetailsItem{

	@SerializedName("banner_repeater_2_banner_image")
	private String bannerRepeater2BannerImage;

	@SerializedName("banner_repeater_2_banner_description")
	private String bannerRepeater2BannerDescription;

	@SerializedName("banner_repeater_1_banner_image_alt")
	private String bannerRepeater1BannerImageAlt;

	@SerializedName("banner_repeater_1_banner_image")
	private String bannerRepeater1BannerImage;

	@SerializedName("banner_repeater_1_banner_description")
	private String bannerRepeater1BannerDescription;

	@SerializedName("banner_repeater_0_banner_image")
	private String bannerRepeater0BannerImage;

	@SerializedName("banner_repeater_0_banner_image_alt")
	private String bannerRepeater0BannerImageAlt;

	@SerializedName("banner_repeater_0_banner_description")
	private String bannerRepeater0BannerDescription;

	public void setBannerRepeater2BannerImage(String bannerRepeater2BannerImage){
		this.bannerRepeater2BannerImage = bannerRepeater2BannerImage;
	}

	public String getBannerRepeater2BannerImage(){
		return bannerRepeater2BannerImage;
	}

	public void setBannerRepeater2BannerDescription(String bannerRepeater2BannerDescription){
		this.bannerRepeater2BannerDescription = bannerRepeater2BannerDescription;
	}

	public String getBannerRepeater2BannerDescription(){
		return bannerRepeater2BannerDescription;
	}

	public void setBannerRepeater1BannerImageAlt(String bannerRepeater1BannerImageAlt){
		this.bannerRepeater1BannerImageAlt = bannerRepeater1BannerImageAlt;
	}

	public String getBannerRepeater1BannerImageAlt(){
		return bannerRepeater1BannerImageAlt;
	}

	public void setBannerRepeater1BannerImage(String bannerRepeater1BannerImage){
		this.bannerRepeater1BannerImage = bannerRepeater1BannerImage;
	}

	public String getBannerRepeater1BannerImage(){
		return bannerRepeater1BannerImage;
	}

	public void setBannerRepeater1BannerDescription(String bannerRepeater1BannerDescription){
		this.bannerRepeater1BannerDescription = bannerRepeater1BannerDescription;
	}

	public String getBannerRepeater1BannerDescription(){
		return bannerRepeater1BannerDescription;
	}

	public void setBannerRepeater0BannerImage(String bannerRepeater0BannerImage){
		this.bannerRepeater0BannerImage = bannerRepeater0BannerImage;
	}

	public String getBannerRepeater0BannerImage(){
		return bannerRepeater0BannerImage;
	}

	public void setBannerRepeater0BannerImageAlt(String bannerRepeater0BannerImageAlt){
		this.bannerRepeater0BannerImageAlt = bannerRepeater0BannerImageAlt;
	}

	public String getBannerRepeater0BannerImageAlt(){
		return bannerRepeater0BannerImageAlt;
	}

	public void setBannerRepeater0BannerDescription(String bannerRepeater0BannerDescription){
		this.bannerRepeater0BannerDescription = bannerRepeater0BannerDescription;
	}

	public String getBannerRepeater0BannerDescription(){
		return bannerRepeater0BannerDescription;
	}

	@Override
 	public String toString(){
		return 
			"BannerDetailsItem{" + 
			"banner_repeater_2_banner_image = '" + bannerRepeater2BannerImage + '\'' + 
			",banner_repeater_2_banner_description = '" + bannerRepeater2BannerDescription + '\'' + 
			",banner_repeater_1_banner_image_alt = '" + bannerRepeater1BannerImageAlt + '\'' + 
			",banner_repeater_1_banner_image = '" + bannerRepeater1BannerImage + '\'' + 
			",banner_repeater_1_banner_description = '" + bannerRepeater1BannerDescription + '\'' + 
			",banner_repeater_0_banner_image = '" + bannerRepeater0BannerImage + '\'' + 
			",banner_repeater_0_banner_image_alt = '" + bannerRepeater0BannerImageAlt + '\'' + 
			",banner_repeater_0_banner_description = '" + bannerRepeater0BannerDescription + '\'' + 
			"}";
		}
}