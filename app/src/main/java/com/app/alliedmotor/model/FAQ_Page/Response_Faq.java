package com.app.alliedmotor.model.FAQ_Page;

import java.util.ArrayList;

public class Response_Faq {

    private String status;
    private String response_code;
    private String message;
    private ArrayList<Faq_Data> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Faq_Data> getData() {
        return data;
    }

    public void setData(ArrayList<Faq_Data> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Response_Faq{" +
                "status='" + status + '\'' +
                ", response_code='" + response_code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}

