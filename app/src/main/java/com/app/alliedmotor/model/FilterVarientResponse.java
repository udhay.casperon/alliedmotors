package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FilterVarientResponse {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("errors")
	public Errors errors;

	@SerializedName("status")
	public String status;


	public class Data{

		@SerializedName("varient_list")
		public ArrayList<VarientListItem> varientList;


		public class VarientListItem{

			@SerializedName("mainId")
			public String mainId;

			@SerializedName("varient_code")
			public String varientCode;
		}
	}

	public class Errors{

		@SerializedName("eftId")
		public String eftId;

		@SerializedName("modId")
		public String modId;

		@SerializedName("makeId")
		public String makeId;

		
		@SerializedName("moId")
		public String moId;


	}


}