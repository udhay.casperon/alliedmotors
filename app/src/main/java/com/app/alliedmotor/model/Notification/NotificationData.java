package com.app.alliedmotor.model.Notification;

import com.google.gson.annotations.SerializedName;

public class NotificationData{

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("created_time")
	private String createdTime;

	@SerializedName("short_description")
	private String shortDescription;

	@SerializedName("post_content")
	private String postContent;

	@SerializedName("post_id")
	private String postId;

	@SerializedName("button_url")
	private String buttonUrl;

	@SerializedName("_thumbnail_id")
	private String thumbnailId;

	@SerializedName("button_text")
	private String buttonText;

	@SerializedName("ennable_button")
	private String ennableButton;

	public void setPostTitle(String postTitle){
		this.postTitle = postTitle;
	}

	public String getPostTitle(){
		return postTitle;
	}

	public void setCreatedTime(String createdTime){
		this.createdTime = createdTime;
	}

	public String getCreatedTime(){
		return createdTime;
	}

	public void setShortDescription(String shortDescription){
		this.shortDescription = shortDescription;
	}

	public String getShortDescription(){
		return shortDescription;
	}

	public void setPostContent(String postContent){
		this.postContent = postContent;
	}

	public String getPostContent(){
		return postContent;
	}

	public void setPostId(String postId){
		this.postId = postId;
	}

	public String getPostId(){
		return postId;
	}

	public void setButtonUrl(String buttonUrl){
		this.buttonUrl = buttonUrl;
	}

	public String getButtonUrl(){
		return buttonUrl;
	}

	public void setThumbnailId(String thumbnailId){
		this.thumbnailId = thumbnailId;
	}

	public String getThumbnailId(){
		return thumbnailId;
	}

	public void setButtonText(String buttonText){
		this.buttonText = buttonText;
	}

	public String getButtonText(){
		return buttonText;
	}

	public void setEnnableButton(String ennableButton){
		this.ennableButton = ennableButton;
	}

	public String getEnnableButton(){
		return ennableButton;
	}

	@Override
 	public String toString(){
		return 
			"NotificationData{" + 
			"post_title = '" + postTitle + '\'' + 
			",created_time = '" + createdTime + '\'' + 
			",short_description = '" + shortDescription + '\'' + 
			",post_content = '" + postContent + '\'' + 
			",post_id = '" + postId + '\'' + 
			",button_url = '" + buttonUrl + '\'' + 
			",_thumbnail_id = '" + thumbnailId + '\'' + 
			",button_text = '" + buttonText + '\'' + 
			",ennable_button = '" + ennableButton + '\'' + 
			"}";
		}
}