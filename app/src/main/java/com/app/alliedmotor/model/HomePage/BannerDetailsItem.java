package com.app.alliedmotor.model.HomePage;

import com.google.gson.annotations.SerializedName;

public class BannerDetailsItem{

	@SerializedName("image")
	private String image;

	@SerializedName("alt")
	private String alt;

	@SerializedName("description")
	private String description;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setAlt(String alt){
		this.alt = alt;
	}

	public String getAlt(){
		return alt;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	@Override
 	public String toString(){
		return 
			"BannerDetailsItem{" + 
			"image = '" + image + '\'' + 
			",alt = '" + alt + '\'' + 
			",description = '" + description + '\'' + 
			"}";
		}
}