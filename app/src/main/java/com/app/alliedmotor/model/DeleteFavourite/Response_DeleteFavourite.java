package com.app.alliedmotor.model.DeleteFavourite;

import com.google.gson.annotations.SerializedName;

public class Response_DeleteFavourite {

	@SerializedName("response_code")
	private String responseCode;

	@SerializedName("commonArr")
	private CommonArr commonArr;

	@SerializedName("long_message")
	private String longMessage;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setResponseCode(String responseCode){
		this.responseCode = responseCode;
	}

	public String getResponseCode(){
		return responseCode;
	}

	public void setCommonArr(CommonArr commonArr){
		this.commonArr = commonArr;
	}

	public CommonArr getCommonArr(){
		return commonArr;
	}

	public void setLongMessage(String longMessage){
		this.longMessage = longMessage;
	}

	public String getLongMessage(){
		return longMessage;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"response_code = '" + responseCode + '\'' + 
			",commonArr = '" + commonArr + '\'' + 
			",long_message = '" + longMessage + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}