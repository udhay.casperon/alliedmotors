package com.app.alliedmotor.model.AppInfo;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("site_icon")
	private String siteIcon;

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("siteurl")
	private String siteurl;

	@SerializedName("posts_per_page")
	private String postsPerPage;

	@SerializedName("unread_notification_count")
	private String unreadNotificationCount;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("mo_customer_validation_otp_validity")
	private String moCustomerValidationOtpValidity;

	@SerializedName("options_contact_phone_number")
	private String optionsContactPhoneNumber;

	@SerializedName("timezone_string")
	private String timezoneString;

	@SerializedName("blogname")
	private String blogname;

	@SerializedName("blogdescription")
	private String blogdescription;

	@SerializedName("spare-parts-stock-list")
	private String sparePartsStockList;

	@SerializedName("mo_customer_validation_otp_length")
	private String moCustomerValidationOtpLength;

	@SerializedName("page_on_front")
	private String pageOnFront;


	@SerializedName("options_whatsapp_2_number")
	private String options_whatsapp_2_number;

	public String getOptions_whatsapp_2_number() {
		return options_whatsapp_2_number;
	}

	public void setOptions_whatsapp_2_number(String options_whatsapp_2_number) {
		this.options_whatsapp_2_number = options_whatsapp_2_number;
	}

	public void setSiteIcon(String siteIcon){
		this.siteIcon = siteIcon;
	}

	public String getSiteIcon(){
		return siteIcon;
	}

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setSiteurl(String siteurl){
		this.siteurl = siteurl;
	}

	public String getSiteurl(){
		return siteurl;
	}

	public void setPostsPerPage(String postsPerPage){
		this.postsPerPage = postsPerPage;
	}

	public String getPostsPerPage(){
		return postsPerPage;
	}

	public void setUnreadNotificationCount(String unreadNotificationCount){
		this.unreadNotificationCount = unreadNotificationCount;
	}

	public String getUnreadNotificationCount(){
		return unreadNotificationCount;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setMoCustomerValidationOtpValidity(String moCustomerValidationOtpValidity){
		this.moCustomerValidationOtpValidity = moCustomerValidationOtpValidity;
	}

	public String getMoCustomerValidationOtpValidity(){
		return moCustomerValidationOtpValidity;
	}

	public void setOptionsContactPhoneNumber(String optionsContactPhoneNumber){
		this.optionsContactPhoneNumber = optionsContactPhoneNumber;
	}

	public String getOptionsContactPhoneNumber(){
		return optionsContactPhoneNumber;
	}

	public void setTimezoneString(String timezoneString){
		this.timezoneString = timezoneString;
	}

	public String getTimezoneString(){
		return timezoneString;
	}

	public void setBlogname(String blogname){
		this.blogname = blogname;
	}

	public String getBlogname(){
		return blogname;
	}

	public void setBlogdescription(String blogdescription){
		this.blogdescription = blogdescription;
	}

	public String getBlogdescription(){
		return blogdescription;
	}

	public void setSparePartsStockList(String sparePartsStockList){
		this.sparePartsStockList = sparePartsStockList;
	}

	public String getSparePartsStockList(){
		return sparePartsStockList;
	}

	public void setMoCustomerValidationOtpLength(String moCustomerValidationOtpLength){
		this.moCustomerValidationOtpLength = moCustomerValidationOtpLength;
	}

	public String getMoCustomerValidationOtpLength(){
		return moCustomerValidationOtpLength;
	}

	public void setPageOnFront(String pageOnFront){
		this.pageOnFront = pageOnFront;
	}

	public String getPageOnFront(){
		return pageOnFront;
	}

	@Override
	public String toString() {
		return "Data{" +
				"siteIcon='" + siteIcon + '\'' +
				", userEmail='" + userEmail + '\'' +
				", siteurl='" + siteurl + '\'' +
				", postsPerPage='" + postsPerPage + '\'' +
				", unreadNotificationCount='" + unreadNotificationCount + '\'' +
				", userName='" + userName + '\'' +
				", moCustomerValidationOtpValidity='" + moCustomerValidationOtpValidity + '\'' +
				", optionsContactPhoneNumber='" + optionsContactPhoneNumber + '\'' +
				", timezoneString='" + timezoneString + '\'' +
				", blogname='" + blogname + '\'' +
				", blogdescription='" + blogdescription + '\'' +
				", sparePartsStockList='" + sparePartsStockList + '\'' +
				", moCustomerValidationOtpLength='" + moCustomerValidationOtpLength + '\'' +
				", pageOnFront='" + pageOnFront + '\'' +
				", options_whatsapp_2_number='" + options_whatsapp_2_number + '\'' +
				'}';
	}
}