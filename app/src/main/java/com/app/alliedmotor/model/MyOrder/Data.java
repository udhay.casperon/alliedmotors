package com.app.alliedmotor.model.MyOrder;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("orders")
	private ArrayList<OrdersItem> orders;

	public void setOrders(ArrayList<OrdersItem> orders){
		this.orders = orders;
	}

	public ArrayList<OrdersItem> getOrders(){
		return orders;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"orders = '" + orders + '\'' + 
			"}";
		}
}