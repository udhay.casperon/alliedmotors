package com.app.alliedmotor.model.CarDetail;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModelOptionListItem implements Serializable {

	public ModelOptionListItem(String moId, String modelOptions) {
		this.moId = moId;
		this.modelOptions = modelOptions;
	}

	@SerializedName("moId")
	private String moId;

	@SerializedName("modelOptions")
	private String modelOptions;

	public void setMoId(String moId){
		this.moId = moId;
	}

	public String getMoId(){
		return moId;
	}

	public void setModelOptions(String modelOptions){
		this.modelOptions = modelOptions;
	}

	public String getModelOptions(){
		return modelOptions;
	}

	@Override
 	public String toString(){
		return 
			"ModelOptionListItem{" + 
			"moId = '" + moId + '\'' + 
			",modelOptions = '" + modelOptions + '\'' + 
			"}";
		}
}