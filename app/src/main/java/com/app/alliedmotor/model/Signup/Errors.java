package com.app.alliedmotor.model.Signup;

import com.google.gson.annotations.SerializedName;

public class Errors{

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("last_name")
	private String last_name;

	@SerializedName("mobile_number")
	private String mobile_number;

	@SerializedName("password")
	private String password;

	@SerializedName("company_role")
	private String company_role;


	@SerializedName("company_name")
	private String company_name;


	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	@Override
 	public String toString(){
		return 
			"Errors{" + 
			"user_email = '" + userEmail + '\'' + 
			",first_name = '" + firstName + '\'' + 
			"}";
		}
}