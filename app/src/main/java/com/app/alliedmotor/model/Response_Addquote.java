package com.app.alliedmotor.model;


import com.google.gson.annotations.SerializedName;

public class Response_Addquote {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("commonArr")
	public CommonArr commonArr;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;

	@SerializedName("errors")
	public Errors errors;


	public class CommonArr{

		@SerializedName("token")
		public String token;
	}
	public class Errors{

		@SerializedName("type")
		public String type;

		@SerializedName("quantity")
		public String quantity;

		@SerializedName("car_id")
		public String car_id;
	}


}