package com.app.alliedmotor.model.ProfileUpdate;

import com.google.gson.annotations.SerializedName;

public class ErrArr{

	/*@SerializedName("user_email")
	private String userEmail;

	@SerializedName("last_name")
	private String last_name;

	@SerializedName("mobile_number")
	private String mobile_number;

	@SerializedName("user_email")
	private String user_email;
*/
	@SerializedName("first_name")
	private String first_name;



	@SerializedName("company_name")
	private String company_name;

	@SerializedName("work_phone_number")
	private String work_phone_number;

	@SerializedName("user_type")
	private String user_type;

	@SerializedName("mobile_number")
	private String mobile_number;

	@SerializedName("user_email")
	private String user_email;

	@SerializedName("last_name")
	private String last_name;

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getWork_phone_number() {
		return work_phone_number;
	}

	public void setWork_phone_number(String work_phone_number) {
		this.work_phone_number = work_phone_number;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	@Override
	public String toString() {
		return "ErrArr{" +
				"first_name='" + first_name + '\'' +
				", company_name='" + company_name + '\'' +
				", work_phone_number='" + work_phone_number + '\'' +
				", user_type='" + user_type + '\'' +
				", mobile_number='" + mobile_number + '\'' +
				", user_email='" + user_email + '\'' +
				", last_name='" + last_name + '\'' +
				'}';
	}
}