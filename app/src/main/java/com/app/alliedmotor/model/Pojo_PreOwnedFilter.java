package com.app.alliedmotor.model;

public class Pojo_PreOwnedFilter {
    public Pojo_PreOwnedFilter(String res_make1, String res_model1, String res_minyear1, String res_maxyear1, String res_mileage1, String res_engine1, String res_transmission1, String res_fueltype1, String res_varient1, String res_door1, String res_extcolor1, String res_intcolor1) {
        this.res_make1 = res_make1;
        this.res_model1 = res_model1;
        this.res_minyear1 = res_minyear1;
        this.res_maxyear1 = res_maxyear1;
        this.res_mileage1 = res_mileage1;
        this.res_engine1 = res_engine1;
        this.res_transmission1 = res_transmission1;
        this.res_fueltype1 = res_fueltype1;
        this.res_varient1 = res_varient1;
        this.res_door1 = res_door1;
        this.res_extcolor1 = res_extcolor1;
        this.res_intcolor1 = res_intcolor1;
    }

    String res_make1;String res_model1;String res_minyear1;String res_maxyear1;String res_mileage1;String res_engine1;String res_transmission1;String res_fueltype1;String res_varient1;String res_door1;String res_extcolor1;String res_intcolor1;

    public String getRes_make1() {
        return res_make1;
    }

    public void setRes_make1(String res_make1) {
        this.res_make1 = res_make1;
    }

    public String getRes_model1() {
        return res_model1;
    }

    public void setRes_model1(String res_model1) {
        this.res_model1 = res_model1;
    }

    public String getRes_minyear1() {
        return res_minyear1;
    }

    public void setRes_minyear1(String res_minyear1) {
        this.res_minyear1 = res_minyear1;
    }

    public String getRes_maxyear1() {
        return res_maxyear1;
    }

    public void setRes_maxyear1(String res_maxyear1) {
        this.res_maxyear1 = res_maxyear1;
    }

    public String getRes_mileage1() {
        return res_mileage1;
    }

    public void setRes_mileage1(String res_mileage1) {
        this.res_mileage1 = res_mileage1;
    }

    public String getRes_engine1() {
        return res_engine1;
    }

    public void setRes_engine1(String res_engine1) {
        this.res_engine1 = res_engine1;
    }

    public String getRes_transmission1() {
        return res_transmission1;
    }

    public void setRes_transmission1(String res_transmission1) {
        this.res_transmission1 = res_transmission1;
    }

    public String getRes_fueltype1() {
        return res_fueltype1;
    }

    public void setRes_fueltype1(String res_fueltype1) {
        this.res_fueltype1 = res_fueltype1;
    }

    public String getRes_varient1() {
        return res_varient1;
    }

    public void setRes_varient1(String res_varient1) {
        this.res_varient1 = res_varient1;
    }

    public String getRes_door1() {
        return res_door1;
    }

    public void setRes_door1(String res_door1) {
        this.res_door1 = res_door1;
    }

    public String getRes_extcolor1() {
        return res_extcolor1;
    }

    public void setRes_extcolor1(String res_extcolor1) {
        this.res_extcolor1 = res_extcolor1;
    }

    public String getRes_intcolor1() {
        return res_intcolor1;
    }

    public void setRes_intcolor1(String res_intcolor1) {
        this.res_intcolor1 = res_intcolor1;
    }


    @Override
    public String toString() {
        return "Pojo_PreOwnedFilter{" +
                "res_make1='" + res_make1 + '\'' +
                ", res_model1='" + res_model1 + '\'' +
                ", res_minyear1='" + res_minyear1 + '\'' +
                ", res_maxyear1='" + res_maxyear1 + '\'' +
                ", res_mileage1='" + res_mileage1 + '\'' +
                ", res_engine1='" + res_engine1 + '\'' +
                ", res_transmission1='" + res_transmission1 + '\'' +
                ", res_fueltype1='" + res_fueltype1 + '\'' +
                ", res_varient1='" + res_varient1 + '\'' +
                ", res_door1='" + res_door1 + '\'' +
                ", res_extcolor1='" + res_extcolor1 + '\'' +
                ", res_intcolor1='" + res_intcolor1 + '\'' +
                '}';
    }
}
