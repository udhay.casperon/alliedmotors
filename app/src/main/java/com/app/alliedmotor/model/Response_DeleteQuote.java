package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

public class Response_DeleteQuote {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("commonArr")
	public CommonArr commonArr;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	public class CommonArr{

		@SerializedName("token")
		public String token;
	}
	public class Errors{

		@SerializedName("car_id")
		public String car_id;
	}

}