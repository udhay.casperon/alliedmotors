package com.app.alliedmotor.model.MyOrderLatest;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class OrderDataItem{

	@SerializedName("orders")
	private ArrayList<OrdersItem> orders;

	@SerializedName("enq_status_ticket")
	private String enqStatusTicket;

	@SerializedName("ticket_id")
	private String ticketId;

	@SerializedName("enq_crtdate")
	private String enqCrtdate;

	@SerializedName("make_mobile_number")
	private String makeMobileNumber;

	@SerializedName("car_category")
	private String carCategory;

	public void setOrders(ArrayList<OrdersItem> orders){
		this.orders = orders;
	}

	public ArrayList<OrdersItem> getOrders(){
		return orders;
	}

	public void setEnqStatusTicket(String enqStatusTicket){
		this.enqStatusTicket = enqStatusTicket;
	}

	public String getEnqStatusTicket(){
		return enqStatusTicket;
	}

	public void setTicketId(String ticketId){
		this.ticketId = ticketId;
	}

	public String getTicketId(){
		return ticketId;
	}

	public void setEnqCrtdate(String enqCrtdate){
		this.enqCrtdate = enqCrtdate;
	}

	public String getEnqCrtdate(){
		return enqCrtdate;
	}

	public void setMakeMobileNumber(String makeMobileNumber){
		this.makeMobileNumber = makeMobileNumber;
	}

	public String getMakeMobileNumber(){
		return makeMobileNumber;
	}

	public void setCarCategory(String carCategory){
		this.carCategory = carCategory;
	}

	public String getCarCategory(){
		return carCategory;
	}

	@Override
 	public String toString(){
		return
			"OrderDataItem{" +
			"orders = '" + orders + '\'' +
			",enq_status_ticket = '" + enqStatusTicket + '\'' +
			",ticket_id = '" + ticketId + '\'' +
			",enq_crtdate = '" + enqCrtdate + '\'' +
			",make_mobile_number = '" + makeMobileNumber + '\'' +
			",car_category = '" + carCategory + '\'' +
			"}";
		}
}