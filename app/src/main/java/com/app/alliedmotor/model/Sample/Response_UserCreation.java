package com.app.alliedmotor.model.Sample;

import com.google.gson.annotations.SerializedName;

public class Response_UserCreation {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("commonArr")
	public CommonArr commonArr;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	public class Data{

		@SerializedName("user_email")
		public String userEmail;

		@SerializedName("device_id")
		public String deviceId;

		@SerializedName("mo_otp_token")
		public String moOtpToken;

		@SerializedName("last_name")
		public String lastName;

		@SerializedName("device_type")
		public String deviceType;

		@SerializedName("otp_type")
		public String otpType;

		@SerializedName("password")
		public String password;

		@SerializedName("user_type")
		public String userType;

		@SerializedName("company_name")
		public String companyName;

		@SerializedName("enable_newsletter")
		public String enableNewsletter;

		@SerializedName("mobile_number")
		public String mobileNumber;

		@SerializedName("company_role")
		public String companyUserRole;

		@SerializedName("first_name")
		public String firstName;
	}

	public class CommonArr{

		@SerializedName("token")
		public String token;
	}


}