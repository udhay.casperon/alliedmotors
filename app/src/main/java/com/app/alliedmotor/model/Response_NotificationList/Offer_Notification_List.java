package com.app.alliedmotor.model.Response_NotificationList;

import com.google.gson.annotations.SerializedName;

public class Offer_Notification_List {


    @SerializedName("post_content")
    private String post_content;


    @SerializedName("post_id")
    private String post_id;

    @SerializedName("short_description")
    private String short_description;

    @SerializedName("post_title")
    private String post_title;

    @SerializedName("created_time")
    private String created_time;

    @SerializedName("ennable_button")
    private String ennable_button;

    @SerializedName("button_text")
    private String button_text;

    @SerializedName("button_url")
    private String button_url;


    @SerializedName("is_unread")
    private String is_unread;


    @SerializedName("_thumbnail_id")
    private String _thumbnail_id;


    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getEnnable_button() {
        return ennable_button;
    }

    public void setEnnable_button(String ennable_button) {
        this.ennable_button = ennable_button;
    }

    public String getButton_text() {
        return button_text;
    }

    public void setButton_text(String button_text) {
        this.button_text = button_text;
    }

    public String getButton_url() {
        return button_url;
    }

    public void setButton_url(String button_url) {
        this.button_url = button_url;
    }

    public String getIs_unread() {
        return is_unread;
    }

    public void setIs_unread(String is_unread) {
        this.is_unread = is_unread;
    }

    public String get_thumbnail_id() {
        return _thumbnail_id;
    }

    public void set_thumbnail_id(String _thumbnail_id) {
        this._thumbnail_id = _thumbnail_id;
    }


    @Override
    public String toString() {
        return "Offer_Notification_List{" +
                "post_id='" + post_id + '\'' +
                ", short_description='" + short_description + '\'' +
                ", post_title='" + post_title + '\'' +
                ", created_time='" + created_time + '\'' +
                ", ennable_button='" + ennable_button + '\'' +
                ", button_text='" + button_text + '\'' +
                ", button_url='" + button_url + '\'' +
                ", is_unread='" + is_unread + '\'' +
                ", _thumbnail_id='" + _thumbnail_id + '\'' +
                '}';
    }
}
