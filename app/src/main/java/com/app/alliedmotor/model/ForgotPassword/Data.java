package com.app.alliedmotor.model.ForgotPassword;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("code")
	private Integer code;

	@SerializedName("dev_mode")
	private Integer devMode;

	public void setCode(Integer code){
		this.code = code;
	}

	public Integer getCode(){
		return code;
	}

	public void setDevMode(Integer devMode){
		this.devMode = devMode;
	}

	public Integer getDevMode(){
		return devMode;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"code = '" + code + '\'' + 
			",dev_mode = '" + devMode + '\'' + 
			"}";
		}
}