package com.app.alliedmotor.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Response_CarModelList {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public ArrayList<DataItem> data;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	public class DataItem{

		@SerializedName("modelName")
		public String modelName;

		@SerializedName("icon")
		public String icon;

		@SerializedName("mkid")
		public String mkid;

		@SerializedName("modid")
		public String modid;
	}
}