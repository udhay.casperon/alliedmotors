package com.app.alliedmotor.model.Pre_OwnedCar;

import com.google.gson.annotations.SerializedName;

public class CarDetailsItem{


	@SerializedName("currency_name")
	private String currency_name;

	public String getCurrency_name() {
		return currency_name;
	}

	public void setCurrency_name(String currency_name) {
		this.currency_name = currency_name;
	}

	@SerializedName("modelYear")
	private String modelYear;

	@SerializedName("vehiceType")
	private String vehiceType;

	@SerializedName("preId")
	private String preId;

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public String getVehiceType() {
		return vehiceType;
	}

	public void setVehiceType(String vehiceType) {
		this.vehiceType = vehiceType;
	}

	@SerializedName("door")
	private String door;

	@SerializedName("secondaryEmail")
	private String secondaryEmail;

	@SerializedName("promo_logo")
	private String promoLogo;

	@SerializedName("mkid")
	private String mkid;

	@SerializedName("preStatus")
	private String preStatus;

	@SerializedName("makename")
	private String makename;

	@SerializedName("modid")
	private String modid;

	@SerializedName("years")
	private String years;

	@SerializedName("cur_name")
	private String curName;

	@SerializedName("modelName")
	private String modelName;

	@SerializedName("transmission")
	private String transmission;

	@SerializedName("fuelType")
	private String fuelType;

	@SerializedName("engine")
	private String engine;

	@SerializedName("price")
	private String price;

	@SerializedName("variant")
	private String variant;

	@SerializedName("main_img")
	private String mainImg;

	@SerializedName("vehicleType")
	private String vehicleType;

	@SerializedName("email")
	private String email;

	@SerializedName("make_mobile_number")
	private String makeMobileNumber;

	@SerializedName("mileage")
	private String mileage;

	@SerializedName("make_icon")
	private String makeIcon;

	public void setPreId(String preId){
		this.preId = preId;
	}

	public String getPreId(){
		return preId;
	}

	public void setDoor(String door){
		this.door = door;
	}

	public String getDoor(){
		return door;
	}

	public void setSecondaryEmail(String secondaryEmail){
		this.secondaryEmail = secondaryEmail;
	}

	public String getSecondaryEmail(){
		return secondaryEmail;
	}

	public void setPromoLogo(String promoLogo){
		this.promoLogo = promoLogo;
	}

	public String getPromoLogo(){
		return promoLogo;
	}

	public void setMkid(String mkid){
		this.mkid = mkid;
	}

	public String getMkid(){
		return mkid;
	}

	public void setPreStatus(String preStatus){
		this.preStatus = preStatus;
	}

	public String getPreStatus(){
		return preStatus;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	public void setModid(String modid){
		this.modid = modid;
	}

	public String getModid(){
		return modid;
	}

	public void setYears(String years){
		this.years = years;
	}

	public String getYears(){
		return years;
	}

	public void setCurName(String curName){
		this.curName = curName;
	}

	public String getCurName(){
		return curName;
	}

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public String getModelName(){
		return modelName;
	}

	public void setTransmission(String transmission){
		this.transmission = transmission;
	}

	public String getTransmission(){
		return transmission;
	}

	public void setFuelType(String fuelType){
		this.fuelType = fuelType;
	}

	public String getFuelType(){
		return fuelType;
	}

	public void setEngine(String engine){
		this.engine = engine;
	}

	public String getEngine(){
		return engine;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setVariant(String variant){
		this.variant = variant;
	}

	public String getVariant(){
		return variant;
	}

	public void setMainImg(String mainImg){
		this.mainImg = mainImg;
	}

	public String getMainImg(){
		return mainImg;
	}

	public void setVehicleType(String vehicleType){
		this.vehicleType = vehicleType;
	}

	public String getVehicleType(){
		return vehicleType;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setMakeMobileNumber(String makeMobileNumber){
		this.makeMobileNumber = makeMobileNumber;
	}

	public String getMakeMobileNumber(){
		return makeMobileNumber;
	}

	public void setMileage(String mileage){
		this.mileage = mileage;
	}

	public String getMileage(){
		return mileage;
	}

	public void setMakeIcon(String makeIcon){
		this.makeIcon = makeIcon;
	}

	public String getMakeIcon(){
		return makeIcon;
	}


	@Override
	public String toString() {
		return "CarDetailsItem{" +
				"preId='" + preId + '\'' +
				", door='" + door + '\'' +
				", secondaryEmail='" + secondaryEmail + '\'' +
				", promoLogo='" + promoLogo + '\'' +
				", mkid='" + mkid + '\'' +
				", preStatus='" + preStatus + '\'' +
				", makename='" + makename + '\'' +
				", modid='" + modid + '\'' +
				", years='" + years + '\'' +
				", curName='" + curName + '\'' +
				", modelName='" + modelName + '\'' +
				", transmission='" + transmission + '\'' +
				", fuelType='" + fuelType + '\'' +
				", engine='" + engine + '\'' +
				", price='" + price + '\'' +
				", variant='" + variant + '\'' +
				", mainImg='" + mainImg + '\'' +
				", vehicleType='" + vehicleType + '\'' +
				", email='" + email + '\'' +
				", makeMobileNumber='" + makeMobileNumber + '\'' +
				", mileage='" + mileage + '\'' +
				", makeIcon='" + makeIcon + '\'' +
				'}';
	}
}