package com.app.alliedmotor.model.CarDetail;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Data{

	@SerializedName("car_details")
	private CarDetails carDetails;

	@SerializedName("car_safety_features")
	private CarSafetyFeatures carSafetyFeatures;

	@SerializedName("car_exterior_features")
	private CarExteriorFeatures carExteriorFeatures;

	@SerializedName("car_gallery_images")
	private ArrayList<String> carGalleryImages;

	@SerializedName("car_interior_features")
	private CarInteriorFeatures carInteriorFeatures;

	@SerializedName("varient_list")
	private ArrayList<VarientListItem> varientList;

	@SerializedName("model_option_list")
	private ArrayList<ModelOptionListItem> modelOptionList;

	@SerializedName("car_other_features")
	private CarOtherFeatures carOtherFeatures;

	@SerializedName("car_gallery_exterior_colors")
	private ArrayList<String> carGalleryExteriorColors;

	@SerializedName("fuel_type_list")
	private ArrayList<FuelTypeListItem> fuelTypeList;

	@SerializedName("car_gallery_interior_colors")
	private ArrayList<String> carGalleryInteriorColors;

	@SerializedName("car_technical_features")
	private CarTechnicalFeatures carTechnicalFeatures;

	public void setCarDetails(CarDetails carDetails){
		this.carDetails = carDetails;
	}

	public CarDetails getCarDetails(){
		return carDetails;
	}

	public void setCarSafetyFeatures(CarSafetyFeatures carSafetyFeatures){
		this.carSafetyFeatures = carSafetyFeatures;
	}

	public CarSafetyFeatures getCarSafetyFeatures(){
		return carSafetyFeatures;
	}

	public void setCarExteriorFeatures(CarExteriorFeatures carExteriorFeatures){
		this.carExteriorFeatures = carExteriorFeatures;
	}

	public CarExteriorFeatures getCarExteriorFeatures(){
		return carExteriorFeatures;
	}

	public void setCarGalleryImages(ArrayList<String> carGalleryImages){
		this.carGalleryImages = carGalleryImages;
	}

	public ArrayList<String> getCarGalleryImages(){
		return carGalleryImages;
	}

	public void setCarInteriorFeatures(CarInteriorFeatures carInteriorFeatures){
		this.carInteriorFeatures = carInteriorFeatures;
	}

	public CarInteriorFeatures getCarInteriorFeatures(){
		return carInteriorFeatures;
	}

	public void setVarientList(ArrayList<VarientListItem> varientList){
		this.varientList = varientList;
	}

	public ArrayList<VarientListItem> getVarientList(){
		return varientList;
	}

	public void setModelOptionList(ArrayList<ModelOptionListItem> modelOptionList){
		this.modelOptionList = modelOptionList;
	}

	public ArrayList<ModelOptionListItem> getModelOptionList(){
		return modelOptionList;
	}

	public void setCarOtherFeatures(CarOtherFeatures carOtherFeatures){
		this.carOtherFeatures = carOtherFeatures;
	}

	public CarOtherFeatures getCarOtherFeatures(){
		return carOtherFeatures;
	}

	public void setCarGalleryExteriorColors(ArrayList<String> carGalleryExteriorColors){
		this.carGalleryExteriorColors = carGalleryExteriorColors;
	}

	public ArrayList<String> getCarGalleryExteriorColors(){
		return carGalleryExteriorColors;
	}

	public void setFuelTypeList(ArrayList<FuelTypeListItem> fuelTypeList){
		this.fuelTypeList = fuelTypeList;
	}

	public ArrayList<FuelTypeListItem> getFuelTypeList(){
		return fuelTypeList;
	}

	public void setCarGalleryInteriorColors(ArrayList<String> carGalleryInteriorColors){
		this.carGalleryInteriorColors = carGalleryInteriorColors;
	}

	public ArrayList<String> getCarGalleryInteriorColors(){
		return carGalleryInteriorColors;
	}

	public void setCarTechnicalFeatures(CarTechnicalFeatures carTechnicalFeatures){
		this.carTechnicalFeatures = carTechnicalFeatures;
	}

	public CarTechnicalFeatures getCarTechnicalFeatures(){
		return carTechnicalFeatures;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"car_details = '" + carDetails + '\'' + 
			",car_safety_features = '" + carSafetyFeatures + '\'' + 
			",car_exterior_features = '" + carExteriorFeatures + '\'' + 
			",car_gallery_images = '" + carGalleryImages + '\'' + 
			",car_interior_features = '" + carInteriorFeatures + '\'' + 
			",varient_list = '" + varientList + '\'' + 
			",model_option_list = '" + modelOptionList + '\'' + 
			",car_other_features = '" + carOtherFeatures + '\'' + 
			",car_gallery_exterior_colors = '" + carGalleryExteriorColors + '\'' + 
			",fuel_type_list = '" + fuelTypeList + '\'' + 
			",car_gallery_interior_colors = '" + carGalleryInteriorColors + '\'' + 
			",car_technical_features = '" + carTechnicalFeatures + '\'' + 
			"}";
		}
}