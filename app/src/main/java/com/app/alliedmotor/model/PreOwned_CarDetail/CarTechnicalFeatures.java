package com.app.alliedmotor.model.PreOwned_CarDetail;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CarTechnicalFeatures{

	@SerializedName("features")
	private ArrayList<String> features;

	public void setFeatures(ArrayList<String> features){
		this.features = features;
	}

	public ArrayList<String> getFeatures(){
		return features;
	}
}