package com.app.alliedmotor.model.Login;

import com.google.gson.annotations.SerializedName;

public class LoginError {
    @SerializedName("device_id")
    private String device_id;
    @SerializedName("device_type")
    private String device_type;


    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }


    @Override
    public String toString() {
        return "LoginError{" +
                "device_id='" + device_id + '\'' +
                ", device_type='" + device_type + '\'' +
                '}';
    }
}
