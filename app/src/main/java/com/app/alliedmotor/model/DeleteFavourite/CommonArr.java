package com.app.alliedmotor.model.DeleteFavourite;

import com.google.gson.annotations.SerializedName;

public class CommonArr{

	@SerializedName("token")
	private String token;

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	@Override
 	public String toString(){
		return 
			"CommonArr{" + 
			"token = '" + token + '\'' + 
			"}";
		}
}