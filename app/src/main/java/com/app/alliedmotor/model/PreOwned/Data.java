package com.app.alliedmotor.model.PreOwned;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("car_details")
	private CarDetails carDetails;

	@SerializedName("car_safety_features")
	private CarSafetyFeatures carSafetyFeatures;

	@SerializedName("car_exterior_features")
	private CarExteriorFeatures carExteriorFeatures;

	@SerializedName("car_gallery_images")
	private List<String> carGalleryImages;

	@SerializedName("car_interior_features")
	private CarInteriorFeatures carInteriorFeatures;

	@SerializedName("car_other_features")
	private CarOtherFeatures carOtherFeatures;

	@SerializedName("car_gallery_exterior_colors")
	private List<String> carGalleryExteriorColors;

	@SerializedName("car_gallery_interior_colors")
	private List<String> carGalleryInteriorColors;

	@SerializedName("car_technical_features")
	private CarTechnicalFeatures carTechnicalFeatures;

	public void setCarDetails(CarDetails carDetails){
		this.carDetails = carDetails;
	}

	public CarDetails getCarDetails(){
		return carDetails;
	}

	public void setCarSafetyFeatures(CarSafetyFeatures carSafetyFeatures){
		this.carSafetyFeatures = carSafetyFeatures;
	}

	public CarSafetyFeatures getCarSafetyFeatures(){
		return carSafetyFeatures;
	}

	public void setCarExteriorFeatures(CarExteriorFeatures carExteriorFeatures){
		this.carExteriorFeatures = carExteriorFeatures;
	}

	public CarExteriorFeatures getCarExteriorFeatures(){
		return carExteriorFeatures;
	}

	public void setCarGalleryImages(List<String> carGalleryImages){
		this.carGalleryImages = carGalleryImages;
	}

	public List<String> getCarGalleryImages(){
		return carGalleryImages;
	}

	public void setCarInteriorFeatures(CarInteriorFeatures carInteriorFeatures){
		this.carInteriorFeatures = carInteriorFeatures;
	}

	public CarInteriorFeatures getCarInteriorFeatures(){
		return carInteriorFeatures;
	}

	public void setCarOtherFeatures(CarOtherFeatures carOtherFeatures){
		this.carOtherFeatures = carOtherFeatures;
	}

	public CarOtherFeatures getCarOtherFeatures(){
		return carOtherFeatures;
	}

	public void setCarGalleryExteriorColors(List<String> carGalleryExteriorColors){
		this.carGalleryExteriorColors = carGalleryExteriorColors;
	}

	public List<String> getCarGalleryExteriorColors(){
		return carGalleryExteriorColors;
	}

	public void setCarGalleryInteriorColors(List<String> carGalleryInteriorColors){
		this.carGalleryInteriorColors = carGalleryInteriorColors;
	}

	public List<String> getCarGalleryInteriorColors(){
		return carGalleryInteriorColors;
	}

	public void setCarTechnicalFeatures(CarTechnicalFeatures carTechnicalFeatures){
		this.carTechnicalFeatures = carTechnicalFeatures;
	}

	public CarTechnicalFeatures getCarTechnicalFeatures(){
		return carTechnicalFeatures;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"car_details = '" + carDetails + '\'' + 
			",car_safety_features = '" + carSafetyFeatures + '\'' + 
			",car_exterior_features = '" + carExteriorFeatures + '\'' + 
			",car_gallery_images = '" + carGalleryImages + '\'' + 
			",car_interior_features = '" + carInteriorFeatures + '\'' + 
			",car_other_features = '" + carOtherFeatures + '\'' + 
			",car_gallery_exterior_colors = '" + carGalleryExteriorColors + '\'' + 
			",car_gallery_interior_colors = '" + carGalleryInteriorColors + '\'' + 
			",car_technical_features = '" + carTechnicalFeatures + '\'' + 
			"}";
		}
}