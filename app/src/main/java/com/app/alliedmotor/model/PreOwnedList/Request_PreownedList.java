package com.app.alliedmotor.model.PreOwnedList;

public class Request_PreownedList {

    private String make_id="";
    private String model_id="";
    private String min_year="";
    private String max_year="";
    private String mileage="";
    private String engine="";
    private String transmission="";
    private String fuel_type="";
    private String varient="";
    private String doors="";
    private String ext_color="";
    private String int_color="";


    public String getMake_id() {
        return make_id;
    }

    public void setMake_id(String make_id) {
        this.make_id = make_id;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getMin_year() {
        return min_year;
    }

    public void setMin_year(String min_year) {
        this.min_year = min_year;
    }

    public String getMax_year() {
        return max_year;
    }

    public void setMax_year(String max_year) {
        this.max_year = max_year;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getFuel_type() {
        return fuel_type;
    }

    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }

    public String getVarient() {
        return varient;
    }

    public void setVarient(String varient) {
        this.varient = varient;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getExt_color() {
        return ext_color;
    }

    public void setExt_color(String ext_color) {
        this.ext_color = ext_color;
    }

    public String getInt_color() {
        return int_color;
    }

    public void setInt_color(String int_color) {
        this.int_color = int_color;
    }

    @Override
    public String toString() {
        return "Request_PreownedList{" +
                "make_id='" + make_id + '\'' +
                ", model_id='" + model_id + '\'' +
                ", min_year='" + min_year + '\'' +
                ", max_year='" + max_year + '\'' +
                ", mileage='" + mileage + '\'' +
                ", engine='" + engine + '\'' +
                ", transmission='" + transmission + '\'' +
                ", fuel_type='" + fuel_type + '\'' +
                ", varient='" + varient + '\'' +
                ", doors='" + doors + '\'' +
                ", ext_color='" + ext_color + '\'' +
                ", int_color='" + int_color + '\'' +
                '}';
    }
}
