package com.app.alliedmotor.model.Login12;

import android.util.Log;

import com.app.alliedmotor.model.FileUpload.Response_FileUpload;
import com.app.alliedmotor.model.RHDCar.Response_RHDCar;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.ServiceInterface;
import com.app.alliedmotor.utility.WebServiceData_sample;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Service_FileUpload {

    MultipartBody.Part img = null;
   String st_transactionimg="";
    public Service_FileUpload( String request,String request_img, final ResponseInterface.FileUploadInterface fileUploadInterface) {

        final WebServiceData_sample data = new WebServiceData_sample();
        ServiceInterface serviceInterface = data.retrofit().create(ServiceInterface.class);

        RequestBody prefixname=RequestBody.create(MultipartBody.FORM,request);
        st_transactionimg=request_img;

        if (st_transactionimg != null) {
            File fileToUpload = new File(st_transactionimg);
            if (fileToUpload.exists()) {
                Log.d("exist", "exist");
            } else {
                Log.d("not exist", "not exist");
            }
            File file = new File(st_transactionimg);
            RequestBody requestfile = RequestBody.create(MediaType.parse("multipart/form-data"), fileToUpload);
            img = MultipartBody.Part.createFormData("image", fileToUpload.getName(), requestfile);
        }

        serviceInterface.ServiceCall_fileUpload1( prefixname,img).enqueue(new Callback<Response_FileUpload>() {
            @Override
            public void onResponse(Call<Response_FileUpload> call, Response<Response_FileUpload> response) {
                try {
                    System.out.println(";;;--link---" + response);
                    if (response.body() != null) {
                        System.out.println("22222222--Trial list-" + response.body().getStatus());
                        fileUploadInterface.onFileUploadSuccess(response.body());

                    }
                } catch (Exception e) {
                    System.out.println("e-----Trial list--1----" + e);
                }
            }

            @Override
            public void onFailure(Call<Response_FileUpload> call, Throwable t) {
                Commons.hideProgress();
                // Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show();
                System.out.println("111111111111--Trial list-- -onfailure----");
            }
        });


    }


}
