package com.app.alliedmotor.model.PreOwned;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CarOtherFeatures{

	@SerializedName("features")
	private List<String> features;

	public void setFeatures(List<String> features){
		this.features = features;
	}

	public List<String> getFeatures(){
		return features;
	}

	@Override
 	public String toString(){
		return 
			"CarOtherFeatures{" + 
			"features = '" + features + '\'' + 
			"}";
		}
}