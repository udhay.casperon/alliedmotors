package com.app.alliedmotor.model.PreOwned;

import com.google.gson.annotations.SerializedName;

public class CarDetails{

	@SerializedName("door")
	private String door;

	@SerializedName("referrence")
	private String referrence;

	@SerializedName("secondaryEmail")
	private String secondaryEmail;

	@SerializedName("is_favorite")
	private String isFavorite;

	@SerializedName("imgId")
	private String imgId;

	@SerializedName("vehiceType")
	private String vehiceType;

	@SerializedName("makename")
	private String makename;

	@SerializedName("modId")
	private String modId;

	@SerializedName("modelOptions")
	private String modelOptions;

	@SerializedName("catid")
	private String catid;

	@SerializedName("interiorColor")
	private String interiorColor;

	@SerializedName("transmission")
	private String transmission;

	@SerializedName("makeFolder")
	private String makeFolder;

	@SerializedName("engine")
	private String engine;

	@SerializedName("price")
	private String price;

	@SerializedName("car_image")
	private String carImage;

	@SerializedName("variant")
	private String variant;

	@SerializedName("warranty")
	private String warranty;

	@SerializedName("email")
	private String email;

	@SerializedName("make_icon")
	private String makeIcon;

	@SerializedName("mileage")
	private String mileage;

	@SerializedName("serviceHistory")
	private String serviceHistory;

	@SerializedName("exteriorColor")
	private String exteriorColor;

	@SerializedName("locationName")
	private String locationName;

	@SerializedName("currency_name")
	private String currencyName;

	@SerializedName("efualType")
	private String efualType;

	@SerializedName("mkId")
	private String mkId;

	@SerializedName("modelYear")
	private String modelYear;

	@SerializedName("accidentHistory")
	private String accidentHistory;

	@SerializedName("modelName")
	private String modelName;

	@SerializedName("Details")
	private String details;

	@SerializedName("mainId")
	private String mainId;

	@SerializedName("make_mobile_number")
	private String makeMobileNumber;

	public void setDoor(String door){
		this.door = door;
	}

	public String getDoor(){
		return door;
	}

	public void setReferrence(String referrence){
		this.referrence = referrence;
	}

	public String getReferrence(){
		return referrence;
	}

	public void setSecondaryEmail(String secondaryEmail){
		this.secondaryEmail = secondaryEmail;
	}

	public String getSecondaryEmail(){
		return secondaryEmail;
	}

	public void setIsFavorite(String isFavorite){
		this.isFavorite = isFavorite;
	}

	public String getIsFavorite(){
		return isFavorite;
	}

	public void setImgId(String imgId){
		this.imgId = imgId;
	}

	public String getImgId(){
		return imgId;
	}

	public void setVehiceType(String vehiceType){
		this.vehiceType = vehiceType;
	}

	public String getVehiceType(){
		return vehiceType;
	}

	public void setMakename(String makename){
		this.makename = makename;
	}

	public String getMakename(){
		return makename;
	}

	public void setModId(String modId){
		this.modId = modId;
	}

	public String getModId(){
		return modId;
	}

	public void setModelOptions(String modelOptions){
		this.modelOptions = modelOptions;
	}

	public String getModelOptions(){
		return modelOptions;
	}

	public void setCatid(String catid){
		this.catid = catid;
	}

	public String getCatid(){
		return catid;
	}

	public void setInteriorColor(String interiorColor){
		this.interiorColor = interiorColor;
	}

	public String getInteriorColor(){
		return interiorColor;
	}

	public void setTransmission(String transmission){
		this.transmission = transmission;
	}

	public String getTransmission(){
		return transmission;
	}

	public void setMakeFolder(String makeFolder){
		this.makeFolder = makeFolder;
	}

	public String getMakeFolder(){
		return makeFolder;
	}

	public void setEngine(String engine){
		this.engine = engine;
	}

	public String getEngine(){
		return engine;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setCarImage(String carImage){
		this.carImage = carImage;
	}

	public String getCarImage(){
		return carImage;
	}

	public void setVariant(String variant){
		this.variant = variant;
	}

	public String getVariant(){
		return variant;
	}

	public void setWarranty(String warranty){
		this.warranty = warranty;
	}

	public String getWarranty(){
		return warranty;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setMakeIcon(String makeIcon){
		this.makeIcon = makeIcon;
	}

	public String getMakeIcon(){
		return makeIcon;
	}

	public void setMileage(String mileage){
		this.mileage = mileage;
	}

	public String getMileage(){
		return mileage;
	}

	public void setServiceHistory(String serviceHistory){
		this.serviceHistory = serviceHistory;
	}

	public String getServiceHistory(){
		return serviceHistory;
	}

	public void setExteriorColor(String exteriorColor){
		this.exteriorColor = exteriorColor;
	}

	public String getExteriorColor(){
		return exteriorColor;
	}

	public void setLocationName(String locationName){
		this.locationName = locationName;
	}

	public String getLocationName(){
		return locationName;
	}

	public void setCurrencyName(String currencyName){
		this.currencyName = currencyName;
	}

	public String getCurrencyName(){
		return currencyName;
	}

	public void setEfualType(String efualType){
		this.efualType = efualType;
	}

	public String getEfualType(){
		return efualType;
	}

	public void setMkId(String mkId){
		this.mkId = mkId;
	}

	public String getMkId(){
		return mkId;
	}

	public void setModelYear(String modelYear){
		this.modelYear = modelYear;
	}

	public String getModelYear(){
		return modelYear;
	}

	public void setAccidentHistory(String accidentHistory){
		this.accidentHistory = accidentHistory;
	}

	public String getAccidentHistory(){
		return accidentHistory;
	}

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public String getModelName(){
		return modelName;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setMainId(String mainId){
		this.mainId = mainId;
	}

	public String getMainId(){
		return mainId;
	}

	public void setMakeMobileNumber(String makeMobileNumber){
		this.makeMobileNumber = makeMobileNumber;
	}

	public String getMakeMobileNumber(){
		return makeMobileNumber;
	}

	@Override
 	public String toString(){
		return 
			"CarDetails{" + 
			"door = '" + door + '\'' + 
			",referrence = '" + referrence + '\'' + 
			",secondaryEmail = '" + secondaryEmail + '\'' + 
			",is_favorite = '" + isFavorite + '\'' + 
			",imgId = '" + imgId + '\'' + 
			",vehiceType = '" + vehiceType + '\'' + 
			",makename = '" + makename + '\'' + 
			",modId = '" + modId + '\'' + 
			",modelOptions = '" + modelOptions + '\'' + 
			",catid = '" + catid + '\'' + 
			",interiorColor = '" + interiorColor + '\'' + 
			",transmission = '" + transmission + '\'' + 
			",makeFolder = '" + makeFolder + '\'' + 
			",engine = '" + engine + '\'' + 
			",price = '" + price + '\'' + 
			",car_image = '" + carImage + '\'' + 
			",variant = '" + variant + '\'' + 
			",warranty = '" + warranty + '\'' + 
			",email = '" + email + '\'' + 
			",make_icon = '" + makeIcon + '\'' + 
			",mileage = '" + mileage + '\'' + 
			",serviceHistory = '" + serviceHistory + '\'' + 
			",exteriorColor = '" + exteriorColor + '\'' + 
			",locationName = '" + locationName + '\'' + 
			",currency_name = '" + currencyName + '\'' + 
			",efualType = '" + efualType + '\'' + 
			",mkId = '" + mkId + '\'' + 
			",modelYear = '" + modelYear + '\'' + 
			",accidentHistory = '" + accidentHistory + '\'' + 
			",modelName = '" + modelName + '\'' + 
			",details = '" + details + '\'' + 
			",mainId = '" + mainId + '\'' + 
			",make_mobile_number = '" + makeMobileNumber + '\'' + 
			"}";
		}
}