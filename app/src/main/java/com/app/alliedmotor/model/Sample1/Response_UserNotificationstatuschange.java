package com.app.alliedmotor.model.Sample1;

import com.google.gson.annotations.SerializedName;

public class Response_UserNotificationstatuschange {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("commonArr")
	public CommonArr commonArr;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	public class CommonArr{

		@SerializedName("token")
		public String token;
	}
	

}