package com.app.alliedmotor.model.CarDetail;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FuelTypeListItem implements Serializable {

	@SerializedName("eftId")
	private String eftId;

	@SerializedName("efuelType")
	private String efuelType;

	public void setEftId(String eftId){
		this.eftId = eftId;
	}

	public String getEftId(){
		return eftId;
	}

	public void setEfuelType(String efuelType){
		this.efuelType = efuelType;
	}

	public String getEfuelType(){
		return efuelType;
	}

	@Override
 	public String toString(){
		return 
			"FuelTypeListItem{" + 
			"eftId = '" + eftId + '\'' + 
			",efuelType = '" + efuelType + '\'' + 
			"}";
		}
}