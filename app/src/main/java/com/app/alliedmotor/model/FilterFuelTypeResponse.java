package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FilterFuelTypeResponse {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;

	@SerializedName("errors")
	public Errors errors;

	public class Data{

		@SerializedName("model_option_list")
		public ArrayList<ModelOptionListItem> modelOptionList;


		public class ModelOptionListItem{

			@SerializedName("moId")
			public String moId;

			@SerializedName("modelOptions")
			public String modelOptions;


			public String getMoId() {
				return moId;
			}

			public void setMoId(String moId) {
				this.moId = moId;
			}

			public String getModelOptions() {
				return modelOptions;
			}

			public void setModelOptions(String modelOptions) {
				this.modelOptions = modelOptions;
			}
		}
	}

	public class Errors{

		@SerializedName("eftId")
		public String eftId;

		@SerializedName("modId")
		public String modId;

		@SerializedName("makeId")
		public String makeId;

	
	}

}