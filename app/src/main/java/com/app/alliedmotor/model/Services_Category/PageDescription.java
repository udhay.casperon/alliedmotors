package com.app.alliedmotor.model.Services_Category;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class PageDescription{

	@SerializedName("sales_image")
	private String salesImage;

	@SerializedName("logistics_services_short_description")
	private String logisticsServicesShortDescription;

	@SerializedName("sales_long_description")
	private String salesLongDescription;

	@SerializedName("logistics_services_image")
	private String logisticsServicesImage;

	@SerializedName("additiona_fitments_long_description")
	private ArrayList<AdditionaFitmentsLongDescriptionItem> additionaFitmentsLongDescription;

	@SerializedName("logistics_services_long_description")
	private String logisticsServicesLongDescription;

	@SerializedName("sales_short_description")
	private String salesShortDescription;

	@SerializedName("additiona_fitments_short_description")
	private String additionaFitmentsShortDescription;

	public void setSalesImage(String salesImage){
		this.salesImage = salesImage;
	}

	public String getSalesImage(){
		return salesImage;
	}

	public void setLogisticsServicesShortDescription(String logisticsServicesShortDescription){
		this.logisticsServicesShortDescription = logisticsServicesShortDescription;
	}

	public String getLogisticsServicesShortDescription(){
		return logisticsServicesShortDescription;
	}

	public void setSalesLongDescription(String salesLongDescription){
		this.salesLongDescription = salesLongDescription;
	}

	public String getSalesLongDescription(){
		return salesLongDescription;
	}

	public void setLogisticsServicesImage(String logisticsServicesImage){
		this.logisticsServicesImage = logisticsServicesImage;
	}

	public String getLogisticsServicesImage(){
		return logisticsServicesImage;
	}

	public void setAdditionaFitmentsLongDescription(ArrayList<AdditionaFitmentsLongDescriptionItem> additionaFitmentsLongDescription){
		this.additionaFitmentsLongDescription = additionaFitmentsLongDescription;
	}

	public ArrayList<AdditionaFitmentsLongDescriptionItem> getAdditionaFitmentsLongDescription(){
		return additionaFitmentsLongDescription;
	}

	public void setLogisticsServicesLongDescription(String logisticsServicesLongDescription){
		this.logisticsServicesLongDescription = logisticsServicesLongDescription;
	}

	public String getLogisticsServicesLongDescription(){
		return logisticsServicesLongDescription;
	}

	public void setSalesShortDescription(String salesShortDescription){
		this.salesShortDescription = salesShortDescription;
	}

	public String getSalesShortDescription(){
		return salesShortDescription;
	}

	public void setAdditionaFitmentsShortDescription(String additionaFitmentsShortDescription){
		this.additionaFitmentsShortDescription = additionaFitmentsShortDescription;
	}

	public String getAdditionaFitmentsShortDescription(){
		return additionaFitmentsShortDescription;
	}

	@Override
 	public String toString(){
		return 
			"PageDescription{" + 
			"sales_image = '" + salesImage + '\'' + 
			",logistics_services_short_description = '" + logisticsServicesShortDescription + '\'' + 
			",sales_long_description = '" + salesLongDescription + '\'' + 
			",logistics_services_image = '" + logisticsServicesImage + '\'' + 
			",additiona_fitments_long_description = '" + additionaFitmentsLongDescription + '\'' + 
			",logistics_services_long_description = '" + logisticsServicesLongDescription + '\'' + 
			",sales_short_description = '" + salesShortDescription + '\'' + 
			",additiona_fitments_short_description = '" + additionaFitmentsShortDescription + '\'' + 
			"}";
		}
}