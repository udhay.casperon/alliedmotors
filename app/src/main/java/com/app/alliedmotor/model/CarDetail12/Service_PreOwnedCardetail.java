package com.app.alliedmotor.model.CarDetail12;

import com.app.alliedmotor.model.CarDetail.Response_CarDetail;
import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.ServiceInterface;
import com.app.alliedmotor.utility.WebServiceData_sample;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Service_PreOwnedCardetail {

    public Service_PreOwnedCardetail(String url, HashMap<String,String>request, final ResponseInterface.PReOwnedCarListingInterface pReOwnedCarListingInterface) {

        final WebServiceData_sample data = new WebServiceData_sample();
        ServiceInterface serviceInterface = data.retrofit().create(ServiceInterface.class);
        serviceInterface.ResponsePreOwnedCar_Call(url,request).enqueue(new Callback<Response_PreOwned>() {
            @Override
            public void onResponse(Call<Response_PreOwned> call, Response<Response_PreOwned> response) {
                try {
                    System.out.println(";;;--link---" + response);
                    if (response.body() != null) {
                        System.out.println("22222222--Tpreownd list-" + response.body().getStatus());
                        pReOwnedCarListingInterface.onPreownedSuccess(response.body());

                    }
                } catch (Exception e) {
                    System.out.println("e----Tpreownd --1----" + e);
                }
            }

            @Override
            public void onFailure(Call<Response_PreOwned> call, Throwable t) {
                Commons.hideProgress();
                // Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show();
                System.out.println("11111111-Tpreownd -onfailure----");
            }
        });


    }


}
