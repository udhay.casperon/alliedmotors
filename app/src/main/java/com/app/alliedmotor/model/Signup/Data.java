package com.app.alliedmotor.model.Signup;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("user_email")
	private String userEmail;

	@SerializedName("password")
	private String password;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("otp_data")
	private OtpData otpData;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("enable_newsletter")
	private String enableNewsletter;

	@SerializedName("otp_type")
	private String otpType;

	@SerializedName("mobile_number")
	private String mobileNumber;

	@SerializedName("company_role")
	private String companyUserRole;

	@SerializedName("first_name")
	private String firstName;

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setOtpData(OtpData otpData){
		this.otpData = otpData;
	}

	public OtpData getOtpData(){
		return otpData;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setEnableNewsletter(String enableNewsletter){
		this.enableNewsletter = enableNewsletter;
	}

	public String getEnableNewsletter(){
		return enableNewsletter;
	}

	public void setOtpType(String otpType){
		this.otpType = otpType;
	}

	public String getOtpType(){
		return otpType;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setCompanyUserRole(String companyUserRole){
		this.companyUserRole = companyUserRole;
	}

	public String getCompanyUserRole(){
		return companyUserRole;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}
}