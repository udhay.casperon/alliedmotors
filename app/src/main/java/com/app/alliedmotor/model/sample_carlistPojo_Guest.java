package com.app.alliedmotor.model;

public class sample_carlistPojo_Guest {

    private String car_cat;
    private String car_info;
    private String quantity;
    private String car_url;
    private String mkid;
    private String car_id;
    private String modid;
    private String interior_color;
    private String exterior_color;

    public sample_carlistPojo_Guest(String car_cat, String car_info, String quantity, String car_url, String mkid, String car_id, String modid, String interior_color, String exterior_color) {

        this.car_cat = car_cat;
        this.car_info = car_info;
        this.quantity = quantity;
        this.car_url = car_url;
        this.mkid = mkid;
        this.car_id = car_id;
        this.modid = modid;
        this.interior_color = interior_color;
        this.exterior_color = exterior_color;
    }

    public String getCar_cat() {
        return car_cat;
    }

    public void setCar_cat(String car_cat) {
        this.car_cat = car_cat;
    }

    public String getCar_info() {
        return car_info;
    }

    public void setCar_info(String car_info) {
        this.car_info = car_info;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCar_url() {
        return car_url;
    }

    public void setCar_url(String car_url) {
        this.car_url = car_url;
    }

    public String getMkid() {
        return mkid;
    }

    public void setMkid(String mkid) {
        this.mkid = mkid;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getModid() {
        return modid;
    }

    public void setModid(String modid) {
        this.modid = modid;
    }

    public String getInterior_color() {
        return interior_color;
    }

    public void setInterior_color(String interior_color) {
        this.interior_color = interior_color;
    }

    public String getExterior_color() {
        return exterior_color;
    }

    public void setExterior_color(String exterior_color) {
        this.exterior_color = exterior_color;
    }

    @Override
    public String toString() {
        return "{" +
                "car_cat='" + car_cat + '\'' +
                ", car_info='" + car_info + '\'' +
                ", quantity='" + quantity + '\'' +
                ", car_url='" + car_url + '\'' +
                ", mkid='" + mkid + '\'' +
                ", car_id='" + car_id + '\'' +
                ", modid='" + modid + '\'' +
                ", interior_color='" + interior_color + '\'' +
                ", exterior_color='" + exterior_color + '\'' +
                '}';
    }
}