package com.app.alliedmotor.model.Privacy_Policy;

import java.util.ArrayList;

public class PrivacyPolicy_Response {
    private String status;
    private String response_code;
    private String message;
    private ArrayList<Privacy_Data> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Privacy_Data> getData() {
        return data;
    }

    public void setData(ArrayList<Privacy_Data> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PrivacyPolicy_Response{" +
                "status='" + status + '\'' +
                ", response_code='" + response_code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
