package com.app.alliedmotor.model.NewCar;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("category_list")
	private ArrayList<CategoryListItem> categoryList;

	@SerializedName("make_list")
	private ArrayList<MakeListItem> makeList;

	@SerializedName("car_list")
	private ArrayList<CarListItem> carList;

	@SerializedName("vehicle_type_list")
	private ArrayList<String> vehicleTypeList;


	public ArrayList<String> getVehicleTypeList() {
		return vehicleTypeList;
	}

	public void setVehicleTypeList(ArrayList<String> vehicleTypeList) {
		this.vehicleTypeList = vehicleTypeList;
	}

	public void setCategoryList(ArrayList<CategoryListItem> categoryList){
		this.categoryList = categoryList;
	}

	public ArrayList<CategoryListItem> getCategoryList(){
		return categoryList;
	}

	public void setMakeList(ArrayList<MakeListItem> makeList){
		this.makeList = makeList;
	}

	public ArrayList<MakeListItem> getMakeList(){
		return makeList;
	}

	public void setCarList(ArrayList<CarListItem> carList){
		this.carList = carList;
	}

	public ArrayList<CarListItem> getCarList(){
		return carList;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"category_list = '" + categoryList + '\'' + 
			",make_list = '" + makeList + '\'' + 
			",car_list = '" + carList + '\'' + 
			"}";
		}
}