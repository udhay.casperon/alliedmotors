package com.app.alliedmotor.model.NewCar_details;

import com.google.gson.annotations.SerializedName;

public class CarTechnicalFeatures{

	@SerializedName("motor")
	private String motor;

	@SerializedName("hybrid System Net Output")
	private String hybridSystemNetOutput;

	@SerializedName("tyres Size")
	private String tyresSize;

	@SerializedName("torque")
	private String torque;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("tech Id")
	private String techId;

	@SerializedName("spare Wheel Carrier")
	private String spareWheelCarrier;

	@SerializedName("cargo Space")
	private String cargoSpace;

	@SerializedName("output")
	private String output;

	@SerializedName("electric Motor")
	private String electricMotor;

	@SerializedName("transmission")
	private String transmission;

	@SerializedName("kerb Weight")
	private String kerbWeight;

	@SerializedName("power Output")
	private String powerOutput;

	@SerializedName("engine")
	private String engine;

	@SerializedName("payload")
	private String payload;

	@SerializedName("seating Capacity")
	private String seatingCapacity;

	@SerializedName("max Torque")
	private String maxTorque;

	@SerializedName("Output Hp Combined")
	private String outputHpCombined;

	@SerializedName("emission Control")
	private String emissionControl;

	@SerializedName("Output Hp")
	private String outputHp;

	@SerializedName("rear Suspension")
	private String rearSuspension;

	@SerializedName("terrain")
	private String terrain;

	@SerializedName("front Suspension")
	private String frontSuspension;

	@SerializedName("ground Clearance")
	private String groundClearance;

	@SerializedName("torque Motor")
	private String torqueMotor;

	@SerializedName("torque Nm")
	private String torqueNm;

	@SerializedName("wheel Type")
	private String wheelType;

	@SerializedName("fuel Economy")
	private String fuelEconomy;

	@SerializedName("spec Type")
	private String specType;

	@SerializedName("doors")
	private String doors;

	@SerializedName("torque Engine")
	private String torqueEngine;

	@SerializedName("wheelbase")
	private String wheelbase;

	@SerializedName("Max Output System")
	private String maxOutputSystem;

	@SerializedName("fuel Tank Capacity")
	private String fuelTankCapacity;

	@SerializedName("fuel Type")
	private String fuelType;

	@SerializedName("max Power")
	private String maxPower;

	@SerializedName("dimensions")
	private String dimensions;

	@SerializedName("gross Weight")
	private String grossWeight;

	public void setMotor(String motor){
		this.motor = motor;
	}

	public String getMotor(){
		return motor;
	}

	public void setHybridSystemNetOutput(String hybridSystemNetOutput){
		this.hybridSystemNetOutput = hybridSystemNetOutput;
	}

	public String getHybridSystemNetOutput(){
		return hybridSystemNetOutput;
	}

	public void setTyresSize(String tyresSize){
		this.tyresSize = tyresSize;
	}

	public String getTyresSize(){
		return tyresSize;
	}

	public void setTorque(String torque){
		this.torque = torque;
	}

	public String getTorque(){
		return torque;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setTechId(String techId){
		this.techId = techId;
	}

	public String getTechId(){
		return techId;
	}

	public void setSpareWheelCarrier(String spareWheelCarrier){
		this.spareWheelCarrier = spareWheelCarrier;
	}

	public String getSpareWheelCarrier(){
		return spareWheelCarrier;
	}

	public void setCargoSpace(String cargoSpace){
		this.cargoSpace = cargoSpace;
	}

	public String getCargoSpace(){
		return cargoSpace;
	}

	public void setOutput(String output){
		this.output = output;
	}

	public String getOutput(){
		return output;
	}

	public void setElectricMotor(String electricMotor){
		this.electricMotor = electricMotor;
	}

	public String getElectricMotor(){
		return electricMotor;
	}

	public void setTransmission(String transmission){
		this.transmission = transmission;
	}

	public String getTransmission(){
		return transmission;
	}

	public void setKerbWeight(String kerbWeight){
		this.kerbWeight = kerbWeight;
	}

	public String getKerbWeight(){
		return kerbWeight;
	}

	public void setPowerOutput(String powerOutput){
		this.powerOutput = powerOutput;
	}

	public String getPowerOutput(){
		return powerOutput;
	}

	public void setEngine(String engine){
		this.engine = engine;
	}

	public String getEngine(){
		return engine;
	}

	public void setPayload(String payload){
		this.payload = payload;
	}

	public String getPayload(){
		return payload;
	}

	public void setSeatingCapacity(String seatingCapacity){
		this.seatingCapacity = seatingCapacity;
	}

	public String getSeatingCapacity(){
		return seatingCapacity;
	}

	public void setMaxTorque(String maxTorque){
		this.maxTorque = maxTorque;
	}

	public String getMaxTorque(){
		return maxTorque;
	}

	public void setOutputHpCombined(String outputHpCombined){
		this.outputHpCombined = outputHpCombined;
	}

	public String getOutputHpCombined(){
		return outputHpCombined;
	}

	public void setEmissionControl(String emissionControl){
		this.emissionControl = emissionControl;
	}

	public String getEmissionControl(){
		return emissionControl;
	}

	public void setOutputHp(String outputHp){
		this.outputHp = outputHp;
	}

	public String getOutputHp(){
		return outputHp;
	}

	public void setRearSuspension(String rearSuspension){
		this.rearSuspension = rearSuspension;
	}

	public String getRearSuspension(){
		return rearSuspension;
	}

	public void setTerrain(String terrain){
		this.terrain = terrain;
	}

	public String getTerrain(){
		return terrain;
	}

	public void setFrontSuspension(String frontSuspension){
		this.frontSuspension = frontSuspension;
	}

	public String getFrontSuspension(){
		return frontSuspension;
	}

	public void setGroundClearance(String groundClearance){
		this.groundClearance = groundClearance;
	}

	public String getGroundClearance(){
		return groundClearance;
	}

	public void setTorqueMotor(String torqueMotor){
		this.torqueMotor = torqueMotor;
	}

	public String getTorqueMotor(){
		return torqueMotor;
	}

	public void setTorqueNm(String torqueNm){
		this.torqueNm = torqueNm;
	}

	public String getTorqueNm(){
		return torqueNm;
	}

	public void setWheelType(String wheelType){
		this.wheelType = wheelType;
	}

	public String getWheelType(){
		return wheelType;
	}

	public void setFuelEconomy(String fuelEconomy){
		this.fuelEconomy = fuelEconomy;
	}

	public String getFuelEconomy(){
		return fuelEconomy;
	}

	public void setSpecType(String specType){
		this.specType = specType;
	}

	public String getSpecType(){
		return specType;
	}

	public void setDoors(String doors){
		this.doors = doors;
	}

	public String getDoors(){
		return doors;
	}

	public void setTorqueEngine(String torqueEngine){
		this.torqueEngine = torqueEngine;
	}

	public String getTorqueEngine(){
		return torqueEngine;
	}

	public void setWheelbase(String wheelbase){
		this.wheelbase = wheelbase;
	}

	public String getWheelbase(){
		return wheelbase;
	}

	public void setMaxOutputSystem(String maxOutputSystem){
		this.maxOutputSystem = maxOutputSystem;
	}

	public String getMaxOutputSystem(){
		return maxOutputSystem;
	}

	public void setFuelTankCapacity(String fuelTankCapacity){
		this.fuelTankCapacity = fuelTankCapacity;
	}

	public String getFuelTankCapacity(){
		return fuelTankCapacity;
	}

	public void setFuelType(String fuelType){
		this.fuelType = fuelType;
	}

	public String getFuelType(){
		return fuelType;
	}

	public void setMaxPower(String maxPower){
		this.maxPower = maxPower;
	}

	public String getMaxPower(){
		return maxPower;
	}

	public void setDimensions(String dimensions){
		this.dimensions = dimensions;
	}

	public String getDimensions(){
		return dimensions;
	}

	public void setGrossWeight(String grossWeight){
		this.grossWeight = grossWeight;
	}

	public String getGrossWeight(){
		return grossWeight;
	}

	@Override
 	public String toString(){
		return 
			"CarTechnicalFeatures{" + 
			"motor = '" + motor + '\'' + 
			",hybrid System Net Output = '" + hybridSystemNetOutput + '\'' + 
			",tyres Size = '" + tyresSize + '\'' + 
			",torque = '" + torque + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",tech Id = '" + techId + '\'' + 
			",spare Wheel Carrier = '" + spareWheelCarrier + '\'' + 
			",cargo Space = '" + cargoSpace + '\'' + 
			",output = '" + output + '\'' + 
			",electric Motor = '" + electricMotor + '\'' + 
			",transmission = '" + transmission + '\'' + 
			",kerb Weight = '" + kerbWeight + '\'' + 
			",power Output = '" + powerOutput + '\'' + 
			",engine = '" + engine + '\'' + 
			",payload = '" + payload + '\'' + 
			",seating Capacity = '" + seatingCapacity + '\'' + 
			",max Torque = '" + maxTorque + '\'' + 
			",output Hp Combined = '" + outputHpCombined + '\'' + 
			",emission Control = '" + emissionControl + '\'' + 
			",output Hp = '" + outputHp + '\'' + 
			",rear Suspension = '" + rearSuspension + '\'' + 
			",terrain = '" + terrain + '\'' + 
			",front Suspension = '" + frontSuspension + '\'' + 
			",ground Clearance = '" + groundClearance + '\'' + 
			",torque Motor = '" + torqueMotor + '\'' + 
			",torque Nm = '" + torqueNm + '\'' + 
			",wheel Type = '" + wheelType + '\'' + 
			",fuel Economy = '" + fuelEconomy + '\'' + 
			",spec Type = '" + specType + '\'' + 
			",doors = '" + doors + '\'' + 
			",torque Engine = '" + torqueEngine + '\'' + 
			",wheelbase = '" + wheelbase + '\'' + 
			",max Output System = '" + maxOutputSystem + '\'' + 
			",fuel Tank Capacity = '" + fuelTankCapacity + '\'' + 
			",fuel Type = '" + fuelType + '\'' + 
			",max Power = '" + maxPower + '\'' + 
			",dimensions = '" + dimensions + '\'' + 
			",gross Weight = '" + grossWeight + '\'' + 
			"}";
		}
}