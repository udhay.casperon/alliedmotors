package com.app.alliedmotor.model.RHDCar;

import com.google.gson.annotations.SerializedName;

public class CategoryListItem{

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("post_name")
	private String postName;

	public void setPostTitle(String postTitle){
		this.postTitle = postTitle;
	}

	public String getPostTitle(){
		return postTitle;
	}

	public void setPostName(String postName){
		this.postName = postName;
	}

	public String getPostName(){
		return postName;
	}

	@Override
 	public String toString(){
		return 
			"CategoryListItem{" + 
			"post_title = '" + postTitle + '\'' + 
			",post_name = '" + postName + '\'' + 
			"}";
		}
}