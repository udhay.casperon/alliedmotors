package com.app.alliedmotor.model.HomePage;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("brands")
	private ArrayList<BrandsItem> brands;

	@SerializedName("categories")
	private ArrayList<CategoriesItem> categories;

	@SerializedName("banner_details")
	private ArrayList<BannerDetailsItem> bannerDetails;

	@SerializedName("festival_offers")
	private ArrayList<FestivalOffersItem> festivalOffers;

	public void setBrands(ArrayList<BrandsItem> brands){
		this.brands = brands;
	}

	public ArrayList<BrandsItem> getBrands(){
		return brands;
	}

	public void setCategories(ArrayList<CategoriesItem> categories){
		this.categories = categories;
	}

	public ArrayList<CategoriesItem> getCategories(){
		return categories;
	}

	public void setBannerDetails(ArrayList<BannerDetailsItem> bannerDetails){
		this.bannerDetails = bannerDetails;
	}

	public ArrayList<BannerDetailsItem> getBannerDetails(){
		return bannerDetails;
	}

	public void setFestivalOffers(ArrayList<FestivalOffersItem> festivalOffers){
		this.festivalOffers = festivalOffers;
	}

	public ArrayList<FestivalOffersItem> getFestivalOffers(){
		return festivalOffers;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"brands = '" + brands + '\'' + 
			",categories = '" + categories + '\'' + 
			",banner_details = '" + bannerDetails + '\'' + 
			",festival_offers = '" + festivalOffers + '\'' + 
			"}";
		}
}