package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

public class Response_SellPreOwned {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	@SerializedName("data")
	public Data data;

	@SerializedName("errors")
	public Errors errors;

	public class Errors{

		@SerializedName("body_condition")
		public String bodyCondition;

		@SerializedName("make_id")
		public String make_id;

		@SerializedName("model_id")
		public String model_id;

		@SerializedName("fuel")
		public String fuel;

		@SerializedName("name")
		public String name;

		@SerializedName("email")
		public String email;

		@SerializedName("company_name")
		public String company_name;

		@SerializedName("contact")
		public String contact;

		@SerializedName("transmission")
		public String transmission;

		@SerializedName("exterior")
		public String exterior;

		@SerializedName("interior")
		public String interior;

		@SerializedName("description")
		public String description;

		@SerializedName("year")
		public String year;

		@SerializedName("mileage")
		public String mileage;

		@SerializedName("dealer_warranty")
		public String dealer_warranty;

		@SerializedName("trader_customer")
		public String trader_customer;

		@SerializedName("remarks")
		public String remarks;
	}
	public class Data{

		@SerializedName("temp_id")
		public Integer tempId;
	}

}