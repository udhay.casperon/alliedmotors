package com.app.alliedmotor.model.PreOwnedList;

import android.content.Context;

import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.ServiceInterface;
import com.app.alliedmotor.utility.WebServiceData_sample;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Service_PreownedList {

    public Service_PreownedList(final Context context, final Request_PreownedList request_preownedList, final ResponseInterface.PReownedFilterRI pReownedFilterRI) {

        final WebServiceData_sample data = new WebServiceData_sample();
        ServiceInterface serviceInterface = data.retrofit().create(ServiceInterface.class);
                serviceInterface.PreCarList_Call_List(request_preownedList).enqueue(new Callback<Response_PreOwned>() {
            @Override
            public void onResponse(Call<Response_PreOwned> call, Response<Response_PreOwned> response) {
               if(response.body()!=null)
               {
                   System.out.println("---resp---"+response.body().toString());
                   pReownedFilterRI.onPreOwnedFilterSuccess(response.body());
               }
            }

            @Override
            public void onFailure(Call<Response_PreOwned> call, Throwable t) {
                Commons.hideProgress();
                System.out.println("111111111111---signup mob verify-onfailure----");
            }
        });


    }




}
