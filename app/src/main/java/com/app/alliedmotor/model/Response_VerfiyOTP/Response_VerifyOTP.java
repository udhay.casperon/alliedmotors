package com.app.alliedmotor.model.Response_VerfiyOTP;

import com.google.gson.annotations.SerializedName;

public class Response_VerifyOTP {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;

	public class Data{

		@SerializedName("user_email")
		public String userEmail;

		@SerializedName("otp_data")
		public OtpData otpData;

		@SerializedName("page_name")
		public String pageName;

		@SerializedName("otp_type")
		public String otpType;

		@SerializedName("mobile_number")
		public String mobileNumber;
	}


	public class OtpData{

		@SerializedName("user_email")
		public String userEmail;

		@SerializedName("code")
		public String code;

		@SerializedName("dev_mode")
		public Integer devMode;

		@SerializedName("otp_type")
		public String otpType;

		@SerializedName("mobile_number")
		public String mobileNumber;
	}
	
	
}