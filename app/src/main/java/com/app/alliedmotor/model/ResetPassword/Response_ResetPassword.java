package com.app.alliedmotor.model.ResetPassword;

import com.google.gson.annotations.SerializedName;

public class Response_ResetPassword {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("long_message")
	public String longMessage;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;
}