package com.app.alliedmotor.model.MyOrderLatest;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("order_data")
	private ArrayList<OrderDataItem> orderData;

	public void setOrderData(ArrayList<OrderDataItem> orderData){
		this.orderData = orderData;
	}

	public ArrayList<OrderDataItem> getOrderData(){
		return orderData;
	}

	@Override
 	public String toString(){
		return
			"Data{" +
			"order_data = '" + orderData + '\'' +
			"}";
		}
}