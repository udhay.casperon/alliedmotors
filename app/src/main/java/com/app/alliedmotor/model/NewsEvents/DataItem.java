package com.app.alliedmotor.model.NewsEvents;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("post_title")
	private String postTitle;

	@SerializedName("post_content")
	private String postContent;

	@SerializedName("ID")
	private String iD;

	public void setPostTitle(String postTitle){
		this.postTitle = postTitle;
	}

	public String getPostTitle(){
		return postTitle;
	}

	public void setPostContent(String postContent){
		this.postContent = postContent;
	}

	public String getPostContent(){
		return postContent;
	}

	public void setID(String iD){
		this.iD = iD;
	}

	public String getID(){
		return iD;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"post_title = '" + postTitle + '\'' + 
			",post_content = '" + postContent + '\'' + 
			",iD = '" + iD + '\'' + 
			"}";
		}
}