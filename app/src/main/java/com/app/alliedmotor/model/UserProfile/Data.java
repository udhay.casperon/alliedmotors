package com.app.alliedmotor.model.UserProfile;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("user_details")
	private UserDetails userDetails;

	@SerializedName("support")
	private Support support;

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	public void setSupport(Support support){
		this.support = support;
	}

	public Support getSupport(){
		return support;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"user_details = '" + userDetails + '\'' + 
			",support = '" + support + '\'' + 
			"}";
		}
}