package com.app.alliedmotor.model.Notification;

import android.content.Context;

import com.app.alliedmotor.model.PreOwnedList.Request_PreownedList;
import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.ServiceInterface;
import com.app.alliedmotor.utility.WebServiceData_sample;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Service_FCMNotification {

    public Service_FCMNotification(final Context context, HashMap<String,String>header,HashMap<String,String>request, final ResponseInterface.FCMNotificationInterface fcmNotificationInterface) {

        final WebServiceData_sample data = new WebServiceData_sample();
        ServiceInterface serviceInterface = data.retrofit().create(ServiceInterface.class);
                serviceInterface.ServiceCall_notify(header,request).enqueue(new Callback<Response_Notification>() {
            @Override
            public void onResponse(Call<Response_Notification> call, Response<Response_Notification> response) {
               if(response.body()!=null)
               {
                   System.out.println("---resp---"+response.body().toString());
                   fcmNotificationInterface.onFCMNotificationSuccess(response.body());
               }
            }

            @Override
            public void onFailure(Call<Response_Notification> call, Throwable t) {
                Commons.hideProgress();
                System.out.println("111111111111---signup mob verify-onfailure----");
            }
        });


    }




}
