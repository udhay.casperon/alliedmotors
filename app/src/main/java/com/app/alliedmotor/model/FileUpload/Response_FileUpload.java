package com.app.alliedmotor.model.FileUpload;

import com.google.gson.annotations.SerializedName;

public class Response_FileUpload {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public class Data{

		@SerializedName("file_path")
		public String filePath;

		@SerializedName("raw_name")
		public String rawName;

		@SerializedName("image_size_str")
		public String imageSizeStr;

		@SerializedName("file_name")
		public String fileName;

		@SerializedName("image_width")
		public Integer imageWidth;

		@SerializedName("file_size")
		public Double fileSize;

		@SerializedName("file_ext")
		public String fileExt;

		@SerializedName("image_height")
		public Integer imageHeight;

		@SerializedName("file_type")
		public String fileType;

		@SerializedName("orig_name")
		public String origName;

		@SerializedName("is_image")
		public Boolean isImage;

		@SerializedName("full_path")
		public String fullPath;

		@SerializedName("client_name")
		public String clientName;

		@SerializedName("image_type")
		public String imageType;

		public String getFilePath() {
			return filePath;
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

		public String getRawName() {
			return rawName;
		}

		public void setRawName(String rawName) {
			this.rawName = rawName;
		}

		public String getImageSizeStr() {
			return imageSizeStr;
		}

		public void setImageSizeStr(String imageSizeStr) {
			this.imageSizeStr = imageSizeStr;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public Integer getImageWidth() {
			return imageWidth;
		}

		public void setImageWidth(Integer imageWidth) {
			this.imageWidth = imageWidth;
		}

		public Double getFileSize() {
			return fileSize;
		}

		public void setFileSize(Double fileSize) {
			this.fileSize = fileSize;
		}

		public String getFileExt() {
			return fileExt;
		}

		public void setFileExt(String fileExt) {
			this.fileExt = fileExt;
		}

		public Integer getImageHeight() {
			return imageHeight;
		}

		public void setImageHeight(Integer imageHeight) {
			this.imageHeight = imageHeight;
		}

		public String getFileType() {
			return fileType;
		}

		public void setFileType(String fileType) {
			this.fileType = fileType;
		}

		public String getOrigName() {
			return origName;
		}

		public void setOrigName(String origName) {
			this.origName = origName;
		}

		public Boolean getImage() {
			return isImage;
		}

		public void setImage(Boolean image) {
			isImage = image;
		}

		public String getFullPath() {
			return fullPath;
		}

		public void setFullPath(String fullPath) {
			this.fullPath = fullPath;
		}

		public String getClientName() {
			return clientName;
		}

		public void setClientName(String clientName) {
			this.clientName = clientName;
		}

		public String getImageType() {
			return imageType;
		}

		public void setImageType(String imageType) {
			this.imageType = imageType;
		}
	}

	@Override
	public String toString() {
		return "Response_FileUpload{" +
				"responseCode='" + responseCode + '\'' +
				", data=" + data +
				", message='" + message + '\'' +
				", status='" + status + '\'' +
				'}';
	}
}