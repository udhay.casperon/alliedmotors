package com.app.alliedmotor.model.CarList;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("cars")
	private List<Object> cars;

	@SerializedName("cars_types")
	private List<CarsTypesItem> carsTypes;

	public void setCars(List<Object> cars){
		this.cars = cars;
	}

	public List<Object> getCars(){
		return cars;
	}

	public void setCarsTypes(List<CarsTypesItem> carsTypes){
		this.carsTypes = carsTypes;
	}

	public List<CarsTypesItem> getCarsTypes(){
		return carsTypes;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"cars = '" + cars + '\'' + 
			",cars_types = '" + carsTypes + '\'' + 
			"}";
		}
}