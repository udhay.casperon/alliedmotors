package com.app.alliedmotor.model.NotificationList;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Response_NotificationList {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("commonArr")
	public CommonArr commonArr;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;

	public class Data{

		@SerializedName("notification_list")
		public ArrayList<NotificationListItem> notificationList;
	}

	public class NotificationListItem{

		@SerializedName("created_time")
		public String createdTime;

		@SerializedName("body_html")
		public String bodyHtml;

		@SerializedName("title_html")
		public String titleHtml;

		@SerializedName("is_unread")
		public String isUnread;

		@SerializedName("id")
		public String id;

		@SerializedName("href")
		public String href;

		@SerializedName("ticket_id")
		public Object ticketId;

		@SerializedName("sender_id")
		public String senderId;

		@SerializedName("recipient_id")
		public String recipientId;

		@SerializedName("type_of_notification")
		public String typeOfNotification;
	}

	public class CommonArr{

		@SerializedName("token")
		public String token;
	}
}