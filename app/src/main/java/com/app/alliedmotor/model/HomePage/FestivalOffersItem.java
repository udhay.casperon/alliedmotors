package com.app.alliedmotor.model.HomePage;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class FestivalOffersItem{

	@SerializedName("data")
	private ArrayList<DataItem> data;

	@SerializedName("title")
	private String title;

	public void setData(ArrayList<DataItem> data){
		this.data = data;
	}

	public ArrayList<DataItem> getData(){
		return data;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	@Override
 	public String toString(){
		return 
			"FestivalOffersItem{" + 
			"data = '" + data + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}
}