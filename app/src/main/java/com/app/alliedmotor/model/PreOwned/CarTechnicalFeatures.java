package com.app.alliedmotor.model.PreOwned;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CarTechnicalFeatures{

	@SerializedName("features")
	private List<Object> features;

	public void setFeatures(List<Object> features){
		this.features = features;
	}

	public List<Object> getFeatures(){
		return features;
	}

	@Override
 	public String toString(){
		return 
			"CarTechnicalFeatures{" + 
			"features = '" + features + '\'' + 
			"}";
		}
}