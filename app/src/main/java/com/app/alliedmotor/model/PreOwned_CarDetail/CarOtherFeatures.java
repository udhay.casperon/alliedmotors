package com.app.alliedmotor.model.PreOwned_CarDetail;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class CarOtherFeatures{

	@SerializedName("features")
	private ArrayList<String> features;

	public void setFeatures(ArrayList<String> features){
		this.features = features;
	}

	public ArrayList<String> getFeatures(){
		return features;
	}
}