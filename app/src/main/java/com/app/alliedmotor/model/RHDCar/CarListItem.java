package com.app.alliedmotor.model.RHDCar;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class CarListItem{

	@SerializedName("car_datas")
	private ArrayList<CarDatasItem> carDatas;

	@SerializedName("title")
	private String title;

	public void setCarDatas(ArrayList<CarDatasItem> carDatas){
		this.carDatas = carDatas;
	}

	public ArrayList<CarDatasItem> getCarDatas(){
		return carDatas;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	@Override
 	public String toString(){
		return 
			"CarListItem{" + 
			"car_datas = '" + carDatas + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}
}