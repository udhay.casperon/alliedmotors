package com.app.alliedmotor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Response_SearchbyKeywords {

	@SerializedName("response_code")
	public String responseCode;

	@SerializedName("data")
	public Data data;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public String status;


	@SerializedName("errors")
	public Errors errors;


	public class Data{

		@SerializedName("cars")
		public ArrayList<CarsItem> cars;
	}
	public class CarsItem{

		@SerializedName("car_title")
		public String carTitle;

		@SerializedName("mainId")
		public String mainId;
	}

	public class Errors{

		@SerializedName("search_keywords")
		public String searchKeywords;
	}

}