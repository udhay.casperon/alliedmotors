package com.app.alliedmotor.model.NewCar;

import com.google.gson.annotations.SerializedName;

public class CategoryListItem{

	@SerializedName("catid")
	private String catid;

	@SerializedName("cat_name")
	private String catName;

	@SerializedName("cat_slug")
	private String catSlug;

	public void setCatid(String catid){
		this.catid = catid;
	}

	public String getCatid(){
		return catid;
	}

	public void setCatName(String catName){
		this.catName = catName;
	}

	public String getCatName(){
		return catName;
	}

	public void setCatSlug(String catSlug){
		this.catSlug = catSlug;
	}

	public String getCatSlug(){
		return catSlug;
	}

	@Override
 	public String toString(){
		return 
			"CategoryListItem{" + 
			"catid = '" + catid + '\'' + 
			",cat_name = '" + catName + '\'' + 
			",cat_slug = '" + catSlug + '\'' + 
			"}";
		}
}