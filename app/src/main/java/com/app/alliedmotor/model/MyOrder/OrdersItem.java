package com.app.alliedmotor.model.MyOrder;

import com.google.gson.annotations.SerializedName;

public class OrdersItem{

	@SerializedName("car_details")
	private CarDetails carDetails;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("enq_status")
	private String enqStatus;

	@SerializedName("ticket_id")
	private String ticketId;

	@SerializedName("enq_crtdate")
	private String enqCrtdate;

	@SerializedName("car_category")
	private String carCategory;

	public void setCarDetails(CarDetails carDetails){
		this.carDetails = carDetails;
	}

	public CarDetails getCarDetails(){
		return carDetails;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setEnqStatus(String enqStatus){
		this.enqStatus = enqStatus;
	}

	public String getEnqStatus(){
		return enqStatus;
	}

	public void setTicketId(String ticketId){
		this.ticketId = ticketId;
	}

	public String getTicketId(){
		return ticketId;
	}

	public void setEnqCrtdate(String enqCrtdate){
		this.enqCrtdate = enqCrtdate;
	}

	public String getEnqCrtdate(){
		return enqCrtdate;
	}

	public void setCarCategory(String carCategory){
		this.carCategory = carCategory;
	}

	public String getCarCategory(){
		return carCategory;
	}

	@Override
 	public String toString(){
		return 
			"OrdersItem{" + 
			"car_details = '" + carDetails + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",enq_status = '" + enqStatus + '\'' + 
			",ticket_id = '" + ticketId + '\'' + 
			",enq_crtdate = '" + enqCrtdate + '\'' + 
			",car_category = '" + carCategory + '\'' + 
			"}";
		}
}