package com.app.alliedmotor.model.Pre_OwnedCar;

import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("car_details")
	private ArrayList<CarDetailsItem> carDetails;

	public void setCarDetails(ArrayList<CarDetailsItem> carDetails){
		this.carDetails = carDetails;
	}

	public ArrayList<CarDetailsItem> getCarDetails(){
		return carDetails;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"car_details = '" + carDetails + '\'' + 
			"}";
		}
}