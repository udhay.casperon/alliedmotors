package com.app.alliedmotor.model.Notification;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("notification_data")
	private NotificationData notificationData;

	public void setNotificationData(NotificationData notificationData){
		this.notificationData = notificationData;
	}

	public NotificationData getNotificationData(){
		return notificationData;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"notification_data = '" + notificationData + '\'' + 
			"}";
		}
}