package com.app.alliedmotor.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.app.alliedmotor.model.QuoteDataItemDB;

import java.util.ArrayList;

public class MyDataBase extends SQLiteOpenHelper {
    Context context;
    private static final String TAG = "DatabaseHelper";
    public static final String DATABASE_NAME = "AlliedMotors_main.db";


    public static String TABLE_NAME_LOGIN_Sample = "login_table_Sample";
    public static String TABLE_NAME2 = "GuestLogin_Table2";

    public static final String CAR_MAIN_ID = "Car_main_id";


    public static final String gcarimage = "gcarimage1";
    public static final String gbrandname = "gbrandname1";
    public static final String gcarname = "gcarname1";
    public static final String gyear = "gyear1";
    public static final String gvarientcode = "gvarientcode1";
    public static final String gmodeloption = "gmodeloption1";
    public static final String gquantity = "gquantity1";
    public static final String ginteriorcolor = "ginteriorcolor1";
    public static final String gexteriorcolor = "gexteriorcolor1";

    public static final String garCatID = "gcarcatid1";
    public static final String gmakeid = "gmakeid1";
    public static final String gmodID = "gmodeid1";
    public static final String gslugName = "gslugname1";



    public static final String User_Mailid = "mailid";
    public static final String User_Password = "password";
private SQLiteDatabase db;

    //public static final String

    public MyDataBase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME_LOGIN_Sample + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                + User_Mailid + " VARCHAR NOT NULL UNIQUE , "
                + User_Password + " VARCHAR)");


        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME2 + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                + gcarimage + " VARCHAR , "
                + CAR_MAIN_ID + " VARCHAR , "
                + gcarname + " VARCHAR , "
                + gyear + " VARCHAR , "
                + gvarientcode + " VARCHAR , "
                + gmodeloption + " VARCHAR , "
                + gquantity + " VARCHAR , "
                + ginteriorcolor + " VARCHAR , "
                + gexteriorcolor + " VARCHAR , "
                + garCatID + " VARCHAR , "
                + gmakeid + " VARCHAR , "
                + gmodID + " VARCHAR , "
                + gslugName + " VARCHAR , "
                + gbrandname + " VARCHAR)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_LOGIN_Sample);

        onCreate(db);
    }



    public boolean recordExist_useremail(String emailid) {
        try
        {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectString = "SELECT * FROM " + TABLE_NAME_LOGIN_Sample + " WHERE " + User_Mailid + " =?";

            // Add the String you are searching by here.
            // Put it in an array to avoid an unrecognized token error
            Cursor cursor = db.rawQuery(selectString, new String[]{emailid});
            if (cursor.moveToFirst()) {
                db.close();
                Log.d("Record  Already Exists", "Table is:" + TABLE_NAME_LOGIN_Sample + " ColumnName:" + CAR_MAIN_ID);
                return true;//record Exists

            }
            Log.d("New Record  ", "Table is:" + TABLE_NAME_LOGIN_Sample + " ColumnName:" + CAR_MAIN_ID + " Column Value:" + emailid);
            db.close();
        }
        catch(Exception errorException)
        {
            Log.d("Exception occured", "Exception occured "+errorException);
            // db.close();
        }
        return false;
    }




    public boolean Insert_loginsample(String useremail1,String userpassword1) {

        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT * FROM " + TABLE_NAME_LOGIN_Sample + " WHERE " + User_Mailid + " =?";

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = db.rawQuery(selectString, new String[] {useremail1});
        boolean hasObject = false;
        if(cursor.moveToFirst()) {
            hasObject = true;
            //region if you had multiple records to check for, use this region.
            int count = 0;
            while (cursor.moveToNext()) {
                count++;
            }
        }
        else {
            ContentValues values = new ContentValues();
            values.put(User_Mailid, useremail1); // Contact Name
            values.put(User_Password, userpassword1); // Contact Phone
            // Inserting Row
            db.insert(TABLE_NAME_LOGIN_Sample, null, values);
        }
        return hasObject;
    }



    public ArrayList<String> getUsername()
    {
        ArrayList<String>  allname=new ArrayList<>();
        ArrayList<String>allpassword=new ArrayList<>();
        db=this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_NAME_LOGIN_Sample,null,null,null,null,null,null);
        cursor.moveToFirst();
   while (!cursor.isAfterLast())
    {
    allname.add(cursor.getString(cursor.getColumnIndex(User_Mailid)));
    allpassword.add(cursor.getString(cursor.getColumnIndex(User_Password)));
    cursor.moveToNext();
     }
        cursor.close();
        return  allname;
    }



    public String getpassword(String useremail)
    {
        String allpassword="";
        db=this.getReadableDatabase();
        String selectString = "SELECT * FROM " + TABLE_NAME_LOGIN_Sample + " WHERE " + User_Mailid + " =?";
        Cursor cursor = db.rawQuery(selectString, new String[] {useremail});

  if(cursor!=null)
  {
      System.out.println("---gg--reord exist-");
      cursor=db.query(TABLE_NAME_LOGIN_Sample,null,null,null,null,null,null);
      cursor.moveToFirst();
      while (!cursor.isAfterLast())
      {
          //allpassword.add(cursor.getString(cursor.getColumnIndex(User_Password)));
          allpassword=cursor.getString(cursor.getColumnIndex(User_Password));
          cursor.moveToNext();
      }
  }
  else {
      System.out.println("---gg--reord nnot exist-");
  }

        cursor.close();
        return  allpassword;
    }



    public void update(String useremail,String userpasword1)
    {
         db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User_Mailid, useremail);
        values.put(User_Password, userpasword1);

        db.update(TABLE_NAME_LOGIN_Sample, values, "mailid = ?", new String[]{useremail});
    }


    public boolean recordExist(String carmainid) {
        try
        {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectString = "SELECT * FROM " + TABLE_NAME2 + " WHERE " + CAR_MAIN_ID + " =?";

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = db.rawQuery(selectString, new String[]{carmainid});
        if (cursor.moveToFirst()) {
            db.close();
            Log.d("Record  Already Exists", "Table is:" + TABLE_NAME2 + " ColumnName:" + CAR_MAIN_ID);
            return true;//record Exists

        }
        Log.d("New Record  ", "Table is:" + TABLE_NAME2 + " ColumnName:" + CAR_MAIN_ID + " Column Value:" + carmainid);
        db.close();
    }
       catch(Exception errorException)
    {
        Log.d("Exception occured", "Exception occured "+errorException);
        // db.close();
    }
        return false;
    }




    public boolean Insert_Guestquote_1(String carmainid,String carimage,String slugname,String carcatid,String makeid,String modid, String carbrand,String carname, String year,String modeloptions, String varientcode,String exteriorcolor, String interiorcolor,String quantity)
    {

        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT * FROM " + TABLE_NAME2 + " WHERE " + CAR_MAIN_ID + " =?";

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = db.rawQuery(selectString, new String[] {carmainid});
        boolean hasObject = false;
        if(cursor.moveToFirst()) {
            hasObject = true;
            //Toast.makeText(context,"Record Exist",Toast.LENGTH_SHORT).show();
            System.out.println("Record exixte");
            //region if you had multiple records to check for, use this region.
            int count = 0;
            while (cursor.moveToNext()) {
                count++;
            }
        }
        else {

            ContentValues values = new ContentValues();
            values.put(CAR_MAIN_ID, carmainid); // Contact Name
            values.put(gcarimage, carimage); // Contact Phone
            values.put(gslugName, slugname); // Contact Name
            values.put(garCatID, carcatid);
            values.put(gmakeid, makeid); // Contact Name
            values.put(gmodID, modid);
            values.put(gbrandname, carbrand); // Contact Name
            values.put(gcarname, carname);
            values.put(gyear, year); // Contact Name
            values.put(gmodeloption, modeloptions);
            values.put(gvarientcode, varientcode); // Contact Name
            values.put(gexteriorcolor, exteriorcolor);
            values.put(ginteriorcolor, interiorcolor); // Contact Name
            values.put(gquantity, quantity);

            // Inserting Row
            db.insert(TABLE_NAME2, null, values);
        }
        return hasObject;
    }








    public boolean Insert_Guestquote(String carmainid,String carimage,String slugname,String carcatid,String makeid,String modid, String carbrand,String carname, String year,String modeloptions, String varientcode,String exteriorcolor, String interiorcolor,String quantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        // it add only
        String sql_favourite = "INSERT OR IGNORE INTO " + TABLE_NAME2 + "(gcarimage1,Car_main_id,gslugname1,gcarcatid1,gmakeid1,gmodeid1,gbrandname1,gcarname1,gyear1,gvarientcode1,gmodeloption1,gexteriorcolor1,ginteriorcolor1,gquantity1) VALUES('" + carimage
                + "','" + carmainid
                + "','" + slugname
                + "','" + carcatid
                + "','" + makeid
                + "','" + modid
                + "','" + carbrand
                + "','" + carname
                + "','" + year
                + "','" + modeloptions
                + "','" + varientcode
                + "','" + exteriorcolor
                + "','" + interiorcolor
                + "','" + quantity + "')";
        db.execSQL(sql_favourite);
        return true;
    }

    public boolean updatequotelist1(String carmainid1,String quantity)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(gquantity,quantity);


        db.update(TABLE_NAME2, contentValues, "Car_main_id = ?", new String[]{carmainid1});
        return true;
    }



    public ArrayList<QuoteDataItemDB> getAlldataforquote() {

        ArrayList<QuoteDataItemDB> array_list = new ArrayList<QuoteDataItemDB>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + TABLE_NAME2, null);
        int count = cursor.getCount();

        if (cursor.moveToFirst()) {
            do {
                QuoteDataItemDB note = new QuoteDataItemDB();
                note.setCarUrl(cursor.getString(cursor.getColumnIndex(gcarimage)));
                note.setCarId(cursor.getString(cursor.getColumnIndex(CAR_MAIN_ID)));
                note.setModelYear(cursor.getString(cursor.getColumnIndex(gyear)));
                note.setVarientCode(cursor.getString(cursor.getColumnIndex(gvarientcode)));
                note.setQuantity(cursor.getString(cursor.getColumnIndex(gquantity)));
                note.setExteriorColor(cursor.getString(cursor.getColumnIndex(gexteriorcolor)));
                note.setInteriorColor(cursor.getString(cursor.getColumnIndex(ginteriorcolor)));
                note.setCar_name(cursor.getString(cursor.getColumnIndex(gcarname)));
                note.setCarTransmission(cursor.getString(cursor.getColumnIndex(gmodeloption)));
                note.setCarType(cursor.getString(cursor.getColumnIndex(gbrandname)));
                note.setCarTitle(cursor.getString(cursor.getColumnIndex(gmodeloption)));
                note.setCarCat(cursor.getString(cursor.getColumnIndex(garCatID)));
                note.setMakeid(cursor.getString(cursor.getColumnIndex(gmakeid)));
                note.setModid(cursor.getString(cursor.getColumnIndex(gmodID)));
                note.setCarInfo(cursor.getString(cursor.getColumnIndex(gslugName)));

                array_list.add(note);
            } while (cursor.moveToNext());
        }
        return array_list;
    }


    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME2, "Car_main_id = ?", new String[]{id});
    }

    public void deletefRows() {
        SQLiteDatabase db = this.getWritableDatabase();
       // db.execSQL("delete from " + TABLE_NAME2);
        db.delete(TABLE_NAME2, null, null);

    }


}
