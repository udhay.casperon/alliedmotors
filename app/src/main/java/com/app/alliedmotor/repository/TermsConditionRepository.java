package com.app.alliedmotor.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.TermsCondition.Response_Terms;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsConditionRepository {
    Context context;
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_Terms> termsData = new MutableLiveData<>();

    public MutableLiveData<Response_Terms> getTermsdataResponse() {
        Commons.showProgress(context);
        retrofitInstance.retrofit().TermsCondition_ResponseCall().enqueue(new Callback<Response_Terms>() {
            @Override
            public void onResponse(Call<Response_Terms> call, Response<Response_Terms> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        termsData.setValue(response.body());
                        Log.e("response-faqe->",String.valueOf(response.body()));
                        System.out.println("--res=5==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_Terms> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_faq--->",String.valueOf(t));
            }
        });

        return termsData;
    }


}
