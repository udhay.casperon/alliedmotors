package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.MyFavourite.Response_MyFavourite;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFavouriteRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_MyFavourite> myFavouriteMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Response_MyFavourite> getmyfavouriteResponse(HashMap<String,String>header) {

        retrofitInstance.retrofit().MyFavouriteCall(header).enqueue(new Callback<Response_MyFavourite>() {
            @Override
            public void onResponse(Call<Response_MyFavourite> call, Response<Response_MyFavourite> response) {

                if(response.body()!=null) {
                        myFavouriteMutableLiveData.setValue(response.body());
                        Log.e("response--favpro->",String.valueOf(response.body()));
                        System.out.println("--res=65orfavorute=" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_MyFavourite> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailu orfavorute->",String.valueOf(t));
            }
        });

        return myFavouriteMutableLiveData;
    }


}
