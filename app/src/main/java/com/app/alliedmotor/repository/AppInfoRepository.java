package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppInfoRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_AppInfo> appInfoMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_AppInfo> appInfo(HashMap<String,String>header) {
        retrofitInstance.retrofit().AppInfo_ServiceCall(header).enqueue(new Callback<Response_AppInfo>() {
            @Override
            public void onResponse(Call<Response_AppInfo> call, Response<Response_AppInfo> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                    appInfoMutableLiveData.setValue(response.body());
                        Log.e("response--userpro->",String.valueOf(response.body()));
                        System.out.println("--res=65==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_AppInfo> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailureuserprof-->",String.valueOf(t));
            }
        });

        return appInfoMutableLiveData;
    }


}
