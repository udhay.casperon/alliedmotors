package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.HomePage.Response_Homepage;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePageRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_Homepage> loginData = new MutableLiveData<>();

    public MutableLiveData<Response_Homepage> gethomepageResponse() {

        retrofitInstance.retrofit().ResponseHomePageCall().enqueue(new Callback<Response_Homepage>() {
            @Override
            public void onResponse(Call<Response_Homepage> call, Response<Response_Homepage> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        //Commons.hideProgress();
                        loginData.setValue(response.body());
                        Log.e("response--homepage->",String.valueOf(response.body()));
                        System.out.println("--res=5==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_Homepage> call, Throwable t) {
                //Commons.hideProgress();
                Log.e("OnFailure_Homepage--->",String.valueOf(t));
            }
        });

        return loginData;
    }


}
