package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.LuxuryCar.Response_LuxuryCar;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LuxuryCarRepository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_LuxuryCar> luxuryCarMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_LuxuryCar> getluxuryCardataResponse() {

        retrofitInstance.retrofit().LuxuryCar_ResponseCall().enqueue(new Callback<Response_LuxuryCar>() {
            @Override
            public void onResponse(Call<Response_LuxuryCar> call, Response<Response_LuxuryCar> response) {
                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        luxuryCarMutableLiveData.setValue(response.body());
                        Log.e("response-_NewCar->",String.valueOf(response.body()));
                        System.out.println("--res=5Response_NewCar==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_LuxuryCar> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailResponse_NewC-->",String.valueOf(t));
            }
        });

        return luxuryCarMutableLiveData;
    }


}
