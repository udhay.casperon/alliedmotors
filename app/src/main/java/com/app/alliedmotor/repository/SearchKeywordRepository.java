package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;


import com.app.alliedmotor.model.Response_SearchbyKeywords;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchKeywordRepository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_SearchbyKeywords> searchKeywordMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_SearchbyKeywords> getSearchDataList(HashMap<String,String>request) {

        retrofitInstance.retrofit().SearchbyKeywordCall(request).enqueue(new Callback<Response_SearchbyKeywords>() {
            @Override
            public void onResponse(Call<Response_SearchbyKeywords> call, Response<Response_SearchbyKeywords> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                    searchKeywordMutableLiveData.setValue(response.body());
                        Log.e("searc res->",String.valueOf(response.body().message));
                        System.out.println("--searc res=" + response.body().status);

                }
            }

            @Override
            public void onFailure(Call<Response_SearchbyKeywords> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailu researc res=->",String.valueOf(t));
            }
        });

        return searchKeywordMutableLiveData;
    }


}
