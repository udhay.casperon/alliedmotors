package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.ChangePassword.Response_ChangePassword;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_ChangePassword> changepasswordData = new MutableLiveData<>();

    public MutableLiveData<Response_ChangePassword> getChangepasswordResponse(HashMap<String,String> request_changepassword,HashMap<String,String> header1) {

        retrofitInstance.retrofit().Responsechangepasswordcall(header1,request_changepassword).enqueue(new Callback<Response_ChangePassword>() {
            @Override
            public void onResponse(Call<Response_ChangePassword> call, Response<Response_ChangePassword> response) {
                if (response.body() != null) {
                    changepasswordData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Response_ChangePassword> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_password--->",String.valueOf(t));
            }
        });

        return changepasswordData;
    }


}
