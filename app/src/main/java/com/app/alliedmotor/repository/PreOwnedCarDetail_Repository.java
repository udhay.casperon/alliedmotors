package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.PreOwned_CarDetail.Response_PreOwned_Detail;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreOwnedCarDetail_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_PreOwned_Detail> cardetail_Data = new MutableLiveData<>();

    public MutableLiveData<Response_PreOwned_Detail> getCardetail_Data(String url) {

        retrofitInstance.retrofit().PreCarDeatil_Call(url).enqueue(new Callback<Response_PreOwned_Detail>() {
            @Override
            public void onResponse(Call<Response_PreOwned_Detail> call, Response<Response_PreOwned_Detail> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        cardetail_Data.setValue(response.body());
                        Log.e("respo-carcate->",String.valueOf(response.body()));
                        System.out.println("--res=5 car cate==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_PreOwned_Detail> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_car cat--->",String.valueOf(t));
            }
        });

        return cardetail_Data;
    }


}
