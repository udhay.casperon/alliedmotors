package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.CarDetail.Response_CarDetail;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarDetail_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_CarDetail> cardetail_Data = new MutableLiveData<>();

    public MutableLiveData<Response_CarDetail> getCardetail_Data(String url, HashMap<String, String> header, HashMap<String, String> inputparam) {

        retrofitInstance.retrofit().CarDetail_Call(url, header, inputparam).enqueue(new Callback<Response_CarDetail>() {
            @Override
            public void onResponse(Call<Response_CarDetail> call, Response<Response_CarDetail> response) {

                if (response.body() != null) {
                    Commons.hideProgress();
                    cardetail_Data.postValue(response.body());
                    Log.e("Res-car detail->", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<Response_CarDetail> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_car cat--->", String.valueOf(t));
            }
        });

        return cardetail_Data;
    }


}
