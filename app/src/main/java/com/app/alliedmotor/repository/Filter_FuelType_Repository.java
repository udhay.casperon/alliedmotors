package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.FilterFuelTypeResponse;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Filter_FuelType_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<FilterFuelTypeResponse> filterFuelTypeResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<FilterFuelTypeResponse> getFilterdata(HashMap<String, String> inputparam) {

        retrofitInstance.retrofit().Call_filterFuelType(inputparam).enqueue(new Callback<FilterFuelTypeResponse>() {
            @Override
            public void onResponse(Call<FilterFuelTypeResponse> call, Response<FilterFuelTypeResponse> response) {

                if (response.body() != null) {
                    Commons.hideProgress();
                    filterFuelTypeResponseMutableLiveData.setValue(response.body());
                    Log.e("Res-car detail->", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<FilterFuelTypeResponse> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_car cat--->", String.valueOf(t));
            }
        });

        return filterFuelTypeResponseMutableLiveData;
    }


}
