package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.LuxuryCar.Response_LuxuryCar;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LuxuryCarMakeList_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_LuxuryCar> selectedmakedata = new MutableLiveData<>();

    public MutableLiveData<Response_LuxuryCar> getselectedBrand_Data(String url, HashMap<String, String> inputparam) {

        retrofitInstance.retrofit().SelectedLuxuryMakeList_Call(url,inputparam).enqueue(new Callback<Response_LuxuryCar>() {
            @Override
            public void onResponse(Call<Response_LuxuryCar> call, Response<Response_LuxuryCar> response) {

                if (response.body() != null) {
                    Commons.hideProgress();
                    selectedmakedata.setValue(response.body());
                    Log.e("Res-luxseleted brand->", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<Response_LuxuryCar> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFa lux ted brand ->", String.valueOf(t));
            }
        });

        return selectedmakedata;
    }


}
