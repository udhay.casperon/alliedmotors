package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Sample.Response_UserCreation;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserCreationRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_UserCreation> userCreationMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_UserCreation> usercreation(HashMap<String,String>request) {
        retrofitInstance.retrofit().Call_UserCreation(request).enqueue(new Callback<Response_UserCreation>() {
            @Override
            public void onResponse(Call<Response_UserCreation> call, Response<Response_UserCreation> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                    userCreationMutableLiveData.setValue(response.body());
                        Log.e("resp-usercreation->",String.valueOf(response.body()));
                        System.out.println("--res=65==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_UserCreation> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail usercreation-->",String.valueOf(t));
            }
        });

        return  userCreationMutableLiveData;   }


}
