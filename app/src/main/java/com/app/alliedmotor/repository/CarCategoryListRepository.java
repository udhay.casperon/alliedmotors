package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Car_CategoryList.Response_CarCategoryList;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarCategoryListRepository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_CarCategoryList> carCategoryData = new MutableLiveData<>();

    public MutableLiveData<Response_CarCategoryList> getCarCategoryResponse() {

        retrofitInstance.retrofit().CarCaetgoryList_ResponseCall().enqueue(new Callback<Response_CarCategoryList>() {
            @Override
            public void onResponse(Call<Response_CarCategoryList> call, Response<Response_CarCategoryList> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        carCategoryData.setValue(response.body());
                        Log.e("respo-carcate->",String.valueOf(response.body()));
                        System.out.println("--res=5 car cate==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_CarCategoryList> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_car cat--->",String.valueOf(t));
            }
        });

        return carCategoryData;
    }


}
