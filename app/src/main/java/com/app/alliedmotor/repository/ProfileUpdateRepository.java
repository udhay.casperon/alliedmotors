package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.ProfileUpdate.Response_ProfileUpdate;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileUpdateRepository  {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_ProfileUpdate> profileupdateData = new MutableLiveData<>();

    public MutableLiveData<Response_ProfileUpdate> getProfileUpdateResponse(HashMap<String, String> profileupdate,HashMap<String, String> header1) {
        retrofitInstance.retrofit().Profileupdatecall(header1,profileupdate).enqueue(new Callback<Response_ProfileUpdate>() {
            @Override
            public void onResponse(Call<Response_ProfileUpdate> call, Response<Response_ProfileUpdate> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    profileupdateData.setValue(response.body());
                    Log.e("re profileup-55-->",String.valueOf(response.body()));
                    System.out.println("--res=57==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_ProfileUpdate> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail proupda--->",String.valueOf(t));
            }
        });
        return profileupdateData;
    }
}
