package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.NotificationStatusChange.Response_NotificationStatuschange;
import com.app.alliedmotor.model.Sample1.Response_UserNotificationstatuschange;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Statuschange_NotificationRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_NotificationStatuschange> notificationStatuschangeMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_NotificationStatuschange> notifystatuschange(HashMap<String, String> header1, HashMap<String, String> request) {
        retrofitInstance.retrofit().NotificationStatusChange_call(header1,request).enqueue(new Callback<Response_NotificationStatuschange>() {
            @Override
            public void onResponse(Call<Response_NotificationStatuschange> call, Response<Response_NotificationStatuschange> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    notificationStatuschangeMutableLiveData.setValue(response.body());
                    Log.e("orde cre->",String.valueOf(response.body()));
                    System.out.println("-order cre=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_NotificationStatuschange> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail--order-->",String.valueOf(t));
            }
        });
        return notificationStatuschangeMutableLiveData;
    }
}
