package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_FilterModelOption;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Filter_ModelOption_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_FilterModelOption> filterVarientResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_FilterModelOption> getFilter_varientdata(HashMap<String, String> inputparam) {

        retrofitInstance.retrofit().Call_filterModelOption(inputparam).enqueue(new Callback<Response_FilterModelOption>() {
            @Override
            public void onResponse(Call<Response_FilterModelOption> call, Response<Response_FilterModelOption> response) {

                if (response.body() != null) {
                    Commons.hideProgress();
                    filterVarientResponseMutableLiveData.setValue(response.body());
                    Log.e("Res-car detail->", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<Response_FilterModelOption> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_car cat--->", String.valueOf(t));
            }
        });

        return filterVarientResponseMutableLiveData;
    }


}
