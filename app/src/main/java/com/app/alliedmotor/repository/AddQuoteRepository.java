package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddQuoteRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_Addquote> addquoteMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_Addquote> getAddquote(HashMap<String, String> header1,HashMap<String, String> profileupdate) {
        retrofitInstance.retrofit().AddToQuoteCall(header1,profileupdate).enqueue(new Callback<Response_Addquote>() {
            @Override
            public void onResponse(Call<Response_Addquote> call, Response<Response_Addquote> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    addquoteMutableLiveData.setValue(response.body());
                    Log.e("readdquote-55-->",String.valueOf(response.body()));

                    System.out.println("--res=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_Addquote> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail proupda--->",String.valueOf(t));
            }
        });
        return addquoteMutableLiveData;
    }
}
