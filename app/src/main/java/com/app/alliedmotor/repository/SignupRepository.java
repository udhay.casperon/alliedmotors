package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Signup.Response_SignupResponse;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_SignupResponse> signupData = new MutableLiveData<>();

    public MutableLiveData<Response_SignupResponse> getSignupResponse(HashMap<String, String> signup_request) {

        retrofitInstance.retrofit().ResponseSignupCall(signup_request).enqueue(new Callback<Response_SignupResponse>() {
            @Override
            public void onResponse(Call<Response_SignupResponse> call, Response<Response_SignupResponse> response) {
                Commons.hideProgress();
                if(response.body()!=null) {
                    signupData.setValue(response.body());
                    System.out.println("--res=5==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_SignupResponse> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_signup-->",String.valueOf(t));
            }

        });

        return signupData;
    }


}
