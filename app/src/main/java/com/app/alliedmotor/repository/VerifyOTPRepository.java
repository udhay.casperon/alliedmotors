package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOTPRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_VerifyOTP> verifyOTPMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_VerifyOTP> verfiyotp(HashMap<String,String>header,HashMap<String,String>request) {
        retrofitInstance.retrofit().Call_VerfiyOTP(header,request).enqueue(new Callback<Response_VerifyOTP>() {
            @Override
            public void onResponse(Call<Response_VerifyOTP> call, Response<Response_VerifyOTP> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                    verifyOTPMutableLiveData.setValue(response.body());
                        Log.e("response--userpro->",String.valueOf(response.body()));
                        System.out.println("--res=65==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_VerifyOTP> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail verotp-->",String.valueOf(t));
            }
        });

        return  verifyOTPMutableLiveData;   }


}
