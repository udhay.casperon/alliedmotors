package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_UpdateQuote;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateQuoteRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_UpdateQuote> updateQuoteMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_UpdateQuote> updatequoteLiveData(HashMap<String, String> header1,HashMap<String, String> deletequote) {
        retrofitInstance.retrofit().UpdateQuoteCall(header1,deletequote).enqueue(new Callback<Response_UpdateQuote>() {
            @Override
            public void onResponse(Call<Response_UpdateQuote> call, Response<Response_UpdateQuote> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    updateQuoteMutableLiveData.setValue(response.body());
                    Log.e("deletequote-55-->",String.valueOf(response.body()));
                    System.out.println("-delete-res=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_UpdateQuote> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailletequa--->",String.valueOf(t));
            }
        });
        return updateQuoteMutableLiveData;
    }
}
