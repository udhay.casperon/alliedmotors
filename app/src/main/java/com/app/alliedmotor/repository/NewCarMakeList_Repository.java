package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.NewCar.NewCar_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCarMakeList_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<NewCar_Response> selectedmakedata = new MutableLiveData<>();

    public MutableLiveData<NewCar_Response> getselectedBrand_Data(String url, HashMap<String, String> inputparam) {

        retrofitInstance.retrofit().SelectedMakeList_Call(url,  inputparam).enqueue(new Callback<NewCar_Response>() {
            @Override
            public void onResponse(Call<NewCar_Response> call, Response<NewCar_Response> response) {

                if (response.body() != null) {
                    Commons.hideProgress();
                    selectedmakedata.setValue(response.body());
                    Log.e("Res-seleted brand->", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<NewCar_Response> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailseleted brand ->", String.valueOf(t));
            }
        });

        return selectedmakedata;
    }


}
