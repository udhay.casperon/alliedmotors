package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.FileUpload.Response_FileUpload;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileUpload_Repository {

    MultipartBody.Part img = null;
    String fileprefix="",st_transactionimg="";
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_FileUpload> fileUploadMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_FileUpload> data_fileupload(HashMap<String,String>request) {

        st_transactionimg=request.get("image");
        fileprefix=request.get("file_prefix");
       // RequestBody Filename_Prefix=RequestBody.create(MultipartBody.FORM,fileprefix);
        RequestBody Filename_Prefix = RequestBody.create(MediaType.parse("text/plain"), fileprefix);
        if (st_transactionimg != null) {
            File fileToUpload = new File(st_transactionimg);
            if (fileToUpload.exists()) {
                Log.d("exist", "exist");
            } else {
                Log.d("not exist", "not exist");
            }
            //File file = new File(st_transactionimg);
            RequestBody requestfile = RequestBody.create(MediaType.parse("image/png"), fileToUpload);
            img = MultipartBody.Part.createFormData("image", fileToUpload.getName(), requestfile);
        }



        retrofitInstance.retrofit().ServiceCall_fileUpload(Filename_Prefix,img).enqueue(new Callback<Response_FileUpload>() {
            @Override
            public void onResponse(Call<Response_FileUpload> call, Response<Response_FileUpload> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                        fileUploadMutableLiveData.setValue(response.body());
                        Log.e("respo-sell pre>",String.valueOf(response.body()));
                        System.out.println("--res=sell pre=" + response.body().toString());


                }
            }

            @Override
            public void onFailure(Call<Response_FileUpload> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_sell pre=->",String.valueOf(t));
            }
        });

        return fileUploadMutableLiveData;
    }


}
