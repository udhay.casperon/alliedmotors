package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarMakeListRepository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_CarMakeList> makeListMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_CarMakeList> getMakeListMutableLiveData(HashMap<String,String>request) {

        retrofitInstance.retrofit().ServiceCall_MakeList(request).enqueue(new Callback<Response_CarMakeList>() {
            @Override
            public void onResponse(Call<Response_CarMakeList> call, Response<Response_CarMakeList> response) {
                if(response.body()!=null) {
                        Commons.hideProgress();
                        makeListMutableLiveData.setValue(response.body());
                        Log.e("response-_NewCar->",String.valueOf(response.body()));
                        System.out.println("--res=5Response_NewCar==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_CarMakeList> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailResponse_NewC-->",String.valueOf(t));
            }
        });

        return makeListMutableLiveData;
    }


}
