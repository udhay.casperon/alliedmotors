package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Notification.Response_Notification;
import com.app.alliedmotor.model.NotificationDelete.Response_NotifyDelete;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFCMRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_Notification> deleteMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_Notification> fcm_notification(HashMap<String, String> header1, HashMap<String, String> request) {
        retrofitInstance.retrofit().ServiceCall_notify(header1,request).enqueue(new Callback<Response_Notification>() {
            @Override
            public void onResponse(Call<Response_Notification> call, Response<Response_Notification> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    deleteMutableLiveData.setValue(response.body());
                    Log.e("orde cre->",String.valueOf(response.body()));
                    System.out.println("-order cre=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_Notification> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail--order-->",String.valueOf(t));
            }
        });
        return deleteMutableLiveData;
    }
}
