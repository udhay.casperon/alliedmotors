package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreOwnedRepository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_PreOwned> preOwnedMutableLiveData = new MutableLiveData<Response_PreOwned>();

    public MutableLiveData<Response_PreOwned> getPreOwnedResponse(String url,HashMap<String, String> request) {
        retrofitInstance.retrofit().PreOwnedCar_Call(url,request).enqueue(new Callback<Response_PreOwned>() {
            @Override
            public void onResponse(Call<Response_PreOwned> call, Response<Response_PreOwned> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        preOwnedMutableLiveData.postValue(response.body());
                        Log.e("respo-carcate->",String.valueOf(response.body()));
                        System.out.println("--res=5 car cate==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_PreOwned> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_car cat--->",String.valueOf(t));
            }
        });

        return preOwnedMutableLiveData;
    }


}
