package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepositoryGetQuote {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_GetQuote> getquoteData = new MutableLiveData<>();

    public MutableLiveData<Response_GetQuote> getQuoteResponse(HashMap<String,String>header1) {

        retrofitInstance.retrofit().Call_GetQuote(header1).enqueue(new Callback<Response_GetQuote>() {
            @Override
            public void onResponse(Call<Response_GetQuote> call, Response<Response_GetQuote> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        getquoteData.setValue(response.body());
                        Log.e("respon getquote-->",String.valueOf(response.body()));
                        System.out.println("--res=57s6==" + response.body().toString());
                    }
                    else if(response.body().getStatus().equals("0"))
                    {
                        getquoteData.setValue(response.body());
                        Commons.hideProgress();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_GetQuote> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure getquote-->",String.valueOf(t));
            }
        });

        return getquoteData;
    }


}
