package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.DeleteFavourite.Response_DeleteFavourite;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteFavouriteRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_DeleteFavourite> deleteFavouriteMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Response_DeleteFavourite> getDeleteFavouriteMutableLiveData(HashMap<String,String>header,HashMap<String,String>request) {

        retrofitInstance.retrofit().DeleteFavouritecall(header,request).enqueue(new Callback<Response_DeleteFavourite>() {
            @Override
            public void onResponse(Call<Response_DeleteFavourite> call, Response<Response_DeleteFavourite> response) {

                if(response.body()!=null) {
                    deleteFavouriteMutableLiveData.setValue(response.body());
                        Log.e("respo-delfav->",String.valueOf(response.body()));
                        System.out.println("--respo-delfav=" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_DeleteFavourite> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailu-delfav->",String.valueOf(t));
            }
        });

        return deleteFavouriteMutableLiveData;
    }


}
