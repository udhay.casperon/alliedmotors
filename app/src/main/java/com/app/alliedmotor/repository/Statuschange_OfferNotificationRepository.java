package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.NotificationStatusChange.Response_NotificationStatuschange;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Statuschange_OfferNotificationRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_OfferNotify> offerNotifyMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_OfferNotify> offernotifystatuschange(HashMap<String, String> header1, HashMap<String, String> request) {
        retrofitInstance.retrofit().OfferNotifify_change(header1,request).enqueue(new Callback<Response_OfferNotify>() {
            @Override
            public void onResponse(Call<Response_OfferNotify> call, Response<Response_OfferNotify> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    offerNotifyMutableLiveData.setValue(response.body());
                    Log.e("orde cre->",String.valueOf(response.body()));
                    System.out.println("-order cre=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_OfferNotify> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail--order-->",String.valueOf(t));
            }
        });
        return offerNotifyMutableLiveData;
    }
}
