package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.SpareParts_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SparePartsRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<SpareParts_Response> spareParts_responseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<SpareParts_Response> sparepartsinfo(HashMap<String,String>header) {
        retrofitInstance.retrofit().Spareparts_Call(header).enqueue(new Callback<SpareParts_Response>() {
            @Override
            public void onResponse(Call<SpareParts_Response> call, Response<SpareParts_Response> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                    spareParts_responseMutableLiveData.setValue(response.body());
                        Log.e("response--fhd->",String.valueOf(response.body()));
                        System.out.println("--res=65h==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<SpareParts_Response> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailugsressgof-->",String.valueOf(t));
            }
        });

        return spareParts_responseMutableLiveData;
    }


}
