package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.NotificationDelete.Response_NotifyDelete;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteNotificationRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_NotifyDelete> deleteMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_NotifyDelete> delete_notify(HashMap<String, String> header1, HashMap<String, String> request) {
        retrofitInstance.retrofit().ServiceCall_notify_delete(header1,request).enqueue(new Callback<Response_NotifyDelete>() {
            @Override
            public void onResponse(Call<Response_NotifyDelete> call, Response<Response_NotifyDelete> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    deleteMutableLiveData.setValue(response.body());
                    Log.e("orde cre->",String.valueOf(response.body()));
                    System.out.println("-order cre=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_NotifyDelete> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail--order-->",String.valueOf(t));
            }
        });
        return deleteMutableLiveData;
    }
}
