package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.OrderCreation.OrderCreation_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeOrderRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<OrderCreation_Response> orderCreationResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<OrderCreation_Response> orderCreationResponseMutableLiveData(HashMap<String, String> header1,HashMap<String, String> deletequote) {
        retrofitInstance.retrofit().OrderCreation_Call(header1,deletequote).enqueue(new Callback<OrderCreation_Response>() {
            @Override
            public void onResponse(Call<OrderCreation_Response> call, Response<OrderCreation_Response> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    orderCreationResponseMutableLiveData.setValue(response.body());
                    Log.e("orde cre->",String.valueOf(response.body()));
                    System.out.println("-order cre=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<OrderCreation_Response> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail--order-->",String.valueOf(t));
            }
        });
        return orderCreationResponseMutableLiveData;
    }
}
