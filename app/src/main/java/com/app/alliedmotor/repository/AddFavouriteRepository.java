package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_AddFavourite.Response_AddFavourite;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddFavouriteRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_AddFavourite> addFavouriteMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_AddFavourite> addFavourite(HashMap<String,String>header,HashMap<String,String>request) {
        retrofitInstance.retrofit().AddFavouritecall(header,request).enqueue(new Callback<Response_AddFavourite>() {
            @Override
            public void onResponse(Call<Response_AddFavourite> call, Response<Response_AddFavourite> response) {

                if(response.body()!=null) {
                    if(response.body().status.equals("1")) {
                        Commons.hideProgress();
                        addFavouriteMutableLiveData.setValue(response.body());
                        Log.e("response--userpro->",String.valueOf(response.body()));
                        System.out.println("--res=65==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_AddFavourite> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailureuserprof-->",String.valueOf(t));
            }
        });

        return addFavouriteMutableLiveData;
    }


}
