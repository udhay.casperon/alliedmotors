package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.RHDCar.Response_RHDCar;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RHDCarRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_RHDCar> rhdCarMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_RHDCar> getRHDCardataResponse() {
        retrofitInstance.retrofit().RHDCar_ResponseCall().enqueue(new Callback<Response_RHDCar>() {
            @Override
            public void onResponse(Call<Response_RHDCar> call, Response<Response_RHDCar> response) {
                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        rhdCarMutableLiveData.setValue(response.body());
                        Log.e("response-_NewCar->",String.valueOf(response.body()));
                        System.out.println("--res=5Response_NewCar==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_RHDCar> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailResponse_NewC-->",String.valueOf(t));
            }
        });

        return rhdCarMutableLiveData;
    }


}
