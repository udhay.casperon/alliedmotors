package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_GuestMakeOrder;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GuestMakeOrderRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_GuestMakeOrder> guestMakeOrderMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_GuestMakeOrder> guestmakeorder(HashMap<String,String>request) {
        retrofitInstance.retrofit().Call_GuestMakeOrder(request).enqueue(new Callback<Response_GuestMakeOrder>() {
            @Override
            public void onResponse(Call<Response_GuestMakeOrder> call, Response<Response_GuestMakeOrder> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                        guestMakeOrderMutableLiveData.setValue(response.body());
                        Log.e("resp-guestmake->",String.valueOf(response.body()));
                        System.out.println("--res=65==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_GuestMakeOrder> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail guestmake-->",String.valueOf(t));
            }
        });

        return  guestMakeOrderMutableLiveData;
    }


}
