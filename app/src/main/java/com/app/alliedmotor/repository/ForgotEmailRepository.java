package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.ForgotEmail.Response_ForgotEmail;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotEmailRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_ForgotEmail> forgotEmailMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Response_ForgotEmail> getforgotemailResponse(HashMap<String, String> request1) {
        retrofitInstance.retrofit().forgotemail_ServiceCall(request1).enqueue(new Callback<Response_ForgotEmail>() {
            @Override
            public void onResponse(Call<Response_ForgotEmail> call, Response<Response_ForgotEmail> response) {
                Commons.hideProgress();
                if(response.body()!=null)
                {

                        forgotEmailMutableLiveData.setValue(response.body());
                        System.out.println("--res=forgotemail="+response.body().toString());

                }


            }

            @Override
            public void onFailure(Call<Response_ForgotEmail> call, Throwable t) {
                Log.e("OnFailemail-->",String.valueOf(t));
            }

        });

        return forgotEmailMutableLiveData;
    }


}
