package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.ForgotPassword.Response_ForgotPassword;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_ForgotPassword> forgotPasswordMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Response_ForgotPassword> getSignupResponse(HashMap<String, String> request1) {
        retrofitInstance.retrofit().ResponseforgotpasswordCall(request1).enqueue(new Callback<Response_ForgotPassword>() {
            @Override
            public void onResponse(Call<Response_ForgotPassword> call, Response<Response_ForgotPassword> response) {
                if(response.body()!=null)
                {
                    if(response.body().getStatus().equals("1"))
                    {
                        forgotPasswordMutableLiveData.setValue(response.body());
                        System.out.println("--res=forgot pawd=="+response.body().toString());
                    }
                    else if(response.body().getStatus().equals("0"))
                    {

                        System.out.println("--res=forgot pawd=="+response.body().getLongMessage());
                       // Commons.Alert_custom(ctx,response.body().getLongMessage());
                    }
                }
                else {
                    System.out.println("===dd===forgotpwd=="+response.body().toString());
                }

            }

            @Override
            public void onFailure(Call<Response_ForgotPassword> call, Throwable t) {
                Log.e("OnFail forpwd--->",String.valueOf(t));
            }

        });

        return forgotPasswordMutableLiveData;
    }


}
