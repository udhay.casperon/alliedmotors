package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_UserProfile> userProfileMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_UserProfile> getuserprofileResponse(HashMap<String,String>header) {
        retrofitInstance.retrofit().UserProfile_ResponseCall(header).enqueue(new Callback<Response_UserProfile>() {
            @Override
            public void onResponse(Call<Response_UserProfile> call, Response<Response_UserProfile> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        userProfileMutableLiveData.setValue(response.body());
                        Log.e("response--userpro->",String.valueOf(response.body()));
                        System.out.println("--res=65==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_UserProfile> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailureuserprof-->",String.valueOf(t));
            }
        });

        return userProfileMutableLiveData;
    }


}
