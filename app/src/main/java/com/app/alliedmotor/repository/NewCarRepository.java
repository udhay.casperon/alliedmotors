package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.NewCar.NewCar_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCarRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<NewCar_Response> newCarMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<NewCar_Response> getNewCardataResponse() {
        retrofitInstance.retrofit().NewCarcall().enqueue(new Callback<NewCar_Response>() {
            @Override
            public void onResponse(Call<NewCar_Response> call, Response<NewCar_Response> response) {
                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        newCarMutableLiveData.setValue(response.body());
                        Log.e("response-_NewCar->",String.valueOf(response.body()));
                        System.out.println("--res=5Response_NewCar==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<NewCar_Response> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailResponse_NewC-->",String.valueOf(t));
            }
        });

        return newCarMutableLiveData;
    }


}
