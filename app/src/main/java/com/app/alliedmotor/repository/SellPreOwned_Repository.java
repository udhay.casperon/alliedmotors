package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_SellPreOwned;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellPreOwned_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_SellPreOwned> sellPreOwnedMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_SellPreOwned> data_sellPreowned(HashMap<String,String>request) {

        retrofitInstance.retrofit().SellPreOwned_Call(request).enqueue(new Callback<Response_SellPreOwned>() {
            @Override
            public void onResponse(Call<Response_SellPreOwned> call, Response<Response_SellPreOwned> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                    sellPreOwnedMutableLiveData.setValue(response.body());
                        Log.e("respo-sell pre>",String.valueOf(response.body()));
                        System.out.println("--res=sell pre=" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_SellPreOwned> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_sell pre=->",String.valueOf(t));
            }
        });

        return sellPreOwnedMutableLiveData;
    }


}
