package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.NewsEvents.Response_NewsEvents;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsEventsRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_NewsEvents> responseNewsEventsMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_NewsEvents> getprivacyResponse() {

        retrofitInstance.retrofit().NewsEvents_ResponseCall().enqueue(new Callback<Response_NewsEvents>() {
            @Override
            public void onResponse(Call<Response_NewsEvents> call, Response<Response_NewsEvents> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        responseNewsEventsMutableLiveData.setValue(response.body());
                        Log.e("response--pricypol->",String.valueOf(response.body()));
                        System.out.println("--res=5==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_NewsEvents> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_pricypol-->",String.valueOf(t));
            }
        });

        return responseNewsEventsMutableLiveData;
    }


}
