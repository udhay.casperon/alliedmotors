package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.ResetPassword.Response_ResetPassword;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_ResetPassword> resetPasswordMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Response_ResetPassword> getResetpasswordResponse(HashMap<String,String>header) {


        retrofitInstance.retrofit().ResetPassword_call(header).enqueue(new Callback<Response_ResetPassword>() {
            @Override
            public void onResponse(Call<Response_ResetPassword> call, Response<Response_ResetPassword> response) {

                if(response.body()!=null) {
                        Commons.hideProgress();
                        resetPasswordMutableLiveData.setValue(response.body());
                        Log.e("response--resetpa->",String.valueOf(response.body()));
                        System.out.println("--res=resetpassr=" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_ResetPassword> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailu restpa->",String.valueOf(t));
            }
        });

        return resetPasswordMutableLiveData;
    }


}
