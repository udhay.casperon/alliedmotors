package com.app.alliedmotor.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.FAQ_Page.Response_Faq;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FAQRepository {
    Context context;
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_Faq> faqData = new MutableLiveData<>();

    public MutableLiveData<Response_Faq> getfaqdataResponse() {
        Commons.showProgress(context);
        retrofitInstance.retrofit().FAQ_ResponseCall().enqueue(new Callback<Response_Faq>() {
            @Override
            public void onResponse(Call<Response_Faq> call, Response<Response_Faq> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        faqData.setValue(response.body());
                        Log.e("response-faqe->",String.valueOf(response.body()));
                        System.out.println("--res=5==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Response_Faq> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_faq--->",String.valueOf(t));
            }
        });

        return faqData;
    }


}
