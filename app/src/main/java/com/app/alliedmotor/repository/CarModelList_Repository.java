package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarModelList_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_CarModelList> modelListMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_CarModelList> getCarModelList(HashMap<String,String>request) {

        retrofitInstance.retrofit().ServiceModelList_Call(request).enqueue(new Callback<Response_CarModelList>() {
            @Override
            public void onResponse(Call<Response_CarModelList> call, Response<Response_CarModelList> response) {

                if(response.body()!=null) {

                        Commons.hideProgress();
                        modelListMutableLiveData.setValue(response.body());
                        Log.e("respo-modellist->",String.valueOf(response.body()));
                        System.out.println("--res=5modellist=" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<Response_CarModelList> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_codellist=->",String.valueOf(t));
            }
        });

        return modelListMutableLiveData;
    }


}
