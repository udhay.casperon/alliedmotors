package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_OtpRequest;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPRequest_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_OtpRequest> otpRequestMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_OtpRequest> getOtpRequest(HashMap<String, String> inputparam) {

        retrofitInstance.retrofit().Call_otpRequest(inputparam).enqueue(new Callback<Response_OtpRequest>() {
            @Override
            public void onResponse(Call<Response_OtpRequest> call, Response<Response_OtpRequest> response) {

                if (response.body() != null) {
                    Commons.hideProgress();
                    otpRequestMutableLiveData.setValue(response.body());
                    Log.e("Res-otp request->", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<Response_OtpRequest> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFai_otp request--->", String.valueOf(t));
            }
        });

        return otpRequestMutableLiveData;
    }


}
