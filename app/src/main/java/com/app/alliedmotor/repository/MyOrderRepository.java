package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.MyOrderLatest.Response_MyOrderLatest;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_MyOrderLatest> myOrderResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Response_MyOrderLatest> getMyOrderResponse(HashMap<String,String>header) {


        retrofitInstance.retrofit().MyOrderCall(header).enqueue(new Callback<Response_MyOrderLatest>() {
            @Override
            public void onResponse(Call<Response_MyOrderLatest> call, Response<Response_MyOrderLatest> response) {

                if(response.body()!=null) {
                        Commons.hideProgress();
                        myOrderResponseMutableLiveData.setValue(response.body());
                        Log.e("response--userpro->",String.valueOf(response.body()));
                        System.out.println("--res=65=order=" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_MyOrderLatest> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailu ordemy->",String.valueOf(t));
            }
        });

        return myOrderResponseMutableLiveData;
    }


}
