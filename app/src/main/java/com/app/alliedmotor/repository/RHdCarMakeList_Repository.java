package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.RHDCar.Response_RHDCar;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RHdCarMakeList_Repository {
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_RHDCar> selectedmakedata = new MutableLiveData<>();

    public MutableLiveData<Response_RHDCar> getselectedBrand_Data(String url, HashMap<String, String> inputparam) {

        retrofitInstance.retrofit().SelectedRHdMakeList_Call(url,  inputparam).enqueue(new Callback<Response_RHDCar>() {
            @Override
            public void onResponse(Call<Response_RHDCar> call, Response<Response_RHDCar> response) {

                if (response.body() != null) {
                    Commons.hideProgress();
                    selectedmakedata.setValue(response.body());
                    Log.e("Res-seleted brand->", String.valueOf(response.body()));
                }
            }

            @Override
            public void onFailure(Call<Response_RHDCar> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailseleted brand ->", String.valueOf(t));
            }
        });

        return selectedmakedata;
    }


}
