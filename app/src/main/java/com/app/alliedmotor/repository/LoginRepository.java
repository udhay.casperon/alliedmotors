package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<LoginResponse> loginData = new MutableLiveData<>();

    public MutableLiveData<LoginResponse> getLoginResponse(HashMap<String, String> header) {

        retrofitInstance.retrofit().ResponseLoginCall(header).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Commons.hideProgress();
                if(response.body()!=null) {
                        loginData.setValue(response.body());
                        Log.e("response--->",String.valueOf(response.body()));
                        System.out.println("--res=5==" + response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_Login--->",String.valueOf(t));
            }
        });

        return loginData;
    }


}
