package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Response_NotificationList.Response_NotificationList_folder;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationListRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_NotificationList_folder> notificationListMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_NotificationList_folder> getNotificationListMutableLiveData(HashMap<String, String> header1) {
        retrofitInstance.retrofit().NotificationList_call(header1).enqueue(new Callback<Response_NotificationList_folder>() {
            @Override
            public void onResponse(Call<Response_NotificationList_folder> call, Response<Response_NotificationList_folder> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    notificationListMutableLiveData.setValue(response.body());
                    Log.e("notifyur->",String.valueOf(response.body()));
                    System.out.println("-notifyur cre=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_NotificationList_folder> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail--notifyur-->",String.valueOf(t));
            }
        });
        return notificationListMutableLiveData;
    }
}
