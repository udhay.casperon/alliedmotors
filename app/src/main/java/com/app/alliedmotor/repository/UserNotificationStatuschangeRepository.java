package com.app.alliedmotor.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Sample1.Response_UserNotificationstatuschange;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserNotificationStatuschangeRepository {

    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<Response_UserNotificationstatuschange> orderCreationResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Response_UserNotificationstatuschange> notifystatuschange(HashMap<String, String> header1, HashMap<String, String> request) {
        retrofitInstance.retrofit().UserNotificationStatusChange_call(header1,request).enqueue(new Callback<Response_UserNotificationstatuschange>() {
            @Override
            public void onResponse(Call<Response_UserNotificationstatuschange> call, Response<Response_UserNotificationstatuschange> response) {
                if(response.body()!=null) {
                    Commons.hideProgress();
                    orderCreationResponseMutableLiveData.setValue(response.body());
                    Log.e("orde cre->",String.valueOf(response.body()));
                    System.out.println("-order cre=55==" + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Response_UserNotificationstatuschange> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFail--order-->",String.valueOf(t));
            }
        });
        return orderCreationResponseMutableLiveData;
    }
}
