package com.app.alliedmotor.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.alliedmotor.model.Privacy_Policy.PrivacyPolicy_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.WebServiceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyPolicyRepository {
    Context context;
    private WebServiceData retrofitInstance = new WebServiceData();
    private MutableLiveData<PrivacyPolicy_Response> privacypolicy_Data = new MutableLiveData<>();

    public MutableLiveData<PrivacyPolicy_Response> getprivacyResponse() {
        Commons.showProgress(context);
        retrofitInstance.retrofit().PrivacyPolicy_ResponseCall().enqueue(new Callback<PrivacyPolicy_Response>() {
            @Override
            public void onResponse(Call<PrivacyPolicy_Response> call, Response<PrivacyPolicy_Response> response) {

                if(response.body()!=null) {
                    if(response.body().getStatus().equals("1")) {
                        Commons.hideProgress();
                        privacypolicy_Data.setValue(response.body());
                        Log.e("response--pricypol->",String.valueOf(response.body()));
                        System.out.println("--res=5==" + response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<PrivacyPolicy_Response> call, Throwable t) {
                Commons.hideProgress();
                Log.e("OnFailure_pricypol-->",String.valueOf(t));
            }
        });

        return privacypolicy_Data;
    }


}
