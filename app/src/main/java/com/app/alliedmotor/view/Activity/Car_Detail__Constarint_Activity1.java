package com.app.alliedmotor.view.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.viewpager.widget.ViewPager;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.CarDetail.FuelTypeListItem;
import com.app.alliedmotor.model.CarDetail.ModelOptionListItem;
import com.app.alliedmotor.model.CarDetail.Response_CarDetail;
import com.app.alliedmotor.model.CarDetail.VarientListItem;
import com.app.alliedmotor.model.CarDetail12.Service_Cardetail;
import com.app.alliedmotor.model.DeleteFavourite.Response_DeleteFavourite;
import com.app.alliedmotor.model.FilterFuelTypeResponse;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.QuoteDataItemDB;
import com.app.alliedmotor.model.Response_AddFavourite.Response_AddFavourite;
import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.model.Response_FilterModelOption;
import com.app.alliedmotor.model.TechFeatureList_Pojo;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.OnItemClickListener;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomBoldTextview;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_ExtFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_FuelTypeList;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_IFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_ModelOption;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_OtherFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_SafetyFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_TechFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_VarientList;
import com.app.alliedmotor.view.Adapter.CarDetailImageBanner_Adapter;
import com.app.alliedmotor.view.Adapter.Custom_NumberAdapter;
import com.app.alliedmotor.view.Adapter.Detail_Adapter_CarExterior;
import com.app.alliedmotor.view.Adapter.Detail_Adapter_CarInterior;
import com.app.alliedmotor.view.Adapter.SnapHelperOneByOne;
import com.app.alliedmotor.view.Adapter.newscrollimage;
import com.app.alliedmotor.viewmodel.CarDetailviewModel;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Car_Detail__Constarint_Activity1 extends AppCompatActivity implements View.OnClickListener, OnItemClickListener, Detail_Adapter_CarExterior.EColorCategory, Detail_Adapter_CarInterior.IColorCategory {
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;
    private Context context;
    private CardView exteriorcolor, interiorcolor;
    RecyclerView rv_exteriorcolor, rv_interiorcolor;
    ImageView img_share, img_favourite, img_fb, img_twitter, img_instagram, img_whatsapp;
    CustomTextViewSemiBold tv_carname;
    CustomRegularTextview tv_model_yr, tv_model_type, tv_model_type1;
    String sts_vehicletype = "";

    LinearLayout Ext_ll_inner, Ext_ll_middle, Ext_ll_outer, Int_ll_inner, Int_ll_middle, Int_ll_outer;


    CustomRegularButton bt_download_spec, bt_add_to_quote;
    LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    LinearLayout ll_selection1, ll_selection;
    CarDetailviewModel carDetailviewModel;
    SpinKitView spin_kit;

    ArrayList<Integer> number_count = new ArrayList<>();


    static Car_Detail__Constarint_Activity1 object;
    ProgressBar progressbar;
    CustomTextViewSemiBold textView;
    ImageView imageView, imageView26, imageView27;
    ArrayList<String> exteriorfeatures_list = new ArrayList<>();
    ArrayList<String> interiorFeatures_list = new ArrayList<>();
    ArrayList<String> otherFeatures_list = new ArrayList<>();
    ArrayList<String> safetyFeatures_list = new ArrayList<>();
    ArrayList<String> listexterior_color = new ArrayList<>();
    ArrayList<String> listinterior_color = new ArrayList<>();
    Detail_Adapter_CarInterior adapter_carInterior;
    Detail_Adapter_CarExterior adapter_carExterior;
    String intent_carid = "", search_string = "";
    ArrayList<String> carid_deletefav = new ArrayList<>();
    String tempstr;
    String cardetailurl;
    CustomRegularTextview tv_photo_indicator;
    RecyclerView ll_carimage;
    newscrollimage carDetailImageBanner_adapter;
    private int NUM_PAGES;
    ArrayList<String> img_list = new ArrayList<>();
    public boolean isClickedFirstTime = true;
    String select_interioicolor = "", selected_exteriorcolor = "", st_download;
    ConstraintLayout cl_technical_heading, cl_tech_value, cl_interior_heading, cl_interfeatures_value, cl_exterior_heading, cl_exteriorfeatures_value, cl_safety_heading, cl_safetyfeatures_value, cl_other_heading, cl_otherfeatures_value;
    RecyclerView rv_otherfeatures, rc_safetyfeatures, rc_extriorfeatures, rc_interiorfeatures, rv_techfeatures;
    ImageView techarrow, interorarrow, exteriorarrow, safetyarrow, otherarrow;
    ImageView techarrow1, interorarrow1, exteriorarrow1, safetyarrow1, otherarrow1, img_home, ic_cart, ic_dotmenu;
    CarDetailAdapter_IFeatures Ifeatures_Adapter;
    CarDetailAdapter_ExtFeatures Efeatures_adapter;
    CarDetailAdapter_SafetyFeatures safetyfeat_adapter;
    CarDetailAdapter_OtherFeatures otherfeat_adpatr;
    SharedPref sharedPref;
    View view2_tech, view3_inter, view4_exter, view44_safety, view51_other;
    LinearLayout main_ll;
    LinearLayout cv_ll_color, ll_color1;
    CardView cv_favourite;
    RelativeLayout rl_imagecontainer;
    String no_cars_quotes, auth_token;
    int ADDQUOTE_MIN = 1, ADDQUOTE_MAX = 10000;
    HashMap<String, String> header1 = new HashMap<>();
    JSONArray tech_jsonarray;
    File path, dirfile, finalpath;

    String sts_image, sts_qty, sts_modeloption, sts_varientcode, sts_makename, sts_carname, sts_year, sts_modid, sts_makid, sts_carinfo, sts_carcat;
    private static int REQUEST_CODE = 1;
    CarDetailAdapter_TechFeatures carDetailAdapter_techFeatures;

    ArrayList<FilterFuelTypeResponse.Data.ModelOptionListItem> modelOptionList1 = new ArrayList<>();
    ArrayList<Response_FilterModelOption.Data.VarientListItem> modelOptionArrayList = new ArrayList<>();
    int temp_count = 1;

    String sharingUrl = "";
    MyDataBase myDataBase;
    int maxlimit = 8;
    EditText searchview;
    ImageView searhview_icon;
    ImageView ic_menu;
    String slugname_type = "";
    String res_makeid = "", res_modid = "";
    String filter_mkid, filter_modid, filter_eftid;
    String mainimage_url = "";
    ArrayList<FuelTypeListItem> fuelTypeArrayList = new ArrayList<>();
    ArrayList<ModelOptionListItem> modelOptionListItems = new ArrayList<>();
    ArrayList<VarientListItem> varientListItems = new ArrayList<>();

    CarDetailAdapter_VarientList adapter_varientList;
    CarDetailAdapter_FuelTypeList adapter_fuelTypeList;
    CarDetailAdapter_ModelOption adapter_modelOption;
    String dialog_makename = "", dialog_fueltype = "";
    String st_reseftid = "";
    String social_sharing_content = "";

    Dialog FilterDialog;

    ImageView img_leftarrow, img_leftarrow1, img_rightarrow, img_rightarrow1;

    LinearLayout ll_varient, ll_modeloptions;

    CustomRegularTextview tv_IFshowmore1, tv_IFshowless, tv_EFshowmore1, tv_EFshowless, tv_SFshowmore1, tv_SFshowless, tv_OFshowmore1, tv_OFshowless;
    ArrayList<String> interiorFeatures_list_limit = new ArrayList<>();


    ArrayList<String> exteriorfeatures_list_limit = new ArrayList<>();


    ArrayList<String> otherFeatures_list_limit = new ArrayList<>();

    ArrayList<String> safetyFeatures_list_limit = new ArrayList<>();

    String isFavouirte;
    RecyclerView rv_filter, rv_modeloptions, rv_varientoptions;
    String res_EftID = "";

    CardView cv_searchview;
    String st_modelname, st_varientcode, st_resfueltype;
    ArrayList<TechFeatureList_Pojo> techlist = new ArrayList<>();
    ArrayList<TechFeatureList_Pojo> techlist_new = new ArrayList<>();

    private NavigationView navigationView;
    private DrawerLayout drawer;

    CustomRegularButton bt_signin;
    CustomTextViewSemiBold tv_accountname;
    CustomRegularTextview nav_home, nav_favourite, nav_cart, nav_orders, nav_chat, nav_logout, nav_account, nav_notification;
    LinearLayout ll_logout;
    int position_image = 0;
    int fuel_prev = 0, model_prev = 0, varient_prev = 0;
    int findFirstVisibleItemPosition = 0;
    LinearLayoutManager linearLayoutManager5;
    LinearLayoutManager linearLayoutManager61;
    CustomBoldTextview tv_notification_count;
    CardView cv_cartindicator, cv_notificationcount;

    String notify_count = "";
    CustomBoldTextview sidenav_notification_count;
    ArrayList<QuoteDataItemDB> dataItemDBS = new ArrayList<>();

    androidx.cardview.widget.CardView cv_notification_count, cv_nav_cartcount;
    CustomBoldTextview sidenav_cart_count;

    private CallbackManager callbackManager;
    private LoginManager manager;
    LinearLayout loading_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_car_details_constarint);
        carDetailviewModel = ViewModelProviders.of(this).get(CarDetailviewModel.class);
        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        context = this;
        sharedPref = new SharedPref(context);
        myDataBase = new MyDataBase(context);
        findbyviews();
        clicklistener();
        object = this;
        if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN) == "") {
            cv_favourite.setVisibility(View.GONE);
        }

        if (getIntent() != null) {
            if (getIntent().getStringExtra("carid") != null) {
                intent_carid = getIntent().getStringExtra("carid");
                search_string = getIntent().getStringExtra("searchname");
            } else {
                handleIntent(getIntent());
            }
        }
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

      //  haveStoragePermission();

        for (int i = 1; i < 100; i++) {
            number_count.add(i);
        }

        func_loginOrNot();
        Func_AppInfo();




        auth_token = sharedPref.getString(Constants.JWT_TOKEN);
        tech_jsonarray = new JSONArray();

        if (auth_token == null) {
            header1.put("Auth", "");
        } else {
            header1.put("Auth", auth_token);
        }


        Method_CarDetail(intent_carid);


        sharingUrl = Constants.DEEPLINK_URL + "/new-car/?carId=" + intent_carid;
    }


    public boolean haveStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("Permission error", "You have permission");
                return true;
            } else {

                Log.e("Permission error", "You have asked for permission");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //you dont need to worry about these stuff below api level 23
            Log.e("Permission error", "You already have the permission");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Download_file_insta();
    }

    private void func_loginOrNot() {
        if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN) == "") {
            bt_signin.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.GONE);
            ll_logout.setVisibility(View.GONE);
            cv_notificationcount.setVisibility(View.GONE);
            if (myDataBase != null) {
                dataItemDBS = myDataBase.getAlldataforquote();
                if (dataItemDBS.size() == 0) {
                    cv_cartindicator.setVisibility(View.GONE);
                    cv_nav_cartcount.setVisibility(View.GONE);
                } else {
                    cv_cartindicator.setVisibility(View.VISIBLE);
                    cv_nav_cartcount.setVisibility(View.VISIBLE);
                    String cart_count = String.valueOf(dataItemDBS.size());
                    sharedPref.setString(Constants.PREF_Cartcount, cart_count);
                    tv_notification_count.setText(cart_count);
                    sidenav_cart_count.setText(cart_count);

                }
            }

        } else {
            bt_signin.setVisibility(View.GONE);
            ll_logout.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.VISIBLE);
            tv_accountname.setText(sharedPref.getString(Constants.PREF_USERNAME));
            cv_cartindicator.setVisibility(View.VISIBLE);
            cv_notificationcount.setVisibility(View.VISIBLE);
            notify_count = sharedPref.getString(Constants.PREF_UNREAD_NOTIFICATION);
            sidenav_notification_count.setText(notify_count);
            getquote_func();

        }

    }
    //alliedmotors://alliedmotors.com/alliedmotors/api//new-car/?carId=789


    private void Func_AppInfo() {
        HashMap<String, String> header = new HashMap<>();
        if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
            header.put("Auth", "");
            header.put("langname", "en");
        } else {
            header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname", "en");

        }

        carDetailviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();
                if (response_appInfo.getStatus().equals("1")) {
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION, response_appInfo.getData().getUnreadNotificationCount());
                    if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {

                        cv_notificationcount.setVisibility(View.GONE);
                    } else {

                        String count = response_appInfo.getData().getUnreadNotificationCount();
                        if (count.equals("0")|| count==null) {
                            cv_notificationcount.setVisibility(View.GONE);
                        } else {
                            cv_notificationcount.setVisibility(View.VISIBLE);

                            sidenav_notification_count.setText(count);
                        }
                    }

                } else if (response_appInfo.getStatus().equals("0")) {
                    System.out.println("res===" + response_appInfo.toString());
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getquote_func();

        sidenav_notification_count.setText(notify_count);
        func_loginOrNot();
        Func_AppInfo();
    }


    private void getquote_func() {

        HashMap<String, String> header = new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        carDetailviewModel.getquotedata(header).observe(this, new Observer<Response_GetQuote>() {
            @Override
            public void onChanged(Response_GetQuote response_getQuote) {

                if (response_getQuote.getStatus().equals("1")) {
                    String cart_count = String.valueOf(response_getQuote.getData().getQuoteData().size());
                    if (Integer.parseInt(cart_count) == 0) {
                        cv_cartindicator.setVisibility(View.GONE);
                        cv_nav_cartcount.setVisibility(View.GONE);
                    } else {
                        cv_nav_cartcount.setVisibility(View.VISIBLE);
                        sharedPref.setString(Constants.PREF_Cartcount, cart_count);
                        tv_notification_count.setText(cart_count);
                        sidenav_cart_count.setText(sharedPref.getString(Constants.PREF_Cartcount));
                    }
                }
            }

        });


    }

    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
            String recipeId = appLinkData.getLastPathSegment();
            System.out.println("--red===" + recipeId);


            Uri appData = Uri.parse(appLinkData.toString()).buildUpon()
                    .appendPath(recipeId).build();
            Toast.makeText(context, appData.toString(), Toast.LENGTH_SHORT).show();
            String linkdata = appData.getQuery();
            Toast.makeText(context, linkdata.toString(), Toast.LENGTH_SHORT).show();

            String separator = "=";
            int sepPos = linkdata.indexOf(separator);
            if (sepPos == -1) {
                System.out.println("");
            }
            System.out.println("Substring after separator = " + linkdata.substring(sepPos + separator.length()));

            Toast.makeText(context, linkdata.substring(sepPos + separator.length()), Toast.LENGTH_SHORT).show();
            intent_carid = linkdata.substring(sepPos + separator.length());
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {

        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });

    }

    private void findbyviews() {

        sidenav_cart_count = findViewById(R.id.sidenav_cart_count);
        cv_nav_cartcount = findViewById(R.id.cv_nav_cartcount);

        loading_ll = findViewById(R.id.loading_ll);
        sidenav_notification_count = findViewById(R.id.sidenav_notification_count);
        cv_cartindicator = findViewById(R.id.cv_cartindicator);
        cv_notificationcount = findViewById(R.id.cv_notificationcount);

        tv_notification_count = findViewById(R.id.tv_notification_count);
        img_rightarrow = findViewById(R.id.img_rightarrow);
        img_leftarrow = findViewById(R.id.img_leftarrow);
        img_leftarrow1 = findViewById(R.id.img_leftarrow1);
        img_rightarrow1 = findViewById(R.id.img_rightarrow1);

        Ext_ll_inner = findViewById(R.id.Ext_ll_inner);
        Ext_ll_middle = findViewById(R.id.Ext_ll_middle);
        Ext_ll_outer = findViewById(R.id.Ext_ll_outer);
        Int_ll_inner = findViewById(R.id.Int_ll_inner);
        Int_ll_middle = findViewById(R.id.Int_ll_middle);
        Int_ll_outer = findViewById(R.id.Int_ll_outer);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        ll_logout = findViewById(R.id.ll_logout);
        nav_logout = findViewById(R.id.nav_logout);
        nav_home = findViewById(R.id.nav_home);
        nav_cart = findViewById(R.id.nav_cart);
        nav_favourite = findViewById(R.id.nav_favourite);
        nav_orders = findViewById(R.id.nav_orders);
        nav_chat = findViewById(R.id.nav_chat);
        bt_signin = findViewById(R.id.bt_signin);
        tv_accountname = findViewById(R.id.tv_accountname);


        nav_account = findViewById(R.id.nav_account);
        nav_notification = findViewById(R.id.nav_notification);


        ll_selection1 = findViewById(R.id.ll_selection1);
        ll_selection = findViewById(R.id.ll_selection);

        cv_searchview = findViewById(R.id.cv_searchview);

        tv_IFshowmore1 = findViewById(R.id.tv_IFshowmore1);
        tv_IFshowless = findViewById(R.id.tv_IFshowless);

        tv_EFshowmore1 = findViewById(R.id.tv_EFshowmore1);
        tv_EFshowless = findViewById(R.id.tv_EFshowless);

        tv_SFshowmore1 = findViewById(R.id.tv_SFshowmore1);
        tv_SFshowless = findViewById(R.id.tv_SFshowless);

        tv_OFshowmore1 = findViewById(R.id.tv_OFshowmore1);
        tv_OFshowless = findViewById(R.id.tv_OFshowless);


        ic_menu = findViewById(R.id.ic_menu);
        searchview = findViewById(R.id.searchview);
        searhview_icon = findViewById(R.id.searhview_icon);

        progressbar = findViewById(R.id.progressbar);
        //  spin_kit=findViewById(R.id.spin_kit);

        cv_favourite = findViewById(R.id.cv_favourite);

        main_ll = findViewById(R.id.main_ll);
        ll_color1 = findViewById(R.id.ll_color1);
        cv_ll_color = findViewById(R.id.cv_ll_color);
        rl_imagecontainer = findViewById(R.id.rl_imagecontainer);


        //View Lines
        view2_tech = findViewById(R.id.view2);
        view3_inter = findViewById(R.id.view3);
        view4_exter = findViewById(R.id.view4);
        view44_safety = findViewById(R.id.view44);
        view51_other = findViewById(R.id.view51);


        img_home = findViewById(R.id.img_home);
        ic_cart = findViewById(R.id.ic_cart);

        techarrow = findViewById(R.id.imageView);
        interorarrow = findViewById(R.id.imageView1);
        exteriorarrow = findViewById(R.id.imageView2);
        safetyarrow = findViewById(R.id.imageView41);
        otherarrow = findViewById(R.id.imageView51);


        techarrow1 = findViewById(R.id.imageView001);
        interorarrow1 = findViewById(R.id.imageView121);
        exteriorarrow1 = findViewById(R.id.imageView33);
        safetyarrow1 = findViewById(R.id.imageView42);
        otherarrow1 = findViewById(R.id.imageView52);


        cl_technical_heading = findViewById(R.id.cl_technical_heading);
        cl_tech_value = findViewById(R.id.cl_tech_value);
        cl_interior_heading = findViewById(R.id.cl_interior_heading);
        cl_interfeatures_value = findViewById(R.id.cl_interfeatures_value);
        cl_exterior_heading = findViewById(R.id.cl_exterior_heading);
        cl_exteriorfeatures_value = findViewById(R.id.cl_exteriorfeatures_value);
        cl_safety_heading = findViewById(R.id.cl_safety_heading);
        cl_safetyfeatures_value = findViewById(R.id.cl_safetyfeatures_value);
        cl_other_heading = findViewById(R.id.cl_other_heading);
        cl_otherfeatures_value = findViewById(R.id.cl_otherfeatures_value);
        rv_otherfeatures = findViewById(R.id.rv_otherfeatures);
        rc_safetyfeatures = findViewById(R.id.rc_safetyfeatures);
        rc_extriorfeatures = findViewById(R.id.rc_extriorfeatures);
        rc_interiorfeatures = findViewById(R.id.rc_interiorfeatures);


        rv_techfeatures = findViewById(R.id.rv_techfeatures);


        exteriorcolor = findViewById(R.id.cv_exteriorcolor);
        interiorcolor = findViewById(R.id.cv_interiorrcolor);
        ll_carimage = findViewById(R.id.carimage);
        tv_carname = findViewById(R.id.textView3);
        tv_model_yr = findViewById(R.id.tv_model_yr);
        rv_exteriorcolor = findViewById(R.id.rv_exteriorcolor);
        rv_interiorcolor = findViewById(R.id.rv_interior);
        img_share = findViewById(R.id.imageView8);
        img_favourite = findViewById(R.id.imageView9);
        img_fb = findViewById(R.id.imageView10);
        img_twitter = findViewById(R.id.imageView11);
        img_instagram = findViewById(R.id.imageView12);
        img_whatsapp = findViewById(R.id.imageView13);
        tv_model_type = findViewById(R.id.tv_model_type);
        tv_model_type1 = findViewById(R.id.textView6);
        bt_download_spec = findViewById(R.id.button);
        bt_add_to_quote = findViewById(R.id.button2);
        tv_photo_indicator = findViewById(R.id.tv_photo_indicator);


        textView = findViewById(R.id.textView);


        imageView = findViewById(R.id.imageView);


        linearLayoutManager = new LinearLayoutManager(Car_Detail__Constarint_Activity1.this, LinearLayoutManager.HORIZONTAL, false);
        rv_exteriorcolor.setLayoutManager(linearLayoutManager);


        linearLayoutManager1 = new LinearLayoutManager(Car_Detail__Constarint_Activity1.this, LinearLayoutManager.HORIZONTAL, false);
        rv_interiorcolor.setLayoutManager(linearLayoutManager1);

        rv_techfeatures.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        rv_techfeatures.setHasFixedSize(true);


        LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_interiorfeatures.setLayoutManager(linearLayoutManager3);
        rc_interiorfeatures.setHasFixedSize(true);


        LinearLayoutManager linearLayoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_extriorfeatures.setLayoutManager(linearLayoutManager4);
        rc_extriorfeatures.setHasFixedSize(true);

        linearLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_otherfeatures.setLayoutManager(linearLayoutManager5);
        rv_otherfeatures.setHasFixedSize(true);


        linearLayoutManager61 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_safetyfeatures.setLayoutManager(linearLayoutManager61);
        rc_safetyfeatures.setHasFixedSize(true);
    }

    private void clicklistener() {

        nav_account.setOnClickListener(this);
        nav_notification.setOnClickListener(this);
        tv_accountname = findViewById(R.id.tv_accountname);


        nav_home.setOnClickListener(this);
        nav_cart.setOnClickListener(this);
        nav_favourite.setOnClickListener(this);
        nav_orders.setOnClickListener(this);
        nav_chat.setOnClickListener(this);
        bt_signin.setOnClickListener(this);
        nav_logout.setOnClickListener(this);


        bt_download_spec.setOnClickListener(this);
        bt_add_to_quote.setOnClickListener(this);
        cv_favourite.setOnClickListener(this);
        cl_technical_heading.setOnClickListener(this);
        cl_exterior_heading.setOnClickListener(this);
        cl_interior_heading.setOnClickListener(this);
        cl_safety_heading.setOnClickListener(this);
        cl_other_heading.setOnClickListener(this);
        img_home.setOnClickListener(this);
        ic_cart.setOnClickListener(this);

        img_whatsapp.setOnClickListener(this);
        img_share.setOnClickListener(this);
        rl_imagecontainer.setOnClickListener(this);
        searhview_icon.setOnClickListener(this);
        cv_searchview.setOnClickListener(this);
        searchview.setOnClickListener(this);


        tv_IFshowmore1.setOnClickListener(this);
        tv_IFshowless.setOnClickListener(this);


        tv_EFshowmore1.setOnClickListener(this);
        tv_EFshowless.setOnClickListener(this);


        tv_SFshowmore1.setOnClickListener(this);
        tv_SFshowless.setOnClickListener(this);


        tv_OFshowmore1.setOnClickListener(this);
        tv_OFshowless.setOnClickListener(this);
        ic_menu.setOnClickListener(this);

        img_fb.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        img_instagram.setOnClickListener(this);
    }


    private void shareIntagramIntent() {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.instagram.android");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.instagram.android");
            shareIntent.putExtra(Intent.EXTRA_TEXT, dialog_fueltype);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.instagram.android");
            try {
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
                intent.putExtra(android.content.Intent.EXTRA_TEXT, social_sharing_content);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/jpeg");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.instagram.android"));
            startActivity(intent);
        }
    }

    private void shareFacebookIntent() {
        ShareDialog shareDialog;
        FacebookSdk.sdkInitialize(getApplicationContext());
        shareDialog = new ShareDialog(this);

        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setQuote(social_sharing_content)
                .setContentUrl(Uri.parse(mainimage_url))
                .build();

        shareDialog.show(linkContent);

        /*Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.facebook.katana");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.facebook.katana");
            shareIntent.putExtra(Intent.EXTRA_TEXT, dialog_fueltype);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.facebook.katana");
            try {
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
                shareIntent.putExtra(Intent.EXTRA_TITLE, social_sharing_content);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/*");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.facebook.katana"));
            startActivity(intent);
        }*/
    }

    private void shareLinkedInIntent() {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.linkedin.android");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.linkedin.android");
            shareIntent.putExtra(Intent.EXTRA_TEXT, dialog_fueltype);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.linkedin.android");
            try {
                /*shareIntent.putExtra(Intent.EXTRA_STREAM,
                        Uri.parse(finalpath.toString()));*/
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
                intent.putExtra(android.content.Intent.EXTRA_TEXT, social_sharing_content);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/jpeg");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.linkedin.android"));
            startActivity(intent);
        }
    }

    private void shareWhatsappIntent() {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.whatsapp");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra(Intent.EXTRA_TEXT, dialog_fueltype);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.whatsapp");
            try {
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
                intent.putExtra(Intent.EXTRA_SUBJECT, social_sharing_content);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/jpeg");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.whatsapp"));
            startActivity(intent);
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.nav_account:
                Intent tohomes = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohomes.putExtra("tabposition", "useraccount");
                startActivity(tohomes);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case R.id.nav_notification:
                drawer.closeDrawer(Gravity.RIGHT);
                Intent tonotifify = new Intent(this, NotificationList_Activity.class);
                startActivity(tonotifify);
                break;


            case R.id.ic_menu:
                drawer.openDrawer(GravityCompat.END);
                break;
            case R.id.nav_home:
                Intent tohome = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohome.putExtra("tabposition", "home");
                startActivity(tohome);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case R.id.nav_favourite:
                Intent tocart = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart.putExtra("tabposition", "cart");
                tocart.putExtra("position_tabs", "0");
                startActivity(tocart);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case R.id.nav_cart:
                Intent tocart1 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart1.putExtra("tabposition", "cart");
                tocart1.putExtra("position_tabs", "1");
                startActivity(tocart1);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case R.id.nav_orders:
                Intent tocart2 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart2.putExtra("tabposition", "cart");
                tocart2.putExtra("position_tabs", "2");
                startActivity(tocart2);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case R.id.nav_chat:
                Intent tocart3 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart3.putExtra("tabposition", "chat");
                startActivity(tocart3);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case R.id.nav_logout:
                Intent gotoLogin = new Intent(context, LoginActivity.class);
                sharedPref.clearAll();
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoLogin);
                drawer.closeDrawer(Gravity.RIGHT);
                finish();
                break;

            case R.id.bt_signin:
                Intent tosignin = new Intent(this, LoginActivity.class);
                startActivity(tosignin);
                drawer.closeDrawer(Gravity.RIGHT);
                finish();
                break;
            case R.id.tv_IFshowmore1:
                tv_IFshowmore1.setVisibility(View.GONE);
                tv_IFshowless.setVisibility(View.VISIBLE);
                Ifeatures_Adapter = new CarDetailAdapter_IFeatures(context, interiorFeatures_list);
                rc_interiorfeatures.setAdapter(Ifeatures_Adapter);
                break;
            case R.id.tv_IFshowless:
                tv_IFshowmore1.setVisibility(View.VISIBLE);
                tv_IFshowless.setVisibility(View.GONE);
                Ifeatures_Adapter = new CarDetailAdapter_IFeatures(context, interiorFeatures_list_limit);
                rc_interiorfeatures.setAdapter(Ifeatures_Adapter);
                break;

            case R.id.tv_EFshowmore1:
                tv_EFshowmore1.setVisibility(View.GONE);
                tv_EFshowless.setVisibility(View.VISIBLE);
                Efeatures_adapter = new CarDetailAdapter_ExtFeatures(context, exteriorfeatures_list);
                rc_extriorfeatures.setAdapter(Efeatures_adapter);
                break;
            case R.id.tv_EFshowless:
                tv_EFshowmore1.setVisibility(View.VISIBLE);
                tv_EFshowless.setVisibility(View.GONE);
                Efeatures_adapter = new CarDetailAdapter_ExtFeatures(context, exteriorfeatures_list_limit);
                rc_extriorfeatures.setAdapter(Efeatures_adapter);
                break;

            case R.id.tv_SFshowmore1:
                tv_SFshowmore1.setVisibility(View.GONE);
                tv_SFshowless.setVisibility(View.VISIBLE);
                safetyfeat_adapter = new CarDetailAdapter_SafetyFeatures(context, safetyFeatures_list);
                rc_safetyfeatures.setAdapter(safetyfeat_adapter);
                break;
            case R.id.tv_SFshowless:
                tv_SFshowmore1.setVisibility(View.VISIBLE);
                tv_SFshowless.setVisibility(View.GONE);
                safetyfeat_adapter = new CarDetailAdapter_SafetyFeatures(context, safetyFeatures_list_limit);
                rc_safetyfeatures.setAdapter(safetyfeat_adapter);
                break;


            case R.id.tv_OFshowmore1:
                tv_OFshowmore1.setVisibility(View.GONE);
                tv_OFshowless.setVisibility(View.VISIBLE);
                otherfeat_adpatr = new CarDetailAdapter_OtherFeatures(context, otherFeatures_list);
                rv_otherfeatures.setAdapter(otherfeat_adpatr);
                break;
            case R.id.tv_OFshowless:
                tv_OFshowmore1.setVisibility(View.VISIBLE);
                tv_OFshowless.setVisibility(View.GONE);
                otherfeat_adpatr = new CarDetailAdapter_OtherFeatures(context, otherFeatures_list_limit);
                rv_otherfeatures.setAdapter(otherfeat_adpatr);
                break;

            case R.id.cv_favourite:
                Func_Select_UnSelect_Favourite();
                break;
            case R.id.rl_imagecontainer:
                ToImageView();
                break;

            case R.id.img_home:
                onBackPressed();
                finish();
                break;
            case R.id.ic_cart:
                Intent tocartpage = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocartpage.putExtra("tabposition", "cart");
                tocartpage.putExtra("position_tabs", "1");
                startActivity(tocartpage);
                finish();
                break;
            case R.id.button:
                Commons.showProgress(context);
                Intent pdfIntent = new Intent(context, Download_Specification.class);
                pdfIntent.putExtra("download_specification", st_download);
                pdfIntent.putExtra("downloadtitle", social_sharing_content);
                startActivity(pdfIntent);
                break;

            case R.id.button2:
                dialog_addquote();
                break;
            case R.id.imageView8:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Constants.DEEPLINK_URL + "/new-car/?carId=" + intent_carid);
                sharingIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(sharingIntent);
                break;

            case R.id.imageView9:
                // favourite
                break;

            case R.id.imageView10:
                // fb
                shareFacebookIntent();

               /* ShareLinkContent fbShare = new ShareLinkContent.Builder()
                        .setImageUrl(Uri.fromFile(finalpath))
                        .setContentUrl(Uri.parse("www.google.com"))
                        .build();
                ShareDialog.show(context, fbShare);*/
                break;

            case R.id.imageView11:
                // twitter or LinkedIn
                shareLinkedInIntent();
                break;

            case R.id.imageView12:
                // instagram
                shareIntagramIntent();
                break;

            case R.id.imageView13:
                // whatsapp
                shareWhatsappIntent();
                // AppInstalled.isAppInstalled("com.whatsapp", context);
                break;

            case R.id.searchview:
            case R.id.searhview_icon:
            case R.id.cv_searchview:
                Intent tofilteractivity = new Intent(this, DetailPage_FilterActivity.class);
                tofilteractivity.putExtra("searchtextname", dialog_fueltype);
                tofilteractivity.putExtra("makename", dialog_makename);
                tofilteractivity.putExtra("response_makeid", res_makeid);
                tofilteractivity.putExtra("response_modid", res_modid);
                tofilteractivity.putExtra("fueltypearraylist", fuelTypeArrayList);
                tofilteractivity.putExtra("modeloptionlist", modelOptionListItems);
                tofilteractivity.putExtra("varientlist", varientListItems);
                tofilteractivity.putExtra("namefuel", st_resfueltype);
                tofilteractivity.putExtra("eftid", st_reseftid);
                tofilteractivity.putExtra("namemodel", st_modelname);
                tofilteractivity.putExtra("namevarient", st_varientcode);
                startActivityForResult(tofilteractivity, 1);
                // Dialog_filter();
                break;

            case R.id.cl_technical_heading:
                if (isClickedFirstTime) {
                    isClickedFirstTime = false;
                    cl_tech_value.setVisibility(View.VISIBLE);
                    techarrow1.setVisibility(View.VISIBLE);
                    techarrow.setVisibility(View.GONE);

                } else {
                    isClickedFirstTime = true;
                    cl_tech_value.setVisibility(View.GONE);
                    techarrow1.setVisibility(View.GONE);
                    techarrow.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.cl_interior_heading:
                if (isClickedFirstTime) {
                    interorarrow1.setVisibility(View.VISIBLE);
                    interorarrow.setVisibility(View.GONE);
                    isClickedFirstTime = false;
                    cl_interfeatures_value.setVisibility(View.VISIBLE);
                } else {
                    interorarrow1.setVisibility(View.GONE);
                    interorarrow.setVisibility(View.VISIBLE);
                    isClickedFirstTime = true;
                    cl_interfeatures_value.setVisibility(View.GONE);
                }
                break;

            case R.id.cl_exterior_heading:
                if (isClickedFirstTime) {
                    exteriorarrow1.setVisibility(View.VISIBLE);
                    exteriorarrow.setVisibility(View.GONE);

                    isClickedFirstTime = false;
                    cl_exteriorfeatures_value.setVisibility(View.VISIBLE);
                } else {
                    exteriorarrow1.setVisibility(View.GONE);
                    exteriorarrow.setVisibility(View.VISIBLE);
                    isClickedFirstTime = true;
                    cl_exteriorfeatures_value.setVisibility(View.GONE);
                }
                break;
            case R.id.cl_safety_heading:
                if (isClickedFirstTime) {
                    safetyarrow.setVisibility(View.GONE);
                    safetyarrow1.setVisibility(View.VISIBLE);
                    isClickedFirstTime = false;
                    cl_safetyfeatures_value.setVisibility(View.VISIBLE);
                } else {

                    safetyarrow1.setVisibility(View.GONE);
                    safetyarrow.setVisibility(View.VISIBLE);
                    isClickedFirstTime = true;
                    cl_safetyfeatures_value.setVisibility(View.GONE);
                }
                break;

            case R.id.cl_other_heading:
                if (isClickedFirstTime) {
                    otherarrow.setVisibility(View.GONE);
                    otherarrow1.setVisibility(View.VISIBLE);
                    isClickedFirstTime = false;
                    cl_otherfeatures_value.setVisibility(View.VISIBLE);
                } else {
                    otherarrow.setVisibility(View.VISIBLE);
                    otherarrow1.setVisibility(View.GONE);
                    isClickedFirstTime = true;
                    cl_otherfeatures_value.setVisibility(View.GONE);
                }
                break;


        }

    }


    private void Download_file_insta(){
        String filename=mainimage_url;
        File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES ).getPath()+"/AlliedMotors/" + filename);
        Log.d("Environment", "Environment extraData=" + file.getPath());

        DownloadManager.Request request=new DownloadManager.Request(Uri.parse(mainimage_url))
                .setTitle(filename)
                .setDescription("Downloading")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationUri(Uri.fromFile(file))
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true);
        DownloadManager downloadManager= (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        long referenceID = downloadManager.enqueue(request);
        finalpath = file;

    }


  /*  private void Download_file_insta() {


        path = new File("/alliedmotors/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date()) + ".jpg");
        dirfile = new File(path, Environment.DIRECTORY_DOWNLOADS);
        finalpath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date()) + ".jpg");


        DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(mainimage_url));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setTitle("Downloading");
        request.setDescription("Please wait...");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, String.valueOf(finalpath));
        downloadManager.enqueue(request);


    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                intent_carid = data.getStringExtra("carid");
                selected_exteriorcolor = "";
                select_interioicolor = "";
                Method_CarDetail(intent_carid);

            }
        }


    }

    private void Method_CarDetail(String carid) {
        techlist.clear();
        techlist_new.clear();
        listinterior_color.clear();
        listexterior_color.clear();
        exteriorfeatures_list.clear();
        interiorFeatures_list.clear();
        otherFeatures_list.clear();
        safetyFeatures_list.clear();
        interiorFeatures_list_limit.clear();
        exteriorfeatures_list_limit.clear();
        otherFeatures_list_limit.clear();
        safetyFeatures_list_limit.clear();
        img_list.clear();
        loading_ll.setVisibility(View.VISIBLE);
        // Commons.showProgress(context);
        HashMap<String, String> request_cardetail = new HashMap<>();
        request_cardetail.put("ext_color", selected_exteriorcolor);
        request_cardetail.put("int_color", select_interioicolor);

        Log.e("-->cardetail-req->", request_cardetail.toString());
        System.out.println("==req--car detil--request-------" + request_cardetail + "---" + header1);

        cardetailurl = Constants.BASE_URL + "v1/car-detail/" + carid;
        new Service_Cardetail(cardetailurl, header1, request_cardetail, new ResponseInterface.CardetailInterface() {
            @Override
            public void onCardetailSuccess(Response_CarDetail response_carDetail) {
                Commons.hideProgress();
                loading_ll.setVisibility(View.GONE);
                main_ll.setVisibility(View.VISIBLE);
                System.out.println("===res=car details==" + response_carDetail.toString());
                if (response_carDetail.getStatus().equals("1")) {
                    // To Denote Favourite
                    isFavouirte = response_carDetail.getData().getCarDetails().getIsFavorite();
                    if (isFavouirte.equals("1")) {
                        img_favourite.setImageResource(R.drawable.ic_favourite_selected);
                    } else if (isFavouirte.equals("0")) {
                        img_favourite.setImageResource(R.drawable.ic_heart_1);
                    }

                    listinterior_color = response_carDetail.getData().getCarGalleryInteriorColors();
                    listexterior_color = response_carDetail.getData().getCarGalleryExteriorColors();
                    // To display in dialog
                    st_download = response_carDetail.getData().getCarDetails().getPdfFile();
                    img_list = response_carDetail.getData().getCarGalleryImages();
                    NUM_PAGES = img_list.size();
                    callrecycle();
                    filter_mkid = response_carDetail.getData().getCarDetails().getMkId();
                    filter_modid = response_carDetail.getData().getCarDetails().getModId();
                    filter_eftid = response_carDetail.getData().getCarDetails().getEftId();
                    fuelTypeArrayList = response_carDetail.getData().getFuelTypeList();
                    modelOptionListItems = response_carDetail.getData().getModelOptionList();
                    varientListItems = response_carDetail.getData().getVarientList();
                    mainimage_url = response_carDetail.getData().getCarDetails().getCarImage();
                    exteriorfeatures_list = response_carDetail.getData().getCarExteriorFeatures().getFeatures();
                    interiorFeatures_list = response_carDetail.getData().getCarInteriorFeatures().getFeatures();
                    otherFeatures_list = response_carDetail.getData().getCarOtherFeatures().getFeatures();
                    safetyFeatures_list = response_carDetail.getData().getCarSafetyFeatures().getFeatures();

                    if (response_carDetail.getData().getCarDetails().getIsFavorite().equals("0")) {
                        slugname_type = "new-car";
                        sts_carinfo = slugname_type;
                    } else if (response_carDetail.getData().getCarDetails().getIsFavorite().equals("1")) {
                        slugname_type = "favorite";
                        sts_carinfo = slugname_type;
                    }


                    sts_carname = response_carDetail.getData().getCarDetails().getModelName();
                    sts_image = response_carDetail.getData().getCarDetails().getCarImage();
                    sts_year = response_carDetail.getData().getCarDetails().getModelYear();

                    sts_varientcode = response_carDetail.getData().getCarDetails().getVarientCode();
                    sts_modeloption = response_carDetail.getData().getCarDetails().getModelOptions();
                    sts_makename = response_carDetail.getData().getCarDetails().getMakename();
                    sts_makid = response_carDetail.getData().getCarDetails().getMkId();
                    sts_modid = response_carDetail.getData().getCarDetails().getModId();

                    sts_vehicletype = response_carDetail.getData().getCarDetails().getVehiceType();
                    sts_carcat = response_carDetail.getData().getCarDetails().getCatid();


                    if (sts_vehicletype.contains("(")) {
                        Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(sts_vehicletype);
                        while (m.find()) {
                            System.out.println("===ss===" + m.group(1));
                            sts_vehicletype = m.group(1);
                            //  holder.tv_vehicle_type.setText(m.group(1));
                        }
                    } else {
                        System.out.println("===ss===" + sts_vehicletype);
                        // holder.tv_vehicle_type.setText(AppInstalled.toTitleCase(vehicle_type.toLowerCase()));
                    }


                    res_makeid = response_carDetail.getData().getCarDetails().getMkId();
                    res_modid = response_carDetail.getData().getCarDetails().getModId();
                    // Interior Feautures
                    if (interiorFeatures_list.size() >= 9) {
                        for (int d = 0; d < 8; d++) {
                            interiorFeatures_list_limit.add(interiorFeatures_list.get(d));
                        }

                    }

                   /* else if(interiorFeatures_list.size()==7)
                    {
                        interiorFeatures_list_limit.addAll(interiorFeatures_list);
                        tv_IFshowless.setVisibility(View.GONE);
                        tv_IFshowmore1.setVisibility(View.GONE);
                    }*/
                    else {
                        interiorFeatures_list_limit.addAll(interiorFeatures_list);
                        tv_IFshowless.setVisibility(View.GONE);
                        tv_IFshowmore1.setVisibility(View.GONE);
                    }


                    // Exterior Features Limitation
                    if (exteriorfeatures_list.size() >= 9) {
                        for (int d = 0; d < 8; d++) {
                            exteriorfeatures_list_limit.add(exteriorfeatures_list.get(d));
                        }
                    }/*else if(exteriorfeatures_list.size()==7)
                    {
                        exteriorfeatures_list_limit.addAll(exteriorfeatures_list);
                        tv_EFshowless.setVisibility(View.GONE);
                        tv_EFshowmore1.setVisibility(View.GONE);
                    }*/ else {
                        exteriorfeatures_list_limit.addAll(exteriorfeatures_list);
                        tv_EFshowless.setVisibility(View.GONE);
                        tv_EFshowmore1.setVisibility(View.GONE);
                    }

                    // Safety Features Limitation

                    if (safetyFeatures_list.size() >= 9) {
                        for (int d = 0; d < 8; d++) {
                            safetyFeatures_list_limit.add(safetyFeatures_list.get(d));
                        }

                    } /*else if(safetyFeatures_list.size()==7)
                    {
                        safetyFeatures_list_limit.addAll(safetyFeatures_list);
                        tv_SFshowless.setVisibility(View.GONE);
                        tv_SFshowmore1.setVisibility(View.GONE);
                    }*/ else {
                        safetyFeatures_list_limit.addAll(safetyFeatures_list);
                        tv_SFshowless.setVisibility(View.GONE);
                        tv_SFshowmore1.setVisibility(View.GONE);
                    }


                    // Other Features Limitation
                    if (otherFeatures_list.size() >= 9) {
                        for (int d = 0; d < 8; d++) {
                            otherFeatures_list_limit.add(otherFeatures_list.get(d));
                        }

                    }/*else if(otherFeatures_list.size()==7)
                    {
                        otherFeatures_list_limit.addAll(otherFeatures_list);
                        tv_OFshowless.setVisibility(View.GONE);
                        tv_OFshowmore1.setVisibility(View.GONE);
                    }*/ else {
                        otherFeatures_list_limit.addAll(otherFeatures_list);
                        tv_OFshowless.setVisibility(View.GONE);
                        tv_OFshowmore1.setVisibility(View.GONE);
                    }
                    st_reseftid = response_carDetail.getData().getCarDetails().getEftId();
                    st_resfueltype = response_carDetail.getData().getCarDetails().getEfuelType();
                    st_modelname = response_carDetail.getData().getCarDetails().getModelOptions();
                    st_varientcode = response_carDetail.getData().getCarDetails().getVarientCode();

                    tv_model_type.setText(st_varientcode);
                    tv_model_type1.setText(st_modelname);

                    if (st_resfueltype != null && st_modelname != null && st_varientcode != null) {
                        dialog_fueltype = st_resfueltype + "-" + st_modelname + "-" + st_varientcode;
                        searchview.setText(dialog_fueltype);
                    } else if (st_resfueltype != null && st_varientcode != null) {
                        dialog_fueltype = st_resfueltype + "-" + st_varientcode;
                        searchview.setText(dialog_fueltype);
                    } else if (st_resfueltype != null && st_modelname != null) {
                        dialog_fueltype = st_resfueltype + "-" + st_modelname;
                    } else if (st_modelname != null && st_varientcode != null) {
                        dialog_fueltype = st_modelname + "-" + st_varientcode;
                    }


                    if (response_carDetail.getData().getCarDetails().getModelName() != null) {
                        dialog_makename = response_carDetail.getData().getCarDetails().getMakename() + " " + (response_carDetail.getData().getCarDetails().getModelName());
                        tv_carname.setText(dialog_makename);
                    } else {
                        dialog_makename = response_carDetail.getData().getCarDetails().getMakename();
                        tv_carname.setText(dialog_makename);
                    }
                    tv_model_yr.setText(response_carDetail.getData().getCarDetails().getModelYear());

                    social_sharing_content = dialog_makename + "-" + dialog_fueltype;
                    TechFeatureList_Pojo tech1 = new TechFeatureList_Pojo("Engine", response_carDetail.getData().getCarTechnicalFeatures().engine, R.drawable.t_modelcode1_48);
                    TechFeatureList_Pojo tech3 = new TechFeatureList_Pojo("Spec Type", response_carDetail.getData().getCarTechnicalFeatures().specType, R.drawable.t_spectype48);
                    TechFeatureList_Pojo tech4 = new TechFeatureList_Pojo("Output", response_carDetail.getData().getCarTechnicalFeatures().output, R.drawable.t_output_48);
                    TechFeatureList_Pojo tech5 = new TechFeatureList_Pojo("Torque", response_carDetail.getData().getCarTechnicalFeatures().torque, R.drawable.t_torque_1);
                    TechFeatureList_Pojo tech6 = new TechFeatureList_Pojo("Fuel Type", response_carDetail.getData().getCarTechnicalFeatures().fuelType, R.drawable.t_fueltype48);


                    TechFeatureList_Pojo tech7 = new TechFeatureList_Pojo("Transmission:", response_carDetail.getData().getCarTechnicalFeatures().transmission, R.drawable.t_transmission48);
                    TechFeatureList_Pojo tech8 = new TechFeatureList_Pojo("Terrian", response_carDetail.getData().getCarTechnicalFeatures().terrain, R.drawable.t_terrian48);
                    TechFeatureList_Pojo tech9 = new TechFeatureList_Pojo("Emission Control", response_carDetail.getData().getCarTechnicalFeatures().emissionControl, R.drawable.t_emission48);
                    TechFeatureList_Pojo tech10 = new TechFeatureList_Pojo("Dimension", response_carDetail.getData().getCarTechnicalFeatures().dimensions, R.drawable.t_dimension48);
                    TechFeatureList_Pojo tech11 = new TechFeatureList_Pojo("WheelBase", response_carDetail.getData().getCarTechnicalFeatures().wheelbase, R.drawable.t_wheelbase48);
                    TechFeatureList_Pojo tech12 = new TechFeatureList_Pojo("Ground Clearance", response_carDetail.getData().getCarTechnicalFeatures().groundClearance, R.drawable.t_groundclearnce48);


                    TechFeatureList_Pojo tech13 = new TechFeatureList_Pojo("Gross Weight", response_carDetail.getData().getCarTechnicalFeatures().grossWeight, R.drawable.t_grossweight48);
                    TechFeatureList_Pojo tech14 = new TechFeatureList_Pojo("Kerb Weight", response_carDetail.getData().getCarTechnicalFeatures().kerbWeight, R.drawable.t_kerbweight48);
                    TechFeatureList_Pojo tech15 = new TechFeatureList_Pojo("Seating Capacity", response_carDetail.getData().getCarTechnicalFeatures().seatingCapacity, R.drawable.t_seating_capacity48);
                    TechFeatureList_Pojo tech16 = new TechFeatureList_Pojo("Doors", response_carDetail.getData().getCarTechnicalFeatures().doors, R.drawable.t_door48);
                    TechFeatureList_Pojo tech17 = new TechFeatureList_Pojo("Fuel Tank Capacity", response_carDetail.getData().getCarTechnicalFeatures().fuelTankCapacity, R.drawable.t_fueltank48);
                    TechFeatureList_Pojo tech18 = new TechFeatureList_Pojo("Tyre Size", response_carDetail.getData().getCarTechnicalFeatures().tyresSize, R.drawable.t_tyresize48);


                    TechFeatureList_Pojo tech19 = new TechFeatureList_Pojo("Wheel Type", response_carDetail.getData().getCarTechnicalFeatures().wheelType, R.drawable.t_wheeltype48);
                    TechFeatureList_Pojo tech20 = new TechFeatureList_Pojo("Spare Wheel Carrier", response_carDetail.getData().getCarTechnicalFeatures().spareWheelCarrier, R.drawable.t_sparewheel48);
                    TechFeatureList_Pojo tech21 = new TechFeatureList_Pojo("Front Suspension", response_carDetail.getData().getCarTechnicalFeatures().frontSuspension, R.drawable.t_frontsuspension48);
                    TechFeatureList_Pojo tech22 = new TechFeatureList_Pojo("Rear Suspension", response_carDetail.getData().getCarTechnicalFeatures().rearSuspension, R.drawable.t_rear_suspenion48);
                    TechFeatureList_Pojo tech23 = new TechFeatureList_Pojo("Cargo Space", response_carDetail.getData().getCarTechnicalFeatures().cargoSpace, R.drawable.t_cargospace48);
                    TechFeatureList_Pojo tech24 = new TechFeatureList_Pojo("Electric Motor", response_carDetail.getData().getCarTechnicalFeatures().electricMotor, R.drawable.t_electricmotor48);


                    TechFeatureList_Pojo tech25 = new TechFeatureList_Pojo("Fuel Economy", response_carDetail.getData().getCarTechnicalFeatures().fuelEconomy, R.drawable.t_fuel_economy48);
                    TechFeatureList_Pojo tech26 = new TechFeatureList_Pojo("Motor", response_carDetail.getData().getCarTechnicalFeatures().motor, R.drawable.t_motors_2); //not used
                    TechFeatureList_Pojo tech27 = new TechFeatureList_Pojo("Payload(KG)", response_carDetail.getData().getCarTechnicalFeatures().payload, R.drawable.t_payload48);
                    TechFeatureList_Pojo tech28 = new TechFeatureList_Pojo("Hybrid System Net Output", response_carDetail.getData().getCarTechnicalFeatures().hybridSystemNetOutput, R.drawable.t_hybridsysnetoutput48);
                    TechFeatureList_Pojo tech29 = new TechFeatureList_Pojo("Output-Combined", response_carDetail.getData().getCarTechnicalFeatures().outputHpCombined, R.drawable.t_output_hp48);
                    TechFeatureList_Pojo tech30 = new TechFeatureList_Pojo("Max Output(HP)", response_carDetail.getData().getCarTechnicalFeatures().maxOutputSystem, R.drawable.t_maxoutput_power48);


                    TechFeatureList_Pojo tech31 = new TechFeatureList_Pojo("Max Torque", response_carDetail.getData().getCarTechnicalFeatures().maxTorque, R.drawable.t_torque48);//
                    TechFeatureList_Pojo tech32 = new TechFeatureList_Pojo("Max Power", response_carDetail.getData().getCarTechnicalFeatures().maxPower, R.drawable.t_maxpowerhprpm48);
                    TechFeatureList_Pojo tech33 = new TechFeatureList_Pojo("Output(HP)", response_carDetail.getData().getCarTechnicalFeatures().outputHp, R.drawable.t_outputhp_48);
                    TechFeatureList_Pojo tech34 = new TechFeatureList_Pojo("Power Output", response_carDetail.getData().getCarTechnicalFeatures().powerOutput, R.drawable.t_power_output48);
                    TechFeatureList_Pojo tech35 = new TechFeatureList_Pojo("Torque(Kg-M/RPM)Engine", response_carDetail.getData().getCarTechnicalFeatures().torqueEngine, R.drawable.t_torque_rpm48);//
                    TechFeatureList_Pojo tech36 = new TechFeatureList_Pojo("Torque(NM)", response_carDetail.getData().getCarTechnicalFeatures().torqueNm, R.drawable.max_torque);
                    TechFeatureList_Pojo tech37 = new TechFeatureList_Pojo("Torque Motor", response_carDetail.getData().getCarTechnicalFeatures().torqueMotor, R.drawable.t_torque48);

                    techlist.add(tech1);

                    techlist.add(tech3);
                    techlist.add(tech4);
                    techlist.add(tech5);
                    techlist.add(tech6);
                    techlist.add(tech7);

                    techlist.add(tech8);
                    techlist.add(tech9);
                    techlist.add(tech10);
                    techlist.add(tech11);
                    techlist.add(tech12);
                    techlist.add(tech13);
                    techlist.add(tech14);

                    techlist.add(tech15);
                    techlist.add(tech16);
                    techlist.add(tech17);
                    techlist.add(tech18);
                    techlist.add(tech19);
                    techlist.add(tech20);
                    techlist.add(tech21);

                    techlist.add(tech22);
                    techlist.add(tech23);
                    techlist.add(tech24);
                    techlist.add(tech25);
                    techlist.add(tech26);
                    techlist.add(tech27);
                    techlist.add(tech28);

                    techlist.add(tech29);
                    techlist.add(tech30);
                    techlist.add(tech31);
                    techlist.add(tech32);
                    techlist.add(tech33);
                    techlist.add(tech34);
                    techlist.add(tech35);
                    techlist.add(tech36);
                    techlist.add(tech37);


                    String val1 = "";
                    for (int j = 0; j < techlist.size(); j++) {
                        val1 = techlist.get(j).Value;
                        if (val1.length() != 0) {
                            techlist_new.add(techlist.get(j));
                        }
                    }

                    carDetailAdapter_techFeatures = new CarDetailAdapter_TechFeatures(context, techlist_new);
                    rv_techfeatures.setAdapter(carDetailAdapter_techFeatures);


                    // start Interior color condition
                    if (listinterior_color.size() == 0) {
                        interiorcolor.setVisibility(View.GONE);
                        cv_ll_color.setVisibility(View.GONE);
                        ll_color1.setVisibility(View.GONE);
                        Int_ll_inner.setVisibility(View.GONE);
                        img_leftarrow1.setVisibility(View.GONE);
                        img_rightarrow1.setVisibility(View.GONE);
                    } else {

                        if (response_carDetail.getData().getCarGalleryInteriorColors().size() == 0) {
                            ll_selection.setVisibility(View.GONE);
                        } else {
                            if (response_carDetail.getData().getCarGalleryInteriorColors().size() > 3) {
                                img_leftarrow1.setVisibility(View.VISIBLE);
                                img_rightarrow1.setVisibility(View.VISIBLE);
                                if (select_interioicolor.equals("")) {
                                    select_interioicolor = response_carDetail.getData().getCarGalleryInteriorColors().get(0);

                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(response_carDetail.getData().getCarGalleryInteriorColors().get(0))));

                                } else {

                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));
                                    //  tv_tv_interior_color_value.setCardBackgroundColor((Color.parseColor(select_interioicolor)));
                                }
                            } else {
                                img_leftarrow1.setVisibility(View.GONE);
                                img_rightarrow1.setVisibility(View.GONE);
                                if (select_interioicolor.equals("")) {
                                    select_interioicolor = response_carDetail.getData().getCarGalleryInteriorColors().get(0);
                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));
                                } else {
                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));
                                    //  tv_tv_interior_color_value.setCardBackgroundColor((Color.parseColor(select_interioicolor)));
                                }
                            }
                        }
                        adapter_carInterior = new Detail_Adapter_CarInterior(context, listinterior_color, Car_Detail__Constarint_Activity1.this::onIColorSelected, select_interioicolor);
                        rv_interiorcolor.setAdapter(adapter_carInterior);
                    }


                    img_leftarrow1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (rv_interiorcolor != null) {
                                final RecyclerView.LayoutManager layoutManager = rv_interiorcolor
                                        .getLayoutManager();
                                if (layoutManager instanceof LinearLayoutManager) {
                                    int lastVisibleItem = ((LinearLayoutManager) layoutManager)
                                            .findFirstVisibleItemPosition();
                                    if (lastVisibleItem - 4 > 0) {
                                        lastVisibleItem = lastVisibleItem - 4;
                                    } else {
                                        lastVisibleItem = 0;
                                    }
                                    rv_interiorcolor.scrollToPosition(lastVisibleItem);
                                }
                            }
                        }
                    });


                    img_rightarrow1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (rv_interiorcolor != null) {
                                final RecyclerView.LayoutManager layoutManager = rv_interiorcolor
                                        .getLayoutManager();
                                if (layoutManager instanceof LinearLayoutManager) {
                                    int lastVisibleItem = ((LinearLayoutManager) layoutManager)
                                            .findLastVisibleItemPosition();
                                    if (lastVisibleItem + 3 > listinterior_color.size()) {
                                        lastVisibleItem = lastVisibleItem + 3;
                                    } else {
                                        lastVisibleItem = listinterior_color.size() - 1;
                                    }
                                    rv_interiorcolor.scrollToPosition(lastVisibleItem);
                                }
                            }

                        }
                    });


                    rv_interiorcolor.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);


                            if (linearLayoutManager1.findFirstCompletelyVisibleItemPosition() == 0) {
                                img_leftarrow1.setVisibility(View.INVISIBLE);
                            } else {
                                img_leftarrow1.setVisibility(View.VISIBLE);
                            }

                            if (linearLayoutManager1.findLastCompletelyVisibleItemPosition() == listinterior_color.size() - 1) {
                                img_rightarrow1.setVisibility(View.INVISIBLE);
                            } else
                                img_rightarrow1.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                        }
                    });

                    rv_interiorcolor.scrollToPosition(getPositionInterior(select_interioicolor));


                    // start Exterior  color condition
                    if (listexterior_color.size() == 0) {
                        cv_ll_color.setVisibility(View.GONE);
                        exteriorcolor.setVisibility(View.GONE);
                        ll_color1.setVisibility(View.GONE);
                        img_leftarrow.setVisibility(View.GONE);
                        img_rightarrow.setVisibility(View.GONE);

                    } else {
                        if (response_carDetail.getData().getCarGalleryExteriorColors().size() == 0) {
                            //tv_extcolor_value.setVisibility(View.GONE);
                            ll_selection1.setVisibility(View.GONE);
                            img_leftarrow.setVisibility(View.GONE);
                            img_rightarrow.setVisibility(View.GONE);
                        } else {

                            if (response_carDetail.getData().getCarGalleryExteriorColors().size() > 3) {
                                img_leftarrow.setVisibility(View.VISIBLE);
                                img_rightarrow.setVisibility(View.VISIBLE);
                                if (selected_exteriorcolor.equals("")) {

                                    selected_exteriorcolor = response_carDetail.getData().getCarGalleryExteriorColors().get(0);
                                    Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(response_carDetail.getData().getCarGalleryExteriorColors().get(0))));

                                } else {

                                    Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(selected_exteriorcolor)));
                                }
                            } else {
                                img_leftarrow.setVisibility(View.GONE);
                                img_rightarrow.setVisibility(View.GONE);
                                if (selected_exteriorcolor.equals("")) {

                                    selected_exteriorcolor = response_carDetail.getData().getCarGalleryExteriorColors().get(0);
                                    Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(response_carDetail.getData().getCarGalleryExteriorColors().get(0))));

                                } else {

                                    Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(selected_exteriorcolor)));
                                }
                            }


                        }
                        adapter_carExterior = new Detail_Adapter_CarExterior(context, listexterior_color, Car_Detail__Constarint_Activity1.this::onEColorSelected, selected_exteriorcolor);
                        rv_exteriorcolor.setAdapter(adapter_carExterior);
                    }

                    Download_file_insta();
                    img_leftarrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (rv_exteriorcolor != null) {
                                final RecyclerView.LayoutManager layoutManager = rv_exteriorcolor
                                        .getLayoutManager();
                                if (layoutManager instanceof LinearLayoutManager) {
                                    int lastVisibleItem = ((LinearLayoutManager) layoutManager)
                                            .findFirstVisibleItemPosition();
                                    if (lastVisibleItem - 4 > 0) {
                                        lastVisibleItem = lastVisibleItem - 4;
                                    } else {
                                        lastVisibleItem = 0;
                                    }
                                    rv_exteriorcolor.scrollToPosition(lastVisibleItem);
                                }
                            }
                        }
                    });


                    img_rightarrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (rv_exteriorcolor != null) {
                                final RecyclerView.LayoutManager layoutManager = rv_exteriorcolor
                                        .getLayoutManager();
                                if (layoutManager instanceof LinearLayoutManager) {
                                    int lastVisibleItem = ((LinearLayoutManager) layoutManager)
                                            .findLastVisibleItemPosition();
                                    if (lastVisibleItem + 3 > listexterior_color.size()) {
                                        lastVisibleItem = lastVisibleItem + 3;
                                    } else {
                                        lastVisibleItem = listexterior_color.size() - 1;
                                    }
                                    rv_exteriorcolor.scrollToPosition(lastVisibleItem);
                                }
                            }

                        }
                    });

                    rv_exteriorcolor.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);


                            if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                                img_leftarrow.setVisibility(View.INVISIBLE);
                            } else {
                                img_leftarrow.setVisibility(View.VISIBLE);
                            }

                            if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == listexterior_color.size() - 1) {
                                img_rightarrow.setVisibility(View.INVISIBLE);
                            } else
                                img_rightarrow.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                        }
                    });

                    rv_exteriorcolor.scrollToPosition(getPosition(selected_exteriorcolor));

                    // start Interior Features List condition
                    if (interiorFeatures_list.size() == 0) {
                        cl_interior_heading.setVisibility(View.GONE);
                        view3_inter.setVisibility(View.GONE);
                        tv_IFshowmore1.setVisibility(View.GONE);
                    } else {

                        Ifeatures_Adapter = new CarDetailAdapter_IFeatures(context, interiorFeatures_list_limit);
                        rc_interiorfeatures.setAdapter(Ifeatures_Adapter);
                    }


                    // start Exterior Features List condition

                    if (exteriorfeatures_list.size() == 0) {
                        cl_exterior_heading.setVisibility(View.GONE);
                        view4_exter.setVisibility(View.GONE);
                        tv_EFshowmore1.setVisibility(View.GONE);
                    } else {

                        Efeatures_adapter = new CarDetailAdapter_ExtFeatures(context, exteriorfeatures_list_limit);
                        rc_extriorfeatures.setAdapter(Efeatures_adapter);
                    }


                    // start Other Features List condition

                    if (otherFeatures_list.size() == 0) {
                        cl_other_heading.setVisibility(View.GONE);
                        view51_other.setVisibility(View.GONE);
                        tv_OFshowmore1.setVisibility(View.GONE);
                    } else {
                        otherfeat_adpatr = new CarDetailAdapter_OtherFeatures(context, otherFeatures_list_limit);
                        rv_otherfeatures.setAdapter(otherfeat_adpatr);

                    }

                    // start Safety Features List condition


                    if (safetyFeatures_list.size() == 0) {
                        cl_safety_heading.setVisibility(View.GONE);
                        view44_safety.setVisibility(View.GONE);
                        tv_SFshowmore1.setVisibility(View.GONE);
                    } else {
                        rc_safetyfeatures.setVisibility(View.VISIBLE);
                        safetyfeat_adapter = new CarDetailAdapter_SafetyFeatures(context, safetyFeatures_list_limit);
                        rc_safetyfeatures.setAdapter(safetyfeat_adapter);


                    }




                    /*ll_carimage.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            ToImageView();
                        }
                    });*/

                   /* ll_carimage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                            ll_carimage.getCurrentItem();
                            tv_photo_indicator.setText((ll_carimage.getCurrentItem() + 1) + "/" + NUM_PAGES);
                        }

                        @Override
                        public void onPageSelected(int position) {
                            position_image = position;

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });*/

                } else if (response_carDetail.getStatus().equals("0")) {

                    System.out.println("A_Repsonse-->" + response_carDetail.getMessage());
                    Toast.makeText(context, response_carDetail.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

        });


        //ViewModel
       /* carDetailviewModel.getCarDetaildata(cardetailurl, header1, request_cardetail).observe(this, new Observer<Response_CarDetail>() {
            @Override
            public void onChanged(Response_CarDetail response_carDetail) {
                loading_ll.setVisibility(View.GONE);
                main_ll.setVisibility(View.VISIBLE);
                Commons.hideProgress();
                System.out.println("===res=car details==" + response_carDetail.toString());
                   if (response_carDetail.getStatus().equals("1")) {
                   // To Denote Favourite
                    isFavouirte = response_carDetail.getData().getCarDetails().getIsFavorite();
                    if (isFavouirte.equals("1")) {
                        img_favourite.setImageResource(R.drawable.ic_favourite_selected);
                    } else if (isFavouirte.equals("0")) {
                        img_favourite.setImageResource(R.drawable.ic_heart_1);
                    }

                    listinterior_color = response_carDetail.getData().getCarGalleryInteriorColors();
                    listexterior_color = response_carDetail.getData().getCarGalleryExteriorColors();

                    filter_mkid = response_carDetail.getData().getCarDetails().getMkId();
                    filter_modid = response_carDetail.getData().getCarDetails().getModId();
                    filter_eftid = response_carDetail.getData().getCarDetails().getEftId();
                    fuelTypeArrayList = response_carDetail.getData().getFuelTypeList();
                    modelOptionListItems = response_carDetail.getData().getModelOptionList();
                    varientListItems = response_carDetail.getData().getVarientList();

                    exteriorfeatures_list = response_carDetail.getData().getCarExteriorFeatures().getFeatures();
                    interiorFeatures_list = response_carDetail.getData().getCarInteriorFeatures().getFeatures();
                    otherFeatures_list = response_carDetail.getData().getCarOtherFeatures().getFeatures();
                    safetyFeatures_list = response_carDetail.getData().getCarSafetyFeatures().getFeatures();

                    if (response_carDetail.getData().getCarDetails().getIsFavorite().equals("0")) {
                        slugname_type = "new-car";
                        sts_carinfo = slugname_type;
                    } else if (response_carDetail.getData().getCarDetails().getIsFavorite().equals("1")) {
                        slugname_type = "favorite";
                        sts_carinfo = slugname_type;
                    }


                    sts_carname = response_carDetail.getData().getCarDetails().getModelName();
                    sts_image = response_carDetail.getData().getCarDetails().getCarImage();
                    sts_year = response_carDetail.getData().getCarDetails().getModelYear();

                    sts_varientcode = response_carDetail.getData().getCarDetails().getVarientCode();
                    sts_modeloption = response_carDetail.getData().getCarDetails().getModelOptions();
                    sts_makename = response_carDetail.getData().getCarDetails().getMakename();
                    sts_makid = response_carDetail.getData().getCarDetails().getMkId();
                    sts_modid = response_carDetail.getData().getCarDetails().getModId();

                    sts_carcat = response_carDetail.getData().getCarDetails().getCatid();


                    res_makeid = response_carDetail.getData().getCarDetails().getMkId();
                    res_modid = response_carDetail.getData().getCarDetails().getModId();
                    // Interior Feautures
                    if (interiorFeatures_list.size() >= 9) {
                        for (int d = 0; d < 8; d++) {
                            interiorFeatures_list_limit.add(interiorFeatures_list.get(d));
                        }

                    } else {
                        interiorFeatures_list_limit.addAll(interiorFeatures_list);
                        tv_IFshowless.setVisibility(View.GONE);
                        tv_IFshowmore1.setVisibility(View.GONE);
                    }


                    // Exterior Features Limitation

                    // Safety Features Limitation

                    if (safetyFeatures_list.size() >=9) {
                        for (int d = 0; d < 8; d++) {
                            safetyFeatures_list_limit.add(safetyFeatures_list.get(d));
                        }

                    } else {
                        safetyFeatures_list_limit.addAll(safetyFeatures_list);
                        tv_SFshowless.setVisibility(View.GONE);
                        tv_SFshowmore1.setVisibility(View.GONE);
                    }


                    // Other Features Limitation
                    if (otherFeatures_list.size() >= 9) {
                        for (int d = 0; d < 8; d++) {
                            otherFeatures_list_limit.add(otherFeatures_list.get(d));
                        }

                    } else {
                        otherFeatures_list_limit.addAll(otherFeatures_list);
                        tv_OFshowless.setVisibility(View.GONE);
                        tv_OFshowmore1.setVisibility(View.GONE);
                    }
                   st_reseftid=response_carDetail.getData().getCarDetails().getEftId();
                    st_resfueltype = response_carDetail.getData().getCarDetails().getEfuelType();
                   st_modelname = response_carDetail.getData().getCarDetails().getModelOptions();
                   st_varientcode = response_carDetail.getData().getCarDetails().getVarientCode();

                   tv_model_type.setText(st_varientcode);
                   tv_model_type1.setText(st_modelname);

                        if(st_resfueltype!=null && st_modelname!=null && st_varientcode!=null)
                        {
                            dialog_fueltype = st_resfueltype + "-" + st_modelname + "-" + st_varientcode;
                            searchview.setText(dialog_fueltype);
                        }
                      else if(st_resfueltype!=null && st_varientcode!=null)
                        {
                            dialog_fueltype = st_resfueltype +"-" + st_varientcode;
                            searchview.setText(dialog_fueltype);
                        }
                        else  if(st_resfueltype!=null && st_modelname!=null)
                        {
                            dialog_fueltype = st_resfueltype +"-" + st_modelname;
                        }
                        else  if(st_modelname!=null && st_varientcode!=null)
                   {
                       dialog_fueltype = st_modelname +"-" + st_varientcode;
                   }


                   // To display in dialog
                    st_download = response_carDetail.getData().getCarDetails().getPdfFile();
                    img_list = response_carDetail.getData().getCarGalleryImages();
                    if(response_carDetail.getData().getCarDetails().getModelName()!=null) {
                        dialog_makename = response_carDetail.getData().getCarDetails().getMakename()+ " " + (response_carDetail.getData().getCarDetails().getModelName());
                        tv_carname.setText(dialog_makename);
                    }
                    else
                    {
                        dialog_makename=response_carDetail.getData().getCarDetails().getMakename();
                        tv_carname.setText(dialog_makename);
                    }
                    tv_model_yr.setText(response_carDetail.getData().getCarDetails().getModelYear());



                    TechFeatureList_Pojo tech1 = new TechFeatureList_Pojo("Engine", response_carDetail.getData().getCarTechnicalFeatures().engine, R.drawable.t_modelcode1_48);
                    TechFeatureList_Pojo tech3 = new TechFeatureList_Pojo("Spec Type", response_carDetail.getData().getCarTechnicalFeatures().specType, R.drawable.t_spectype48);
                    TechFeatureList_Pojo tech4 = new TechFeatureList_Pojo("Output", response_carDetail.getData().getCarTechnicalFeatures().output, R.drawable.t_output_48);
                    TechFeatureList_Pojo tech5 = new TechFeatureList_Pojo("Torque", response_carDetail.getData().getCarTechnicalFeatures().torque, R.drawable.t_torque_1);
                    TechFeatureList_Pojo tech6 = new TechFeatureList_Pojo("Fuel Type", response_carDetail.getData().getCarTechnicalFeatures().fuelType, R.drawable.t_fueltype48);


                    TechFeatureList_Pojo tech7 = new TechFeatureList_Pojo("Transmission:", response_carDetail.getData().getCarTechnicalFeatures().transmission, R.drawable.t_transmission48);
                    TechFeatureList_Pojo tech8 = new TechFeatureList_Pojo("Terrian", response_carDetail.getData().getCarTechnicalFeatures().terrain, R.drawable.t_terrian48);
                    TechFeatureList_Pojo tech9 = new TechFeatureList_Pojo("Emission Control", response_carDetail.getData().getCarTechnicalFeatures().emissionControl, R.drawable.t_emission48);
                    TechFeatureList_Pojo tech10 = new TechFeatureList_Pojo("Dimension", response_carDetail.getData().getCarTechnicalFeatures().dimensions, R.drawable.t_dimension48);
                    TechFeatureList_Pojo tech11 = new TechFeatureList_Pojo("WheelBase", response_carDetail.getData().getCarTechnicalFeatures().wheelbase, R.drawable.t_wheelbase48);
                    TechFeatureList_Pojo tech12 = new TechFeatureList_Pojo("Ground Clearance", response_carDetail.getData().getCarTechnicalFeatures().groundClearance, R.drawable.t_groundclearnce48);


                    TechFeatureList_Pojo tech13 = new TechFeatureList_Pojo("Gross Weight", response_carDetail.getData().getCarTechnicalFeatures().grossWeight, R.drawable.t_grossweight48);
                    TechFeatureList_Pojo tech14 = new TechFeatureList_Pojo("Kerb Weight", response_carDetail.getData().getCarTechnicalFeatures().kerbWeight, R.drawable.t_kerbweight48);
                    TechFeatureList_Pojo tech15 = new TechFeatureList_Pojo("Seating Capacity", response_carDetail.getData().getCarTechnicalFeatures().seatingCapacity, R.drawable.t_seating_capacity48);
                    TechFeatureList_Pojo tech16 = new TechFeatureList_Pojo("Doors", response_carDetail.getData().getCarTechnicalFeatures().doors, R.drawable.t_door48);
                    TechFeatureList_Pojo tech17 = new TechFeatureList_Pojo("Fuel Tank Capacity", response_carDetail.getData().getCarTechnicalFeatures().fuelTankCapacity, R.drawable.t_fueltank48);
                    TechFeatureList_Pojo tech18 = new TechFeatureList_Pojo("Tyre Size", response_carDetail.getData().getCarTechnicalFeatures().tyresSize, R.drawable.t_tyresize48);


                    TechFeatureList_Pojo tech19 = new TechFeatureList_Pojo("Wheel Type", response_carDetail.getData().getCarTechnicalFeatures().wheelType, R.drawable.t_wheeltype48);
                    TechFeatureList_Pojo tech20 = new TechFeatureList_Pojo("Spare Wheel Carrier", response_carDetail.getData().getCarTechnicalFeatures().spareWheelCarrier, R.drawable.t_sparewheel48);
                    TechFeatureList_Pojo tech21 = new TechFeatureList_Pojo("Front Suspension", response_carDetail.getData().getCarTechnicalFeatures().frontSuspension, R.drawable.t_frontsuspension48);
                    TechFeatureList_Pojo tech22 = new TechFeatureList_Pojo("Rear Suspension", response_carDetail.getData().getCarTechnicalFeatures().rearSuspension, R.drawable.t_rear_suspenion48);
                    TechFeatureList_Pojo tech23 = new TechFeatureList_Pojo("Cargo Space", response_carDetail.getData().getCarTechnicalFeatures().cargoSpace, R.drawable.t_cargospace48);
                    TechFeatureList_Pojo tech24 = new TechFeatureList_Pojo("Electric Motor", response_carDetail.getData().getCarTechnicalFeatures().electricMotor, R.drawable.t_electricmotor48);


                    TechFeatureList_Pojo tech25 = new TechFeatureList_Pojo("Fuel Economy", response_carDetail.getData().getCarTechnicalFeatures().fuelEconomy, R.drawable.t_fuel_economy48);
                    TechFeatureList_Pojo tech26 = new TechFeatureList_Pojo("Motor", response_carDetail.getData().getCarTechnicalFeatures().motor, R.drawable.t_motors_2); //not used
                    TechFeatureList_Pojo tech27 = new TechFeatureList_Pojo("Payload(KG)", response_carDetail.getData().getCarTechnicalFeatures().payload, R.drawable.t_payload48);
                    TechFeatureList_Pojo tech28 = new TechFeatureList_Pojo("Hybrid System Net Output", response_carDetail.getData().getCarTechnicalFeatures().hybridSystemNetOutput, R.drawable.t_hybridsysnetoutput48);
                    TechFeatureList_Pojo tech29 = new TechFeatureList_Pojo("Output-Combined", response_carDetail.getData().getCarTechnicalFeatures().outputHpCombined, R.drawable.t_output_hp48);
                    TechFeatureList_Pojo tech30 = new TechFeatureList_Pojo("Max Output(HP)", response_carDetail.getData().getCarTechnicalFeatures().maxOutputSystem, R.drawable.t_maxoutput_power48);


                    TechFeatureList_Pojo tech31 = new TechFeatureList_Pojo("Max Torque", response_carDetail.getData().getCarTechnicalFeatures().maxTorque, R.drawable.t_torque48);//
                    TechFeatureList_Pojo tech32 = new TechFeatureList_Pojo("Max Power", response_carDetail.getData().getCarTechnicalFeatures().maxPower, R.drawable.t_maxpowerhprpm48);
                    TechFeatureList_Pojo tech33 = new TechFeatureList_Pojo("Output(HP)", response_carDetail.getData().getCarTechnicalFeatures().outputHp, R.drawable.t_outputhp_48);
                    TechFeatureList_Pojo tech34 = new TechFeatureList_Pojo("Power Output", response_carDetail.getData().getCarTechnicalFeatures().powerOutput, R.drawable.t_power_output48);
                    TechFeatureList_Pojo tech35 = new TechFeatureList_Pojo("Torque(Kg-M/RPM)Engine", response_carDetail.getData().getCarTechnicalFeatures().torqueEngine, R.drawable.t_torque_rpm48);//
                    TechFeatureList_Pojo tech36 = new TechFeatureList_Pojo("Torque(NM)", response_carDetail.getData().getCarTechnicalFeatures().torqueNm, R.drawable.max_torque);
                   TechFeatureList_Pojo tech37 = new TechFeatureList_Pojo("Torque Motor", response_carDetail.getData().getCarTechnicalFeatures().torqueMotor, R.drawable.t_torque48);

                    techlist.add(tech1);

                    techlist.add(tech3);
                    techlist.add(tech4);
                    techlist.add(tech5);
                    techlist.add(tech6);
                    techlist.add(tech7);

                    techlist.add(tech8);
                    techlist.add(tech9);
                    techlist.add(tech10);
                    techlist.add(tech11);
                    techlist.add(tech12);
                    techlist.add(tech13);
                    techlist.add(tech14);

                    techlist.add(tech15);
                    techlist.add(tech16);
                    techlist.add(tech17);
                    techlist.add(tech18);
                    techlist.add(tech19);
                    techlist.add(tech20);
                    techlist.add(tech21);

                    techlist.add(tech22);
                    techlist.add(tech23);
                    techlist.add(tech24);
                    techlist.add(tech25);
                    techlist.add(tech26);
                    techlist.add(tech27);
                    techlist.add(tech28);

                    techlist.add(tech29);
                    techlist.add(tech30);
                    techlist.add(tech31);
                    techlist.add(tech32);
                    techlist.add(tech33);
                    techlist.add(tech34);
                    techlist.add(tech35);
                    techlist.add(tech36);
                   techlist.add(tech37);


                    String val1 = "";
                    for (int j = 0; j < techlist.size(); j++) {
                        val1 = techlist.get(j).Value;
                        if (val1.length() != 0) {
                            techlist_new.add(techlist.get(j));
                        }
                    }

                    carDetailAdapter_techFeatures = new CarDetailAdapter_TechFeatures(context, techlist_new);
                    rv_techfeatures.setAdapter(carDetailAdapter_techFeatures);



                    // start Interior color condition
                    if (listinterior_color.size() == 0) {
                        interiorcolor.setVisibility(View.GONE);
                        cv_ll_color.setVisibility(View.GONE);
                        ll_color1.setVisibility(View.GONE);
                        Int_ll_inner.setVisibility(View.GONE);
                        img_leftarrow1.setVisibility(View.GONE);
                        img_rightarrow1.setVisibility(View.GONE);
                    } else {

                        if (response_carDetail.getData().getCarGalleryInteriorColors().size() == 0) {
                            ll_selection.setVisibility(View.GONE);
                        } else {
                            if(response_carDetail.getData().getCarGalleryInteriorColors().size()>3) {
                                img_leftarrow1.setVisibility(View.VISIBLE);
                                img_rightarrow1.setVisibility(View.VISIBLE);
                                if (select_interioicolor.equals("")) {
                                    select_interioicolor = response_carDetail.getData().getCarGalleryInteriorColors().get(0);

                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(response_carDetail.getData().getCarGalleryInteriorColors().get(0))));

                                } else {

                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));
                                    //  tv_tv_interior_color_value.setCardBackgroundColor((Color.parseColor(select_interioicolor)));
                                }
                            }
                            else
                            {
                                img_leftarrow1.setVisibility(View.GONE);
                                img_rightarrow1.setVisibility(View.GONE);
                                if (select_interioicolor.equals("")) {
                                    select_interioicolor = response_carDetail.getData().getCarGalleryInteriorColors().get(0);
                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));
                                } else {
                                    Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));
                                    //  tv_tv_interior_color_value.setCardBackgroundColor((Color.parseColor(select_interioicolor)));
                                }
                            }
                        }
                        adapter_carInterior = new Detail_Adapter_CarInterior(context, listinterior_color, Car_Detail__Constarint_Activity1.this::onIColorSelected,select_interioicolor);
                        rv_interiorcolor.setAdapter(adapter_carInterior);
                    }


                   img_leftarrow1.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           if (rv_interiorcolor != null) {
                               final RecyclerView.LayoutManager layoutManager = rv_interiorcolor
                                       .getLayoutManager();
                               if (layoutManager instanceof LinearLayoutManager) {
                                   int lastVisibleItem = ((LinearLayoutManager) layoutManager)
                                           .findFirstVisibleItemPosition();
                                   if(lastVisibleItem-4 > 0) {
                                       lastVisibleItem = lastVisibleItem-4;
                                   } else {
                                       lastVisibleItem = 0;
                                   }
                                   rv_interiorcolor.scrollToPosition(lastVisibleItem);
                               }
                           }
                       }
                   });


                   img_rightarrow1.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           if (rv_interiorcolor != null) {
                               final RecyclerView.LayoutManager layoutManager = rv_interiorcolor
                                       .getLayoutManager();
                               if (layoutManager instanceof LinearLayoutManager) {
                                   int lastVisibleItem =  ((LinearLayoutManager) layoutManager)
                                           .findLastVisibleItemPosition();
                                   if(lastVisibleItem + 3 > listinterior_color.size()) {
                                       lastVisibleItem = lastVisibleItem + 3;
                                   } else {
                                       lastVisibleItem = listinterior_color.size() - 1;
                                   }
                                   rv_interiorcolor.scrollToPosition(lastVisibleItem);
                               }
                           }

                       }
                   });


                   rv_interiorcolor.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);


                            if (linearLayoutManager1.findFirstCompletelyVisibleItemPosition()==0){
                                img_leftarrow1.setVisibility(View.INVISIBLE);
                            }else{
                                img_leftarrow1.setVisibility(View.VISIBLE);
                            }

                            if (linearLayoutManager1.findLastCompletelyVisibleItemPosition()==listinterior_color.size()-1){
                                img_rightarrow1.setVisibility(View.INVISIBLE);
                            }else
                                img_rightarrow1.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                        }
                    });

                   rv_interiorcolor.scrollToPosition(getPositionInterior(select_interioicolor));

                 // start Exterior  color condition
                    if (listexterior_color.size() == 0) {
                        cv_ll_color.setVisibility(View.GONE);
                        exteriorcolor.setVisibility(View.GONE);
                        ll_color1.setVisibility(View.GONE);
                        img_leftarrow.setVisibility(View.GONE);
                        img_rightarrow.setVisibility(View.GONE);

                    } else {
                        if (response_carDetail.getData().getCarGalleryExteriorColors().size() == 0) {
                            //tv_extcolor_value.setVisibility(View.GONE);
                            ll_selection1.setVisibility(View.GONE);
                            img_leftarrow.setVisibility(View.GONE);
                            img_rightarrow.setVisibility(View.GONE);
                        } else {

                            if(response_carDetail.getData().getCarGalleryExteriorColors().size()>3)
                                {
                                    img_leftarrow.setVisibility(View.VISIBLE);
                                    img_rightarrow.setVisibility(View.VISIBLE);
                                    if (selected_exteriorcolor.equals("")) {

                                        selected_exteriorcolor = response_carDetail.getData().getCarGalleryExteriorColors().get(0);
                                        Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                        DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(response_carDetail.getData().getCarGalleryExteriorColors().get(0))));

                                    } else {

                                        Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                        Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                        DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(selected_exteriorcolor)));
                                    }
                                }
                            else
                            {
                                img_leftarrow.setVisibility(View.GONE);
                                img_rightarrow.setVisibility(View.GONE);
                                if (selected_exteriorcolor.equals("")) {

                                    selected_exteriorcolor = response_carDetail.getData().getCarGalleryExteriorColors().get(0);
                                    Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(response_carDetail.getData().getCarGalleryExteriorColors().get(0))));

                                } else {

                                    Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                    Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                    DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(selected_exteriorcolor)));
                                }
                            }





                        }
                        adapter_carExterior = new Detail_Adapter_CarExterior(context, listexterior_color, Car_Detail__Constarint_Activity1.this::onEColorSelected,selected_exteriorcolor);
                        rv_exteriorcolor.setAdapter(adapter_carExterior);
                    }



                    img_leftarrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (rv_exteriorcolor != null) {
                                final RecyclerView.LayoutManager layoutManager = rv_exteriorcolor
                                        .getLayoutManager();
                                if (layoutManager instanceof LinearLayoutManager) {
                                    int lastVisibleItem = ((LinearLayoutManager) layoutManager)
                                            .findFirstVisibleItemPosition();
                                    if(lastVisibleItem-4 > 0) {
                                        lastVisibleItem = lastVisibleItem-4;
                                    } else {
                                        lastVisibleItem = 0;
                                    }
                                    rv_exteriorcolor.scrollToPosition(lastVisibleItem);
                                }
                            }
                        }
                    });


                    img_rightarrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (rv_exteriorcolor != null) {
                                final RecyclerView.LayoutManager layoutManager = rv_exteriorcolor
                                        .getLayoutManager();
                                if (layoutManager instanceof LinearLayoutManager) {
                                    int lastVisibleItem =  ((LinearLayoutManager) layoutManager)
                                            .findLastVisibleItemPosition();
                                    if(lastVisibleItem + 3 > listexterior_color.size()) {
                                        lastVisibleItem = lastVisibleItem + 3;
                                    } else {
                                        lastVisibleItem = listexterior_color.size() - 1;
                                    }
                                    rv_exteriorcolor.scrollToPosition(lastVisibleItem);
                                }
                            }

                        }
                    });

                    rv_exteriorcolor.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);


                            if (linearLayoutManager.findFirstCompletelyVisibleItemPosition()==0){
                                img_leftarrow.setVisibility(View.INVISIBLE);
                            }else{
                                img_leftarrow.setVisibility(View.VISIBLE);
                            }

                            if (linearLayoutManager.findLastCompletelyVisibleItemPosition()==listexterior_color.size()-1){
                                img_rightarrow.setVisibility(View.INVISIBLE);
                            }else
                                img_rightarrow.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                        }
                    });

                   rv_exteriorcolor.scrollToPosition(getPosition(selected_exteriorcolor));

                    // start Interior Features List condition
                    if (interiorFeatures_list.size() == 0) {
                        cl_interior_heading.setVisibility(View.GONE);
                        view3_inter.setVisibility(View.GONE);
                        tv_IFshowmore1.setVisibility(View.GONE);
                    } else {

                        Ifeatures_Adapter = new CarDetailAdapter_IFeatures(context, interiorFeatures_list_limit);
                        rc_interiorfeatures.setAdapter(Ifeatures_Adapter);
                    }


                    // start Exterior Features List condition

                    if (exteriorfeatures_list.size() == 0) {
                        cl_exterior_heading.setVisibility(View.GONE);
                        view4_exter.setVisibility(View.GONE);
                        tv_EFshowmore1.setVisibility(View.GONE);
                    } else {
                        if (exteriorfeatures_list.size() >= 9) {
                            for (int d = 0; d < 8; d++) {
                                exteriorfeatures_list_limit.add(exteriorfeatures_list.get(d));
                            }

                        } else {
                            exteriorfeatures_list_limit.addAll(exteriorfeatures_list);
                            tv_EFshowless.setVisibility(View.GONE);
                            tv_EFshowmore1.setVisibility(View.GONE);
                        }
                        Efeatures_adapter = new CarDetailAdapter_ExtFeatures(context, exteriorfeatures_list_limit);
                        rc_extriorfeatures.setAdapter(Efeatures_adapter);
                    }


                    // start Other Features List condition

                    if (otherFeatures_list.size() == 0) {
                        cl_other_heading.setVisibility(View.GONE);
                        view51_other.setVisibility(View.GONE);
                        tv_OFshowmore1.setVisibility(View.GONE);
                    } else {
                        otherfeat_adpatr = new CarDetailAdapter_OtherFeatures(context, otherFeatures_list_limit);
                        rv_otherfeatures.setAdapter(otherfeat_adpatr);

                    }

                    // start Safety Features List condition


                    if (safetyFeatures_list.size() == 0) {
                        cl_safety_heading.setVisibility(View.GONE);
                        view44_safety.setVisibility(View.GONE);
                        tv_SFshowmore1.setVisibility(View.GONE);
                    } else {
                        rc_safetyfeatures.setVisibility(View.VISIBLE);
                        safetyfeat_adapter = new CarDetailAdapter_SafetyFeatures(context, safetyFeatures_list_limit);
                        rc_safetyfeatures.setAdapter(safetyfeat_adapter);
                        rc_safetyfeatures.scrollToPosition(0);

                    }


                    NUM_PAGES = img_list.size();
                    carDetailImageBanner_adapter = new CarDetailImageBanner_Adapter(context, img_list);
                    ll_carimage.setAdapter(carDetailImageBanner_adapter);
                    ll_carimage.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            ToImageView();
                        }
                    });

                    ll_carimage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                            ll_carimage.getCurrentItem();
                            tv_photo_indicator.setText((ll_carimage.getCurrentItem() + 1) + "/" + NUM_PAGES);
                        }

                        @Override
                        public void onPageSelected(int position) {
                            position_image=position;

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });

                } else if (response_carDetail.getStatus().equals("0")) {

                    System.out.println("A_Repsonse-->" + response_carDetail.getMessage());
                    Toast.makeText(context, response_carDetail.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }*/

    }

    private void callrecycle() {
        tv_photo_indicator.setText(1 + "/" + NUM_PAGES);
        carDetailImageBanner_adapter = new newscrollimage(context, img_list,this);
        ll_carimage.setHasFixedSize(true);
        LinearSnapHelper linearSnapHelper = new SnapHelperOneByOne();
        linearSnapHelper.attachToRecyclerView(ll_carimage);
        ll_carimage.setLayoutManager(new LinearLayoutManager(Car_Detail__Constarint_Activity1.this, LinearLayoutManager.HORIZONTAL, false));
        ll_carimage.setAdapter(carDetailImageBanner_adapter);

        ll_carimage.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int ydy = 0;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager lManager = (LinearLayoutManager) ll_carimage.getLayoutManager();
                int total = lManager.getItemCount();
                int firstVisibleItemCount = lManager.findFirstVisibleItemPosition();
                int lastVisibleItemCount = lManager.findLastVisibleItemPosition();
                System.out.println("fixed-->"+firstVisibleItemCount);
                tv_photo_indicator.setText((firstVisibleItemCount + 1) + "/" + NUM_PAGES);
            }
        });
    }


    private void Func_Select_UnSelect_Favourite() {

        if (isFavouirte.equals("0")) {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.custom_alert_layout_title_ios);
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Do you want to add this car to favourite list?");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Func_SetFavourite();
                    alertDialog.dismiss();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();
        } else if (isFavouirte.equals("1")) {

            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.custom_alert_layout_title_ios);
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Do you want to remove this car from favourite list?");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Func_Delet_Favourite();
                    alertDialog.dismiss();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();

        }
    }

    private void Func_Delet_Favourite() {

        Commons.showProgress(context);
        HashMap<String, String> header1 = new HashMap<>();
        header1.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String, String> addfav_req = new HashMap<>();

        carid_deletefav.add(intent_carid);

        for (int i = 0; i < carid_deletefav.size(); i++) {
            addfav_req.put("car_id[" + i + "]", carid_deletefav.get(i));
        }


        carDetailviewModel.deleteFavourite(header1, addfav_req).observe(this, new Observer<Response_DeleteFavourite>() {
            @Override
            public void onChanged(Response_DeleteFavourite response_deleteFavourite) {
                // spin_kit.setVisibility(View.GONE);
                Commons.hideProgress();
                if (response_deleteFavourite.getStatus().equals("1")) {
                    Commons.Alert_custom(context, "The Selected car removed from favourite list");
                    img_favourite.setImageResource(R.drawable.ic_heart_1);
                    isFavouirte = "0";
                    // Method_CarDetail();

                } else if (response_deleteFavourite.getStatus().equals("0")) {
                    Commons.Alert_custom(context, response_deleteFavourite.getMessage());
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    private void dialog_addquote() {
        final Dialog dialog = new Dialog(Car_Detail__Constarint_Activity1.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);

        dialog.setContentView(R.layout.bottomsheet_add_to_quote_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height

        ImageView bt_increment, bt_decrement;
        RecyclerView rv_number;


        CustomRegularButton button_addquote;
        button_addquote = dialog.findViewById(R.id.dialog_addquote);
        bt_increment = dialog.findViewById(R.id.bt_increment);
        bt_decrement = dialog.findViewById(R.id.bt_decrement);
        rv_number = dialog.findViewById(R.id.rv_number);

        no_cars_quotes = "1";
        temp_count = 1;

        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_number.setLayoutManager(linearLayoutManager);
        rv_number.setHasFixedSize(true);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rv_number);
        Custom_NumberAdapter custom_numberAdapter;

        custom_numberAdapter = new Custom_NumberAdapter(context, number_count);
        rv_number.setAdapter(custom_numberAdapter);


        rv_number.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == rv_number.SCROLL_STATE_DRAGGING) {
                    linearLayoutManager.scrollToPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition());
                    temp_count = linearLayoutManager.findFirstCompletelyVisibleItemPosition() + 1;
                    no_cars_quotes = String.valueOf(temp_count);
                    System.out.println("--f----" + temp_count);
                }
                if (newState == rv_number.SCROLL_STATE_IDLE) {
                    linearLayoutManager.scrollToPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition());
                    temp_count = linearLayoutManager.findFirstCompletelyVisibleItemPosition() + 1;
                    no_cars_quotes = String.valueOf(temp_count);
                    System.out.println("--f----" + temp_count);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                System.out.println("--f---nnnnn-" + temp_count);
            }
        });


        bt_increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutManager.scrollToPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition() + 1);
                temp_count = linearLayoutManager.findFirstCompletelyVisibleItemPosition() + 2;
                no_cars_quotes = String.valueOf(temp_count);


            }
        });

        bt_decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    dialog.dismiss();
                } else {
                    linearLayoutManager.scrollToPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition() - 1);
                    temp_count = linearLayoutManager.findFirstCompletelyVisibleItemPosition() - 1;
                    no_cars_quotes = String.valueOf(temp_count);

                }
            }
        });


        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog alertDialog;
                alertDialog = new Dialog(context);
                alertDialog.setContentView(R.layout.custom_alert_layout_title_ios);
                alertDialog.setCancelable(true);
                alertDialog.setCanceledOnTouchOutside(true);
                if (alertDialog.getWindow() != null) {
                    alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }

                alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
                CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
                txttitle.setText("Allied Motors");
                txtInstructionDialog.setText("Do you want to add this car to Quote?");
                txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
                CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);
                CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
                btokay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                        autoCompleteHandler.postDelayed(autoCompleteRunnable, 1000);
                        alertDialog.dismiss();
                        dialog.dismiss();
                    }
                });
                autoCompleteHandler = new Handler();
                autoCompleteRunnable = new Runnable() {
                    public void run() {
                        if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {

                            boolean recordexxit = myDataBase.recordExist(intent_carid);
                            if (recordexxit) {
                                // Toast.makeText(getBaseContext(), "exist", Toast.LENGTH_SHORT).show();
                                Commons.Alert_custom(context, "Already added to quote");
                            } else {
                                String cardetailname = sts_makename + " " + sts_carname + " " + st_resfueltype + " " + sts_vehicletype;
                                myDataBase.Insert_Guestquote_1(intent_carid, sts_image, sts_carinfo, sts_carcat, sts_makid, sts_modid, sts_makename, sts_carname, sts_year, sts_modeloption, sts_varientcode, selected_exteriorcolor, select_interioicolor, no_cars_quotes);
                                String content_desc = cardetailname + " is added to quotation list. Do you want to view the quotation list page?";
                                dialog_completionquotation(content_desc);
                                func_loginOrNot();
                            }
                        } else {
                            addquote();
                        }

                    }
                };


                bt_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                if (!alertDialog.isShowing())
                    alertDialog.show();


            }
        });
        dialog.show();

    }


    public void ToImageView() {
        Intent toimageview = new Intent(Car_Detail__Constarint_Activity1.this, ImageViewer_Activity.class);
        toimageview.putStringArrayListExtra("car_images", img_list);
        toimageview.putExtra("position", position_image);
        toimageview.putExtra("sharingurl", sharingUrl);
        startActivity(toimageview);
    }


    public void addquote() {
        Commons.showProgress(context);
        HashMap<String, String> header1 = new HashMap<>();
        header1.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String, String> addfav_req = new HashMap<>();
        addfav_req.put("car_id", intent_carid);
        addfav_req.put("exterior_color", selected_exteriorcolor);
        addfav_req.put("interior_color", select_interioicolor);
        addfav_req.put("type", sts_carinfo);
        addfav_req.put("quantity", no_cars_quotes);

        Log.e("-->car-add quote->", addfav_req.toString());
        System.out.println("==req-add quote--" + addfav_req + "---" + header1);
        carDetailviewModel.addquoteLiveData(header1, addfav_req).observe(this, new Observer<Response_Addquote>() {
            @Override
            public void onChanged(Response_Addquote response_addquote) {
                Commons.hideProgress();
                //spin_kit.setVisibility(View.GONE);
                if (response_addquote.status.equals("1")) {
                    System.out.println("success--->addquote-->" + response_addquote.toString());
                    Log.e("->add quote-->", response_addquote.toString());
                    // Commons.Alert_custom(context, response_addquote.longMessage);
                    //  String content_desc="Car has been added to your quotation list. Do you want to view the quotation list page?";
                    //dialog_completionquotation(response_addquote.longMessage);

                    String content_desc = response_addquote.longMessage + ". Do you want to view the quotation list page?";
                    dialog_completionquotation(content_desc);
                    getquote_func();

                    // Toast.makeText(context, response_addquote.longMessage, Toast.LENGTH_SHORT).show();
                } else if (response_addquote.status.equals("0")) {
                    Commons.Alert_custom(context, response_addquote.longMessage);
                }
            }
        });
    }

    private void dialog_completionquotation(String message) {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_button_linear_quotation1);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
        txttitle.setText("Allied Motors");
        txtInstructionDialog.setText(message);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
        CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
        btokay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tocart = new Intent(Car_Detail__Constarint_Activity1.this, HomeActivity_searchimage_BottomButton.class);
                tocart.putExtra("tabposition", "cart");
                tocart.putExtra("position_tabs", "1");
                startActivity(tocart);
                alertDialog.dismiss();
            }
        });


        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    private void Func_SetFavourite() {
        Commons.showProgress(context);
        HashMap<String, String> header1 = new HashMap<>();
        header1.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String, String> addfav_req = new HashMap<>();
        addfav_req.put("car_id", intent_carid);
        addfav_req.put("exterior_color", selected_exteriorcolor);
        addfav_req.put("interior_color", select_interioicolor);

        System.out.println("---add favourite===" + addfav_req.toString());

        carDetailviewModel.addfavouritedata(header1, addfav_req).observe(this, new Observer<Response_AddFavourite>() {
            @Override
            public void onChanged(Response_AddFavourite response_addFavourite) {

                Commons.hideProgress();
                if (response_addFavourite.status.equals("1")) {
                    System.out.println("success--->addfav-->" + response_addFavourite.toString());
                    Log.e("->addfav-->", response_addFavourite.toString());
                    img_favourite.setImageResource(R.drawable.ic_favourite_blue);
                    isFavouirte = "1";
                    dialog_completionFavourite(response_addFavourite.longMessage);

                    //  Method_CarDetail();
                } else if (response_addFavourite.status.equals("0")) {
                    Commons.Alert_custom(context, response_addFavourite.message);
                }
            }
        });
    }

    private void dialog_completionFavourite(String message) {
        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.custom_alert_button_linear);
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText(message);
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tocart = new Intent(Car_Detail__Constarint_Activity1.this, HomeActivity_searchimage_BottomButton.class);
                    tocart.putExtra("tabposition", "cart");
                    tocart.putExtra("position_tabs", "0");
                    startActivity(tocart);
                    alertDialog.dismiss();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();
        }

    }


    @Override
    public void onEColorSelected(String data) {
        selected_exteriorcolor = data;
        System.out.println("==dhh=exterior=6=" + data);
        System.out.println("==dhh==extererior=666=" + selected_exteriorcolor);
        select_interioicolor = "";
        Method_CarDetail(intent_carid);

    }

    @Override
    public void onIColorSelected(String data) {
        select_interioicolor = data;
        System.out.println("==dhh==interior=444=" + data);
        System.out.println("==dhh==interior=44=" + select_interioicolor);
        Method_CarDetail(intent_carid);
    }

    private int getPosition(String chosencolor) {
        for (int i = 0; i < listexterior_color.size(); i++) {
            if (listexterior_color.get(i).equals(chosencolor)) {
                return i;
            }
        }
        return 0;
    }

    private int getPositionInterior(String chosencolor1) {
        for (int i = 0; i < listinterior_color.size(); i++) {
            if (listinterior_color.get(i).equals(chosencolor1)) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onItemClick(int postion) {
        position_image = postion;
        ToImageView();
    }
}
