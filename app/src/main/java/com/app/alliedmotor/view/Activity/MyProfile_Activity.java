package com.app.alliedmotor.view.Activity;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.ProfileUpdate.Response_ProfileUpdate;
import com.app.alliedmotor.model.Response_OtpRequest;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.CountryDialCode;
import com.app.alliedmotor.utility.GPSTracker;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.viewmodel.ProfileUpdateviewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hbb20.CountryCodePicker;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class MyProfile_Activity extends AppCompatActivity implements View.OnClickListener {


    private Context context;
    ImageView back_setting;
    CustomRegularTextview tv_forward_ic;
    CustomTextViewSemiBold tv_change_pwd;
    CustomRegularEditText et_firstname,et_lastname,et_emailid,et_workno,et_company_name,et_mobile,et_role;
    String fname,lname,email,mobile,workno,comp_name;
    String st_fname,st_lname,st_email,st_mobile,st_workno,st_comp_name,st_usertype,st_ismobileverified="",st_role;
    ProfileUpdateviewModel profileUpdateviewModel;
    CustomRegularButton bt_update;
    SharedPref sharedPref;

    LinearLayout loading_ll,ll_changepwd;
    CustomRegularTextview tv_notverfied;
    ImageView approval_tick;

    private LinearLayout myCountryCodeLAY;
    private ImageView myFlagIMG;
    private TextView myCountryCodeTXT;
    private CountryPicker myPicker;
    private String myFlagStr = "",st_full_mobile;
    private GPSTracker myGPSTracker;
    LinearLayout ll_companyset;

    private LinearLayout myCountryCodeLAY2;
    private ImageView myFlagIMG2;
    private TextView myCountryCodeTXT2;
    private CountryPicker myPicke2r;
    private String myFlagStr2 = "",st_full_mobile2;
    private GPSTracker myGPSTracker2;

    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;


    Runnable autoCompleteRunnable1;
    Handler autoCompleteHandler1;

    String res_usertype="";

    String temp_mobilenumber="",temp_ctrycode="",temp_mobilenoalone="";

    String tempworknum="",temp_workctrycode="",temp_workmobilealone="";

    CountryCodePicker ctycodepicker,ctycodepicker_worknumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_layout);

        profileUpdateviewModel = ViewModelProviders.of(this).get(ProfileUpdateviewModel.class);
        context = this;
        sharedPref=new SharedPref(context);
        findbyviews();
        clicklistener();
        Func_MyProfile_Update();

      //  classAndWidgetInitialize();
        System.out.println("---dsh==="+sharedPref.getString(Constants.JWT_TOKEN));
        Log.e("asa",sharedPref.getString(Constants.JWT_TOKEN));

        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        String cod=tm.getNetworkCountryIso();
        System.out.println("==countrycode==="+countryCodeValue+"88==>"+cod);
        Log.e("coutrycod==>",countryCodeValue);
        Log.e("cpod==>",cod);


        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                Func_OTPRequest();
            }
        };


        autoCompleteHandler1 = new Handler();
        autoCompleteRunnable1 = new Runnable() {
            public void run() {
                Update_Method();
            }
        };


    }

    private void findbyviews() {

        ctycodepicker = (CountryCodePicker) findViewById(R.id.ctycodepicker);

        ctycodepicker_worknumber=(CountryCodePicker)findViewById(R.id.ctycodepicker_worknumber);
        tv_notverfied=findViewById(R.id.tv_notverfied);
        approval_tick=findViewById(R.id.approval_tick);


        back_setting=findViewById(R.id.back_setting);
        tv_change_pwd=findViewById(R.id.tv_change_pwd);
        tv_forward_ic=findViewById(R.id.tv_forward_ic);
        et_firstname=findViewById(R.id.et_firstname);
        et_mobile=findViewById(R.id.et_mobile);
        et_lastname=findViewById(R.id.et_lastname);
        et_emailid=findViewById(R.id.et_emailid);
        et_workno=findViewById(R.id.et_workno);
        et_company_name=findViewById(R.id.et_company_name);
        et_role=findViewById(R.id.et_role);
        bt_update=findViewById(R.id.bt_update);
        loading_ll=findViewById(R.id.loading_ll);
        ll_companyset=findViewById(R.id.ll_companyset);

        ll_changepwd=findViewById(R.id.ll_changepwd);


        myGPSTracker = new GPSTracker(MyProfile_Activity.this);
        myCountryCodeLAY = (LinearLayout)findViewById(R.id.activity_signup_mobile_no_LAY1);
       /* myFlagIMG = (ImageView) findViewById(R.id.activity_signup_country_flag_IMG1);
        myCountryCodeTXT = findViewById(R.id.activity_signup_country_code_TXT1);
*/
        myCountryCodeLAY2 = (LinearLayout)findViewById(R.id.activity_signup_mobile_no_LAY2);
       /* myFlagIMG2 = (ImageView) findViewById(R.id.activity_signup_country_flag_IMG2);
        myCountryCodeTXT2 = findViewById(R.id.activity_signup_country_code_TXT2);
*/


    }

    private void clicklistener() {
        back_setting.setOnClickListener(this);
        tv_change_pwd.setOnClickListener(this);
        tv_forward_ic.setOnClickListener(this);
        bt_update.setOnClickListener(this);
        ll_changepwd.setOnClickListener(this);
        myCountryCodeLAY.setOnClickListener(this);
        myCountryCodeLAY2.setOnClickListener(this);
        tv_notverfied.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back_setting:
                onBackPressed();
                break;
            case R.id.tv_notverfied:
                autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                autoCompleteHandler.postDelayed(autoCompleteRunnable, 700);

                break;

            case R.id.ll_changepwd:
            case R.id.tv_forward_ic:
            case R.id.tv_change_pwd:
                Intent toIntent=new Intent(MyProfile_Activity.this,ChangePassword.class);
                startActivity(toIntent);
                break;
            case R.id.bt_update:
                autoCompleteHandler1.removeCallbacks(autoCompleteRunnable1);
                autoCompleteHandler1.postDelayed(autoCompleteRunnable1, 700);


                break;

            case R.id.activity_signup_mobile_no_LAY1:
                myPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;

            case R.id.activity_signup_mobile_no_LAY2:
                myPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;

        }
    }

    private void Func_OTPRequest() {
        Commons.showProgress(context);
        String r_pagename="changeprofile";

        HashMap<String, String> request = new HashMap<>();
            request.put("otp_type","phone");
            request.put("page_name",r_pagename);
            request.put("user_email",sharedPref.getString(Constants.PREF_USERMAIL));
            request.put("mobile_number",ctycodepicker.getSelectedCountryCodeWithPlus()+et_mobile.getText().toString().trim());

            profileUpdateviewModel.OtpRequestData(request).observe(this, new Observer<Response_OtpRequest>() {
                @Override
                public void onChanged(Response_OtpRequest response_otpRequest) {
                    Commons.hideProgress();
                    System.out.println("dv==="+response_otpRequest.toString());
                    if(response_otpRequest.status.equals("1")) {


                                Intent tootppage=new Intent(MyProfile_Activity.this, OTPVerification_Activity_MyProfile.class);
                                tootppage.putExtra("otptype",response_otpRequest.data.otpData.otpType);
                                tootppage.putExtra("useremail",response_otpRequest.data.otpData.userEmail);
                                tootppage.putExtra("mobilenumber",response_otpRequest.data.otpData.mobileNumber);
                                tootppage.putExtra("pagename",response_otpRequest.data.pageName);
                                tootppage.putExtra("otpdevmode",String.valueOf(response_otpRequest.data.otpData.devMode));
                                tootppage.putExtra("contentmessage",response_otpRequest.longMessage);
                                tootppage.putExtra("otptoken",response_otpRequest.data.otpData.code);
                                startActivity(tootppage);
                                finish();


                            }

                    else if(response_otpRequest.status.equals("0"))
                    {
                        Commons.Alert_custom(context,response_otpRequest.message);

                    }
                }
            });



    }


    private void classAndWidgetInitialize() {

        myPicker = CountryPicker.newInstance("Select Country");
        myPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flag) {
                myPicker.dismiss();
                myCountryCodeTXT.setText(dialCode);
              //  myFlagIMG.setVisibility(View.VISIBLE);
                myFlagStr = "" + flag;
                myFlagIMG.setImageResource(flag);
                String  exampleNumber= String.valueOf(PhoneNumberUtil.getInstance().getExampleNumber(code).getNationalNumber());
                et_mobile.setFilters( new InputFilter[] {new InputFilter.LengthFilter(exampleNumber.length())});  //mypicker.setlistener

                myCountryCodeTXT2.setText(dialCode);
              //  myFlagIMG2.setVisibility(View.VISIBLE);
                myFlagStr2 = "" + flag;
                myFlagIMG2.setImageResource(flag);
                String  exampleNumber1= String.valueOf(PhoneNumberUtil.getInstance().getExampleNumber(code).getNationalNumber());
                et_workno.setFilters( new InputFilter[] {new InputFilter.LengthFilter(exampleNumber1.length())});  //mypicker.setlistener
            }
        });
        setCountryDataValues();

    }

    private void setCountryDataValues() {
        if (myGPSTracker.canGetLocation() && myGPSTracker.isgpsenabled()) {
            double MyCurrent_lat = myGPSTracker.getLatitude();
            double MyCurrent_long = myGPSTracker.getLongitude();
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String aCountryCodeStr = addresses.get(0).getCountryCode();
                    System.out.println("---countrycodestr---"+aCountryCodeStr);
                    if (aCountryCodeStr.length() > 0 && !aCountryCodeStr.equals(null) && !aCountryCodeStr.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(aCountryCodeStr);
                     /*   if(temp_ctrycode!=null || temp_ctrycode!="")
                        {

                            myCountryCodeTXT.setText(temp_ctrycode);
                            myCountryCodeTXT2.setText(temp_ctrycode);
                        }
                        else {
                            myCountryCodeTXT.setText(Str_countyCode);
                            myCountryCodeTXT2.setText(Str_countyCode);
                        }*/
                       /* String drawableName = "flag_"
                                + aCountryCodeStr.toLowerCase(Locale.ENGLISH);
                        myFlagIMG.setImageResource(getResId(drawableName));
                        myFlagStr = "" + getResId(drawableName);
*/
                      /*  String drawableName1 = "flag_"
                                + aCountryCodeStr.toLowerCase(Locale.ENGLISH);

                        myFlagIMG2.setImageResource(getResId(drawableName1));
                        myFlagStr2 = "" + getResId(drawableName1);
*/
                        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(Str_countyCode));
                        String  exampleNumber= String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                        int phoneLength=exampleNumber.length();
                        et_workno.setFilters( new InputFilter[] {
                                new InputFilter.LengthFilter(phoneLength)});
                        et_workno.setEms(phoneLength);
                        System.out.println("==d==="+et_workno.getText().toString().trim());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getResId(String drawableName) {
        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
        }
        return -1;
    }






    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void Update_Method() {
        st_fname=et_firstname.getText().toString().trim();
        st_lname=et_lastname.getText().toString().trim();
        st_email=et_emailid.getText().toString().trim();
        st_mobile=ctycodepicker.getSelectedCountryCodeWithPlus()+et_mobile.getText().toString().trim();
        st_workno=ctycodepicker_worknumber.getSelectedCountryCodeWithPlus()+et_workno.getText().toString().trim();
        st_comp_name=et_company_name.getText().toString().trim();
        st_role=et_role.getText().toString().trim();


            if(st_fname.isEmpty()&&st_lname.isEmpty()&&st_email.isEmpty()&&st_comp_name.isEmpty()&&st_mobile.isEmpty())
            {
                Commons.showAlertDialog1(context,"Please enter Details");
                et_firstname.requestFocus();
            }
            else if(st_fname.isEmpty())
            {
                Commons.showAlertDialog1(context,"Please enter Username");
                et_firstname.requestFocus();
            }

            else if(st_lname.isEmpty())
            {
                Commons.showAlertDialog1(context,"Please enter Lastname");
                et_lastname.requestFocus();
            }

            else if(st_email.isEmpty())
            {
                Commons.showAlertDialog1(context,"Please enter Email");
                et_emailid.requestFocus();
            }
            else if (!validEmail(st_email)) {
                Commons.showAlertDialog1(context, "Please Enter Valid Maild-id");
                et_emailid.requestFocus();
            }

            else if(st_mobile.isEmpty())
            {
                Commons.showAlertDialog1(context,"Please enter mobile Number");
                et_mobile.requestFocus();
            }

            else if(res_usertype.equals("customer"))
        {
            ll_companyset.setVisibility(View.GONE);
            Servicecall_ProfileUpdate();
        }
        else if(res_usertype.equals("individual")||res_usertype.equals("company")){
            ll_companyset.setVisibility(View.VISIBLE);
             if(st_comp_name.isEmpty())
                {
                    Commons.showAlertDialog1(context,"Please enter Company name");
                    et_company_name.requestFocus();
                }
               else {
                 Servicecall_ProfileUpdate();
             }
        }

           /* else if(st_comp_name.isEmpty())
            {
                Commons.showAlertDialog1(context,"Please enter Company name");
                et_company_name.requestFocus();
            }
*/
            else {

                Servicecall_ProfileUpdate();
            }




    }

    private void Servicecall_ProfileUpdate() {

        HashMap<String, String> request_update_profile = new HashMap<>();
        request_update_profile.put("last_name",st_lname);
        request_update_profile.put("user_email",st_email);
        request_update_profile.put("mobile_number",st_mobile);
        request_update_profile.put("user_type",res_usertype);
        request_update_profile.put("company_name",st_comp_name);
        request_update_profile.put("company_role",st_role);
        request_update_profile.put("first_name",st_fname);
        request_update_profile.put("work_phone_number",st_workno);


        HashMap<String,String>header1=new HashMap<>();
        header1.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));


        System.out.println("==req--prof-update--"+request_update_profile+"---"+header1);

       // Commons.showProgress(context);
        loading_ll.setVisibility(View.VISIBLE);
        profileUpdateviewModel.getProfileupdateData(request_update_profile,header1).observe(this, new Observer<Response_ProfileUpdate>() {
            @Override
            public void onChanged(Response_ProfileUpdate response_profileUpdate) {
                loading_ll.setVisibility(View.GONE);
                if(response_profileUpdate.getStatus().equals("1")) {
                    System.out.println("===res=34==" + response_profileUpdate.toString());
                    Commons.Alert_custom(context,response_profileUpdate.getLongMessage());
                   // Toast.makeText(context, response_profileUpdate.getLongMessage(), Toast.LENGTH_SHORT).show();
                    Func_MyProfile_Update();
                }
                else if(response_profileUpdate.getStatus().equals("0"))
                {
                    System.out.println("===res=34=status--0=" + response_profileUpdate.toString());
                    Toast.makeText(context, response_profileUpdate.getLongMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void Func_MyProfile_Update() {

            Commons.showProgress(context);
            HashMap<String ,String> header=new HashMap<>();
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            System.out.println("dhh---------"+sharedPref.getString(Constants.JWT_TOKEN));
            profileUpdateviewModel.getuserprofiledata(header).observe(this, new Observer<Response_UserProfile>() {
                @Override
                public void onChanged(Response_UserProfile response_userProfile) {
                    Commons.hideProgress();
                    System.out.println("===res=user_profile==" + response_userProfile.toString());

                    if(response_userProfile.getStatus().equals("1"))
                    {
                        String cntrycode=response_userProfile.getData().getUserDetails().getMobileNumber();
                        et_firstname.setText(response_userProfile.getData().getUserDetails().getFirst_name());
                        et_lastname.setText(response_userProfile.getData().getUserDetails().getLast_name());
                        et_emailid.setText(response_userProfile.getData().getUserDetails().getUserEmail());
                       // et_mobile.setText(response_userProfile.getData().getUserDetails().getMobileNumber());
                        et_role.setText(response_userProfile.getData().getUserDetails().getCompany_role());
                        et_workno.setText(response_userProfile.getData().getUserDetails().getWork_phone_number());
                        et_company_name.setText(response_userProfile.getData().getUserDetails().getCompanyName());
                        st_ismobileverified=String.valueOf(response_userProfile.getData().getUserDetails().getIsPhoneVerified());
                        res_usertype=response_userProfile.getData().getUserDetails().getUser_type();

                        temp_mobilenumber=response_userProfile.getData().getUserDetails().getMobileNumber();
                        tempworknum=response_userProfile.getData().getUserDetails().getWork_phone_number();
                        toparse();

                        toparse_work();
                        if(res_usertype.equals("customer"))
                        {
                            ll_companyset.setVisibility(View.GONE);
                        }
                        else if(res_usertype.equals("individual")||res_usertype.equals("company")){
                            ll_companyset.setVisibility(View.VISIBLE);
                        }
                        if(st_ismobileverified.equals("1"))
                        {
                            approval_tick.setVisibility(View.VISIBLE);
                            tv_notverfied.setVisibility(View.GONE);
                            et_mobile.setEnabled(false);
                        }
                        else if(st_ismobileverified.equals("0"))
                        {
                            approval_tick.setVisibility(View.GONE);
                            tv_notverfied.setVisibility(View.VISIBLE);
                            et_mobile.setEnabled(true);
                        }

                    }
                    else
                    {
                        Toast.makeText(context,response_userProfile.getMessage(),Toast.LENGTH_SHORT).show();
                    }


                }
            });



    }

    private void toparse_work()
    {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
        Phonenumber.PhoneNumber worknumberProto = phoneUtil.parse(tempworknum, "");
            temp_workmobilealone=String.valueOf(worknumberProto.getNationalNumber());
            et_workno.setText(temp_workmobilealone);
            ctycodepicker_worknumber.setCountryForPhoneCode(worknumberProto.getCountryCode());

        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
    }

    private void toparse() {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {

            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(temp_mobilenumber, "");




            System.out.println("Country code: " + numberProto.getCountryCode());
            System.out.println("Country code1: " + numberProto.getExtension());
            System.out.println("Country code2: " + numberProto.getPreferredDomesticCarrierCode());
            System.out.println("Country code3: " + numberProto.getNationalNumber());
            temp_mobilenoalone=String.valueOf(numberProto.getNationalNumber());
            et_mobile.setText(temp_mobilenoalone);



            temp_ctrycode=String.valueOf("+"+numberProto.getCountryCode());

         //   myCountryCodeTXT.setText(temp_ctrycode);
            ctycodepicker.setCountryForPhoneCode(numberProto.getCountryCode());

            //This prints "Country code: 91"
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
    }
}
