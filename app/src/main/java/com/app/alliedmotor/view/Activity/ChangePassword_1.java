package com.app.alliedmotor.view.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.ResetPassword.Response_ResetPassword;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.PasswordValidator;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.viewmodel.ResetPasswordviewModel;

import java.util.HashMap;

public class ChangePassword_1 extends AppCompatActivity implements View.OnClickListener{

    private Context context;
    ImageView back_setting;
    CustomRegularEditText et_currentpwd,et_newpwd,et_confirmpwd;
    CustomRegularButton bt_submit;
    String st_currentpwd,st_newpwd,st_confirmpwd;
    PasswordValidator passwordValidator;
    SharedPref sharedPref;
    ResetPasswordviewModel resetPasswordviewModel;
    LinearLayout loading_ll;
 MyDataBase myDataBase;
    String res_useremail="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_1);
        resetPasswordviewModel = ViewModelProviders.of(this).get(ResetPasswordviewModel.class);
        context=this;

        sharedPref=new SharedPref(context);
        myDataBase=new MyDataBase(context);
        passwordValidator=new PasswordValidator();
        finbyviews();
        clicklistener();
        if(getIntent()!=null)
        {
            res_useremail=getIntent().getStringExtra("useremail");
        }


    }

    private void clicklistener() {
        back_setting.setOnClickListener(this);
        bt_submit.setOnClickListener(this);
    }

    private void finbyviews() {
        back_setting=findViewById(R.id.back_setting);
        //et_currentpwd=findViewById(R.id.et_currentpwd);
        et_newpwd=findViewById(R.id.et_newpwd);
        et_confirmpwd=findViewById(R.id.et_confirmpwd);
        bt_submit=findViewById(R.id.bt_submit);
        loading_ll=findViewById(R.id.loading_ll);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back_setting:
                onBackPressed();
                finish();
                break;

            case R.id.bt_submit:
                Validation();
                break;
        }
    }

    private void Validation() {
      //  st_currentpwd=et_currentpwd.getText().toString().trim();
        st_newpwd=et_newpwd.getText().toString().trim();
        st_confirmpwd=et_confirmpwd.getText().toString().trim();

        if(st_newpwd.isEmpty() && st_confirmpwd.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter all Details");
        }
        else if(st_newpwd.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Password");
            et_newpwd.requestFocus();
        }
        else if (!passwordValidator.validate(st_newpwd)) {
            Commons.Alert_custom(context,"Minimum of 8 characters in length, Should contain atleast one uppercase,lowercase,number and special Character");
            et_newpwd.setError("Minimum of 8 characters in length, Should contain atleast one uppercase,lowercase,number and special Character");
            et_newpwd.requestFocus();
        } else if (st_confirmpwd.isEmpty()) {
            et_confirmpwd.requestFocus();
            Commons.Alert_custom(context, "Please Enter confirm password");
        } else if (!passwordValidator.validate(st_confirmpwd)) {
            Commons.Alert_custom(context,"Minimum of 8 characters in length, Should contain atleast one uppercase,lowercase,number and special Character");
            et_confirmpwd.requestFocus();
        } else if (!st_newpwd.equals(st_confirmpwd)) {
            et_confirmpwd.requestFocus();
            Commons.Alert_custom(context, "The Password and confirm password not same");
        } else {
            Method_ChanePassword();
        }
    }

    private void Method_ChanePassword() {

        HashMap<String, String> request_changepassword = new HashMap<>();
        request_changepassword.put("user_email",res_useremail);
        request_changepassword.put("password",st_newpwd);


        System.out.println("==req---login---"+"==="+request_changepassword);
        //Commons.showProgress(context);
        loading_ll.setVisibility(View.VISIBLE);
        resetPasswordviewModel.getresetpassword(request_changepassword).observe(this, new Observer<Response_ResetPassword>() {
            @Override
            public void onChanged(Response_ResetPassword response_changePassword) {
                //Commons.hideProgress();
                loading_ll.setVisibility(View.GONE);
                if(response_changePassword.status.equals("1"))
                {
                   // Toast.makeText(context, response_changePassword.longMessage, Toast.LENGTH_SHORT).show();
                    Commons.Alert_custom(context,response_changePassword.longMessage);
                    dialog_savepassword();
                    System.out.println("===res=change password==" + response_changePassword.toString());

                }
                else if(response_changePassword.status.equals("0"))
                {
                    Commons.Alert_custom(context,response_changePassword.message);
                }
            }
        });
    }


    private void dialog_savepassword() {

        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.bottomsheet_changepassword);
            alertDialog.setCancelable(true);
            Window window = alertDialog.getWindow();
            window.setGravity(Gravity.BOTTOM);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Your Password has been changed, do you want to save the password?");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myDataBase.update(res_useremail,st_newpwd);
                    alertDialog.dismiss();
                    Intent tologin=new Intent(ChangePassword_1.this,LoginActivity.class);
                    startActivity(tologin);
                    finish();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tologin=new Intent(ChangePassword_1.this,LoginActivity.class);
                    startActivity(tologin);
                    finish();
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();


        }
    }


}