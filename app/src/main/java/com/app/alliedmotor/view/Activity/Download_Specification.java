package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Download_Specification extends AppCompatActivity {

    File path,dirfile,finalpath;



    ImageView back,img_share;
    WebView webview;
    Context context;
    CustomRegularTextview tv_titlebar_heading;
    LinearLayout loading_ll,main_ll;

    String pdfurl,strtitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download__specification_linear);
        context=this;
        back=findViewById(R.id.img_back);
        webview=findViewById(R.id.webview);
        loading_ll=findViewById(R.id.loading_ll);
        main_ll=findViewById(R.id.main_ll);
        loading_ll.setVisibility(View.VISIBLE);
        img_share=findViewById(R.id.img_share);


        deleteTempFolder("/alliedmotors");
        Intent h_Intent = getIntent();
        pdfurl = h_Intent.getStringExtra("download_specification");
        strtitle=h_Intent.getStringExtra("downloadtitle");

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Download_file();
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                sharingIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                sharingIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath));
                sharingIntent.setType("application/pdf");
                 startActivity(sharingIntent);
               // startActivity(Intent.createChooser(sharingIntent, "Share"));

            }
        });


       // Commons.showProgress(context);
        Commons.hideProgress();
        webview.requestFocus();
        webview.getSettings().setJavaScriptEnabled(true);
        WebSettings webSettings = webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        String url = "https://docs.google.com/viewer?embedded=true&url=" + pdfurl;
        loading_ll.setVisibility(View.GONE);
        main_ll.setVisibility(View.VISIBLE);
        webview.loadUrl(url);
        /*webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loading_ll.setVisibility(View.GONE);
                main_ll.setVisibility(View.VISIBLE);
                }

        });
*/

        Commons.hideProgress();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
            }


    private void deleteTempFolder(String dir1) {
        /*File myDir = new File(Environment.getExternalStorageDirectory() + "/"+dir);
        if (myDir.isDirectory()) {
            String[] children = myDir.list();
            for (int i = 0; i < children.length; i++) {
                new File(myDir, children[i]).delete();
            }
        }*/

        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+dir1);
        if (dir.isDirectory())
        {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(dir, children[i]).delete();
            }
        }
    }

    private void Download_file() {

        path=new File("/alliedmotors/" + strtitle+".pdf");
        dirfile=new File(path, Environment.DIRECTORY_DOWNLOADS);
        finalpath=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), String.valueOf(path));
        DownloadManager downloadManager= (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(pdfurl));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setTitle("Downloading");
        request.setDescription("Please wait...");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, String.valueOf(path));
        downloadManager.enqueue(request);
       // Toast.makeText(context, "Download Complete", Toast.LENGTH_SHORT).show();
    }

}