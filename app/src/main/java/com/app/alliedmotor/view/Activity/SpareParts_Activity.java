package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.model.SpareParts_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.CountryDialCode;
import com.app.alliedmotor.utility.GPSTracker;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditTextBold;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.viewmodel.CarCategoryviewModel;
import com.app.alliedmotor.viewmodel.SparePartsViewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.android.gms.common.internal.service.Common;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class SpareParts_Activity extends AppCompatActivity implements View.OnClickListener {


    ImageView back;
    Context context;
    CustomRegularEditTextBold et_make,et_model,et_year,et_part,et_partname,et_desc,et_name,et_companyname,et_contact,et_email,et_remarks;
    CustomRegularButton bt_submit;
    SparePartsViewModel sparePartsViewModel;
    String temp_makelist="",temp_model="",temp_fuel="",temp_tranmission="",temp_year="",temp_maxyear="",temp_mileage="",temp_extcolor="",temp_intcolor="";
    ConstraintLayout ll_select_year,ll_model;
    ConstraintLayout ll_make;
    int makelistsize,modellistsize;
    String[] str_makedid;
    String[] strmodellist;
    String ip_makeid,ipmodelid;
    ArrayList<Response_CarMakeList.DataItem> carmakelist=new ArrayList<>();
    ArrayList<Response_CarModelList.DataItem>carmodellist=new ArrayList<>();
    int index=0;

    String temp_part,temp_partname,temp_desc,temp_name,temp_companyname,temp_contact,temp_email,temp_remarks;


    private LinearLayout myCountryCodeLAY;
    private ImageView myFlagIMG;
    private TextView myCountryCodeTXT;
    private CountryPicker myPicker;
    private String myFlagStr = "",st_full_mobile;
    private GPSTracker myGPSTracker;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spare_parts);
        sparePartsViewModel = ViewModelProviders.of(this).get(SparePartsViewModel.class);
        context = this;




        findviews();
        clicklistener();
        WebServiceCall_MakeList();
        classAndWidgetInitialize();

        back.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View v){
                onBackPressed();
                finish();
            }
        });
    }

    private void clicklistener() {
  ll_make.setOnClickListener(this);
  ll_model.setOnClickListener(this);
  ll_select_year.setOnClickListener(this);
  bt_submit.setOnClickListener(this);

        myCountryCodeLAY.setOnClickListener(this);


    }

    private void findviews() {
        back = findViewById(R.id.back);

        et_make=findViewById(R.id.et_make);
        et_model=findViewById(R.id.et_model);
        et_year=findViewById(R.id.et_year);

        et_part=findViewById(R.id.et_part);
        et_partname=findViewById(R.id.et_partname);
        et_desc=findViewById(R.id.et_desc);
        et_name=findViewById(R.id.et_name);
        et_companyname=findViewById(R.id.et_companyname);
        et_contact=findViewById(R.id.et_contact);
        et_email=findViewById(R.id.et_email);
        et_remarks=findViewById(R.id.et_remarks);
        bt_submit=findViewById(R.id.bt_submit);

        ll_select_year=findViewById(R.id.ll_select_year);
        ll_model=findViewById(R.id.ll_model);
        ll_make=findViewById(R.id.ll_make);

        myGPSTracker = new GPSTracker(SpareParts_Activity.this);
        myCountryCodeLAY = (LinearLayout)findViewById(R.id.activity_signup_mobile_no_LAY1);
        myFlagIMG = (ImageView) findViewById(R.id.activity_signup_country_flag_IMG1);
        myCountryCodeTXT = findViewById(R.id.activity_signup_country_code_TXT1);

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.activity_signup_mobile_no_LAY1:
                myPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;

            case R.id.ll_select_year:
                    Dialog_Year();
                break;

            case R.id.ll_model:
                if(temp_makelist.isEmpty())
                {
                    Commons.Alert_custom(context,"Please select the make options");
                }
                else {
                    Dialog_NumberPicker_ModelList();
                }
                break;

            case R.id.ll_make:
                        Dialog_NumberPicker_MakeList();
                break;

            case R.id.bt_submit:
                Validation_function();
                break;
        }

    }

    private void classAndWidgetInitialize() {

        myPicker = CountryPicker.newInstance("Select Country");
        myPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flag) {
                myPicker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                myFlagIMG.setVisibility(View.VISIBLE);
                myFlagStr = "" + flag;
                myFlagIMG.setImageResource(flag);
                String  exampleNumber= String.valueOf(PhoneNumberUtil.getInstance().getExampleNumber(code).getNationalNumber());
                et_contact.setFilters( new InputFilter[] {new InputFilter.LengthFilter(exampleNumber.length())});  //mypicker.setlistener

            }
        });
        setCountryDataValues();

    }


    private void setCountryDataValues() {
        if (myGPSTracker.canGetLocation() && myGPSTracker.isgpsenabled()) {
            double MyCurrent_lat = myGPSTracker.getLatitude();
            double MyCurrent_long = myGPSTracker.getLongitude();
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String aCountryCodeStr = addresses.get(0).getCountryCode();
                    if (aCountryCodeStr.length() > 0 && !aCountryCodeStr.equals(null) && !aCountryCodeStr.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(aCountryCodeStr);
                        myCountryCodeTXT.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + aCountryCodeStr.toLowerCase(Locale.ENGLISH);
                        myFlagIMG.setImageResource(getResId(drawableName));
                        myFlagStr = "" + getResId(drawableName);
                        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(Str_countyCode));
                        String  exampleNumber= String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                        int phoneLength=exampleNumber.length();
                        et_contact.setFilters( new InputFilter[] {
                                new InputFilter.LengthFilter(phoneLength)});
                        et_contact.setEms(phoneLength);
                        System.out.println("==d==="+et_contact.getText().toString().trim());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getResId(String drawableName) {
        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
        }
        return -1;
    }



    private void Validation_function() {

            temp_makelist=et_make.getText().toString().trim();
        temp_model=et_model.getText().toString().trim();
        temp_year=et_year.getText().toString().trim();
        temp_part=et_part.getText().toString().trim();
        temp_partname=et_partname.getText().toString().trim();
        temp_desc=et_desc.getText().toString().trim();
        temp_name=et_name.getText().toString().trim();
        temp_companyname=et_companyname.getText().toString().trim();
        temp_contact=et_contact.getText().toString().trim();
        temp_email=et_email.getText().toString().trim();
        temp_remarks=et_remarks.getText().toString().trim();


        if(temp_makelist.isEmpty() && temp_model.isEmpty() && temp_year.isEmpty() && temp_partname.isEmpty() && temp_name.isEmpty() && temp_contact.isEmpty() && temp_email.isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }
        else if(temp_makelist.isEmpty())

        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }
        else if(temp_model.isEmpty())

        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }

        else if(temp_year.isEmpty())

        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }

        else if(temp_partname.isEmpty())

        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }

        else if(temp_name.isEmpty())

        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }

        else if(temp_contact.isEmpty())

        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }

        else if(temp_email.isEmpty())

        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }else
        {
            Webservice_SpareParts();
        }



    }

    private void Webservice_SpareParts() {

        HashMap<String,String>request=new HashMap<>();
        request.put("make_id",temp_makelist);
        request.put("model_id",temp_model);
        request.put("name",temp_name);
        request.put("email",temp_email);
        request.put("company_name",temp_companyname);
        request.put("contact",temp_contact);
        request.put("year",temp_year);
        request.put("part_no",temp_part);
        request.put("part_name",temp_partname);
        request.put("description",temp_desc);
        request.put("remarks",temp_remarks);
        request.put("country_code_part",temp_contact);

        sparePartsViewModel.func_spareparts(request).observe(SpareParts_Activity.this, new Observer<SpareParts_Response>() {
            @Override
            public void onChanged(SpareParts_Response spareParts_response) {
                Commons.hideProgress();
                    if(spareParts_response.status.equals("1"))
                    {
                        //Commons.Alert_custom(context,spareParts_response.long_message);
                        onBackPressed();
                    }
                    else if(spareParts_response.status.equals("0"))
                    {
                        Commons.Alert_custom(context,spareParts_response.long_message);
                    }

            }
        });


    }


    private void WebServiceCall_MakeList() {

        HashMap<String,String> request=new HashMap<>();
        request.put("isPreowned","no");

        sparePartsViewModel.getcarmakelist(request).observe(this, new Observer<Response_CarMakeList>() {
            @Override
            public void onChanged(Response_CarMakeList response_carMakeList) {

                if(response_carMakeList.status.equals("1")) {
                    System.out.println("===res=preowned=makelist=" + response_carMakeList.data.toString());
                    makelistsize=response_carMakeList.data.size();
                    carmakelist=response_carMakeList.data;
                    Log.i("res-=preownr maklist0->",response_carMakeList.message);

                }
                else if(response_carMakeList.status.equals("0"))
                {
                    Log.i("r=preownr maklist->",response_carMakeList.message);
                    System.out.println("===r=preownr maklist=" + response_carMakeList.data);
                    Toast.makeText(context,response_carMakeList.message,Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void WebServiceCall_ModelList() {

        Commons.showProgress(context);
        HashMap<String,String> request=new HashMap<>();
        request.put("make_id",ip_makeid);
        request.put("isPreowned","no");

        System.out.println("url-modellist->"+request);
        Log.e("--modellist--->",request.toString());
        sparePartsViewModel.getCarModeldata(request).observe(this, new Observer<Response_CarModelList>() {
            @Override
            public void onChanged(Response_CarModelList response_carModelList) {

                Commons.hideProgress();
                System.out.println("===res=car model list=" + response_carModelList.message);
                if(response_carModelList.status.equals("1"))
                {
                    carmodellist=response_carModelList.data;
                    modellistsize=carmodellist.size();
                }
                else if(response_carModelList.status.equals("0"))
                {
                    Toast.makeText(context,response_carModelList.message,Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    private void Dialog_NumberPicker_MakeList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(makelistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmakelist.size()];
        str_makedid=new String[carmakelist.size()];
        for (int i=0;i<carmakelist.size();i++)
        {
            str[i]=carmakelist.get(i).makename;
            str_makedid[i]=carmakelist.get(i).mkid;
        }

        np.setDisplayedValues(str);
        np.setValue(index);
        temp_makelist=str[index];
        ip_makeid=str_makedid[index];

        button_addquote.setOnClickListener(v -> {
            et_make.setText(temp_makelist);
            //  PreOwned__Fragment1.res_make=temp_makelist;
            dialog.dismiss();
            WebServiceCall_ModelList();
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_makelist=str[newVal];
                ip_makeid=str_makedid[newVal];
                index=newVal;
            }
        });
        dialog.show();
    }

    private void Dialog_NumberPicker_ModelList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(modellistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmodellist.size()];
        strmodellist=new String[carmodellist.size()];
        for (int i=0;i<carmodellist.size();i++)
        {
            str[i]=carmodellist.get(i).modelName;
            strmodellist[i]=carmodellist.get(i).modid;
        }
        np.setDisplayedValues(str);
        temp_model=str[index];
        ipmodelid=strmodellist[index];


        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_model.setText(temp_model);
                // PreOwned__Fragment1.res_make=temp_model;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_model=str[newVal];
                ipmodelid=strmodellist[newVal];
            }
        });
        dialog.show();
    }


    private void Dialog_Year() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Year_Preowned);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_year=transmission_array[index];

        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_year.setText(temp_year);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_year=transmission_array[newVal];
            }
        });
        dialog.show();
    }

}
