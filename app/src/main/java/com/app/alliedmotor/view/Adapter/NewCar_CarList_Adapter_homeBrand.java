package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.NewCar.CarDatasItem;
import com.app.alliedmotor.model.NewCar.CarListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.util.ArrayList;

public class NewCar_CarList_Adapter_homeBrand extends RecyclerView.Adapter<NewCar_CarList_Adapter_homeBrand.ViewHolder> {

    ArrayList<CarListItem> carListItems = new ArrayList<>();
    private final int limit = 4;

    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;
    NewCar_Dataitems_Adapter_homebrand newCarDataitemsAdapter;
    ArrayList<CarDatasItem> dataItems = new ArrayList<>();

    public NewCar_CarList_Adapter_homeBrand(Context context, ArrayList<CarListItem> listdata) {
        this.mcontext = context;
        this.carListItems = listdata;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.festival_rv_homepagebrandslection, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        dataItems=carListItems.get(position).getCarDatas();
        holder.textview1.setText(carListItems.get(position).getTitle());


        GridLayoutManager gridLayoutManager = new GridLayoutManager(mcontext,2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
        holder.rv_list_festivals.setLayoutManager(gridLayoutManager);
        holder.rv_list_festivals.setHasFixedSize(true);


        newCarDataitemsAdapter = new NewCar_Dataitems_Adapter_homebrand(mcontext, dataItems);
        holder.rv_list_festivals.setAdapter(newCarDataitemsAdapter);




    }

    @Override
    public int getItemCount() {

        return carListItems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewSemiBold textview1;
        RecyclerView rv_list_festivals;
        //CustomRegularButton viewall;


        public ViewHolder(View itemView) {

            super(itemView);

            this.textview1 = itemView.findViewById(R.id.textview1);
            this.rv_list_festivals = itemView.findViewById(R.id.rv_list_festivals);
          //  this.viewall = itemView.findViewById(R.id.textView23);

        }
    }
}

