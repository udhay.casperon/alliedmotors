package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.MyOrderLatest.OrderDataItem;
import com.app.alliedmotor.model.MyOrderLatest.OrdersItem;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewMedium;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class YourOrder_Adapter extends RecyclerView.Adapter<YourOrder_Adapter.ViewHolder> {

    ArrayList<OrderDataItem> dataItems;
    ArrayList<OrdersItem>adata=new ArrayList<>();

    LinearLayoutManager linearLayoutManager;
    SharedPref sharedPref;
    private Context mcontext;
    public boolean isClickedFirstTime = true;
    String dateStr="";
    MyOrder_rowsAdpater myOrder_rowsAdpater;
    ArrayList<Boolean> CheckList = new ArrayList<Boolean>();
    String ticketid="";

    String mobilenum="",sharing_content="";


    public YourOrder_Adapter(Context context, ArrayList<OrderDataItem> dataItems,String ticketid) {
        this.mcontext = context;
        this.dataItems = dataItems;
        this.ticketid=ticketid;
      if(CheckList.size()>0){
          CheckList.clear();
      }
        for(int i = 0; i<dataItems.size();i++){
            CheckList.add(false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_your_order_linear, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false);
        holder.rv_order_rows.setLayoutManager(linearLayoutManager);


        dateStr = dataItems.get(position).getEnqCrtdate();
        String resultDate = convertStringDateToAnotherStringDate(dateStr, "dd-MM-yyyy hh:mm a", "dd/MM/yyyy hh:mm a");


        if(CheckList.get(position)){
            holder.ll_detailed_content.setVisibility(View.VISIBLE);
            holder.ll_short_content.setVisibility(View.GONE);
            adata=dataItems.get(position).getOrders();
                    holder.ll_detailed_content.setBackgroundResource(R.drawable.cardview_round_plain_blue);
                    myOrder_rowsAdpater=new MyOrder_rowsAdpater(mcontext,adata);
                    holder.rv_order_rows.setAdapter(myOrder_rowsAdpater);

        }else {
            holder.ll_detailed_content.setVisibility(View.GONE);
            holder.ll_short_content.setVisibility(View.VISIBLE);
        }


        if(ticketid!=null)
        {
            if(ticketid.equals(dataItems.get(position).getTicketId()))
            {

                holder.ll_detailed_content.setVisibility(View.VISIBLE);
                holder.ll_short_content.setVisibility(View.GONE);
                adata=dataItems.get(position).getOrders();
                holder.ll_detailed_content.setBackgroundResource(R.drawable.cardview_round_plain_blue);
                myOrder_rowsAdpater=new MyOrder_rowsAdpater(mcontext,adata);
                holder.rv_order_rows.setAdapter(myOrder_rowsAdpater);

            }
        }



        holder.tv_carCategory.setText(dataItems.get(position).getCarCategory());
        holder.tv_orderdate.setText(resultDate);
        holder.tv_ticketno.setText("Ticket no "+dataItems.get(position).getTicketId());
        holder.tv_carCategory1.setText(dataItems.get(position).getCarCategory());
        holder.tv_orderdate1.setText(resultDate);
        holder.tv_ticketno1.setText("Ticket no "+dataItems.get(position).getTicketId());

        String status_ticket=dataItems.get(position).getEnqStatusTicket();
        holder.tv_ticket_result1.setText(status_ticket);
        holder.tv_ticket_result.setText(status_ticket);




        if(status_ticket.equalsIgnoreCase("pending"))
        {

            holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
            holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
            holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.tv_ticket_result1.setText("RFQ-submitted");
            holder.tv_ticket_result.setText("RFQ-submitted");
        }
        if(status_ticket.equalsIgnoreCase("Read"))
        {
            holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
            holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
            holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.tv_ticket_result1.setText(status_ticket);
            holder.tv_ticket_result.setText(status_ticket);
        }

        if(status_ticket.equalsIgnoreCase("In Progress"))
        {
            holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
            holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
            holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.tv_ticket_result1.setText(status_ticket);
            holder.tv_ticket_result.setText(status_ticket);
        }

        if(status_ticket.equalsIgnoreCase("Won"))
        {
            holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.won_text_green));
            holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.won_text_green));
            holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.tv_ticket_result1.setText(status_ticket);
            holder.tv_ticket_result.setText(status_ticket);
        }


       if(status_ticket.equalsIgnoreCase("Unfulfilled") )
        {
            holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rejected_bg));
            holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rejected_bg));
            holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.tv_ticket_result1.setText(status_ticket);
            holder.tv_ticket_result.setText(status_ticket);
        }

       if(status_ticket.equalsIgnoreCase("Cancelled"))
       {
           holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rejected_bg));
           holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
           holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rejected_bg));
           holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
           holder.tv_ticket_result1.setText(status_ticket);
           holder.tv_ticket_result.setText(status_ticket);
       }

        holder.ll_short_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(CheckList.get(position)){
//                   holder.ll_short_content.setVisibility(View.GONE);
                   CheckList.set(position,false);
               }else {
//                   holder.ll_short_content.setVisibility(View.VISIBLE);
                   CheckList.set(position,true);
               }
               notifyDataSetChanged();
//                if(isClickedFirstTime)
//                {
//                    isClickedFirstTime=false;
//                    holder.ll_detailed_content.setVisibility(View.VISIBLE);
//                    holder.ll_short_content.setVisibility(View.GONE);
//                    adata=dataItems.get(position).getOrders();
//                    holder.ll_detailed_content.setBackgroundResource(R.drawable.cardview_round_plain_blue);
//                    myOrder_rowsAdpater=new MyOrder_rowsAdpater(mcontext,adata);
//                    holder.rv_order_rows.setAdapter(myOrder_rowsAdpater);
//
//                }
//                else
//                {
//                    isClickedFirstTime=true;
//                    holder.ll_detailed_content.setVisibility(View.GONE);
//                    holder.ll_short_content.setVisibility(View.VISIBLE);
//                }
            }
        });


        holder.ll_detailed_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isClickedFirstTime)
                {
                    isClickedFirstTime=false;
                    holder.ll_detailed_content.setVisibility(View.GONE);
                    holder.ll_short_content.setVisibility(View.VISIBLE);
                }
                else
                {
                    isClickedFirstTime=true;
                    holder.ll_detailed_content.setVisibility(View.VISIBLE);
                    holder.ll_short_content.setVisibility(View.GONE);
                }
            }
        });

        holder.cl_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+dataItems.get(position).getMakeMobileNumber()));
                mcontext.startActivity(intent);
            }
        });

        holder.cv_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  AppInstalled.isAppInstalled("com.whatsapp", mcontext);
                mobilenum=dataItems.get(position).getMakeMobileNumber();
                openWhatsApp1(mobilenum);
            }
        });
    }


    private void openWhatsApp1(String mobilenum1) {
        try {
            Uri uri = Uri.parse("smsto:" + mobilenum1);
            //  Intent shareIntent = new Intent(Intent.ACTION_SENDTO, uri);

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
         //   shareIntent.putExtra(Intent.EXTRA_TEXT, sharing_content);
            shareIntent.putExtra("jid", mobilenum1 + "@s.whatsapp.net");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, sharing_content);
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            shareIntent.setPackage("com.whatsapp");

            mcontext.startActivity(shareIntent);
        }
        catch(Exception e)

        {
            e.printStackTrace();
            Commons.Alert_custom(mcontext, "Please Install WhatsApp in your phone");
        }
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewMedium tv_ticket_result,tv_ticket_result1;
        CustomTextViewRegular tv_orderdate,tv_ticketno,tv_orderdate1,tv_ticketno1;
        CardView cl_Call,cv_message,cv_status,cv_status1;

        LinearLayout ll_short_content,ll_detailed_content;

        RecyclerView rv_order_rows;

        CustomTextViewSemiBold tv_carCategory,tv_carCategory1;


        public ViewHolder(View itemView) {

            super(itemView);

            rv_order_rows=itemView.findViewById(R.id.rv_order_rows);
            tv_carCategory=itemView.findViewById(R.id.tv_carname);
            tv_ticket_result=itemView.findViewById(R.id.tv_ticket_result);
            tv_ticket_result1=itemView.findViewById(R.id.tv_ticket_result1);
            tv_orderdate=itemView.findViewById(R.id.tv_orderdate);
            tv_carCategory1=itemView.findViewById(R.id.tv_carname1);
            tv_ticketno=itemView.findViewById(R.id.tv_ticketno);
            tv_orderdate1=itemView.findViewById(R.id.tv_orderdate1);
            tv_ticketno1=itemView.findViewById(R.id.tv_ticketno1);
            cl_Call=itemView.findViewById(R.id.cl_Call);
            cv_message=itemView.findViewById(R.id.cv_message);



            ll_short_content=itemView.findViewById(R.id.ll_short_content);
            ll_detailed_content=itemView.findViewById(R.id.ll_detailed_content);
            cv_status=itemView.findViewById(R.id.cv_status);

            cv_status1=itemView.findViewById(R.id.cv_status1);

        }
    }

    public String convertStringDateToAnotherStringDate(String dateStr, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(dateStr);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    private int getcurrentview(String chosen1)
    {
        for(int i=0; i < dataItems.size(); i++){
            if(dataItems.get(i).getTicketId().equals(chosen1)){
                return i;
            }
        }
        return 0;
    }


}

