package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.app.alliedmotor.R;
import com.app.alliedmotor.view.Activity.ImageViewer_Activity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CarDetailImageBanner_Adapter extends PagerAdapter {
    Context context;
    ArrayList<String> cargallery_list;

    LayoutInflater mLayoutInflater;


    public CarDetailImageBanner_Adapter(Context context, ArrayList<String> cargallery_list) {
        this.context = context;
        this.cargallery_list = cargallery_list;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // return the number of images
        return cargallery_list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((CardView) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        // inflating the item.xml
        View itemView = mLayoutInflater.inflate(R.layout.banner_image_slider_detail, container, false);

        // referencing the image view from the item.xml file
        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_banner);

        LinearLayout ll_progress=itemView.findViewById(R.id.ll_progress);


        Picasso.get().load(cargallery_list.get(position)).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        ll_progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
                // Adding the View
        (container).addView(itemView);

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toimageview=new Intent(context, ImageViewer_Activity.class);
                toimageview.putStringArrayListExtra("car_images",cargallery_list);
                context.startActivity(toimageview);
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((CardView) object);
    }



    }



