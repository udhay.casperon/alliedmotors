package com.app.alliedmotor.view.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.ForgotEmail.Response_ForgotEmail;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.CountryDialCode;
import com.app.alliedmotor.utility.GPSTracker;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.viewmodel.ForgotEmailviewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ForgotEmail extends AppCompatActivity implements View.OnClickListener {

    Context context;
    ImageView back_setting;
    CustomRegularEditText et_resetcode, et_phone;
    CustomRegularButton bt_resetpassword;

    String st_phone, st_resotp, st_resetcode;


    private LinearLayout myCountryCodeLAY;
    private ImageView myFlagIMG;
    private TextView myCountryCodeTXT;
    private CountryPicker myPicker;
    private String myFlagStr = "";
    private GPSTracker myGPSTracker;
    ForgotEmailviewModel forgotEmailviewModel;
    LinearLayout loading_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_email);
        forgotEmailviewModel = ViewModelProviders.of(this).get(ForgotEmailviewModel.class);

        context = this;
        findbyviews();
        clicklistener();
        classAndWidgetInitialize();


    }

    private void classAndWidgetInitialize() {

        myPicker = CountryPicker.newInstance("Select Country");
        myPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flag) {
                myPicker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                myFlagIMG.setVisibility(View.VISIBLE);
                myFlagStr = "" + flag;
                myFlagIMG.setImageResource(flag);
                String exampleNumber = String.valueOf(PhoneNumberUtil.getInstance().getExampleNumber(code).getNationalNumber());
                et_phone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(exampleNumber.length())});  //mypicker.setlistener

            }
        });
        setCountryDataValues();

    }

    private void setCountryDataValues() {
        if (myGPSTracker.canGetLocation() && myGPSTracker.isgpsenabled()) {
            double MyCurrent_lat = myGPSTracker.getLatitude();
            double MyCurrent_long = myGPSTracker.getLongitude();
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String aCountryCodeStr = addresses.get(0).getCountryCode();
                    if (aCountryCodeStr.length() > 0 && !aCountryCodeStr.equals(null) && !aCountryCodeStr.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(aCountryCodeStr);
                        myCountryCodeTXT.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + aCountryCodeStr.toLowerCase(Locale.ENGLISH);
                        myFlagIMG.setImageResource(getResId(drawableName));
                        myFlagStr = "" + getResId(drawableName);
                        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(Str_countyCode));
                        String exampleNumber = String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                        int phoneLength = exampleNumber.length();
                        et_phone.setFilters(new InputFilter[]{
                                new InputFilter.LengthFilter(phoneLength)});
                        et_phone.setEms(phoneLength);
                        st_phone = et_phone.getText().toString().trim();
                        System.out.println("==d==ff=" + et_phone.getText().toString().trim());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getResId(String drawableName) {
        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
        }
        return -1;
    }

    private void findbyviews() {
        back_setting = findViewById(R.id.back_setting);
        et_resetcode = findViewById(R.id.et_resetcode);
        et_phone = findViewById(R.id.et_phone);
        bt_resetpassword = findViewById(R.id.bt_resetpassword);

        myGPSTracker = new GPSTracker(ForgotEmail.this);
        myCountryCodeLAY = (LinearLayout) findViewById(R.id.activity_signup_mobile_no_LAY1);
        myFlagIMG = (ImageView) findViewById(R.id.activity_signup_country_flag_IMG1);
        myCountryCodeTXT = findViewById(R.id.activity_signup_country_code_TXT1);
        loading_ll = findViewById(R.id.loading_ll);


    }

    private void clicklistener() {
        back_setting.setOnClickListener(this);
        bt_resetpassword.setOnClickListener(this);
        myCountryCodeLAY.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back_setting:
                Intent tologin = new Intent(ForgotEmail.this, LoginActivity.class);
                startActivity(tologin);
                finish();
                break;

            case R.id.bt_resetpassword:
                Method_PasswordRest();
                break;

            case R.id.activity_signup_mobile_no_LAY1:
                myPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
        }
    }

    private void Method_Otp() {

        st_resetcode = et_resetcode.getText().toString().trim();

        if (st_resetcode.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter the Reset Code");
            et_resetcode.requestFocus();
        } else if (!st_resetcode.equals(st_resotp)) {
            Commons.Alert_custom(context, "Please Enter valid reset code");
            et_resetcode.requestFocus();
        } else {
            Intent tochangepwd = new Intent(ForgotEmail.this, LoginActivity.class);
            startActivity(tochangepwd);
            finish();
        }


    }

    private void Method_PasswordRest() {
        st_phone = myCountryCodeTXT.getText().toString().trim() + "" + et_phone.getText().toString().trim();


        if (st_phone.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter Registered Mobile number");
            et_phone.requestFocus();
        } else {
            Method_Resetcode();
        }
    }

    private void Method_Resetcode() {

        HashMap<String, String> request_forgotemail = new HashMap<>();
        request_forgotemail.put("mobile_number", st_phone);
        System.out.println("==req-forgot-usermail---" + request_forgotemail);
        loading_ll.setVisibility(View.VISIBLE);

        forgotEmailviewModel.getforgotemaildata(request_forgotemail).observe(ForgotEmail.this, new Observer<Response_ForgotEmail>() {
            @Override
            public void onChanged(Response_ForgotEmail response_forgotEmail) {

                loading_ll.setVisibility(View.GONE);
                System.out.println("===respo---forgot pwd==" + response_forgotEmail.toString());
                if (response_forgotEmail.getStatus().equals("1")) {
                    dialog_ViewEmail(context, response_forgotEmail.getData().getEmailAddress());
                    /*if(response_forgotEmail.getData().getDevMode()==1)
                    {
                        System.out.println("===localserver===");
                        st_resotp=String.valueOf(response_forgotEmail.getData().getCode());

                        Intent tootppage=new Intent(ForgotEmail.this,OTPVerification_Activity_forgotmail.class);
                        tootppage.putExtra("otptype","Phone");

                        tootppage.putExtra("mobilenumber",st_phone);
                        tootppage.putExtra("pagename","verifyphonenumber");
                        tootppage.putExtra("otpdevmode",String.valueOf(response_forgotEmail.getData().getDevMode()));
                        tootppage.putExtra("contentmessage",response_forgotEmail.getLongMessage());
                        tootppage.putExtra("otptoken",response_forgotEmail.getData().getCode());
                        startActivity(tootppage);

                    }
                    else
                    {
                        System.out.println("==production===");
                        st_resotp=String.valueOf(response_forgotEmail.getData().getCode());
                        Intent tootppage=new Intent(ForgotEmail.this,OTPVerification_Activity_forgotmail.class);
                        tootppage.putExtra("otptype","Phone");
                        tootppage.putExtra("useremail",response_forgotEmail.getData().getEmailAddress());
                        tootppage.putExtra("mobilenumber",st_phone);
                        tootppage.putExtra("pagename","verifyphonenumber");
                        tootppage.putExtra("otpdevmode",String.valueOf(response_forgotEmail.getData().getDevMode()));
                        tootppage.putExtra("contentmessage",response_forgotEmail.getLongMessage());
                        tootppage.putExtra("otptoken",response_forgotEmail.getData().getCode());
                        startActivity(tootppage);
                       // Commons.Alert_custom(context,response_forgotEmail.getData().getCode().toString());
                    }*/
                } else {
                    Commons.Alert_custom(context, response_forgotEmail.getLongMessage());
                }

            }
        });

    }

    private void dialog_ViewEmail(final Context context, String message) {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_ios_singlebutton);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        txtInstructionDialog.setText(message);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview rlClose = alertDialog.findViewById(R.id.bt_okay);
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tootppage=new Intent(ForgotEmail.this,LoginActivity.class);
                startActivity(tootppage);
                alertDialog.dismiss();

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();



    }
}