package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;

import java.util.ArrayList;

public class Detail_Adapter_CarInterior extends RecyclerView.Adapter<Detail_Adapter_CarInterior.ViewHolder> {

    ArrayList<String> interiorcolor;

    Detail_Adapter_CarInterior.IColorCategory listener;
    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;
    String auth_token="";
    int curr_position=0;
    String selecetdcolor="";

    public Detail_Adapter_CarInterior(Context context, ArrayList<String> listdata,Detail_Adapter_CarInterior.IColorCategory listener,String selectedcolor) {
        this.mcontext = context;
        this.interiorcolor = listdata;
        this.listener = listener;
        this.selecetdcolor=selectedcolor;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_interiorcolor, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        auth_token=sharedPref.getString(Constants.JWT_TOKEN);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String interior_color=String.valueOf(interiorcolor.get(position));
        if(interior_color.startsWith("#")) {
            Drawable unwrappedDrawable = holder.tv_tick1_bg.getBackground();
            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
            DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(interiorcolor.get(position))));
        }
        else
        {
            holder.ll_selection.setVisibility(View.GONE);
        }

        if(selecetdcolor.equals(interiorcolor.get(position))) {
            curr_position=interiorcolor.indexOf(interiorcolor.get(position));
            if (curr_position == position) {

                holder.Ext_ll_outer.setBackgroundResource(R.drawable.circle_bg);
            } else {

                holder.Ext_ll_outer.setBackgroundResource(R.drawable.circle_bg_white);

            }

        }


        holder.tv_tick1_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onIColorSelected(interiorcolor.get(position));
                curr_position=position;
                //notifyDataSetChanged();
            }
        });



    }

    @Override
    public int getItemCount() {
        return interiorcolor.size();
    }

    public interface IColorCategory {
        void onIColorSelected(String data);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout tv_tick1_bg,Ext_ll_middle,Ext_ll_outer;
        LinearLayout ll_selection;


        public ViewHolder(View itemView) {

            super(itemView);
            this.Ext_ll_outer = itemView.findViewById(R.id.Ext_ll_outer);
            this.Ext_ll_middle = itemView.findViewById(R.id.Ext_ll_middle);
            this.tv_tick1_bg = itemView.findViewById(R.id.tv_tick1_bg);
            this.ll_selection = itemView.findViewById(R.id.ll_selection);


        }
    }
}

