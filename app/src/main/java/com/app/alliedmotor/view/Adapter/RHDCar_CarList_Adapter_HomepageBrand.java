package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.RHDCar.CarDatasItem;
import com.app.alliedmotor.model.RHDCar.CarListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomBoldTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.textview.BoldCustomTextView;

import java.util.ArrayList;

public class RHDCar_CarList_Adapter_HomepageBrand extends RecyclerView.Adapter<RHDCar_CarList_Adapter_HomepageBrand.ViewHolder> {

    ArrayList<CarListItem> carListItems = new ArrayList<>();


    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;
    RHDCar_Dataitems_Adapter_Homebrand rhdCar_dataitems_adapter;
    ArrayList<CarDatasItem> dataItems = new ArrayList<>();

    public RHDCar_CarList_Adapter_HomepageBrand(Context context, ArrayList<CarListItem> listdata) {
        this.mcontext = context;
        this.carListItems = listdata;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.festival_rv_homepagebrandslection, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        dataItems=carListItems.get(position).getCarDatas();
        holder.textview1.setText(carListItems.get(position).getTitle());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mcontext,2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
        holder.rv_list_festivals.setLayoutManager(gridLayoutManager);
        holder.rv_list_festivals.setHasFixedSize(true);


        for (int i=0;i<dataItems.size();i++)
        {
            if(dataItems.get(i).getModId().equals("0"))
            {
                dataItems.remove(dataItems.get(i));
            }
            else
            {
                dataItems=dataItems;
            }
        }


        rhdCar_dataitems_adapter = new RHDCar_Dataitems_Adapter_Homebrand(mcontext, dataItems);
        holder.rv_list_festivals.setAdapter(rhdCar_dataitems_adapter);
    }

    @Override
    public int getItemCount() {
        return carListItems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewSemiBold textview1;
        RecyclerView rv_list_festivals;
        CustomBoldTextview tv_nodatafound;
      //  CustomRegularButton viewall;


        public ViewHolder(View itemView) {

            super(itemView);

           // this.viewall = itemView.findViewById(R.id.textView23);
            this.textview1 = itemView.findViewById(R.id.textview1);
            this.rv_list_festivals = itemView.findViewById(R.id.rv_list_festivals);
            this.tv_nodatafound = itemView.findViewById(R.id.tv_nodatafound);
        }
    }
}

