package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;

import java.util.ArrayList;

public class Custom_NumberAdapter extends RecyclerView.Adapter<Custom_NumberAdapter.ViewHolder> {

    ArrayList<Integer> features;

    SharedPref sharedPref;
    private Context mcontext;
    countListener listener;
    ArrayList<Integer>number_count=new ArrayList<>();

    public interface countListener {
        void onclickbutton(View v,int position);
    }
    public Custom_NumberAdapter(Context context, ArrayList<Integer> features) {
        this.mcontext = context;
        this.features = features;
        this.listener=listener;

        for(int i=1;i<100;i++)
        {
            number_count.add(i);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_snap_number, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    public Integer getItem(int position) {
        return features.get(position);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tv_numberpicker.setText(String.valueOf(features.get(position)));


    }

    @Override
    public int getItemCount() {
                    return features.size();
                                }



    public class ViewHolder extends RecyclerView.ViewHolder {

       CustomRegularTextview tv_numberpicker;
        public ViewHolder(View itemView) {

            super(itemView);

            tv_numberpicker=itemView.findViewById(R.id.tv_numberpicker);
        }
    }
}

