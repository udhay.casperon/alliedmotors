package com.app.alliedmotor.view.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Response_OtpRequest;
import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.viewmodel.VerifyOTPviewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.poovam.pinedittextfield.LinePinField;

import java.util.HashMap;

public class OTPVerification_Activity_MyProfile extends AppCompatActivity implements View.OnClickListener{



    CustomRegularButton bt_submit;
  ImageView back_setting;
    String st_otp;
    Context context;
    VerifyOTPviewModel verifyOTPviewModel;
    CustomRegularTextview tv_resent_otp;
    CustomRegularTextview tv_otpviewcontent;

    String res_otp,otp_type,r_contentmessage,r_otpdevmode,r_otptoken;
    SharedPref sharedPref;

    LinePinField otppinfield;
    String r_fname="",r_lname="",r_emai="",e_mobile="",r_usertype="",r_company="",r_userrole="",r_password="",r_userletter="",r_otptype="";
  String r_pagename="verifyphonenumber";

    String FCM_token="";
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_verification_);
        verifyOTPviewModel = ViewModelProviders.of(this).get(VerifyOTPviewModel.class);

        context=this;
        findbyviews();
        clicklistener();
        sharedPref=new SharedPref(context);



         Generate_FCMToken();

        Intent getIntent=getIntent();

        if(getIntent!=null) {

            r_emai = getIntent.getStringExtra("useremail");
            e_mobile = getIntent.getStringExtra("mobilenumber");
            r_otptype = getIntent.getStringExtra("otptype");
            r_contentmessage = getIntent.getStringExtra("contentmessage");
            r_otpdevmode = getIntent.getStringExtra("otpdevmode");
            r_otptoken = getIntent.getStringExtra("otptoken");
            r_pagename=getIntent.getStringExtra("pagename");
        }
        tv_otpviewcontent.setText(r_contentmessage);
        Commons.Alert_custom(context,r_contentmessage);


        if(r_otpdevmode.equals("1"))
        {
            System.out.println("--Development---");
            otppinfield.setText(r_otptoken);
        }
        else if(r_otpdevmode.equals("0"))
        {
            System.out.println("--Production---");
            new OTPReceiver_myprofile().setEditText_otp(otppinfield);

        }

        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                Func_OtpResent();
            }
        };
        requestPermissions();
        new OTPReceiver_myprofile().setEditText_otp(otppinfield);

    }

    private void Generate_FCMToken() {

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        FCM_token = task.getResult();

                        System.out.println("------token--------"+FCM_token);
                    }
                });

    }

    private void findbyviews() {
        bt_submit=findViewById(R.id.bt_submit);
        back_setting=findViewById(R.id.back_setting);
        otppinfield=findViewById(R.id.otppinfield);
        tv_otpviewcontent=findViewById(R.id.tv_otpviewcontent);
        tv_resent_otp=findViewById(R.id.tv_resent_otp);

    }


    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(OTPVerification_Activity_MyProfile.this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(OTPVerification_Activity_MyProfile.this,new String[]{
                    Manifest.permission.RECEIVE_SMS
            },100);
        }
    }

    private void clicklistener() {
        bt_submit.setOnClickListener(this);
        tv_resent_otp.setOnClickListener(this);
        back_setting.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back_setting:
                onBackPressed();
                break;
            case R.id.tv_resent_otp:
                autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                autoCompleteHandler.postDelayed(autoCompleteRunnable, 500);
                break;
            case R.id.bt_submit:
                Validtion_OTP();
                break;

        }
    }



    private void Func_OtpResent() {

       Commons.showProgress(context);
        HashMap<String, String> request = new HashMap<>();
        request.put("otp_type",r_otptype);
        request.put("page_name",r_pagename);
        request.put("user_email",r_emai);
        request.put("mobile_number",e_mobile);

        verifyOTPviewModel.getOTPRequestdata(request).observe(this, new Observer<Response_OtpRequest>() {
            @Override
            public void onChanged(Response_OtpRequest response_otpRequest) {
                Commons.hideProgress();
                System.out.println("dv==="+response_otpRequest.toString());
                if(response_otpRequest.status.equals("1")) {
                    otppinfield.getText().clear();
                    r_otpdevmode=String.valueOf(response_otpRequest.data.otpData.devMode);
                    r_otptoken=response_otpRequest.data.otpData.code;
                    if(r_otpdevmode.equals("1"))
                    {
                        System.out.println("--Development---");
                        otppinfield.setText(r_otptoken);
                    }
                    else if(r_otpdevmode.equals("0"))
                    {
                        System.out.println("--Production---");
                        new OTPReceiver_myprofile().setEditText_otp(otppinfield);
                        otppinfield.setText(r_otptoken);
                    }

                }
                else if(response_otpRequest.status.equals("0"))
                {
                    Commons.Alert_custom(context,response_otpRequest.longMessage);

                }
            }
        });



    }

    private void Validtion_OTP() {
         st_otp=otppinfield.getText().toString().trim();

         if(st_otp.isEmpty())
         {
             Commons.Alert_custom(context,"Please Enter OTP");
         }

         else if(!r_otptoken.equals(st_otp))
         {
             Commons.Alert_custom(context,"Entered Otp wrong");
         }
        else
        {
            Call_API_VerifyOTP();
        }
    }

    private void Call_API_VerifyOTP() {

        Commons.showProgress(context);

        HashMap<String, String> header = new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String, String> request = new HashMap<>();
        request.put("received_otp",r_otptoken);
        request.put("enter_otp",st_otp);
        request.put("otp_type",r_otptype);

        verifyOTPviewModel.getVerifyOTpdata(header,request).observe(this, new Observer<Response_VerifyOTP>() {
            @Override
            public void onChanged(Response_VerifyOTP responseVerifyOTP) {
                Commons.hideProgress();
                System.out.println("dv==="+responseVerifyOTP.toString());
                if(responseVerifyOTP.status.equals("1")) {
                   Commons.Alert_custom(context,responseVerifyOTP.longMessage);
                   onBackPressed();
                }
                else if(responseVerifyOTP.status.equals("0"))
                {
                    Commons.Alert_custom(context,responseVerifyOTP.longMessage);

                }
            }
        });

    }

    }
