package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.HomePage.CategoriesItem;
import com.app.alliedmotor.model.HomePage.DataItem;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.Car_Detail__Constarint_Activity1;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Activity.PreOwnedCar_Detail_Activity;
import com.mohammedalaa.gifloading.LoadingView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FestivalList_Fullcar_Adapter extends RecyclerView.Adapter<FestivalList_Fullcar_Adapter.ViewHolder> {

    ArrayList<DataItem> caritems = new ArrayList<>();

    FestivalList_Fullcar_Adapter.Categoires listener;
    SharedPref sharedPref;
    private Context mcontext;


    public FestivalList_Fullcar_Adapter(Context context, ArrayList<DataItem> listdata) {
        this.mcontext = context;
        this.caritems = listdata;
        this.listener = listener;
    }

    public int getScreenWidth() {

        WindowManager wm = (WindowManager) mcontext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size.x;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.festival_car_rv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String imgurl = caritems.get(position).getCarImage();
        String promoimage=caritems.get(position).getPromo_logo();
        System.out.println("--dhd-11--"+sharedPref.getString(Constants.JWT_TOKEN));

        if(caritems.get(position).getMainId()!=null) {
            Picasso.get().load(caritems.get(position).getMain_img())
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        }
        if(caritems.get(position).getPreId()!=null)
        {
            Picasso.get().load(caritems.get(position).getMain_img())
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        }

        if( promoimage == null) {
            holder.promologo.setVisibility(View.GONE);
        }
        else
        {
            Picasso.get().load(caritems.get(position).getPromo_logo()).into(holder.promologo);
        }
        Picasso.get().load(caritems.get(position).getCurrencyFlagImage()).into(holder.img_country_flag);
        Picasso.get().load(caritems.get(position).getMakeIcon()).into(holder.img_carlogo);
        holder.tv_makername.setText(caritems.get(position).getMakename());
        holder.tv_model_name.setText(caritems.get(position).getModelName());
        holder.tv_model_year.setText(caritems.get(position).getModelYear());
        String vehicle_price=caritems.get(position).getPrice();
        holder.tv_vehicle_type.setText(caritems.get(position).getVehiceType());


        if (sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null)) {
            holder.tv_veh_price.setText("Login for Price");
                holder.tv_veh_price.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent tologin = new Intent(mcontext, LoginActivity.class);
                        mcontext.startActivity(tologin);
                    }
                });

        }else if (sharedPref.getString(Constants.JWT_TOKEN)!=null) {

            if (vehicle_price.isEmpty() || vehicle_price.equals(null) || vehicle_price.equals("")) {
                holder.tv_veh_price.setText("Price on Request");
            } else {

                holder.tv_veh_price.setText("From * " + caritems.get(position).getCurrencyName() + " " + caritems.get(position).getPrice());
                holder.tv_model_year.setText(caritems.get(position).getModelYear());
                holder.tv_vehicle_type.setText(caritems.get(position).getVehiceType());
            }
        }

        holder.cv_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(caritems.get(position).getMainId()!=null) {
                    Intent toCarDetail = new Intent(mcontext.getApplicationContext(), Car_Detail__Constarint_Activity1.class);
                    toCarDetail.putExtra("carid", caritems.get(position).getMainId());
                    toCarDetail.putExtra("searchname", caritems.get(position).getEfuelType());
                    mcontext.startActivity(toCarDetail);

                }
                if(caritems.get(position).getPreId()!=null)
                {
                    Intent toCarDetail = new Intent(mcontext.getApplicationContext(), PreOwnedCar_Detail_Activity.class);
                    toCarDetail.putExtra("carid", caritems.get(position).getPreId());
                    toCarDetail.putExtra("searchname", caritems.get(position).getEfuelType());
                    mcontext.startActivity(toCarDetail);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return caritems.size();
    }

    public interface Categoires {
        void onCategorySelected(CategoriesItem data, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressbar;
        LoadingView loader;
        CardView cv_car;
        public ImageView imageView,img_carlogo,img_country_flag,promologo;
        CustomRegularTextview tv_makername,tv_model_year,tv_vehicle_type;
        CustomTextViewSemiBold tv_model_name,tv_veh_price;
        public ViewHolder(View itemView) {

            super(itemView);
            this.progressbar =  itemView.findViewById(R.id.progressbar);
            this.promologo =  itemView.findViewById(R.id.promo_logo);
            this.cv_car =  itemView.findViewById(R.id.cv_car);
            this.imageView =  itemView.findViewById(R.id.imageview1);
            this.img_carlogo =  itemView.findViewById(R.id.img_carlogo);
            this.img_country_flag =  itemView.findViewById(R.id.img_country_flag);
            this.tv_makername =  itemView.findViewById(R.id.tv_makername);
            this.tv_model_name =  itemView.findViewById(R.id.tv_model_name);
            this.tv_model_year =  itemView.findViewById(R.id.tv_model_year);
            this.tv_vehicle_type =  itemView.findViewById(R.id.tv_vehicle_type);
            this.tv_veh_price =  itemView.findViewById(R.id.tv_veh_price);


        }
    }
}

