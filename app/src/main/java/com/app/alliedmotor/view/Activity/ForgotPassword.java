package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.ForgotPassword.Response_ForgotPassword;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.viewmodel.ForgotPasswordviewModel;

import java.util.HashMap;
import java.util.regex.Pattern;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    Context context;
    ImageView back_setting;
    CustomRegularEditText et_resetcode,et_email;
    CustomRegularButton bt_resetpassword,bt_verify;
    LinearLayout ll_resetcode;
    String st_email,st_resotp,st_resetcode;
    ForgotPasswordviewModel forgotPasswordviewModel;


    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        forgotPasswordviewModel = ViewModelProviders.of(this).get(ForgotPasswordviewModel.class);
        context=this;
        findbyviews();
        clicklistener();

        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                Method_PasswordRest();
            }
        };



    }

    private void findbyviews() {
        back_setting=findViewById(R.id.back_setting);
        et_resetcode=findViewById(R.id.et_resetcode);
        et_email=findViewById(R.id.et_email);
        bt_resetpassword=findViewById(R.id.bt_resetpassword);
        bt_verify=findViewById(R.id.bt_verify);
        ll_resetcode=findViewById(R.id.ll_resetcode);
    }

    private void clicklistener() {
        back_setting.setOnClickListener(this);
        bt_resetpassword.setOnClickListener(this);
        bt_verify.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back_setting:
                Intent tologin=new Intent(ForgotPassword.this,LoginActivity.class);
                startActivity(tologin);
                finish();
                break;

            case R.id.bt_verify:
                Method_OtpVerify();
                break;

            case R.id.bt_resetpassword:
                autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                autoCompleteHandler.postDelayed(autoCompleteRunnable, 700);
                break;
        }
    }



    private void Method_PasswordRest() {

        st_email=et_email.getText().toString().trim();

        if(st_email.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Email-id");
            et_email.requestFocus();
        }
        else if(!validEmail(st_email))

        {
            Commons.Alert_custom(context,"Please Enter Valid Emailid");
            et_email.requestFocus();
        }
        else
        {
            Method_Resetcode();
        }
    }

    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void Method_Resetcode() {
        Commons.showProgress(context);
        HashMap<String, String> request_forgotpassword = new HashMap<>();
        request_forgotpassword.put("user_email",st_email);

        System.out.println("==req---login---"+request_forgotpassword);

        forgotPasswordviewModel.getforgotassworddata(request_forgotpassword).observe(ForgotPassword.this, new Observer<Response_ForgotPassword>() {
            @Override
            public void onChanged(Response_ForgotPassword response_forgotPassword) {
              //  ll_resetcode.setVisibility(View.VISIBLE);
                Commons.hideProgress();
                bt_resetpassword.setVisibility(View.VISIBLE);
                System.out.println("===respo---forgot pwd=="+response_forgotPassword.toString());
                if(response_forgotPassword.getStatus().equals("1"))
                {
                    if(response_forgotPassword.getData().getDevMode()==1)
                    {

                        System.out.println("===localserver===");
                        st_resotp=String.valueOf(response_forgotPassword.getData().getCode());

                        Intent tootppage=new Intent(ForgotPassword.this,OTPVerification_Activity_forgotpassword.class);
                        tootppage.putExtra("otptype","email");
                        tootppage.putExtra("useremail",st_email);
                        tootppage.putExtra("otpdevmode",String.valueOf(response_forgotPassword.getData().getDevMode()));
                        tootppage.putExtra("contentmessage",response_forgotPassword.getLongMessage());
                        tootppage.putExtra("otptoken",String.valueOf(response_forgotPassword.getData().getCode()));
                        startActivity(tootppage);
                        finish();
                        System.out.println("===localserver===");


                    }
                    else
                    {
                        System.out.println("==production===");
                        st_resotp=String.valueOf(response_forgotPassword.getData().getCode());
                        Intent tootppage=new Intent(ForgotPassword.this,OTPVerification_Activity_forgotpassword.class);
                        tootppage.putExtra("otptype","email");
                        tootppage.putExtra("useremail",st_email);
                        tootppage.putExtra("otpdevmode",String.valueOf(response_forgotPassword.getData().getDevMode()));
                        tootppage.putExtra("contentmessage",response_forgotPassword.getLongMessage());
                        tootppage.putExtra("otptoken",String.valueOf(response_forgotPassword.getData().getCode()));
                        startActivity(tootppage);
                        finish();
                      //  Commons.Alert_custom(context,response_forgotPassword.getData().getCode().toString());
                    }
                }
                else {
                    Commons.Alert_custom(context,response_forgotPassword.getLongMessage());
                }

            }
        });
        }

    private void Method_OtpVerify() {
        st_resetcode=et_resetcode.getText().toString().trim();

        if(st_resetcode.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter the Reset Code");
            et_resetcode.requestFocus();
        }
        else if(!st_resetcode.equals(st_resotp))
        {
            Commons.Alert_custom(context,"Please Enter valid reset code");
            et_resetcode.requestFocus();
        }
        else
        {
            Intent tochangepwd=new Intent(ForgotPassword.this,ChangePassword.class);
            startActivity(tochangepwd);
            finish();
        }

    }

}