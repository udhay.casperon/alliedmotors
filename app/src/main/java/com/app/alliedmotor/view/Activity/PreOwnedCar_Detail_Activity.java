package com.app.alliedmotor.view.Activity;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.viewpager.widget.ViewPager;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.Pojo_PreOwnedFilter;
import com.app.alliedmotor.model.PreOwned_CarDetail.Response_PreOwned_Detail;
import com.app.alliedmotor.model.QuoteDataItemDB;
import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.model.TechFeatureList_Pojo;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomBoldTextview;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularEditTextBold;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_ExtFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_IFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_OtherFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_SafetyFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_TechFeatures;
import com.app.alliedmotor.view.Adapter.CarDetailImageBanner_Adapter;
import com.app.alliedmotor.view.Adapter.Custom_NumberAdapter;
import com.app.alliedmotor.view.Adapter.Detail_Adapter_CarExterior;
import com.app.alliedmotor.view.Adapter.Detail_Adapter_CarInterior;
import com.app.alliedmotor.viewmodel.PreOwnedCarDetailviewModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.material.navigation.NavigationView;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class    PreOwnedCar_Detail_Activity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private CardView exteriorcolor, interiorcolor;
    RecyclerView rv_exteriorcolor, rv_interiorcolor;
    ImageView img_share, img_favourite, img_fb, img_twitter, img_instagram, img_whatsapp;
    CustomTextViewSemiBold tv_carname;
    CustomRegularTextview tv_model_yr, tv_model_type, tv_model_type1;
    CardView tv_extcolor_value, tv_tv_interior_color_value;
    CustomRegularButton bt_download_spec, bt_add_to_quote;
    LinearLayoutManager linearLayoutManager, linearLayoutManager1,linearLayoutManager10;
    public static final int REQUEST_CODE = 1;
    String slugnmae_type="pre-car";
    MyDataBase myDataBase;
    String sts_image,sts_modeloption,sts_varientcode,sts_makename,sts_carname,sts_year,sts_modid,sts_makid,sts_carcat;

    String sts_carinfo="pre-car";
    LinearLayout ll_selection1,ll_selection;

    CustomTextViewSemiBold textView;


    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;

    ImageView imageView, imageView27;
    ArrayList<String> exteriorfeatures_list = new ArrayList<>();
    ArrayList<String> interiorFeatures_list = new ArrayList<>();
    ArrayList<String> otherFeatures_list = new ArrayList<>();
    ArrayList<String> safetyFeatures_list = new ArrayList<>();
    ArrayList<String> listexterior_color = new ArrayList<>();
    ArrayList<String> listinterior_color = new ArrayList<>();
    Detail_Adapter_CarInterior adapter_carInterior;
    Detail_Adapter_CarExterior adapter_carExterior;
    String intent_carid = "";
    String cardetailurl;
    CustomRegularTextview tv_photo_indicator;
    ClickableViewPager ll_carimage;
    CarDetailImageBanner_Adapter carDetailImageBanner_adapter;
    private int NUM_PAGES;
    ArrayList<String> img_list;
    public boolean isClickedFirstTime = true;
    String select_interioicolor = "", selected_exteriorcolor = "", st_download;
    ConstraintLayout cl_technical_heading, cl_tech_value, cl_interior_heading, cl_interfeatures_value, cl_exterior_heading, cl_exteriorfeatures_value, cl_safety_heading, cl_safetyfeatures_value, cl_other_heading, cl_otherfeatures_value;
    RecyclerView rv_otherfeatures, rc_safetyfeatures, rc_extriorfeatures, rc_interiorfeatures,rv_techfeatures;
    ImageView techarrow, interorarrow, exteriorarrow, safetyarrow, otherarrow,ic_cart;
    ImageView techarrow1, interorarrow1, exteriorarrow1, safetyarrow1, otherarrow1,img_home;
    CarDetailAdapter_IFeatures Ifeatures_Adapter;
    CarDetailAdapter_ExtFeatures Efeatures_adapter;
    CarDetailAdapter_SafetyFeatures safetyfeat_adapter;
    CarDetailAdapter_OtherFeatures otherfeat_adpatr;
    SharedPref sharedPref;
    Dialog Image_ViewerDialog;
    View view2_tech,view3_inter,view4_exter,view44_safety,view51_other;
    LinearLayout loading_ll,main_ll;
    LinearLayout cv_ll_color,ll_color1;
    CardView cv_favourite;
    RelativeLayout rl_imagecontainer;
    PreOwnedCarDetailviewModel preOwnedCarDetailviewModel;
    int temp_count=1;
    String no_cars_quotes, auth_token;
    CarDetailAdapter_TechFeatures carDetailAdapter_techFeatures;
    ArrayList <TechFeatureList_Pojo>techlist=new ArrayList<>();
    ArrayList <TechFeatureList_Pojo>techlist_new=new ArrayList<>();
    CustomRegularTextview tv_IFshowmore1,tv_IFshowless,tv_EFshowmore1,tv_EFshowless,tv_SFshowmore1,tv_SFshowless,tv_OFshowmore1,tv_OFshowless;
    ArrayList<String> otherFeatures_list_limit = new ArrayList<>();
    LinearLayout ll_searchview;
    EditText searchview;

    ImageView ic_menu,searhview_icon;
    String sharingUrl="";
    ShareDialog shareDialog;

    String mainimage_url="";
    Dialog FilterDialog;
    CustomRegularEditTextBold et_make,et_model,et_fuel,et_transmission,et_year,et_maxyear,et_mileage,et_body_condition,et_dealer_warranty,et_exteriorcolor,et_interior_color,et_descrption, et_engine,et_varient,et_door;
    LinearLayout ll_make,ll_model,ll_fueltype,ll_transmission,ll_select_year,ll_select_maxyear;
    LinearLayout ll_enginetype,ll_varienttype,ll_door,ll_mileage,ll_body_condition,ll_dealer_warranty,ll_exteriorcolor,ll_interior_color,ll_descrption;
    String temp_makelist="",temp_model="",temp_fuel="",temp_tranmission="",temp_year="",temp_maxyear="",temp_mileage="",temp_extcolor="",temp_intcolor="";
    String temp_engine="",tempvarient="",tempdoors="";
    int makelistsize,modellistsize;
    String[] str_makedid;
    String[] strmodellist;
    String ip_makeid,ipmodelid;
    File path,dirfile,finalpath;
    ArrayList<Response_CarMakeList.DataItem>carmakelist=new ArrayList<>();
    ArrayList<Response_CarModelList.DataItem>carmodellist=new ArrayList<>();
    CustomBoldTextview tv_notification_count;
    String notify_count="";
    private NavigationView navigationView;
    private DrawerLayout drawer;
    CustomRegularButton bt_signin;
    CustomTextViewSemiBold tv_accountname;
    CustomRegularTextview nav_home,nav_favourite,nav_cart,nav_orders,nav_chat,nav_logout,nav_account,nav_notification;
    ArrayList<Integer>number_count=new ArrayList<>();
    CustomBoldTextview sidenav_notification_count;
    LinearLayout ll_logout;
    private CallbackManager callbackManager;
    LinearLayout Ext_ll_inner,Ext_ll_middle,Ext_ll_outer,Int_ll_inner,Int_ll_middle,Int_ll_outer;

    ImageView img_leftarrow,img_leftarrow1,img_rightarrow,img_rightarrow1;

    int position_image=0;

  LinearLayout ll_modeloptions;
  CardView cv_cartindicator;
    ArrayList<QuoteDataItemDB> dataItemDBS=new ArrayList<>();


    androidx.cardview.widget.CardView cv_nav_cartcount,cv_notificationcount_side;
    CustomBoldTextview sidenav_cart_count;
        String sharedata="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.layout_car_details_preowned);
        preOwnedCarDetailviewModel = ViewModelProviders.of(this).get(PreOwnedCarDetailviewModel.class);

        context = this;
        sharedPref=new SharedPref(context);
        myDataBase=new MyDataBase(context);
        shareDialog=new ShareDialog(this);
        findbyviews();
        clicklistener();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN) == "")
        {
            cv_favourite.setVisibility(View.GONE);
        }

        if(getIntent()!=null)
        {
            if(getIntent().getStringExtra("carid")!=null) {
                intent_carid = getIntent().getStringExtra("carid");
            }
            else
            {
                handleIntent(getIntent());
            }
        }
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        callbackManager = CallbackManager.Factory.create();

        LoginorNot();
        Func_AppInfo();
        for(int i=1;i<100;i++)
        {
            number_count.add(i);
        }

        Method_CarDetail();

        sharingUrl=Constants.DEEPLINK_URL+"/pre-owned-cars/?preownedId="+intent_carid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Call callbackManager.onActivityResult to pass login result to the LoginManager via callbackManager.
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            Log.v("---", "Successfully posted");
            // Write some code to do some operations when you shared content successfully.
        }

        @Override
        public void onCancel() {
            Log.v("---", "Sharing cancelled");
            // Write some code to do some operations when you cancel sharing content.
        }

        @Override
        public void onError(FacebookException error) {
            Log.v("---", error.getMessage());
            // Write some code to do some operations when some error occurs while sharing content.
        }
    };


    private void LoginorNot() {
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN)=="")
        {
            bt_signin.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.GONE);
            ll_logout.setVisibility(View.GONE);
            cv_notificationcount_side.setVisibility(View.GONE);
            if(myDataBase!=null) {
                dataItemDBS = myDataBase.getAlldataforquote();
                if(dataItemDBS.size()==0)
                {
                    cv_cartindicator.setVisibility(View.GONE);
                    cv_nav_cartcount.setVisibility(View.GONE);
                }
                else {
                    cv_cartindicator.setVisibility(View.VISIBLE);
                    cv_nav_cartcount.setVisibility(View.VISIBLE);
                    String cart_count=String.valueOf(dataItemDBS.size());
                    sharedPref.setString(Constants.PREF_Cartcount,cart_count);
                    tv_notification_count.setText(cart_count);
                    sidenav_cart_count.setText(cart_count);

                }
            }
        }
        else
        {
            bt_signin.setVisibility(View.GONE);
            ll_logout.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.VISIBLE);
            cv_notificationcount_side.setVisibility(View.VISIBLE);
            tv_accountname.setText(sharedPref.getString(Constants.PREF_USERNAME));
            cv_cartindicator.setVisibility(View.VISIBLE);
            notify_count=sharedPref.getString(Constants.PREF_UNREAD_NOTIFICATION);

            sidenav_notification_count.setText(notify_count);
            getquote_func();
        }


    }


    private void getquote_func() {

        HashMap<String,String>header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        preOwnedCarDetailviewModel.getquotedata(header).observe(this, new Observer<Response_GetQuote>() {
            @Override
            public void onChanged(Response_GetQuote response_getQuote) {

                if(response_getQuote.getStatus().equals("1")) {
                    String cart_count=String.valueOf(response_getQuote.getData().getQuoteData().size());
                    if (Integer.parseInt(cart_count)==0)
                    {
                        cv_cartindicator.setVisibility(View.GONE);
                        cv_nav_cartcount.setVisibility(View.GONE);
                    }
                    else {
                        cv_nav_cartcount.setVisibility(View.VISIBLE);
                        sharedPref.setString(Constants.PREF_Cartcount, cart_count);
                        tv_notification_count.setText(sharedPref.getString(Constants.PREF_Cartcount));
                        sidenav_cart_count.setText(sharedPref.getString(Constants.PREF_Cartcount));
                    }
                    }
                }

        });

    }


    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        preOwnedCarDetailviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                    if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                        cv_notificationcount_side.setVisibility(View.GONE);
                    }
                    else {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                        String count = response_appInfo.getData().getUnreadNotificationCount();
                        if(count.equals("0")) {
                            cv_notificationcount_side.setVisibility(View.GONE);
                        }
                        else
                        {
                            cv_notificationcount_side.setVisibility(View.VISIBLE);
                            tv_notification_count.setText(count);
                            sidenav_notification_count.setText(count);
                        }
                    }

                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }



    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            String recipeId = appLinkData.getLastPathSegment();
            System.out.println("--red==="+recipeId);


            Uri appData = Uri.parse(appLinkData.toString()).buildUpon()
                    .appendPath(recipeId).build();
          // full link--->  Toast.makeText(context,appData.toString(),Toast.LENGTH_SHORT).show();
            String linkdata=appData.getQuery();
            //last params --->
             Toast.makeText(context,linkdata,Toast.LENGTH_SHORT).show();

            String separator ="=";
            int sepPos = linkdata.indexOf(separator);
            if (sepPos == -1) {
                System.out.println("");
            }
            String begore= linkdata.substring(0 , sepPos); //this will give abc
            System.out.println("Substring----"+begore);
            Toast.makeText(context,begore,Toast.LENGTH_SHORT).show();
            System.out.println("Substring after separator = "+linkdata.substring(sepPos +     separator.length()));

            //particular id alone---
              //Toast.makeText(context,linkdata.substring(sepPos +       separator.length()),Toast.LENGTH_SHORT).show();
            intent_carid=linkdata.substring(sepPos +       separator.length());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getquote_func();
        Func_AppInfo();
        sidenav_notification_count.setText(notify_count);
        LoginorNot();
    }

    private void findbyviews() {

        cv_notificationcount_side=findViewById(R.id.cv_notificationcount);

        sidenav_cart_count=findViewById(R.id.sidenav_cart_count);
        cv_nav_cartcount=findViewById(R.id.cv_nav_cartcount);

        sidenav_notification_count=findViewById(R.id.sidenav_notification_count);


        cv_cartindicator=findViewById(R.id.cv_cartindicator);
        tv_notification_count=findViewById(R.id.tv_notification_count);
        img_rightarrow=findViewById(R.id.img_rightarrow);
        img_leftarrow=findViewById(R.id.img_leftarrow);
        img_leftarrow1=findViewById(R.id.img_leftarrow1);
        img_rightarrow1=findViewById(R.id.img_rightarrow1);

        ll_modeloptions=findViewById(R.id.ll_modeloptions);

        Ext_ll_inner=findViewById(R.id.Ext_ll_inner);
        Ext_ll_middle=findViewById(R.id.Ext_ll_middle);
        Ext_ll_outer=findViewById(R.id.Ext_ll_outer);
        Int_ll_inner=findViewById(R.id.Int_ll_inner);
        Int_ll_middle=findViewById(R.id.Int_ll_middle);
        Int_ll_outer=findViewById(R.id.Int_ll_outer);



        ic_menu = findViewById(R.id.ic_menu);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        ll_logout=findViewById(R.id.ll_logout);
        nav_logout=findViewById(R.id.nav_logout);
        nav_home=findViewById(R.id.nav_home);
        nav_cart=findViewById(R.id.nav_cart);
        nav_favourite=findViewById(R.id.nav_favourite);
        nav_orders=findViewById(R.id.nav_orders);
        nav_chat=findViewById(R.id.nav_chat);
        bt_signin=findViewById(R.id.bt_signin);
        tv_accountname=findViewById(R.id.tv_accountname);
        nav_account=findViewById(R.id.nav_account);
        nav_notification=findViewById(R.id.nav_notification);




        ll_selection1=findViewById(R.id.ll_selection1);
        ll_selection=findViewById(R.id.ll_selection);
        ic_cart=findViewById(R.id.ic_cart);

        ll_searchview=findViewById(R.id.ll_searchview);
        searchview=findViewById(R.id.searchview);
        searhview_icon=findViewById(R.id.searhview_icon);
        cv_favourite=findViewById(R.id.cv_favourite);
        loading_ll=findViewById(R.id.loading_ll);
        main_ll=findViewById(R.id.main_ll);
        ll_color1=findViewById(R.id.ll_color1);
        cv_ll_color=findViewById(R.id.cv_ll_color);
        rl_imagecontainer=findViewById(R.id.rl_imagecontainer);


        //View Lines
        view2_tech = findViewById(R.id.view2);
        view3_inter = findViewById(R.id.view3);
        view4_exter = findViewById(R.id.view4);
        view44_safety = findViewById(R.id.view44);
        view51_other = findViewById(R.id.view51);




        img_home=findViewById(R.id.img_home);
        techarrow = findViewById(R.id.imageView);
        interorarrow = findViewById(R.id.imageView1);
        exteriorarrow = findViewById(R.id.imageView2);
        safetyarrow = findViewById(R.id.imageView41);
        otherarrow = findViewById(R.id.imageView51);



        tv_OFshowmore1=findViewById(R.id.tv_OFshowmore1);
        tv_OFshowless=findViewById(R.id.tv_OFshowless);

        techarrow1 = findViewById(R.id.imageView001);
        interorarrow1 = findViewById(R.id.imageView121);
        exteriorarrow1 = findViewById(R.id.imageView33);
        safetyarrow1 = findViewById(R.id.imageView42);
        otherarrow1 = findViewById(R.id.imageView52);


        cl_technical_heading = findViewById(R.id.cl_technical_heading);
        cl_tech_value = findViewById(R.id.cl_tech_value);
        cl_interior_heading = findViewById(R.id.cl_interior_heading);
        cl_interfeatures_value = findViewById(R.id.cl_interfeatures_value);
        cl_exterior_heading = findViewById(R.id.cl_exterior_heading);
        cl_exteriorfeatures_value = findViewById(R.id.cl_exteriorfeatures_value);
        cl_safety_heading = findViewById(R.id.cl_safety_heading);
        cl_safetyfeatures_value = findViewById(R.id.cl_safetyfeatures_value);
        cl_other_heading = findViewById(R.id.cl_other_heading);
        cl_otherfeatures_value = findViewById(R.id.cl_otherfeatures_value);
        rv_otherfeatures = findViewById(R.id.rv_otherfeatures);
        rc_safetyfeatures = findViewById(R.id.rc_safetyfeatures);
        rc_extriorfeatures = findViewById(R.id.rc_extriorfeatures);
        rc_interiorfeatures = findViewById(R.id.rc_interiorfeatures);

        rv_techfeatures=findViewById(R.id.rv_techfeatures);
        exteriorcolor = findViewById(R.id.cv_exteriorcolor);
        interiorcolor = findViewById(R.id.cv_interiorrcolor);
        ll_carimage = findViewById(R.id.carimage);
        tv_carname = findViewById(R.id.textView3);
        tv_model_yr = findViewById(R.id.tv_model_yr);
        rv_exteriorcolor = findViewById(R.id.rv_exteriorcolor);
        rv_interiorcolor = findViewById(R.id.rv_interior);
        img_share = findViewById(R.id.imageView8);
        img_favourite = findViewById(R.id.imageView9);
        img_fb = findViewById(R.id.imageView10);
        img_twitter = findViewById(R.id.imageView11);
        img_instagram = findViewById(R.id.imageView12);
        img_whatsapp = findViewById(R.id.imageView13);
        tv_model_type = findViewById(R.id.tv_model_type);
        tv_model_type1 = findViewById(R.id.textView6);
        tv_extcolor_value = findViewById(R.id.tv_tick1_bg);
        tv_tv_interior_color_value = findViewById(R.id.tv_tick2_bg);
        bt_download_spec = findViewById(R.id.button);
        bt_add_to_quote = findViewById(R.id.button2);
        tv_photo_indicator = findViewById(R.id.tv_photo_indicator);

        textView = findViewById(R.id.textView);
        imageView = findViewById(R.id.imageView);

       // imageView27 = findViewById(R.id.imageView27);

        linearLayoutManager = new LinearLayoutManager(PreOwnedCar_Detail_Activity.this, LinearLayoutManager.HORIZONTAL, false);
        rv_exteriorcolor.setLayoutManager(linearLayoutManager);
        // rv_exteriorcolor.setHasFixedSize(true);

        linearLayoutManager1 = new LinearLayoutManager(PreOwnedCar_Detail_Activity.this, LinearLayoutManager.HORIZONTAL, false);
        rv_interiorcolor.setLayoutManager(linearLayoutManager1);

        rv_techfeatures.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        rv_techfeatures.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context,2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
        rc_interiorfeatures.setLayoutManager(gridLayoutManager);
        rc_interiorfeatures.setHasFixedSize(true);



        rc_extriorfeatures.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.VERTICAL,false));
        rc_extriorfeatures.setHasFixedSize(true);


        LinearLayoutManager linearLayoutManager5=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rv_otherfeatures.setLayoutManager(linearLayoutManager5);
        rv_otherfeatures.setHasFixedSize(true);



        rc_safetyfeatures.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.VERTICAL,false));
        rc_safetyfeatures.setHasFixedSize(true);



    }

    private void clicklistener() {

        ic_menu.setOnClickListener(this);
        nav_home.setOnClickListener(this);
        nav_cart.setOnClickListener(this);
        nav_favourite.setOnClickListener(this);
        nav_orders.setOnClickListener(this);
        nav_chat.setOnClickListener(this);
        bt_signin.setOnClickListener(this);
        nav_logout.setOnClickListener(this);

        nav_account.setOnClickListener(this);
        nav_notification.setOnClickListener(this);




        bt_download_spec.setOnClickListener(this);
        bt_add_to_quote.setOnClickListener(this);
        ll_carimage.setOnClickListener(this);
        cl_technical_heading.setOnClickListener(this);
        cl_exterior_heading.setOnClickListener(this);
        cl_interior_heading.setOnClickListener(this);
        cl_safety_heading.setOnClickListener(this);
        cl_other_heading.setOnClickListener(this);
        img_home.setOnClickListener(this);
        img_whatsapp.setOnClickListener(this);
        img_share.setOnClickListener(this);
        rl_imagecontainer.setOnClickListener(this);
        ic_cart.setOnClickListener(this);

        tv_OFshowmore1.setOnClickListener(this);
        tv_OFshowless.setOnClickListener(this);

        ll_searchview.setOnClickListener(this);
        searchview.setOnClickListener(this);
        searhview_icon.setOnClickListener(this);

        img_fb.setOnClickListener(this);
        img_instagram.setOnClickListener(this);

    }


    private void publishImage(){
        Bitmap image = BitmapFactory.decodeResource(getResources(),     R.mipmap.ic_launcher);

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("Welcome To Facebook Photo Sharing on steroids!")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
    }




    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {

            case R.id.nav_account:
                System.out.println("==Menu===");
                Intent tohomes = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohomes.putExtra("tabposition", "useraccount");
                startActivity(tohomes);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case R.id.nav_notification:
                Intent tonotifify=new Intent(this,NotificationList_Activity.class);
                startActivity(tonotifify);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case R.id.ic_menu:
                drawer.openDrawer(GravityCompat.END);
                break;


            case  R.id.nav_home:
                System.out.println("==Menu===");
                Intent tohome = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohome.putExtra("tabposition", "home");
                startActivity(tohome);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_favourite:
                Intent tocart = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart.putExtra("tabposition", "cart");
                tocart.putExtra("position_tabs","0");
                startActivity(tocart);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);

                break;

            case  R.id.nav_cart:
                Intent tocart1 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart1.putExtra("tabposition", "cart");
                tocart1.putExtra("position_tabs","1");
                startActivity(tocart1);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_orders:
                Intent tocart2 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart2.putExtra("tabposition", "cart");
                tocart2.putExtra("position_tabs","2");
                startActivity(tocart2);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case  R.id.nav_chat:
                Intent tocart3 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart3.putExtra("tabposition", "chat");
                startActivity(tocart3);
                finish();
                System.out.println("==Menu===");
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_logout:
                System.out.println("==Menu===");
                Intent gotoLogin = new Intent(context, LoginActivity.class);
                System.out.println("===vc===" + sharedPref.getString(Constants.JWT_TOKEN));
                sharedPref.clearAll();
                drawer.closeDrawer(Gravity.RIGHT);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoLogin);
                finish();
                break;

            case R.id.bt_signin:
                drawer.closeDrawer(Gravity.RIGHT);
                Intent tosignin=new Intent(this,LoginActivity.class);
                startActivity(tosignin);
                finish();
                break;
                case R.id.ic_cart:
                Intent tocart11=new Intent(this,HomeActivity_searchimage_BottomButton.class);
                tocart11.putExtra("tabposition","cart");
                tocart11.putExtra("position_tabs","1");
                startActivity(tocart11);
                finish();
                break;


            case R.id.carimage:
                ToImageView();
                break;

            case R.id.img_home:
                onBackPressed();
                break;
            case R.id.button:

                break;
            case R.id.searhview_icon:
            case R.id.searchview:
            case R.id.ll_searchview:
                Intent toSearchpage=new Intent(PreOwnedCar_Detail_Activity.this,FilterPreOwnedDetail_Activity.class);
                startActivity(toSearchpage);

                break;

            case R.id.button2:
                dialog_addquote();
                break;
            case R.id.imageView8:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
               // sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Constants.DEEPLINK_URL+"/pre-owned-cars/?preownedId="+intent_carid));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Constants.DEEPLINK_URL+"/pre-owned-cars/?preownedId="+intent_carid);
                sharingIntent.setType("text/plain");
                sharingIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(sharingIntent);
                break;

            case R.id.imageView9:
                // favourite
                break;

            case R.id.imageView10:
                shareFacebookIntent();
                break;

            case R.id.imageView11:
                // twitter
                shareLinkedInIntent();
                break;

            case R.id.imageView12:
                // instagram
                shareIntagramIntent();

                break;

            case R.id.imageView13:
                // whatsapp
              //  AppInstalled.isAppInstalled("com.whatsapp", context);

               shareWhatsappIntent();
                break;

            case R.id.cl_technical_heading:
                if (isClickedFirstTime) {
                    isClickedFirstTime = false;
                    cl_tech_value.setVisibility(View.VISIBLE);
                    techarrow1.setVisibility(View.VISIBLE);
                    techarrow.setVisibility(View.GONE);
                } else {
                    isClickedFirstTime = true;
                    cl_tech_value.setVisibility(View.GONE);
                    techarrow1.setVisibility(View.GONE);
                    techarrow.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.cl_interior_heading:
                if (isClickedFirstTime) {
                    interorarrow1.setVisibility(View.VISIBLE);
                    interorarrow.setVisibility(View.GONE);
                    isClickedFirstTime = false;
                    cl_interfeatures_value.setVisibility(View.VISIBLE);
                } else {

                    interorarrow1.setVisibility(View.GONE);
                    interorarrow.setVisibility(View.VISIBLE);
                    isClickedFirstTime = true;
                    cl_interfeatures_value.setVisibility(View.GONE);
                }
                break;

            case R.id.cl_exterior_heading:
                if (isClickedFirstTime) {
                    exteriorarrow1.setVisibility(View.VISIBLE);
                    exteriorarrow.setVisibility(View.GONE);
                    isClickedFirstTime = false;
                    cl_exteriorfeatures_value.setVisibility(View.VISIBLE);
                } else {

                    exteriorarrow1.setVisibility(View.GONE);
                    exteriorarrow.setVisibility(View.VISIBLE);


                    isClickedFirstTime = true;
                    cl_exteriorfeatures_value.setVisibility(View.GONE);
                }
                break;
            case R.id.cl_safety_heading:
                if (isClickedFirstTime) {

                    safetyarrow.setVisibility(View.GONE);
                    safetyarrow1.setVisibility(View.VISIBLE);

                    isClickedFirstTime = false;
                    cl_safetyfeatures_value.setVisibility(View.VISIBLE);
                } else {

                    safetyarrow1.setVisibility(View.GONE);
                    safetyarrow.setVisibility(View.VISIBLE);
                    isClickedFirstTime = true;
                    cl_safetyfeatures_value.setVisibility(View.GONE);
                }
                break;

            case R.id.cl_other_heading:
                if (isClickedFirstTime) {

                    otherarrow.setVisibility(View.GONE);
                    otherarrow1.setVisibility(View.VISIBLE);


                    isClickedFirstTime = false;
                    cl_otherfeatures_value.setVisibility(View.VISIBLE);
                } else {

                    otherarrow.setVisibility(View.VISIBLE);
                    otherarrow1.setVisibility(View.GONE);

                    isClickedFirstTime = true;
                    cl_otherfeatures_value.setVisibility(View.GONE);
                }
                break;
            case R.id.tv_OFshowmore1:
                tv_OFshowmore1.setVisibility(View.GONE);
                tv_OFshowless.setVisibility(View.VISIBLE);
                otherfeat_adpatr = new CarDetailAdapter_OtherFeatures(context, otherFeatures_list);
                rv_otherfeatures.setAdapter(otherfeat_adpatr);
                break;
            case R.id.tv_OFshowless:
                tv_OFshowmore1.setVisibility(View.VISIBLE);
                tv_OFshowless.setVisibility(View.GONE);
                otherfeat_adpatr = new CarDetailAdapter_OtherFeatures(context, otherFeatures_list_limit);
                rv_otherfeatures.setAdapter(otherfeat_adpatr);
                break;

        }

    }


    private void shareIntagramIntent() {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.instagram.android");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.instagram.android");
            shareIntent.putExtra(Intent.EXTRA_TEXT, sharedata);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.instagram.android");
            try {
                /*shareIntent.putExtra(Intent.EXTRA_STREAM,
                        Uri.parse(finalpath.toString()));*/
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/jpeg");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.instagram.android"));
            startActivity(intent);
        }
    }

    private void shareFacebookIntent() {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.facebook.katana");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.facebook.katana");
            shareIntent.putExtra(Intent.EXTRA_TEXT, sharedata);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.facebook.katana");
            try {
                /*shareIntent.putExtra(Intent.EXTRA_STREAM,
                        Uri.parse(finalpath.toString()));*/
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/jpeg");

            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.facebook.katana"));
            startActivity(intent);
        }
    }

    private void shareLinkedInIntent() {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.linkedin.android");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.linkedin.android");
            shareIntent.putExtra(Intent.EXTRA_TEXT, sharedata);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.linkedin.android");
            try {
                /*shareIntent.putExtra(Intent.EXTRA_STREAM,
                        Uri.parse(finalpath.toString()));*/
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/jpeg");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.linkedin.android"));
            startActivity(intent);
        }
    }

    private void shareWhatsappIntent() {
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage("com.whatsapp");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra(Intent.EXTRA_TEXT, sharedata);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            shareIntent.setPackage("com.whatsapp");
            try {
                /*shareIntent.putExtra(Intent.EXTRA_STREAM,
                        Uri.parse(finalpath.toString()));*/
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath)); // did in imageview activity
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            shareIntent.setType("image/jpeg");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id="
                    + "com.whatsapp"));
            startActivity(intent);
        }
    }



    private void Download_file_insta() {

        path=new File("/alliedmotors/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date())+".jpg");
        dirfile=new File(path, Environment.DIRECTORY_DOWNLOADS);
        finalpath=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), String.valueOf(path));
        DownloadManager downloadManager= (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(mainimage_url));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setTitle("Downloading");
        request.setDescription("Please wait...");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, String.valueOf(path));
        downloadManager.enqueue(request);
    }



    private void FbValidation() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                //Toast.makeText(context, "login succful", Toast.LENGTH_SHORT).show();
                //System.out.println("--social fb user id===" + st_socialid);

            }
            @Override
            public void onCancel() {
                // App code
                // Toast.makeText(context, "login cancel succful", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code

                if (exception instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }

            }
        });


    }

    private void setupDrawerContent(NavigationView navigationView) {

        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });

    }

    private void Method_CarDetail() {

        loading_ll.setVisibility(View.VISIBLE);
        cardetailurl = Constants.BASE_URL + "v1/preowned-car-detail/" + intent_carid;

        Log.e("-->preowned-->",cardetailurl);
        preOwnedCarDetailviewModel.getCarDetaildata(cardetailurl).observe(this, new Observer<Response_PreOwned_Detail>() {
            @Override
            public void onChanged(Response_PreOwned_Detail response_carDetail) {
                loading_ll.setVisibility(View.GONE);
                main_ll.setVisibility(View.VISIBLE);
                Commons.hideProgress();
                System.out.println("===res=car details==" + response_carDetail.toString());
                listinterior_color = response_carDetail.getData().getCarGalleryInteriorColors();
                listexterior_color = response_carDetail.getData().getCarGalleryExteriorColors();
                exteriorfeatures_list=response_carDetail.getData().getCarExteriorFeatures().getFeatures();
                interiorFeatures_list=response_carDetail.getData().getCarInteriorFeatures().getFeatures();
                otherFeatures_list=response_carDetail.getData().getCarOtherFeatures().getFeatures();
                safetyFeatures_list=response_carDetail.getData().getCarSafetyFeatures().getFeatures();


                st_download = response_carDetail.getData().getCarDetails().getPrice();
                img_list = response_carDetail.getData().getCarGalleryImages();
                tv_carname.setText(response_carDetail.getData().getCarDetails().getMakename()+" "+response_carDetail.getData().getCarDetails().getModelName());
                tv_model_yr.setText(response_carDetail.getData().getCarDetails().getModelYear());
                mainimage_url=response_carDetail.getData().getCarDetails().getCarImage();
                tv_model_type.setText(response_carDetail.getData().getCarDetails().getReferrence());

                String modeloption=response_carDetail.getData().getCarDetails().getModelOptions();

                if(modeloption.equals(null) || modeloption==null || modeloption.equals("") || modeloption=="")
                {
                    ll_modeloptions.setVisibility(View.GONE);
                }
                else {
                    tv_model_type1.setText(response_carDetail.getData().getCarDetails().getModelOptions());
                }
                sts_carname=response_carDetail.getData().getCarDetails().getMakename();
                sts_image=response_carDetail.getData().getCarDetails().getCarImage();
                sts_year=response_carDetail.getData().getCarDetails().getModelYear();
                sts_carname=response_carDetail.getData().getCarDetails().getModelOptions();
                sts_varientcode=response_carDetail.getData().getCarDetails().getVariant();
                sts_modeloption=response_carDetail.getData().getCarDetails().getModelOptions();
                sts_makename=response_carDetail.getData().getCarDetails().getMakename();
                sts_makid=response_carDetail.getData().getCarDetails().getMkId();
                sts_modid=response_carDetail.getData().getCarDetails().getModId();
                sts_carcat=response_carDetail.getData().getCarDetails().getCatid();
                Download_file_insta();

                sharedata=sts_carname+"-"+sts_varientcode;  // to send data along with sharing
                TechFeatureList_Pojo tech1=new TechFeatureList_Pojo("Make",response_carDetail.getData().getCarDetails().getMakename(),R.drawable.pre_car_make);
                TechFeatureList_Pojo tech2=new TechFeatureList_Pojo("Model",response_carDetail.getData().getCarDetails().getModelName(),R.drawable.pre_notepad_64);
                TechFeatureList_Pojo tech3=new TechFeatureList_Pojo("Year",response_carDetail.getData().getCarDetails().getModelYear(),R.drawable.pre_calendar64);
                TechFeatureList_Pojo tech4=new TechFeatureList_Pojo("Car Mileage",response_carDetail.getData().getCarDetails().getMileage(),R.drawable.pre_mileage64);
                TechFeatureList_Pojo tech5=new TechFeatureList_Pojo("Engine",response_carDetail.getData().getCarDetails().getEngine(),R.drawable.pre_engine_1);

                TechFeatureList_Pojo tech6=new TechFeatureList_Pojo("Vehicle Type",response_carDetail.getData().getCarDetails().getVehiceType(),R.drawable.pre_car_make);


                TechFeatureList_Pojo tech7=new TechFeatureList_Pojo("Car Transmission:",response_carDetail.getData().getCarDetails().getTransmission(),R.drawable.t_transmission48);
                TechFeatureList_Pojo tech8=new TechFeatureList_Pojo("Fuel Type",response_carDetail.getData().getCarDetails().getEfualType(),R.drawable.t_fueltype48);
                TechFeatureList_Pojo tech9=new TechFeatureList_Pojo("Door",response_carDetail.getData().getCarDetails().getDoor(),R.drawable.t_door48);
                TechFeatureList_Pojo tech10=new TechFeatureList_Pojo("Exterior Color",response_carDetail.getData().getCarDetails().getExteriorColor(),R.drawable.pre_contrast_64);
                TechFeatureList_Pojo tech11=new TechFeatureList_Pojo("Interior Color",response_carDetail.getData().getCarDetails().getInteriorColor(),R.drawable.pre_contrast_64);
                TechFeatureList_Pojo tech12=new TechFeatureList_Pojo("Service History",response_carDetail.getData().getCarDetails().getServiceHistory(),R.drawable.pre_history48);


                TechFeatureList_Pojo tech13=new TechFeatureList_Pojo("Accident History",response_carDetail.getData().getCarDetails().getAccidentHistory(),R.drawable.pre_car_collision);
                TechFeatureList_Pojo tech14=new TechFeatureList_Pojo("Warranty",response_carDetail.getData().getCarDetails().getWarranty(),R.drawable.pre_badge48);
                TechFeatureList_Pojo tech15=new TechFeatureList_Pojo("Reference",response_carDetail.getData().getCarDetails().getReferrence(),R.drawable.pre_refence48);


                techlist.add(tech1);
                techlist.add(tech2);
                techlist.add(tech3);
                techlist.add(tech4);
                techlist.add(tech5);
                techlist.add(tech6);
                techlist.add(tech7);
                techlist.add(tech8);
                techlist.add(tech9);
                techlist.add(tech10);
                techlist.add(tech11);
                techlist.add(tech12);
                techlist.add(tech13);
                techlist.add(tech14);
                techlist.add(tech15);
                String val1="";
                for(int j=0;j<techlist.size();j++)
                {
                    val1=techlist.get(j).Value;
                    if(val1.length()!=0)
                    {
                        techlist_new.add(techlist.get(j));
                    }
                }

                carDetailAdapter_techFeatures=new CarDetailAdapter_TechFeatures(context,techlist_new);
                rv_techfeatures.setAdapter(carDetailAdapter_techFeatures);









                if (listinterior_color.size() == 0) {
                    interiorcolor.setVisibility(View.GONE);
                    cv_ll_color.setVisibility(View.GONE);
                    ll_color1.setVisibility(View.GONE);
                    Int_ll_inner.setVisibility(View.GONE);
                    img_leftarrow1.setVisibility(View.GONE);
                    img_rightarrow1.setVisibility(View.GONE);

                } else {
                    if (response_carDetail.getData().getCarGalleryExteriorColors().size() == 0) {
                        //tv_tv_interior_color_value.setVisibility(View.GONE);
                        ll_selection1.setVisibility(View.GONE);
                        img_leftarrow1.setVisibility(View.GONE);
                        img_rightarrow1.setVisibility(View.GONE);
                    } else {
                        img_leftarrow1.setVisibility(View.GONE);
                        img_rightarrow1.setVisibility(View.GONE);
                        if (select_interioicolor.equals("")) {
                            select_interioicolor=response_carDetail.getData().getCarGalleryInteriorColors().get(0);
                            if(select_interioicolor==null || select_interioicolor.equals(null))
                            {
                                interiorcolor.setVisibility(View.GONE);
                            }
                            else {
                                select_interioicolor=response_carDetail.getData().getCarGalleryInteriorColors().get(0);
                                Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                                Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));

                                adapter_carInterior = new Detail_Adapter_CarInterior(context, listinterior_color, PreOwnedCar_Detail_Activity.this::Interiorcolor,select_interioicolor);
                                rv_interiorcolor.setAdapter(adapter_carInterior);
                                func_intRecycyer_sscroll();
                            }
                           // tv_tv_interior_color_value.setCardBackgroundColor((Color.parseColor(response_carDetail.getData().getCarGalleryInteriorColors().get(0))));
                        } else {

                            Drawable unwrappedDrawable = Int_ll_inner.getBackground();
                            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                            DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(select_interioicolor)));

                            adapter_carInterior = new Detail_Adapter_CarInterior(context, listinterior_color, PreOwnedCar_Detail_Activity.this::Interiorcolor,select_interioicolor);
                            rv_interiorcolor.setAdapter(adapter_carInterior);
                            func_intRecycyer_sscroll();
                            //tv_tv_interior_color_value.setCardBackgroundColor((Color.parseColor(select_interioicolor)));
                        }
                    }

                }












                if (listexterior_color.size() == 0) {
                    cv_ll_color.setVisibility(View.GONE);
                    exteriorcolor.setVisibility(View.GONE);
                    ll_color1.setVisibility(View.GONE);
                    img_leftarrow.setVisibility(View.GONE);
                    img_rightarrow.setVisibility(View.GONE);

                }
               else  {
                    if (response_carDetail.getData().getCarGalleryExteriorColors().size() == 0) {
                        ll_selection.setVisibility(View.GONE);
                        tv_extcolor_value.setVisibility(View.GONE);
                        img_leftarrow.setVisibility(View.GONE);
                        img_rightarrow.setVisibility(View.GONE);
                    } else {

                        if (selected_exteriorcolor.equals("")) {
                            img_leftarrow.setVisibility(View.GONE);
                            img_rightarrow.setVisibility(View.GONE);
                            selected_exteriorcolor=response_carDetail.getData().getCarGalleryExteriorColors().get(0);
                            if(selected_exteriorcolor==null || selected_exteriorcolor.equals(null))
                            {
                                exteriorcolor.setVisibility(View.GONE);
                            }
                            else {
                                selected_exteriorcolor=response_carDetail.getData().getCarGalleryExteriorColors().get(0);
                                Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                                Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                                DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(selected_exteriorcolor)));
                                adapter_carExterior = new Detail_Adapter_CarExterior(context, listexterior_color, PreOwnedCar_Detail_Activity.this::Exteriorcolor,selected_exteriorcolor);
                                rv_exteriorcolor.setAdapter(adapter_carExterior);
                                func_extRecycelr_scroll();

                            }
                        } else {

                            Drawable unwrappedDrawable = Ext_ll_inner.getBackground();
                            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                            DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(selected_exteriorcolor)));
                            adapter_carExterior = new Detail_Adapter_CarExterior(context, listexterior_color, PreOwnedCar_Detail_Activity.this::Exteriorcolor,selected_exteriorcolor);
                            rv_exteriorcolor.setAdapter(adapter_carExterior);
                            func_extRecycelr_scroll();

                        }
                    }

                                   }

               if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null)) {
                   bt_download_spec.setText("Login for Price");
               }
               else {
                   bt_download_spec.setText(response_carDetail.getData().getCarDetails().getCurrencyName() + " " + response_carDetail.getData().getCarDetails().getPrice());
               }

// Interior Features
                    if(interiorFeatures_list.size()==0)
                    {
                        cl_interior_heading.setVisibility(View.GONE);
                        view3_inter.setVisibility(View.GONE);
                    }
                    else {
                        Ifeatures_Adapter = new CarDetailAdapter_IFeatures(context, interiorFeatures_list);
                        rc_interiorfeatures.setAdapter(Ifeatures_Adapter);
                    }

                    // Exterior Features
                    if(exteriorfeatures_list.size()==0)
                    {
                        cl_exterior_heading.setVisibility(View.GONE);
                        view4_exter.setVisibility(View.GONE);
                    }
                else {
                        Efeatures_adapter = new CarDetailAdapter_ExtFeatures(context, exteriorfeatures_list);
                        rc_extriorfeatures.setAdapter(Efeatures_adapter);
                    }

                // Safety Features
                if(safetyFeatures_list.size()==0)
                {
                    cl_safety_heading.setVisibility(View.GONE);
                    view44_safety.setVisibility(View.GONE);
                }
                else {

                    safetyfeat_adapter = new CarDetailAdapter_SafetyFeatures(context, safetyFeatures_list);
                    rc_safetyfeatures.setAdapter(safetyfeat_adapter);
                }



                if (otherFeatures_list.size() >= 9) {
                    for (int d = 0; d < 8; d++) {
                        otherFeatures_list_limit.add(otherFeatures_list.get(d));
                    }

                } else {
                    otherFeatures_list_limit.addAll(otherFeatures_list);
                    tv_OFshowless.setVisibility(View.GONE);
                    tv_OFshowmore1.setVisibility(View.GONE);
                }


                if(otherFeatures_list.size()==0)
                {
                    cl_other_heading.setVisibility(View.GONE);
                    view51_other.setVisibility(View.GONE);
                    tv_OFshowmore1.setVisibility(View.GONE);
                }
                else {
                    otherfeat_adpatr = new CarDetailAdapter_OtherFeatures(context, otherFeatures_list_limit);
                    rv_otherfeatures.setAdapter(otherfeat_adpatr);
                }



                NUM_PAGES = img_list.size();
                carDetailImageBanner_adapter = new CarDetailImageBanner_Adapter(context, img_list);
                ll_carimage.setAdapter(carDetailImageBanner_adapter);


                ll_carimage.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        ToImageView();
                    }
                });

                ll_carimage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        ll_carimage.getCurrentItem();
                        tv_photo_indicator.setText((ll_carimage.getCurrentItem() + 1) + "/" + NUM_PAGES);
                    }

                    @Override
                    public void onPageSelected(int position) {
                        position_image=position;

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });

            }
        });


    }

    private void func_intRecycyer_sscroll() {
        rv_interiorcolor.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


                if (linearLayoutManager1.findFirstCompletelyVisibleItemPosition()==0){
                    img_leftarrow1.setVisibility(View.INVISIBLE);
                }else{
                    img_leftarrow1.setVisibility(View.VISIBLE);
                }

                if (linearLayoutManager1.findLastCompletelyVisibleItemPosition()==listinterior_color.size()-1){
                    img_rightarrow1.setVisibility(View.INVISIBLE);
                }else
                    img_rightarrow1.setVisibility(View.VISIBLE);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        rv_interiorcolor.scrollToPosition(getPositionInterior(select_interioicolor));
    }

    private void func_extRecycelr_scroll() {
        rv_exteriorcolor.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


                if (linearLayoutManager.findFirstCompletelyVisibleItemPosition()==0){
                    img_leftarrow.setVisibility(View.INVISIBLE);
                }else{
                    img_leftarrow.setVisibility(View.VISIBLE);
                }

                if (linearLayoutManager.findLastCompletelyVisibleItemPosition()==listexterior_color.size()-1){
                    img_rightarrow.setVisibility(View.INVISIBLE);
                }else
                    img_rightarrow.setVisibility(View.VISIBLE);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        rv_exteriorcolor.scrollToPosition(getPosition(selected_exteriorcolor));
    }

    private void dialog_addquote() {
        final Dialog dialog = new Dialog(PreOwnedCar_Detail_Activity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);

        dialog.setContentView(R.layout.bottomsheet_add_to_quote_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height

        ImageView bt_increment, bt_decrement;
        // CustomRegularTextview tv_numberpicker;
        RecyclerView rv_number;

        //  NumberPicker rv_number;

        CustomRegularButton button_addquote;
        button_addquote = dialog.findViewById(R.id.dialog_addquote);
        bt_increment = dialog.findViewById(R.id.bt_increment);
        bt_decrement = dialog.findViewById(R.id.bt_decrement);
        rv_number=dialog.findViewById(R.id.rv_number);
        // tv_numberpicker = dialog.findViewById(R.id.tv_numberpicker);
        no_cars_quotes="1";
        temp_count=1;
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_number.setLayoutManager(linearLayoutManager);
        rv_number.setHasFixedSize(true);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rv_number);
        Custom_NumberAdapter custom_numberAdapter;

        custom_numberAdapter=new Custom_NumberAdapter(context,number_count);
        rv_number.setAdapter(custom_numberAdapter);



        rv_number.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState== rv_number.SCROLL_STATE_IDLE)
                {
                    linearLayoutManager.scrollToPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition()+1);
                    temp_count=linearLayoutManager.findFirstCompletelyVisibleItemPosition()+2;
                    no_cars_quotes=String.valueOf(temp_count);
                    System.out.println("--f----"+temp_count);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                System.out.println("--f---nnnnn-"+temp_count);
            }
        });



        bt_increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutManager.scrollToPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition()+1);
                temp_count=linearLayoutManager.findFirstCompletelyVisibleItemPosition()+2;
                no_cars_quotes=String.valueOf(temp_count);


            }
        });

        bt_decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(linearLayoutManager.findFirstCompletelyVisibleItemPosition()==0)
                {
                    dialog.dismiss();
                }
                else {
                    linearLayoutManager.scrollToPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition() - 1);
                    temp_count=linearLayoutManager.findFirstCompletelyVisibleItemPosition()-1;
                    no_cars_quotes=String.valueOf(temp_count);

                }
            }
        });



        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog alertDialog;
                alertDialog = new Dialog(context);
                alertDialog.setContentView(R.layout.custom_alert_layout_title_ios);
                alertDialog.setCancelable(true);
                alertDialog.setCanceledOnTouchOutside(true);
                if (alertDialog.getWindow() != null) {
                    alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }

                alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
                CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
                txttitle.setText("Allied Motors");
                txtInstructionDialog.setText("Do you want to add this car to Quote?");
                txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
                CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);
                CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
                btokay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                        autoCompleteHandler.postDelayed(autoCompleteRunnable, 1000);
                        alertDialog.dismiss();
                        dialog.dismiss();
                    }
                });
                autoCompleteHandler = new Handler();
                autoCompleteRunnable = new Runnable() {
                    public void run() {
                        if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {

                            boolean recordexxit = myDataBase.recordExist(intent_carid);
                            if (recordexxit) {
                                // Toast.makeText(getBaseContext(), "exist", Toast.LENGTH_SHORT).show();
                                Commons.Alert_custom(context,"Already added to quote");
                            }
                            else {
                                myDataBase.Insert_Guestquote_1(intent_carid, sts_image, sts_carinfo, sts_carcat, sts_makid, sts_modid, sts_makename, sts_carname, sts_year, sts_modeloption, sts_varientcode, selected_exteriorcolor, select_interioicolor, no_cars_quotes);
                                String content_desc="Car has been added to your quotation list. Do you want to view the quotation list page?";
                                dialog_completionquotation(content_desc);
                                LoginorNot();
                            }
                        }
                        else {
                            addquote();
                        }

                    }
                };




                bt_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                if (!alertDialog.isShowing())
                    alertDialog.show();


            }
        });
        dialog.show();

    }


    public void addquote() {
        Commons.showProgress(context);
        HashMap<String, String> header1 = new HashMap<>();

            header1.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String, String> addfav_req = new HashMap<>();
        addfav_req.put("car_id", intent_carid);
        addfav_req.put("exterior_color", selected_exteriorcolor);
        addfav_req.put("interior_color", select_interioicolor);
        addfav_req.put("type", sts_carinfo);
        addfav_req.put("quantity", no_cars_quotes);

        Log.e("-->Precar-add quote->", addfav_req.toString());
        System.out.println("==precar quote--" + addfav_req + "---" + header1);
        preOwnedCarDetailviewModel.addquoteLiveData(header1, addfav_req).observe(this, new Observer<Response_Addquote>() {
            @Override
            public void onChanged(Response_Addquote response_addquote) {
                Commons.hideProgress();

                if (response_addquote.status.equals("1")) {
                    System.out.println("success--->addquote-->" + response_addquote.toString());
                    Log.e("->add quote-->", response_addquote.toString());
                    String content_desc="Car has been added to your quotation list. Do you want to view the quotation list page?";
                    dialog_completionquotation(content_desc);
                    getquote_func();
                } else if (response_addquote.status.equals("0")) {
                    Commons.Alert_custom(context, response_addquote.longMessage);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getquote_func();
        LoginorNot();
    }

    private void dialog_completionquotation(String message) {

        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.custom_alert_button_linear_quotation);
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText(message);
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tocart = new Intent(PreOwnedCar_Detail_Activity.this, HomeActivity_searchimage_BottomButton.class);
                    tocart.putExtra("tabposition", "cart");
                    tocart.putExtra("position_tabs","1");
                    startActivity(tocart);
                    alertDialog.dismiss();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();
        }
    }

    public void ToImageView()
    {
        Intent toimageview=new Intent(PreOwnedCar_Detail_Activity.this,ImageViewer_Activity.class);
        toimageview.putStringArrayListExtra("car_images",img_list);
        toimageview.putExtra("position",position_image);
        toimageview.putExtra("sharingurl",sharingUrl);
        startActivity(toimageview);
    }

       public void Interiorcolor(String icolor) {
        select_interioicolor = icolor;
        System.out.println("==dhh==interior=444=" + icolor);
        System.out.println("==dhh==interior=44=" + select_interioicolor);
         Method_CarDetail();
    }

    public void Exteriorcolor(String ecolor) {
        selected_exteriorcolor = ecolor;
        System.out.println("==dhh=exterior=6=" + ecolor);
        System.out.println("==dhh==extererior=666=" + selected_exteriorcolor);
        //  Method_CarDetail();
    }




    private int getPositionInterior(String chosencolor1)
    {
        for(int i=0; i < listinterior_color.size(); i++){
            if(listinterior_color.get(i).equals(chosencolor1)){
                return i;
            }
        }
        return 0;
    }



    private int getPosition(String chosencolor)
    {
        for(int i=0; i < listexterior_color.size(); i++){
            if(listexterior_color.get(i).equals(chosencolor)){
                return i;
            }
        }
        return 0;
    }
}

