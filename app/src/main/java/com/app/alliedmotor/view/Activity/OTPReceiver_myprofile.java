package com.app.alliedmotor.view.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import com.poovam.pinedittextfield.LinePinField;

public class OTPReceiver_myprofile extends BroadcastReceiver {

    private static LinePinField editText_otp;

    public void setEditText_otp(LinePinField editText){
        OTPReceiver_myprofile.editText_otp = editText;
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        SmsMessage[] smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        for (SmsMessage smsMessage : smsMessages){

            String message_body = smsMessage.getMessageBody();
            System.out.println("gfg===="+message_body);
            String getOTP = message_body.split("is ")[1];
            editText_otp.setText(getOTP);
        }
    }
}
