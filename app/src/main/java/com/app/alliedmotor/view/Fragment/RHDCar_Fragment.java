package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Login12.Service_RHDcarList;
import com.app.alliedmotor.model.RHDCar.CarListItem;
import com.app.alliedmotor.model.RHDCar.MakeListItem;
import com.app.alliedmotor.model.RHDCar.Response_RHDCar;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.RHDCar_CarList_Adapter;
import com.app.alliedmotor.view.Adapter.RHDCar_CarList_Adapter_HomepageBrand;
import com.app.alliedmotor.view.Adapter.RHDCar_MakerList_Adapter;
import com.app.alliedmotor.viewmodel.RHDcarviewModel;

import java.util.ArrayList;
import java.util.HashMap;

public class RHDCar_Fragment extends Fragment implements RHDCar_MakerList_Adapter.IRHDCarMakelist{


    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;
    RecyclerView rv_carlist,rv_car_makerlist;
    LinearLayoutManager linearLayoutManager,linearLayoutManager1;
    View root;
    RHDcarviewModel rhDcarviewModel;
    ArrayList<CarListItem>Rhd_carlist=new ArrayList<>();
    ArrayList<MakeListItem>Rhd_makelist=new ArrayList<>();
    RHDCar_CarList_Adapter rhdCar_carList_adapter;

    RHDCar_CarList_Adapter_HomepageBrand rhdCar_carList_adapter_home;
    RHDCar_MakerList_Adapter rhdCar_makerList_adapter;
    private Context context;
    LinearLayout loading_ll,main_ll;
    CustomTextViewRegular tv_emptydata;
    ImageView ic_vehicletype;
    CustomTextViewSemiBold tv_cartype;
    ArrayList<String> vehicle_typelist=new ArrayList<>();

    String slugname="rhd-car-details";
    String makeid="";
    String vehicle_type="";

    public RHDCar_Fragment(String makeid) {
        this.makeid = makeid;
    }

    public RHDCar_Fragment() {

    }

    private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                makeid="";
                Func_FilterOption();
                Func_Selection();
                _hasLoadedOnce = true;
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(root == null) {
            root = inflater.inflate(R.layout.fragment_newcar, null);
            rhDcarviewModel = ViewModelProviders.of(this).get(RHDcarviewModel.class);
            context=getActivity();

          //  loading_ll=root.findViewById(R.id.loading_ll);
            main_ll=root.findViewById(R.id.main_ll);
            rv_car_makerlist = root.findViewById(R.id.rv_car_makerlist);
            rv_carlist = root.findViewById(R.id.rv_carlist);

            ic_vehicletype = root.findViewById(R.id.ic_vehicletype);
            tv_cartype = root.findViewById(R.id.tv_cartype);
            tv_emptydata=root.findViewById(R.id.tv_emptydata);


            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rv_carlist.setLayoutManager(linearLayoutManager);
            linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rv_car_makerlist.setLayoutManager(linearLayoutManager1);

            if(makeid==null) {
               // Method_RHDCar();

                makeid="";
                tv_cartype.setText("All");
                Func_Selection();
            }
            else {
                Func_Selection();
            }
            Func_FilterOption();
        }

        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                Func_FilterOption();
                vehicle_type=tv_cartype.getText().toString().trim();
                if(makeid==null ||makeid=="" || makeid.equals(""))
                {
                  //  Method_RHDCar();
                    Func_Selection();
                }
                else {
                    Func_Selection();
                }
            }
        };
        return root;
    }

    private void Method_RHDCar() {

        rhDcarviewModel.getRHDCardata().observe(getActivity(), new Observer<Response_RHDCar>() {
            @Override
            public void onChanged(Response_RHDCar response_rhdCar) {

                main_ll.setVisibility(View.VISIBLE);

                Log.i("res-1=rhdcar->",response_rhdCar.toString());
                System.out.println("===res=new car=="+response_rhdCar.toString());
                if(response_rhdCar.getStatus().equals("1"))
                {
                    if(response_rhdCar.getData().getCarList().size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",response_rhdCar.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        Rhd_carlist = response_rhdCar.getData().getCarList();
                        Rhd_makelist = response_rhdCar.getData().getMakeList();
                        vehicle_typelist = response_rhdCar.getData().getVehicleTypeList();
                        Func_Adapter();
                    }
            }
                else if(response_rhdCar.getStatus().equals("0"))
                {
                    Toast.makeText(context,response_rhdCar.getMessage(),Toast.LENGTH_SHORT).show();
                }

                }
        });

    }

    private void Func_Adapter() {
        rhdCar_makerList_adapter =new RHDCar_MakerList_Adapter(context,Rhd_makelist,this::BrandSelected,makeid);
        rv_car_makerlist.setAdapter(rhdCar_makerList_adapter);

        rhdCar_carList_adapter =new RHDCar_CarList_Adapter(context,Rhd_carlist);
        rv_carlist.setAdapter(rhdCar_carList_adapter);
        rv_car_makerlist.scrollToPosition(getPosition(makeid));

    }

    private void Func_FilterOption() {
        ic_vehicletype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);

                //  popup.getMenuInflater().inflate(R.menu.popup_options, popup.getMenu());
                for(int i=0;i<vehicle_typelist.size();i++) {
                    popup.getMenu().add(vehicle_typelist.get(i));
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        tv_cartype.setText(item.getTitle());
                        vehicle_type=tv_cartype.getText().toString().trim();
                        Func_Selection();
                        return true;
                    }
                });
                popup.show();//showing popup menu
            }
        });
    }



    @Override
    public void BrandSelected(MakeListItem data, int position) {
        makeid=data.getMkid();
        tv_cartype.setText("All");

        autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
        autoCompleteHandler.postDelayed(autoCompleteRunnable, 500);
    }

    private void Func_Selection() {

        Rhd_carlist.clear();
        if(makeid==null)
        {
            makeid="";
        }
        HashMap<String, String> request = new HashMap<>();
        request.put("make_id",makeid);
        request.put("vehicle_type",vehicle_type);

        String url= Constants.BASE_URL+"v1/"+slugname+"/";

        new Service_RHDcarList(url, request, new ResponseInterface.RHDCarListInterface() {
            @Override
            public void onRHDCarListSuccess(Response_RHDCar response_rhdCar) {
                Commons.hideProgress();
                System.out.println("---response-selected brand--"+response_rhdCar.toString());


                if(response_rhdCar.getStatus().equals("1")) {
                    Rhd_carlist = response_rhdCar.getData().getCarList();
                    Rhd_makelist = response_rhdCar.getData().getMakeList();
                    if(Rhd_carlist.size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",response_rhdCar.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        Rhd_carlist = response_rhdCar.getData().getCarList();
                        Rhd_makelist = response_rhdCar.getData().getMakeList();
                        vehicle_typelist = response_rhdCar.getData().getVehicleTypeList();
                        if( !makeid.equals("")){
                            Func_Adapter1();}
                        else {
                            Func_Adapter();
                        }


                    }
                }
                else if(response_rhdCar.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + response_rhdCar.toString());
                    Log.i("res_newcar0-->",response_rhdCar.toString());
                    Toast.makeText(context,response_rhdCar.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }
        });

       /* rhDcarviewModel.getselectedMakeList(url,request).observe(getActivity(), new Observer<Response_RHDCar>() {
            @Override
            public void onChanged(Response_RHDCar responseRhdCar) {

                Commons.hideProgress();
                System.out.println("---response-selected brand--"+responseRhdCar.toString());


                if(responseRhdCar.getStatus().equals("1")) {
                    Rhd_carlist = responseRhdCar.getData().getCarList();
                    Rhd_makelist = responseRhdCar.getData().getMakeList();
                    if(Rhd_carlist.size()==0)
                    {
                        rv_carlist.setVisibility(View.INVISIBLE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",responseRhdCar.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        Rhd_carlist = responseRhdCar.getData().getCarList();
                        Rhd_makelist = responseRhdCar.getData().getMakeList();
                        vehicle_typelist = responseRhdCar.getData().getVehicleTypeList();
                        Func_Adapter1();



                    }
                }
                else if(responseRhdCar.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + responseRhdCar.toString());
                    Log.i("res_newcar0-->",responseRhdCar.toString());
                    Toast.makeText(context,responseRhdCar.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }
        });
*/
    }

    private void Func_Adapter1() {

        rhdCar_makerList_adapter = new RHDCar_MakerList_Adapter(context, Rhd_makelist,this,makeid);
        rv_car_makerlist.setAdapter(rhdCar_makerList_adapter);


        rhdCar_carList_adapter_home = new RHDCar_CarList_Adapter_HomepageBrand(context, Rhd_carlist);
        rv_carlist.setAdapter(rhdCar_carList_adapter_home);

        rv_car_makerlist.scrollToPosition(getPosition(makeid));
    }


    private int getPosition(String makeid)
    {
        for(int i=0; i < Rhd_makelist.size(); i++){
            if(Rhd_makelist.get(i).getMkid().equals(makeid)){
                return i;
            }
        }
        return 0;
    }

}
