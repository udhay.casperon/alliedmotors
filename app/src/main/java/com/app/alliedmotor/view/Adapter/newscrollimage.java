package com.app.alliedmotor.view.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.OnItemClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class newscrollimage extends RecyclerView.Adapter<newscrollimage.ViewHolder>{
    ArrayList<String> cargallery_list;
    Context context;
    private final OnItemClickListener listener;

    // RecyclerView recyclerView;
    public newscrollimage(Context context, ArrayList<String> cargallery_list, OnItemClickListener listener) {
        this.context = context;
        this.cargallery_list = cargallery_list;
        this.listener = listener;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.banner_image_slider_detail, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Picasso.get().load(cargallery_list.get(position)).into(holder.imageView, new Callback() {
            @Override
            public void onSuccess() {
                holder.ll_progress.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return cargallery_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        LinearLayout ll_progress;
        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.img_banner);
            this.ll_progress=itemView.findViewById(R.id.ll_progress);
        }
    }
}
