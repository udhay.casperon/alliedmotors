package com.app.alliedmotor.view.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.view.Fragment.Account_Fragment;
import com.app.alliedmotor.view.Fragment.Cart_Fragment;

import com.app.alliedmotor.view.Fragment.Categories_Fragment;
import com.app.alliedmotor.view.Fragment.Chat_Fragment;
import com.app.alliedmotor.view.Fragment.HomeFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener
        , View.OnClickListener {

    private Context context;
    ImageView img_home, ic_notification, ic_menu;
    LinearLayout ll_customised_toolbar;
    BottomNavigationView navigation;
    EditText searchview;
    String backstatename;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customised_toolbar);

        context = this;
        sharedPref = new SharedPref(context);
        findbyviews();
        clicklistener();

        loadFragment(new HomeFragment());
        navigation.setOnNavigationItemSelectedListener(this);

    }

    private void clicklistener() {
        img_home.setOnClickListener(this);
        ic_menu.setOnClickListener(this);
        ic_notification.setOnClickListener(this);
    }

    private void findbyviews() {
        navigation = findViewById(R.id.navigation);
        ll_customised_toolbar = findViewById(R.id.ll_customised_toolbar);
        img_home = findViewById(R.id.img_home);
        ic_notification = findViewById(R.id.ic_notification);
        ic_menu = findViewById(R.id.ic_menu);
        searchview = findViewById(R.id.searchview);

    }


    private boolean loadFragment(Fragment fragment) {
        backstatename = fragment.getClass().getName();
        //switching fragment
        if (fragment != null) {
            //   getSupportFragmentManager().popBackStack();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    /* .addToBackStack(backstatename)*/
                    .commit();

            return true;
        }

        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.bn_home:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                menuItem.setIcon(R.drawable.home_new);
                fragment = new HomeFragment();
                break;

            case R.id.bn_categories:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                fragment = new Categories_Fragment();
                break;

            case R.id.bn_chat:
                ll_customised_toolbar.setVisibility(View.GONE);
                fragment = new Chat_Fragment();
                break;

            case R.id.bn_cart:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                fragment = new Cart_Fragment();
                break;
            case R.id.bn_account:
                ll_customised_toolbar.setVisibility(View.GONE);
                fragment = new Account_Fragment();
                break;
        }

        return loadFragment(fragment);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.ic_menu:
                System.out.println("==Menu===");
                break;
            case R.id.ic_notification:
                System.out.println("=notification===");
                break;
            case R.id.img_home:
                onBackPressed();
                System.out.println("==home icon===");
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {

    }

}