package com.app.alliedmotor.view.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Sample.Response_UserCreation;
import com.app.alliedmotor.model.Signup.Response_SignupResponse;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.viewmodel.VerifyOTPviewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.poovam.pinedittextfield.LinePinField;

import java.util.HashMap;

public class OTPVerification_SignupActivity extends AppCompatActivity  {


    private int RESOLVE_HINT = 2;

    CustomRegularButton bt_submit;
  ImageView back_setting;
    String st_otp,st_reotp;
    Context context;
    VerifyOTPviewModel verifyOTPviewModel;
    CustomRegularTextview tv_resent_otp;
    CustomRegularTextview tv_otpviewcontent;


    String res_otp,otp_type,st_deviceid,r_contentmessage,r_otpdevmode,r_otptoken;
    SharedPref sharedPref;

    LinePinField otppinfield;
    String r_fname="",r_lname="",r_emai="",e_mobile="",r_usertype="",r_company="",r_userrole="",r_password="",r_userletter="",r_otptype="";
  String r_pagename="verifyphonenumber";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_verification_);
        verifyOTPviewModel = ViewModelProviders.of(this).get(VerifyOTPviewModel.class);

        context=this;
        findbyviews();
        sharedPref=new SharedPref(context);





        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        // Get new FCM registration token
                        st_deviceid = task.getResult();

                        System.out.println("------token--------"+st_deviceid);
                    }
                });


        Intent getIntent=getIntent();

        if(getIntent!=null) {
            r_fname = getIntent.getStringExtra("fname");
            r_lname = getIntent.getStringExtra("lname");

            r_emai = getIntent.getStringExtra("useremail");
            e_mobile = getIntent.getStringExtra("mobilenumber");
            r_otptype = getIntent.getStringExtra("otptype");

            r_usertype = getIntent.getStringExtra("usertype");
            r_company = getIntent.getStringExtra("companyname");
            r_userrole = getIntent.getStringExtra("companyuserole");
            r_userletter = getIntent.getStringExtra("useletter");
            r_password = getIntent.getStringExtra("password");

            r_contentmessage = getIntent.getStringExtra("contentmessage");
            r_otpdevmode = getIntent.getStringExtra("otpdevmode");
            r_otptoken = getIntent.getStringExtra("otptoken");
            r_pagename=getIntent.getStringExtra("pagename");
        }
        Commons.Alert_custom(context,r_contentmessage);
        tv_otpviewcontent.setText(r_contentmessage);

        requestPermissions();
        if(r_otpdevmode.equals("1"))
        {
            System.out.println("--Development---");
            otppinfield.setText(r_otptoken);
        }
        else if(r_otpdevmode.equals("0"))
        {
            System.out.println("--Production---");
            new OTPReceiver_myprofile().setEditText_otp(otppinfield);
        }


        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validtion_OTP();
            }
        });

        back_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tosignup=new Intent(OTPVerification_SignupActivity.this,Signup_Activity.class);
                startActivity(tosignup);
            }
        });


        tv_resent_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commons.showProgress(context);
               Func_OtpResent();

            }
        });

        new OTPReceiver_Signup().setEditText_otp(otppinfield);

    }

    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(OTPVerification_SignupActivity.this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(OTPVerification_SignupActivity.this,new String[]{
                    Manifest.permission.RECEIVE_SMS
            },100);
        }
    }

    private void Func_OtpResent() {

        HashMap<String, String> request_signup = new HashMap<>();
        request_signup.put("first_name", r_fname);
        request_signup.put("last_name", r_lname);
        request_signup.put("user_email", r_emai);
        request_signup.put("mobile_number", e_mobile);
        request_signup.put("user_type", r_usertype);
        request_signup.put("company_name", r_company);
        request_signup.put("company_role", r_userrole);
        request_signup.put("enable_newsletter",r_userletter );
        request_signup.put("password", r_password);
        request_signup.put("otp_type",r_otptype);
        verifyOTPviewModel.getSignupData(request_signup).observe(this, new Observer<Response_SignupResponse>() {
            @Override
            public void onChanged(Response_SignupResponse signupResponse) {
                Commons.hideProgress();
                System.out.println("===res=signup==" + signupResponse.toString());
                if(signupResponse.getStatus().equals("1")) {
                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getCode());
                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getMobileNumber());
                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getUserEmail());
                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getOtpType());
                    if(signupResponse.getData().getOtpData().getDevMode()==1) {
                        r_otpdevmode=String.valueOf(signupResponse.getData().getOtpData().getDevMode());
                        r_otptoken=signupResponse.getData().getOtpData().getCode();
                        otppinfield.setText(r_otptoken);
                    }
                    else
                    {
                        r_otpdevmode=String.valueOf(signupResponse.getData().getOtpData().getDevMode());
                        r_otptoken=signupResponse.getData().getOtpData().getCode();
                        new OTPReceiver_Signup().setEditText_otp(otppinfield);
                    }

                }

                else if(signupResponse.getStatus().equals("0"))
                {
                    if(signupResponse.getErrors().getMobile_number()!=null)
                    {
                        Commons.Alert_custom(context,signupResponse.getErrors().getMobile_number());
                    }
                    else if(signupResponse.getErrors().getUserEmail()!=null) {
                        Commons.Alert_custom(context, signupResponse.getErrors().getUserEmail());
                    }


                }
            }
        });


    }

    private void Validtion_OTP() {
         st_otp=otppinfield.getText().toString().trim();

         if(st_otp.isEmpty())
         {
             Commons.Alert_custom(context,"Please Enter OTP");
         }
         else if(!r_otptoken.equals(st_otp))
         {
             Commons.Alert_custom(context,"Entered Otp wrong");
         }
        else
        {
            Call_API_MobileVerfiy();
        }
    }

    private void Call_API_MobileVerfiy() {

        Commons.showProgress(context);
        HashMap<String, String> request = new HashMap<>();
        request.put("first_name",r_fname);
        request.put("last_name",r_lname);
        request.put("user_email",r_emai);
        request.put("mobile_number",e_mobile);
        request.put("user_type",r_usertype);
        request.put("company_name",r_company);
        request.put("company_role",r_userrole);
        request.put("enable_newsletter",r_userletter);
        request.put("password",r_password);
        request.put("otp_type",r_otptype);
        request.put("mo_otp_token",r_otptoken);
        request.put("device_id",st_deviceid);
        request.put("device_type","Android");

        verifyOTPviewModel.getusercreation(request).observe(this, new Observer<Response_UserCreation>() {
            @Override
            public void onChanged(Response_UserCreation response_userCreation) {
                Commons.hideProgress();
                System.out.println("dv==="+response_userCreation.toString());
                if(response_userCreation.status.equals("1")) {
                    Alert_custom(context,response_userCreation.longMessage);

                    sharedPref.setString(Constants.JWT_TOKEN,response_userCreation.commonArr.token);
                    sharedPref.setString(Constants.PREF_USERMAIL,response_userCreation.data.userEmail);
                    sharedPref.setString(Constants.PREF_USERMOBILE,response_userCreation.data.mobileNumber);
                    sharedPref.setString(Constants.PREF_USERNAME,response_userCreation.data.firstName);
                    sharedPref.setString(Constants.PREF_OTPTYPE,response_userCreation.data.otpType);

                }
                else if(response_userCreation.status.equals("0"))
                {
                    Commons.Alert_custom(context,response_userCreation.longMessage);

                }
            }
        });



    }

    public  void Alert_custom(final Context context,String message)
    {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_ios_singlebutton);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if(alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        txtInstructionDialog.setText(message);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview rlClose = alertDialog.findViewById(R.id.bt_okay);
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tomain=new Intent(OTPVerification_SignupActivity.this,HomeActivity_searchimage_BottomButton.class);
                startActivity(tomain);
                finish();
                alertDialog.dismiss();

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }







    private void findbyviews() {
        bt_submit=findViewById(R.id.bt_submit);
        back_setting=findViewById(R.id.back_setting);
        otppinfield=findViewById(R.id.otppinfield);
        tv_otpviewcontent=findViewById(R.id.tv_otpviewcontent);
        tv_resent_otp=findViewById(R.id.tv_resent_otp);

    }


}