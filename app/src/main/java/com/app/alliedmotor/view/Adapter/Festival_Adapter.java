package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.HomePage.DataItem;
import com.app.alliedmotor.model.HomePage.FestivalOffersItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewBold;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.FullCarList_Category;

import java.util.ArrayList;

public class Festival_Adapter extends RecyclerView.Adapter<Festival_Adapter.ViewHolder> {

    ArrayList<FestivalOffersItem> festivalOffersItems = new ArrayList<>();

    Categoires listener;
    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;
    FestivalList_car_Adapter festivalList_car_adapter;
    ArrayList<DataItem> dataItems = new ArrayList<>();
    LinearSnapHelper snapHelper;


    public Festival_Adapter(Context context, ArrayList<FestivalOffersItem> listdata,Categoires listener) {
        this.mcontext = context;
        this.festivalOffersItems = listdata;
        this.listener = listener;
        snapHelper=new LinearSnapHelper();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.festival_rv_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        //listItem.getLayoutParams().width = (int) (getScreenWidth() / 2);
        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    public int getScreenWidth() {

        WindowManager wm = (WindowManager) mcontext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size.x;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        dataItems=festivalOffersItems.get(position).getData();
        holder.textview1.setText(festivalOffersItems.get(position).getTitle());
        linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false);
        holder.rv_list_festivals.setLayoutManager(linearLayoutManager);

       /* snapHelper.attachToRecyclerView(holder.rv_list_festivals);*/
        festivalList_car_adapter = new FestivalList_car_Adapter(mcontext, dataItems);
        holder.rv_list_festivals.setAdapter(festivalList_car_adapter);

        holder.fulllist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* dataItems=festivalOffersItems.get(position).getData();
                listener.onCategorySelected(dataItems,position);*/

               Intent tointent=new Intent(mcontext, FullCarList_Category.class);
               tointent.putExtra("festive_title",festivalOffersItems.get(position).getTitle());
               tointent.putExtra("festive_datalist",festivalOffersItems.get(position).getData());
               mcontext.startActivity(tointent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return festivalOffersItems.size();
    }

    public interface Categoires {
        void onCategorySelected(ArrayList<DataItem> data, int position);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewBold textview1;
        RecyclerView rv_list_festivals;
        CustomTextViewSemiBold fulllist;


        public ViewHolder(View itemView) {

            super(itemView);

            this.textview1 = itemView.findViewById(R.id.textview1);
            this.rv_list_festivals = itemView.findViewById(R.id.rv_list_festivals);
            this.fulllist = itemView.findViewById(R.id.textView23);


        }
    }
}

