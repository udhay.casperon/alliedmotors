package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.viewmodel.AppInfoviewModel;
import com.bumptech.glide.Glide;

import java.util.HashMap;

public class OfferPage_Activity extends AppCompatActivity {


    ImageView back_img,carimage;
    CustomRegularTextview tv_long_desc_value;
    CustomRegularButton bt_button;
    Context context;
    LinearLayout loading_ll;
    String imgeurl="",enablebutton="",buttontext="",shortdesc="",longdesc="",posttitle="",buttonurl="";
    CustomTextViewSemiBold offer_title,tv_short_desc_value;
    CardView cv_image;
    LinearLayout ll_short_content,ll_longcontent;
    AppInfoviewModel appInfoviewModel;
    SharedPref sharedPref;
    String res_postid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_page_);
        appInfoviewModel = ViewModelProviders.of(this).get(AppInfoviewModel.class);
        context=this;
        sharedPref=new SharedPref(context);


        if(getIntent()!=null)
        {
            res_postid=getIntent().getStringExtra("postid");
            posttitle=getIntent().getStringExtra("posttitle");
            imgeurl=getIntent().getStringExtra("imageurl");
            buttontext=getIntent().getStringExtra("buttontext");
            enablebutton=getIntent().getStringExtra("enablebutton");
            longdesc=getIntent().getStringExtra("postcontent");
            shortdesc=getIntent().getStringExtra("shortdesc");
            buttonurl=getIntent().getStringExtra("buttonurl");
        }
      //  loading_ll.setVisibility(View.GONE);
        findviews();


        Function_Set();
        offerselected();

            back_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    Intent tonotify=new Intent(OfferPage_Activity.this,NotificationList_Activity.class);
                    startActivity(tonotify);
                    finish();

                }
            });

        bt_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent towebview=new Intent(OfferPage_Activity.this,OfferWebview.class);
                towebview.putExtra("buttonurl",buttonurl);
                startActivity(towebview);
            }
        });
    }


    private void offerselected()
    {
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("post_id", res_postid);


        appInfoviewModel.statuschange_offer(header,request).observe(this, new Observer<Response_OfferNotify>() {
            @Override
            public void onChanged(Response_OfferNotify offerNotify) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+offerNotify.message);
                if(offerNotify.status.equals("1"))
                {
                    Func_AppInfo();
                }
                else if((offerNotify.status.equals("0")))
                {
                }
            }
        });

    }

    private void Func_AppInfo() {
        HashMap<String,String> header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else if(sharedPref.getString(Constants.JWT_TOKEN)!=null)
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        appInfoviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());

                    sharedPref.setString(Constants.PREF_USERNAME, response_appInfo.getData().getUserName());
                    sharedPref.setString(Constants.PREF_USERMAIL, response_appInfo.getData().getUserEmail());
                    sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }


    private void Function_Set() {
        offer_title.setText(posttitle);
        if(imgeurl==null || imgeurl.equals("") || imgeurl=="")
        {
            cv_image.setVisibility(View.GONE);
        }
        else
        {
            cv_image.setVisibility(View.VISIBLE);
            Glide.with(context).load(imgeurl).into(carimage);
        }


        if(buttonurl.equals("")|| buttontext.equals("") || buttontext==null || buttonurl==null || enablebutton=="0"|| enablebutton.equals("0"))
        {
            bt_button.setVisibility(View.GONE);
        }
        else
        {
            bt_button.setVisibility(View.VISIBLE);
            bt_button.setText(buttontext);
        }

        if(shortdesc.equals("") || shortdesc=="")
        {
            ll_short_content.setVisibility(View.GONE);
        }
        else {
            tv_short_desc_value.setText(shortdesc);
        }
        if(longdesc.equals("") || longdesc=="") {
            ll_longcontent.setVisibility(View.GONE);
        }
        else {
            tv_long_desc_value.setText(longdesc);
        }

    }

    private void findviews() {
        back_img=findViewById(R.id.back_img);
        carimage=findViewById(R.id.carimage);
        offer_title=findViewById(R.id.offer_title);
        tv_short_desc_value=findViewById(R.id.tv_short_desc_value);
        tv_long_desc_value=findViewById(R.id.tv_long_desc_value);
        bt_button=findViewById(R.id.bt_button);
        cv_image=findViewById(R.id.cv_image);
        ll_short_content=findViewById(R.id.ll_short_content);
        ll_longcontent=findViewById(R.id.ll_longcontent);
    }
}