package com.app.alliedmotor.view.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.view.Fragment.Account_Fragment_Linear;
import com.app.alliedmotor.view.Fragment.Cart_Fragment;
import com.app.alliedmotor.view.Fragment.Categories_Fragment;
import com.app.alliedmotor.view.Fragment.Chat_Fragment;
import com.app.alliedmotor.view.Fragment.HomeFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.greenrobot.eventbus.EventBus;

public class HomeActivity_searchimage extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener
,View.OnClickListener
{

    private Context context;
    ImageView img_home,ic_notification,ic_menu;
    LinearLayout ll_customised_toolbar;
    BottomNavigationView navigation;
    EditText searchview;
    ImageView searhview_icon;
    String backstatename;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customised_toolbar_image);

        context=this;

        sharedPref = new SharedPref(context);
        findbyviews();
        clicklistener();

        loadFragment(new HomeFragment());
        navigation.setItemIconTintList(null);
        navigation.setOnNavigationItemSelectedListener(this);
        if(searchview.isCursorVisible())
        {
            searchview.setCursorVisible(true);
            searhview_icon.setVisibility(View.GONE);
        }
        else
        {
            searchview.setCursorVisible(false);
            searhview_icon.setVisibility(View.VISIBLE);
        }

        ic_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(HomeActivity_searchimage.this, v);
                popup.getMenuInflater().inflate(R.menu.popup_options, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(HomeActivity_searchimage.this, "Some Text" + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });
                popup.show();//showing popup menu
            }
        });




    }

    private void clicklistener() {
        img_home.setOnClickListener(this);
        ic_menu.setOnClickListener(this);
        ic_notification.setOnClickListener(this);
        searhview_icon.setOnClickListener(this);
    }

    private void findbyviews()
    {
        navigation = findViewById(R.id.navigation);
        ll_customised_toolbar=findViewById(R.id.ll_customised_toolbar);
        img_home=findViewById(R.id.img_home);
        ic_notification=findViewById(R.id.ic_notification);
        ic_menu=findViewById(R.id.ic_menu);
        searchview=findViewById(R.id.searchview);
        searhview_icon=findViewById(R.id.searhview_icon);

    }


    private boolean loadFragment(Fragment fragment) {
        backstatename=fragment.getClass().getName();
        //switching fragment
        if (fragment != null) {
         //   getSupportFragmentManager().popBackStack();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                   /* .addToBackStack(backstatename)*/
                    .commit();
            return true;
        }

        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.bn_home:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                menuItem.setIcon(R.drawable.home_new);
                fragment = new HomeFragment();
                break;

            case R.id.bn_categories:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                fragment = new Categories_Fragment();
                break;

            case R.id.bn_chat:
                ll_customised_toolbar.setVisibility(View.GONE);
                fragment = new Chat_Fragment();
                break;

            case R.id.bn_cart:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                fragment = new Cart_Fragment();
                break;
            case R.id.bn_account:
                ll_customised_toolbar.setVisibility(View.GONE);
                fragment = new Account_Fragment_Linear();
                break;
        }

        return loadFragment(fragment);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case  R.id.ic_menu:
                System.out.println("==Menu===");
                break;
            case  R.id.ic_notification:
                break;
            case  R.id.img_home:
                onBackPressed();
                break;
            case  R.id.searhview_icon:
                searhview_icon.setVisibility(View.GONE);
                searchview.setCursorVisible(true);
               // System.out.println("==home icon===");
                break;

            case  R.id.searchview:
                searhview_icon.setVisibility(View.GONE);
                searchview.setCursorVisible(true);
                    break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }




}