package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.MyOrderLatest.OrderDataItem;
import com.app.alliedmotor.model.MyOrderLatest.OrdersItem;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewMedium;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChatOrder_Adapter extends RecyclerView.Adapter<ChatOrder_Adapter.ViewHolder> {

    ArrayList<OrderDataItem> dataItems;
    ArrayList<OrdersItem>adata=new ArrayList<>();

    LinearLayoutManager linearLayoutManager;
    SharedPref sharedPref;
    private Context mcontext;
    public boolean isClickedFirstTime = true;
    String dateStr="";
    MyOrder_rowsAdpater myOrder_rowsAdpater;
    String mobilenum="",sharing_content="";

    public ChatOrder_Adapter(Context context, ArrayList<OrderDataItem> dataItems) {
        this.mcontext = context;
        this.dataItems = dataItems;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_your_order_linear, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }


     @Override
       public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        adata=dataItems.get(position).getOrders();
         sharing_content="I want to follow up inquiry with ticket no: "+dataItems.get(position).getTicketId() +" for "+dataItems.get(position).getCarCategory();

         dateStr = dataItems.get(position).getEnqCrtdate();
         String resultDate = convertStringDateToAnotherStringDate(dateStr, "dd-MM-yyyy hh:mm a", "dd/MM/yyyy hh:mm a");

         holder.tv_carCategory.setText(dataItems.get(position).getCarCategory());
        holder.tv_orderdate.setText(resultDate);
        holder.tv_ticketno.setText("Ticket no "+dataItems.get(position).getTicketId());
        holder.tv_carCategory1.setText(dataItems.get(position).getCarCategory());
        holder.tv_orderdate1.setText(resultDate);
        holder.tv_ticketno1.setText("Ticket no "+dataItems.get(position).getTicketId());
         holder.tv_ticket_result1.setText("RFQ-submitted");
         holder.tv_ticket_result.setText("RFQ-submitted");

        linearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false);
        holder.rv_order_rows.setLayoutManager(linearLayoutManager);

        String status_ticket=dataItems.get(position).getEnqStatusTicket();

         if(status_ticket.equalsIgnoreCase("pending")||status_ticket.equalsIgnoreCase("Read")||status_ticket.equalsIgnoreCase("In Progress"))
         {

             holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
             holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
             holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rfq_text_yellow));
             holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
             holder.tv_ticket_result1.setText("RFQ-submitted");
             holder.tv_ticket_result.setText("RFQ-submitted");
         }
         if(status_ticket.equalsIgnoreCase("Won"))
         {
             holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.won_text_green));
             holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
             holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.won_text_green));
             holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
             holder.tv_ticket_result1.setText(status_ticket);
             holder.tv_ticket_result.setText(status_ticket);
         }


         else if(status_ticket.equalsIgnoreCase("Unfulfilled") || status_ticket.equalsIgnoreCase("Cancelled"))
         {
             holder.cv_status.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rejected_bg));
             holder.tv_ticket_result.setTextColor(mcontext.getResources().getColor(R.color.white));
             holder.cv_status1.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rejected_bg));
             holder.tv_ticket_result1.setTextColor(mcontext.getResources().getColor(R.color.white));
             holder.tv_ticket_result1.setText(status_ticket);
             holder.tv_ticket_result.setText(status_ticket);
         }

        holder.ll_short_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobilenum=dataItems.get(position).getMakeMobileNumber();
               // openWhatsApp(mobilenum);
                openWhatsApp1(mobilenum);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public void openWhatsApp(String mobilenum1){
        try {
            String toNumber = mobilenum1; // Replace with mobile phone number without +Sign or leading zeros, but with country code.
            String url = "https://api.whatsapp.com/send?phone=" + toNumber;
            PackageManager pm = mcontext.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.setData(Uri.parse(url));
            mcontext.startActivity(i);
        }
        catch (Exception e){
            e.printStackTrace();
            Commons.Alert_custom(mcontext,"Please Install WhatsApp in your phone");
        }
    }

    private void openWhatsApp1(String mobilenum1) {
        try {
            Uri uri = Uri.parse("smsto:" + mobilenum1);
          //  Intent shareIntent = new Intent(Intent.ACTION_SENDTO, uri);

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, sharing_content);
            shareIntent.putExtra("jid", mobilenum1 + "@s.whatsapp.net");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, sharing_content);
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            shareIntent.setPackage("com.whatsapp");

            mcontext.startActivity(shareIntent);
        }
        catch(Exception e)

        {
            e.printStackTrace();
            Commons.Alert_custom(mcontext, "Please Install WhatsApp in your phone");
        }
    }




    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewMedium tv_ticket_result,tv_ticket_result1;
        CustomTextViewRegular tv_orderdate,tv_ticketno,tv_orderdate1,tv_ticketno1;
        CardView cl_Call,cv_message,cv_status,cv_status1;
        CustomTextViewSemiBold tv_carCategory,tv_carCategory1;
        LinearLayout ll_short_content,ll_detailed_content;

        RecyclerView rv_order_rows;


        public ViewHolder(View itemView) {

            super(itemView);

            rv_order_rows=itemView.findViewById(R.id.rv_order_rows);
            tv_carCategory=itemView.findViewById(R.id.tv_carname);
            tv_ticket_result=itemView.findViewById(R.id.tv_ticket_result);
            tv_ticket_result1=itemView.findViewById(R.id.tv_ticket_result1);
            tv_orderdate=itemView.findViewById(R.id.tv_orderdate);
            tv_carCategory1=itemView.findViewById(R.id.tv_carname1);
            tv_ticketno=itemView.findViewById(R.id.tv_ticketno);
            tv_orderdate1=itemView.findViewById(R.id.tv_orderdate1);
            tv_ticketno1=itemView.findViewById(R.id.tv_ticketno1);
            cl_Call=itemView.findViewById(R.id.cl_Call);
            cv_message=itemView.findViewById(R.id.cv_message);



            ll_short_content=itemView.findViewById(R.id.ll_short_content);
            ll_detailed_content=itemView.findViewById(R.id.ll_detailed_content);
            cv_status=itemView.findViewById(R.id.cv_status);

            cv_status1=itemView.findViewById(R.id.cv_status1);

        }
    }

    public String convertStringDateToAnotherStringDate(String dateStr, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(dateStr);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }




}

