package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.NotificationDelete.Response_NotifyDelete;
import com.app.alliedmotor.model.NotificationStatusChange.Response_NotificationStatuschange;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.model.Response_NotificationList.NotificationListItem_folder;
import com.app.alliedmotor.model.Response_NotificationList.Offer_Notification_List;
import com.app.alliedmotor.model.Response_NotificationList.Response_NotificationList_folder;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.SwipeController;
import com.app.alliedmotor.utility.SwipeControllerActions;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.NotificationList_Adapter;
import com.app.alliedmotor.view.Adapter.OfferNotificationList_Adapter;
import com.app.alliedmotor.viewmodel.Notification_viewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class NotificationList_Activity extends AppCompatActivity implements NotificationList_Adapter.INotify,OfferNotificationList_Adapter.IOfferNotify{


    ImageView img_home;
    Context context;
    SharedPref sharedPref;
    RecyclerView rv_notification;
    LinearLayoutManager linearLayoutManager1,linearLayoutManager_offer;

    LinearLayoutManager linearLayoutManager1a,linearLayoutManager_offer1a;
    SwipeController swipeController = null;
    Notification_viewModel notification_viewModel;
    ArrayList<NotificationListItem_folder>arrayList=new ArrayList<>();
    ArrayList<Offer_Notification_List>offer_notification_lists=new ArrayList<>();
    NotificationList_Adapter notificationList_adapter;
    LinearLayout loading_ll,ll_empty,ll_nonlogin;
    LinearLayout main_ll,ll_loginfeature_text;
    String isread="1",isUnread="0";
    CustomTextViewSemiBold tv_loginfeature;
    OfferNotificationList_Adapter offerNotificationListAdapter;
    LinearLayout main_ll_offer,main_ll_normal,main_ll_offer1,main_ll_normal1;
    RecyclerView rv_notification_offer,rv_notification_offer1,rv_notification1;
    String headertitle="";
    LinearLayout ll_general,ll_offerfirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifcation_list1);
        notification_viewModel = ViewModelProviders.of(this).get(Notification_viewModel.class);
        context=this;
        sharedPref=new SharedPref(context);
        img_home=findViewById(R.id.img_home);
        rv_notification=findViewById(R.id.rv_notification);
        main_ll=findViewById(R.id.main_ll);
        ll_empty=findViewById(R.id.ll_empty);
        rv_notification_offer=findViewById(R.id.rv_notification_offer);
        main_ll_offer=findViewById(R.id.main_ll_offer);
        main_ll_normal=findViewById(R.id.main_ll_normal);
        loading_ll=findViewById(R.id.loading_ll);
        ll_loginfeature_text=findViewById(R.id.ll_loginfeature_text1);
        ll_nonlogin=findViewById(R.id.ll_nonlogin);
        tv_loginfeature=findViewById(R.id.tv_loginfeature);

        ll_general=findViewById(R.id.ll_general);
        ll_offerfirst=findViewById(R.id.ll_offerfirst);


        main_ll_offer1=findViewById(R.id.main_ll_offer1);
        main_ll_normal1=findViewById(R.id.main_ll_normal1);

        rv_notification_offer1=findViewById(R.id.rv_notification_offer1);
        rv_notification1=findViewById(R.id.rv_notification1);


        linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_notification.setLayoutManager(linearLayoutManager1);
        rv_notification.setHasFixedSize(true);


        linearLayoutManager_offer = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_notification_offer.setLayoutManager(linearLayoutManager_offer);
        rv_notification_offer.setHasFixedSize(true);



        linearLayoutManager1a = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_notification1.setLayoutManager(linearLayoutManager1a);
        rv_notification1.setHasFixedSize(true);


        linearLayoutManager_offer1a = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_notification_offer1.setLayoutManager(linearLayoutManager_offer1a);
        rv_notification_offer1.setHasFixedSize(true);



        Function1();


       Func_Event();
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent tohome=new Intent(NotificationList_Activity.this,HomeActivity_searchimage_BottomButton.class);
               startActivity(tohome);
               finish();
            }
        });

    }

    private void Function1() {

        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null))
        {
            ll_nonlogin.setVisibility(View.VISIBLE);
            main_ll.setVisibility(View.GONE);
            ll_empty.setVisibility(View.GONE);
        }
        else {
            Func_Notification();
        }

    }

    private void Func_Event() {
        tv_loginfeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tologin=new Intent(context, LoginActivity.class);
                startActivity(tologin);
                finish();
            }
        });

        ll_loginfeature_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tologin=new Intent(context, LoginActivity.class);
                startActivity(tologin);
                finish();
            }
        });

    }

    private void Func_Notification() {
        loading_ll.setVisibility(View.VISIBLE);
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        notification_viewModel.orderCreationResponseLiveData(header).observe(this, new Observer<Response_NotificationList_folder>() {
            @Override
            public void onChanged(Response_NotificationList_folder notificationList) {
                    loading_ll.setVisibility(View.GONE);
                    main_ll.setVisibility(View.VISIBLE);
                Commons.hideProgress();
                System.out.println("---update quote--"+notificationList.getMessage());
                if(notificationList.getStatus().equals("1"))
                {
                    headertitle=notificationList.getData().getHeader();

                    arrayList=notificationList.getData().getNotificationList();
                    offer_notification_lists=notificationList.getData().getOffer_notification_list();
                    if(headertitle.equals("general"))
                    {
                        ll_general.setVisibility(View.VISIBLE);
                        ll_offerfirst.setVisibility(View.GONE);
                        if(arrayList.size()==0 && offer_notification_lists.size()==0)
                        {
                            ll_empty.setVisibility(View.VISIBLE);
                        }
                        else if(offer_notification_lists.size()==0 && arrayList.size()!=0)
                        {
                            main_ll_offer1.setVisibility(View.GONE);
                            main_ll_normal1.setVisibility(View.VISIBLE);

                        }
                        else if(offer_notification_lists.size()!=0 && arrayList.size()==0)
                        {
                            main_ll_normal1.setVisibility(View.GONE);
                            main_ll_offer1.setVisibility(View.VISIBLE);
                        }
                        else if(offer_notification_lists.size()!=0 && arrayList.size()!=0)
                        {
                            main_ll.setVisibility(View.VISIBLE);
                            main_ll_normal1.setVisibility(View.VISIBLE);
                            main_ll_offer1.setVisibility(View.VISIBLE);
                        }
                        Func_Adapter();
                    }
                    else {
                        if (arrayList.size() == 0 && offer_notification_lists.size() == 0) {
                            ll_empty.setVisibility(View.VISIBLE);
                        } else if (offer_notification_lists.size() == 0 && arrayList.size() != 0) {
                            main_ll_offer.setVisibility(View.GONE);
                            main_ll_normal.setVisibility(View.VISIBLE);
                        } else if (offer_notification_lists.size() != 0 && arrayList.size() == 0) {
                            main_ll_normal.setVisibility(View.GONE);
                            main_ll_offer.setVisibility(View.VISIBLE);
                        } else if (offer_notification_lists.size() != 0 && arrayList.size() != 0) {
                            main_ll.setVisibility(View.VISIBLE);
                            main_ll_normal.setVisibility(View.VISIBLE);
                            main_ll_offer.setVisibility(View.VISIBLE);

                        }
                        Func_Adapter();
                    }
                }
                else if((notificationList.getStatus().equals("0")))
                {
                  //  ll_nonlogin.setVisibility(View.VISIBLE);
                    main_ll.setVisibility(View.GONE);
                    ll_empty.setVisibility(View.GONE);
                  //  Toast.makeText(context,notificationList.message,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void Func_Adapter() {
        Collections.sort(arrayList, (r1, r2) -> r2.getCreatedTime().compareTo(r1.getCreatedTime()));

        notificationList_adapter = new NotificationList_Adapter(context, arrayList,this);
        rv_notification.setAdapter(notificationList_adapter);

        notificationList_adapter = new NotificationList_Adapter(context, arrayList,this);
        rv_notification1.setAdapter(notificationList_adapter);

        Collections.sort(offer_notification_lists, (r1, r2) -> r2.getCreated_time().compareTo(r1.getCreated_time()));

        offerNotificationListAdapter=new OfferNotificationList_Adapter(context,offer_notification_lists,this);
        rv_notification_offer.setAdapter(offerNotificationListAdapter);

        offerNotificationListAdapter=new OfferNotificationList_Adapter(context,offer_notification_lists,this);
        rv_notification_offer1.setAdapter(offerNotificationListAdapter);
    }

    @Override
    public void Notifiy_Selected(String data) {

        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("notification_id", data);
        request.put("is_unread", "0");

        notification_viewModel.statuschange_notification(header,request).observe(this, new Observer<Response_NotificationStatuschange>() {
            @Override
            public void onChanged(Response_NotificationStatuschange notificationStatuschange) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+notificationStatuschange.message);
                if(notificationStatuschange.status.equals("1"))
                {
                    Func_AppInfo();
                    Func_Notification();
                }
                else if((notificationStatuschange.status.equals("0")))
                {
                }
            }
        });

    }

    @Override
    public void General_NotifyDelete(String ticktid) {
      //  dialog_confirmdelete(ticktid);
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("notification_id", ticktid);
        notification_viewModel.notification_delete(header,request).observe(this, new Observer<Response_NotifyDelete>() {
            @Override
            public void onChanged(Response_NotifyDelete response_notifyDelete) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+response_notifyDelete.getMessage());
                if(response_notifyDelete.getStatus().equals("1"))
                {
                    Func_AppInfo();
                    Func_Notification();
                }
                else if((response_notifyDelete.getStatus().equals("0")))
                {
                }
            }
        });

    }

   public  void APICall_GeneralNotifyDelete(String ticktid)
    {
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("notification_id", ticktid);
        notification_viewModel.notification_delete(header,request).observe(this, new Observer<Response_NotifyDelete>() {
            @Override
            public void onChanged(Response_NotifyDelete response_notifyDelete) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+response_notifyDelete.getMessage());
                if(response_notifyDelete.getStatus().equals("1"))
                {
                    Func_AppInfo();
                    Func_Notification();
                }
                else if((response_notifyDelete.getStatus().equals("0")))
                {
                }
            }
        });
    }



    private void dialog_confirmdelete(String ticketid1) {

        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.bottomsheet_notification);
            alertDialog.setCancelable(true);
            Window window = alertDialog.getWindow();
            window.setGravity(Gravity.BOTTOM);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Would you like to delete this Your Activities Notification?");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APICall_GeneralNotifyDelete(ticketid1);
                    alertDialog.dismiss();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();


        }
    }


    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        notification_viewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }

    @Override
    public void OfferNotifiy_Selected(String data) {
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("post_id", data);


        notification_viewModel.statuschange_offer(header,request).observe(this, new Observer<Response_OfferNotify>() {
            @Override
            public void onChanged(Response_OfferNotify offerNotify) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+offerNotify.message);
                if(offerNotify.status.equals("1"))
                {
                    Func_AppInfo();
                    Func_Notification();
                }
                else if((offerNotify.status.equals("0")))
                {
                }
            }
        });

    }

    @Override
    public void NotificationOffer_delete(String postid) {
        //dialog_confirmdelete_offer(postid);
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("post_id", postid);
        notification_viewModel.notification_delete(header,request).observe(this, new Observer<Response_NotifyDelete>() {
            @Override
            public void onChanged(Response_NotifyDelete response_notifyDelete) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+response_notifyDelete.getMessage());
                if(response_notifyDelete.getStatus().equals("1"))
                {
                    Func_AppInfo();
                    Func_Notification();
                }
                else if((response_notifyDelete.getStatus().equals("0")))
                {
                }
            }
        });
    }




    private void dialog_confirmdelete_offer(String postid1) {

        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.bottomsheet_notification);
            alertDialog.setCancelable(true);
            Window window = alertDialog.getWindow();
            window.setGravity(Gravity.BOTTOM);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Would you like to delete this Promotions & Offers Notification?");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APICall_OfferNotifyDelete(postid1);

                    alertDialog.dismiss();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();


        }
    }

    private void APICall_OfferNotifyDelete(String postid1) {
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("post_id", postid1);
        notification_viewModel.notification_delete(header,request).observe(this, new Observer<Response_NotifyDelete>() {
            @Override
            public void onChanged(Response_NotifyDelete response_notifyDelete) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+response_notifyDelete.getMessage());
                if(response_notifyDelete.getStatus().equals("1"))
                {
                    Func_AppInfo();
                    Func_Notification();
                }
                else if((response_notifyDelete.getStatus().equals("0")))
                {
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Function1();
    }
}