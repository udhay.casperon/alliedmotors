package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.TechFeatureList_Pojo;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.util.ArrayList;

public class CarDetailAdapter_TechFeatures extends RecyclerView.Adapter<CarDetailAdapter_TechFeatures.ViewHolder> {

    ArrayList<TechFeatureList_Pojo> features;

    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;

    public CarDetailAdapter_TechFeatures(Context context, ArrayList<TechFeatureList_Pojo> features) {
        this.mcontext = context;
        this.features = features;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.tech_features_cv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tv_heading.setText(features.get(position).KeyTitle);
        holder.tv_values.setText(AppInstalled.toTitleCase(features.get(position).Value));
        holder.img_resource.setImageResource(features.get(position).ImgRsrc);



    }

    @Override
    public int getItemCount() {

                return features.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

       CustomTextViewSemiBold tv_heading,tv_values;
       ImageView img_resource;
        public ViewHolder(View itemView) {

            super(itemView);

            tv_heading=itemView.findViewById(R.id.tv_heading);
            tv_values=itemView.findViewById(R.id.tv_values);
            img_resource=itemView.findViewById(R.id.img_resource);
        }
    }
}

