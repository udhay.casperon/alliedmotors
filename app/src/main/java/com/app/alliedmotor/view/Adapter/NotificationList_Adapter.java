package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Response_NotificationList.NotificationListItem_folder;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.SwipeReveal.SwipeRevealLayout;
import com.app.alliedmotor.utility.widgets.CustomBoldTextview;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewBold;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.view.Activity.HomeActivity_searchimage_BottomButton;
import com.app.alliedmotor.view.Activity.MyProfile_Activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NotificationList_Adapter extends RecyclerView.Adapter<NotificationList_Adapter.ViewHolder> {

    ArrayList<NotificationListItem_folder> dataItems = new ArrayList<>();

    INotify listener;
    SharedPref sharedPref;
    private Context mcontext;
    String dateStr="";
    HomeActivity_searchimage_BottomButton obj;

    public NotificationList_Adapter(Context context, ArrayList<NotificationListItem_folder> dataItems, INotify listener) {
        this.mcontext = context;
        this.dataItems = dataItems;
        this.listener=listener;



    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_notification_new, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

   String ticketid=String.valueOf(dataItems.get(position).getTicketId());
        holder.tv_title.setText(dataItems.get(position).getTitleHtml());

        dateStr = dataItems.get(position).getCreatedTime();
        String resultDate = convertStringDateToAnotherStringDate(dateStr, "yyyy-MM-dd ", "dd MMM YYYY");

        holder.tv_date.setText(resultDate);
        holder.tv_descrption.setText(dataItems.get(position).getBodyHtml());

        if(ticketid.equals("null")|| ticketid.equals(null) || ticketid==null || ticketid.equals("")) {
            holder.tv_ticketno.setVisibility(View.INVISIBLE);
        }
        else {
            holder.tv_ticketno.setVisibility(View.VISIBLE);
            holder.tv_ticketno.setText("Ticket no:"+dataItems.get(position).getTicketId());
        }
        if(dataItems.get(position).getIsUnread().equals("0"))
        {
            holder.cv_cartindicator.setVisibility(View.GONE);
        }
        else if(dataItems.get(position).getIsUnread().equals("1"))
        {
            holder.cv_cartindicator.setVisibility(View.VISIBLE);
        }

  holder.rl_selected_data.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          listener.Notifiy_Selected(dataItems.get(position).getId());
          if(dataItems.get(position).getHref().equals("profile"))
          {
              Intent tocartpage = new Intent(mcontext, MyProfile_Activity.class);
              mcontext.startActivity(tocartpage);
          }
          else if(dataItems.get(position).getHref().equals("user-creation"))
          {
              Intent tocartpage = new Intent(mcontext, HomeActivity_searchimage_BottomButton.class);
              tocartpage.putExtra("tabposition", "useraccount");
              mcontext.startActivity(tocartpage);
          }
          else if(dataItems.get(position).getHref().equals("enquires")) {
              Intent tocartpage = new Intent(mcontext, HomeActivity_searchimage_BottomButton.class);
              tocartpage.putExtra("tabposition", "cart");
              tocartpage.putExtra("position_tabs", "2");
              tocartpage.putExtra("ticketid", ticketid);
              mcontext.startActivity(tocartpage);
          }
      }
  });

        holder.tv_deletenotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.General_NotifyDelete(dataItems.get(position).getId());
            }
        });


    }




    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public interface INotify {
        void Notifiy_Selected(String data);
        void General_NotifyDelete(String ticktid);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewBold tv_title;
        CustomRegularTextview tv_date;
        CustomTextViewBold tv_ticketno;
        CustomTextViewRegular tv_descrption;
        LinearLayout rl_selected_data;
        CardView cv_cartindicator;
        SwipeRevealLayout cardview_swipe;
        CustomRegularTextview tv_deletenotification;


        public ViewHolder(View itemView) {

            super(itemView);

            cardview_swipe=itemView.findViewById(R.id.cardview_swipe);
            tv_deletenotification=itemView.findViewById(R.id.tv_deletenotification);

            cv_cartindicator=itemView.findViewById(R.id.cv_cartindicator);

            tv_title=itemView.findViewById(R.id.tv_title);
            tv_date=itemView.findViewById(R.id.tv_date);
            tv_descrption=itemView.findViewById(R.id.tv_descrption);
            tv_ticketno=itemView.findViewById(R.id.tv_ticketno);

            rl_selected_data=itemView.findViewById(R.id.rl_selected_data);

        }
    }

    public String convertStringDateToAnotherStringDate(String dateStr, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(dateStr);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }


}

