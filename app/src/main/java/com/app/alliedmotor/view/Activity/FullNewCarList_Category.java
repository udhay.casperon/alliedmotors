package com.app.alliedmotor.view.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;


import com.app.alliedmotor.model.NewCar.CarDatasItem;
import com.app.alliedmotor.utility.widgets.CustomTextViewBold;
import com.app.alliedmotor.view.Adapter.NewCarList_Fullcar_Adapter;

import java.util.ArrayList;

public class FullNewCarList_Category extends AppCompatActivity {


    private Context context;
    String festive_title,festive_data;
    ArrayList<CarDatasItem> dataItems = new ArrayList<>();
    LinearLayout main_ll,loading_ll;
    ImageView img_home;
    RecyclerView rv_fulllist;
    CustomTextViewBold tv_carbrandTitle;
    LinearLayoutManager linearLayoutManager;
    NewCarList_Fullcar_Adapter newCarList_fullcar_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_car_list__category);

        context=this;
        main_ll=findViewById(R.id.main_ll);
        loading_ll=findViewById(R.id.loading_ll);
        rv_fulllist=findViewById(R.id.rv_fulllist);
        img_home=findViewById(R.id.img_home);
        tv_carbrandTitle=findViewById(R.id.tv_carbrandTitle);
        Intent fromIntent=getIntent();
        festive_title=fromIntent.getStringExtra("festive_title");
        dataItems=(ArrayList<CarDatasItem>)fromIntent.getSerializableExtra("festive_datalist");

        System.out.println("===arraylit--valu==>"+dataItems+"mshd==="+festive_title);
        tv_carbrandTitle.setText(festive_title);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context,2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
        rv_fulllist.setLayoutManager(gridLayoutManager);
        rv_fulllist.setHasFixedSize(true);
        loading_ll.setVisibility(View.GONE);
        main_ll.setVisibility(View.VISIBLE);

        for (int i=0;i<dataItems.size();i++)
        {
            if(dataItems.get(i).getMoId().equals("0"))
            {
                dataItems.remove(dataItems.get(i));
            }
            else
            {
                dataItems=dataItems;
            }
        }



        newCarList_fullcar_adapter = new NewCarList_Fullcar_Adapter(context, dataItems);
        rv_fulllist.setAdapter(newCarList_fullcar_adapter);

        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }
}