package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.HomePage.BrandsItem;
import com.app.alliedmotor.model.HomePage.CategoriesItem;
import com.app.alliedmotor.utility.SharedPref;

import com.app.alliedmotor.view.Activity.NewCar_Tablayout_Activity_static;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomePage_Brand_Adapter extends RecyclerView.Adapter<HomePage_Brand_Adapter.ViewHolder> {

    ArrayList<BrandsItem> brandsItems = new ArrayList<>();

    HomePage_Brand_Adapter.Categoires listener;
    SharedPref sharedPref;
    private Context mcontext;


    public HomePage_Brand_Adapter(Context context, ArrayList<BrandsItem> listdata) {
        this.mcontext = context;
        this.brandsItems = listdata;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.brand_rv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String imgurl = brandsItems.get(position).getIcon();
        System.out.println("==img url===" + imgurl);
        Picasso.get().load(imgurl).into(holder.imageView);

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(brandsItems.get(position).getCat_name().equals("LHD Cars"))
                {
                    Intent toIntent = new Intent(mcontext, NewCar_Tablayout_Activity_static.class);
                    toIntent.putExtra("tab_position", "0");
                    toIntent.putExtra("brand_selected", brandsItems.get(position).getMkid());
                    Log.e("-->", String.valueOf(position));
                    mcontext.startActivity(toIntent);
                }
                 else if(brandsItems.get(position).getCat_name().equals("Luxury LHD Cars")) {
                    Intent toIntent = new Intent(mcontext, NewCar_Tablayout_Activity_static.class);
                    toIntent.putExtra("tab_position", "1");
                    toIntent.putExtra("brand_selected", brandsItems.get(position).getMkid());
                    Log.e("-->", String.valueOf(position));
                    mcontext.startActivity(toIntent);
                }
                else if(brandsItems.get(position).getCat_name().equals("RHD Cars"))
                {
                    Intent toIntent = new Intent(mcontext, NewCar_Tablayout_Activity_static.class);
                    toIntent.putExtra("tab_position", "2");
                    toIntent.putExtra("brand_selected", brandsItems.get(position).getMkid());
                    Log.e("-->", String.valueOf(position));
                    mcontext.startActivity(toIntent);
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return brandsItems.size();
    }

    public interface Categoires {
        void onCategorySelected(CategoriesItem data, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ImageView imageView;
        CardView cardview;


        public ViewHolder(View itemView) {

            super(itemView);

            this.imageView =  itemView.findViewById(R.id.imageview1);
            this.cardview =  itemView.findViewById(R.id.cardview);

        }
    }
}

