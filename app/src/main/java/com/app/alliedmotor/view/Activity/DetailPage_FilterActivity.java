package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.CarDetail.FuelTypeListItem;
import com.app.alliedmotor.model.CarDetail.ModelOptionListItem;
import com.app.alliedmotor.model.CarDetail.VarientListItem;
import com.app.alliedmotor.model.FilterFuelTypeResponse;
import com.app.alliedmotor.model.Response_FilterModelOption;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewBold;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_FuelTypeList;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_ModelOption;
import com.app.alliedmotor.view.Adapter.CarDetailAdapter_VarientList;
import com.app.alliedmotor.viewmodel.Cardetail_FilterViewModel;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailPage_FilterActivity extends AppCompatActivity implements CarDetailAdapter_FuelTypeList.Listener_Fuelid,CarDetailAdapter_ModelOption.ModelOptionSelected,CarDetailAdapter_VarientList.VarientOption {



    static public int dialog_fuel=0,dialog_model=0,dialog_varient=0;


    ArrayList<FilterFuelTypeResponse.Data.ModelOptionListItem> modelOptionList1 = new ArrayList<>();
    ArrayList<Response_FilterModelOption.Data.VarientListItem> varientListItemArrayList1 = new ArrayList<>();
    ImageView img_home;
    EditText searchview;
    CustomTextViewBold tv_makename;
    Context context;
    SharedPref sharedPref;
    LinearLayout ll_varient, ll_modeloptions;

    RecyclerView rv_filter, rv_modeloptions, rv_varientoptions;

    Cardetail_FilterViewModel cardetail_filterViewModel;

    String res_makeid = "", res_modid = "";

    String st_searchtxtname="",st_makename="";
    ArrayList<FuelTypeListItem> fuelTypeArrayList = new ArrayList<>();
    ArrayList<ModelOptionListItem> modelOptionListItemArrayList = new ArrayList<>();
    ArrayList<VarientListItem> varientListItemArrayList = new ArrayList<>();


    CarDetailAdapter_VarientList adapter_varientList;
    CarDetailAdapter_FuelTypeList adapter_fuelTypeList;
    CarDetailAdapter_ModelOption adapter_modelOption;
    String name_fuel="",name_model="",name_varient="";
    String name_fuel1="",name_model1="",name_varient1="";
    String st_eftid="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_page__filter);
        cardetail_filterViewModel = ViewModelProviders.of(this).get(Cardetail_FilterViewModel.class);

        context=this;
        sharedPref=new SharedPref(context);
        findviews();
        if(getIntent()!=null)
        {
            st_searchtxtname=getIntent().getStringExtra("searchtextname");
            st_makename=getIntent().getStringExtra("makename");
            fuelTypeArrayList= (ArrayList<FuelTypeListItem>) getIntent().getSerializableExtra("fueltypearraylist");
            modelOptionListItemArrayList= (ArrayList<ModelOptionListItem>) getIntent().getSerializableExtra("modeloptionlist");
            varientListItemArrayList= (ArrayList<VarientListItem>) getIntent().getSerializableExtra("varientlist");
            res_makeid=getIntent().getStringExtra("response_makeid");
            res_modid=getIntent().getStringExtra("response_modid");
            name_fuel=getIntent().getStringExtra("namefuel");
            name_model=getIntent().getStringExtra("namemodel");
            name_varient=getIntent().getStringExtra("namevarient");
            st_eftid=getIntent().getStringExtra("eftid");
        }
        searchview.setText(st_searchtxtname);
        tv_makename.setText(st_makename);

            Func_Adapter();
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void Func_Adapter() {

        adapter_fuelTypeList=new CarDetailAdapter_FuelTypeList(context,fuelTypeArrayList,this::onfuelSelected,name_fuel);
        rv_filter.setAdapter(adapter_fuelTypeList);

        adapter_modelOption=new CarDetailAdapter_ModelOption(context,modelOptionListItemArrayList,this::onmodelOptionSelected,name_model);
        rv_modeloptions.setAdapter(adapter_modelOption);

        adapter_varientList=new CarDetailAdapter_VarientList(context,varientListItemArrayList,this::onVarientOption,name_varient);
        rv_varientoptions.setAdapter(adapter_varientList);
    }

    private void findviews() {
        
        ll_varient=findViewById(R.id.ll_varient);
        ll_modeloptions=findViewById(R.id.ll_modeloptions);
        searchview=findViewById(R.id.searchview1);
        img_home=findViewById(R.id.img_home1);
        ll_varient=findViewById(R.id.ll_varient);
        ll_modeloptions=findViewById(R.id.ll_modeloptions);

        tv_makename=findViewById(R.id.tv_makename);

        rv_filter=findViewById(R.id.rv_filter);
        rv_modeloptions=findViewById(R.id.rv_modeloptions);
        rv_varientoptions=findViewById(R.id.rv_varientoptions);

        LinearLayoutManager linearLayoutManager1=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rv_filter.setLayoutManager(linearLayoutManager1);
        rv_filter.setHasFixedSize(true);


        LinearLayoutManager linearLayoutManager2=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rv_modeloptions.setLayoutManager(linearLayoutManager2);
        rv_modeloptions.setHasFixedSize(true);


        LinearLayoutManager linearLayoutManager3=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rv_varientoptions.setLayoutManager(linearLayoutManager3);
        rv_varientoptions.setHasFixedSize(true);
    }

    @Override
    public void onfuelSelected(String data) {
        Commons.showProgress(context);
        st_eftid =data;
        HashMap<String, String> request_filter_fuel = new HashMap<>();
        request_filter_fuel.put("makeId", res_makeid);
        request_filter_fuel.put("modId", res_modid);
        request_filter_fuel.put("eftId", data);

        Log.e("-->cardetail-->", request_filter_fuel.toString());
        System.out.println("==req--car detil--" + request_filter_fuel);

        cardetail_filterViewModel.filterFuelTypedata(request_filter_fuel).observe(this, new Observer<FilterFuelTypeResponse>() {
            @Override
            public void onChanged(FilterFuelTypeResponse filterFuelTypeResponse) {

                Commons.hideProgress();
                System.out.println("===res=car details==" + filterFuelTypeResponse.toString());
                if (filterFuelTypeResponse.status.equals("1")) {
                    modelOptionListItemArrayList.clear();
                    modelOptionList1 = filterFuelTypeResponse.data.modelOptionList;
                    if (modelOptionList1.size() == 0) {
                        ll_modeloptions.setVisibility(View.GONE);
                        Commons.Alert_custom(context, "Model list not available, Please choose another Fuel Type");

                    } else {
                        for (int j = 0; j < modelOptionList1.size(); j++) {
                            ll_modeloptions.setVisibility(View.VISIBLE);
                            modelOptionListItemArrayList.add(new ModelOptionListItem(modelOptionList1.get(j).moId, modelOptionList1.get(j).modelOptions));
                        }
                        func_adpater1();
                    }
                }
            }
        });
    }

    private void func_adpater1() {

        adapter_modelOption = new CarDetailAdapter_ModelOption(context, modelOptionListItemArrayList, this::onmodelOptionSelected,name_model1);
        rv_modeloptions.setAdapter(adapter_modelOption);
        ll_varient.setVisibility(View.GONE);
    }

    @Override
    public void onmodelOptionSelected(String data,String name_model) {

        Commons.showProgress(context);
        HashMap<String, String> request_filter_fuel = new HashMap<>();
        request_filter_fuel.put("makeId", res_makeid);
        request_filter_fuel.put("modId", res_modid);
        request_filter_fuel.put("eftId", st_eftid);
        request_filter_fuel.put("moId", data);

        Log.e("-->cardetail-->", request_filter_fuel.toString());
        System.out.println("==req--car detil--" + request_filter_fuel );

        cardetail_filterViewModel.filtermodeloption(request_filter_fuel).observe(this, new Observer<Response_FilterModelOption>() {
            @Override
            public void onChanged(Response_FilterModelOption response_filterModelOption) {

                Commons.hideProgress();
                System.out.println("===res=car details==" + response_filterModelOption.toString());
                if (response_filterModelOption.status.equals("1")) {
                    varientListItemArrayList.clear();

                    varientListItemArrayList1 = response_filterModelOption.data.varientList;


                    if (varientListItemArrayList1.size() == 0) {
                        Commons.Alert_custom(context, "Varient list not available, Please choose another model options");
                    } else {
                        ll_varient.setVisibility(View.VISIBLE);
                        for (int j = 0; j < varientListItemArrayList1.size(); j++) {
                            varientListItemArrayList.add(new VarientListItem(varientListItemArrayList1.get(j).mainId, varientListItemArrayList1.get(j).varientCode));
                        }
                        Func_Adpater2();
                    }
                }
            }
        });



    }

    private void Func_Adpater2() {
        adapter_varientList=new CarDetailAdapter_VarientList(context,varientListItemArrayList,this::onVarientOption,name_varient1);
        rv_varientoptions.setAdapter(adapter_varientList);


    }

    @Override
    public void onVarientOption(String data) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //displayData();
                Intent  resultIntent=new Intent();
                resultIntent.putExtra("carid",data);
                setResult(RESULT_OK,resultIntent);
                finish();
            }
        }, 700);

    }
}