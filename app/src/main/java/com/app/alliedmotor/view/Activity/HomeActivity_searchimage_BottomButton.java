package com.app.alliedmotor.view.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.FCMMessagePojo;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.HomePage.CategoriesItem;
import com.app.alliedmotor.model.HomePage.DataItem;
import com.app.alliedmotor.model.HomePage.Response_Homepage;
import com.app.alliedmotor.model.Notification.Response_Notification;
import com.app.alliedmotor.model.Notification.Service_FCMNotification;
import com.app.alliedmotor.model.Pojo_PreOwnedFilter;
import com.app.alliedmotor.model.QuoteDataItemDB;
import com.app.alliedmotor.model.Response_SearchbyKeywords;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomBoldTextview;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.HomePage_Categories_Adapter;
import com.app.alliedmotor.view.Adapter.SearchListAdapter;
import com.app.alliedmotor.view.Fragment.Account_Fragment_Linear;
import com.app.alliedmotor.view.Fragment.Car_FullList_Fragment;
import com.app.alliedmotor.view.Fragment.Cart_Fragment;
import com.app.alliedmotor.view.Fragment.Categories_Fragment;
import com.app.alliedmotor.view.Fragment.Chat_Fragment;
import com.app.alliedmotor.view.Fragment.HomeFragment;
import com.app.alliedmotor.viewmodel.HomeActivityViewModel;
import com.app.cardview_shadowcolor.CardView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.textview.BoldCustomTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeActivity_searchimage_BottomButton extends AppCompatActivity implements View.OnClickListener
{

    private Context context;
    ImageView img_home,ic_notification,ic_menu;
    LinearLayout ll_customised_toolbar;
    BottomNavigationView navigation;
    EditText searchview;
    ImageView searhview_icon;
    String backstatename;
    Fragment fragment = null;
    SharedPref sharedPref;
    RecyclerView rv_notification;
    HomeActivityViewModel homeActivityViewModel;
    ImageView bn_home,bn_categories,bn_chat,bn_account,bn_cart;
    LinearLayout ll_myprofile,ll_cart,ll_chat,ll_categ,ll_home,ll_navigation;
    LinearLayout ll_myprofile_bg,ll_cart_bg,ll_chat_bg,ll_categ_bg,ll_home_bg,ll_navigation_bg;
    ImageView bn_home_bg,bn_categories_bg,bn_chat_bg,bn_account_bg,bn_cart_bg;
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;
    int Img_WIDTH=15;
    String st_searchStr="";
     Dialog alertDialog;
    LinearLayout ll_logout;
    CustomRegularTextview tv_bn_account,tv_bn_cart,tv_bn_chat,tv_bn_categories,tv_bn_home;
    MyDataBase myDataBase;
    RecyclerView rv_searchlist;
    SearchListAdapter searchListAdapter;
    ArrayList<Response_SearchbyKeywords.CarsItem>arrayList=new ArrayList<>();
    RelativeLayout rl_searchlist;
    RelativeLayout fragment_container;
    LinearLayout ll_searchbar;
static public HomeActivity_searchimage_BottomButton object;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    String st_resposne;
    String st_ticketid="";
    CustomRegularButton bt_signin;
    CustomTextViewSemiBold tv_accountname;
    CardView cv_search;
    CustomRegularTextview nav_home,nav_favourite,nav_cart,nav_orders,nav_chat,nav_logout,nav_account,nav_notification;
    androidx.cardview.widget.CardView cv_notificationcount;
    int position_tab=0;
    String logi_usermail="",login_password="";
    ArrayList<CategoriesItem> categoriesItems = new ArrayList<>();
    HomePage_Categories_Adapter homePageCategories_adapter;
   public static CustomBoldTextview tv_notification_count;
    String notify_count="";
    androidx.cardview.widget.CardView cv_notification_count,cv_nav_cartcount;
    CustomBoldTextview sidenav_cart_count;
    public static CustomBoldTextview sidenav_notification_count;
    ArrayList<QuoteDataItemDB> dataItemDBS=new ArrayList<>();
public  String fcmmessage_isread="";
    androidx.cardview.widget.CardView cv_searchview;


    private long pressedTime;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        homeActivityViewModel = ViewModelProviders.of(this).get(HomeActivityViewModel.class);
        context=this;
        sharedPref=new SharedPref(context);
        object=this;
        findbyviews();
        myDataBase=new MyDataBase(context);
        Method_Call();

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        String token = task.getResult();

                        System.out.println("------tokensssssss--------"+token);
                    }
                });

        LoginOrNot();
        Func_AppInfo();

        if(getIntent()!=null) {
            Intent fromIntnet = getIntent();
            st_resposne = fromIntnet.getStringExtra("tabposition");

            if(fromIntnet.getStringExtra("position_tabs")!=null) {
                position_tab = Integer.parseInt(fromIntnet.getStringExtra("position_tabs"));
                st_ticketid=fromIntnet.getStringExtra("ticketid");

            }
        }

        if(st_resposne==null)
      {
          bn_home_bg.setVisibility(View.VISIBLE);
          bn_home.setVisibility(View.GONE);
          loadFragment(new HomeFragment());
      }
      else
      {
          if(st_resposne.equals("cart")) {
              loadFragment(new Cart_Fragment(position_tab,st_ticketid));
              bn_cart_bg.setVisibility(View.VISIBLE);
              bn_cart.setVisibility(View.GONE);
          }
         else if(st_resposne.equals("categories")) {
              loadFragment(new HomeFragment());
              bn_home_bg.setVisibility(View.VISIBLE);
              bn_home.setVisibility(View.GONE);
          }
          else if(st_resposne.equals("home")) {
              loadFragment(new HomeFragment());
              bn_home_bg.setVisibility(View.VISIBLE);
              bn_home.setVisibility(View.GONE);
          }
          else if(st_resposne.equals("chat")) {
              loadFragment(new Chat_Fragment());
              bn_chat_bg.setVisibility(View.VISIBLE);
              ll_customised_toolbar.setVisibility(View.GONE);
              bn_chat.setVisibility(View.GONE);
          }
          else if(st_resposne.equalsIgnoreCase("useraccount"))
          {
              loadFragment(new Account_Fragment_Linear());
              bn_account_bg.setVisibility(View.VISIBLE);
              ll_customised_toolbar.setVisibility(View.GONE);
              bn_account.setVisibility(View.GONE);
          }



      }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Func_AppInfo();
        LoginOrNot();
    }

    public void LoginOrNot() {

        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN)=="")
        {
            bt_signin.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.GONE);
            ll_logout.setVisibility(View.GONE);
            cv_notification_count.setVisibility(View.GONE);
            cv_notificationcount.setVisibility(View.GONE);
            if(myDataBase!=null) {
                dataItemDBS = myDataBase.getAlldataforquote();
                if(dataItemDBS.size()==0)
                {
                    cv_nav_cartcount.setVisibility(View.GONE);
                }
                else {
                    cv_nav_cartcount.setVisibility(View.VISIBLE);
                    String cart_count=String.valueOf(dataItemDBS.size());
                    sharedPref.setString(Constants.PREF_Cartcount,cart_count);
                    sidenav_cart_count.setText(cart_count);

                }
            }
        }
        else
        {
            bt_signin.setVisibility(View.GONE);
            ll_logout.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.VISIBLE);
            tv_accountname.setText(sharedPref.getString(Constants.PREF_USERNAME));
            cv_notification_count.setVisibility(View.VISIBLE);
            cv_notificationcount.setVisibility(View.VISIBLE);
            notify_count=sharedPref.getString(Constants.PREF_UNREAD_NOTIFICATION);
            tv_notification_count.setText(notify_count);
            Method_UserProfile();

            sidenav_notification_count.setText(notify_count);
            getquote_func();
        }
    }

    private void getquote_func() {
        HashMap<String,String>header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        homeActivityViewModel.getquotedata(header).observe(this, new Observer<Response_GetQuote>() {
            @Override
            public void onChanged(Response_GetQuote response_getQuote) {
                if(response_getQuote.getStatus().equals("1")) {
                    String cart_count=String.valueOf(response_getQuote.getData().getQuoteData().size());
                    if (Integer.parseInt(cart_count)==0)
                    {
                        cv_nav_cartcount.setVisibility(View.GONE);
                    }
                    else {
                        cv_nav_cartcount.setVisibility(View.VISIBLE);
                        sharedPref.setString(Constants.PREF_Cartcount, cart_count);
                        sidenav_cart_count.setText(sharedPref.getString(Constants.PREF_Cartcount));
                    }
                }
            }

        });


    }

    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        homeActivityViewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                    if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
                        cv_notification_count.setVisibility(View.GONE);
                        cv_notificationcount.setVisibility(View.GONE);
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());
                    }
                    else {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                       String count = response_appInfo.getData().getUnreadNotificationCount();
                       if(count.equals("0")) {
                           cv_notification_count.setVisibility(View.GONE);
                           cv_notificationcount.setVisibility(View.GONE);

                       }
                       else
                       {
                           cv_notification_count.setVisibility(View.VISIBLE);
                           cv_notificationcount.setVisibility(View.VISIBLE);
                           tv_notification_count.setText(count);
                           sidenav_notification_count.setText(count);
                       }
                   }

                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }


    private void Method_UserProfile() {

        HashMap<String ,String> header=new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
        System.out.println("dhh---------"+sharedPref.getString(Constants.JWT_TOKEN));
        homeActivityViewModel.getuserprofiledata(header).observe(this, new Observer<Response_UserProfile>() {
            @Override
            public void onChanged(Response_UserProfile response_userProfile) {
                System.out.println("===res=user_profile==" + response_userProfile.toString());
               sharedPref.setString(Constants.tv_mobile_value,response_userProfile.getData().getSupport().getMobile());
               sharedPref.setString(Constants.tv_whatsapp_value,response_userProfile.getData().getSupport().getWhatsapp());
               sharedPref.setString(Constants.PREF_USERNAME,response_userProfile.getData().getUserDetails().getDisplayName());
               sharedPref.setString(Constants.PREF_USERMAIL,response_userProfile.getData().getUserDetails().getUserEmail());
                sharedPref.setString(Constants.PREF_SEEKBAR_STATUS,response_userProfile.getData().getUserDetails().getNotificationStatus());

            }
        });

    }

    private void clicklistener() {

        tv_accountname=findViewById(R.id.tv_accountname);

        nav_account.setOnClickListener(this);
        nav_notification.setOnClickListener(this);




        nav_home.setOnClickListener(this);
        nav_cart.setOnClickListener(this);
        nav_favourite.setOnClickListener(this);
        nav_orders.setOnClickListener(this);
        nav_chat.setOnClickListener(this);
        bt_signin.setOnClickListener(this);
        nav_logout.setOnClickListener(this);



        img_home.setOnClickListener(this);
        ic_menu.setOnClickListener(this);
        ic_notification.setOnClickListener(this);
        searhview_icon.setOnClickListener(this);
        searchview.setOnClickListener(this);

        ll_home.setOnClickListener(this);
        ll_categ.setOnClickListener(this);
        ll_chat.setOnClickListener(this);
        ll_cart.setOnClickListener(this);
        ll_myprofile.setOnClickListener(this);
        ll_searchbar.setOnClickListener(this);
        tv_accountname.setOnClickListener(this);
        cv_searchview.setOnClickListener(this);
        }

    private void findbyviews()
    {


        cv_searchview=findViewById(R.id.cv_searchview);
        sidenav_cart_count=findViewById(R.id.sidenav_cart_count);
        cv_nav_cartcount=findViewById(R.id.cv_nav_cartcount);


        cv_notificationcount=findViewById(R.id.cv_notificationcount);
        sidenav_notification_count=findViewById(R.id.sidenav_notification_count);
        cv_notification_count=findViewById(R.id.cv_notification_count);
        tv_notification_count=findViewById(R.id.tv_notification_count);
        ll_logout=findViewById(R.id.ll_logout);
        nav_logout=findViewById(R.id.nav_logout);
        nav_home=findViewById(R.id.nav_home);
        nav_cart=findViewById(R.id.nav_cart);
        nav_favourite=findViewById(R.id.nav_favourite);
        nav_orders=findViewById(R.id.nav_orders);
        nav_chat=findViewById(R.id.nav_chat);
        bt_signin=findViewById(R.id.bt_signin);
        tv_accountname=findViewById(R.id.tv_accountname);
        nav_account=findViewById(R.id.nav_account);
        nav_notification=findViewById(R.id.nav_notification);





        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }




        tv_bn_home=findViewById(R.id.tv_bn_home);
        tv_bn_categories=findViewById(R.id.tv_bn_categories);
        tv_bn_chat=findViewById(R.id.tv_bn_chat);
        tv_bn_cart=findViewById(R.id.tv_bn_cart);
        tv_bn_account=findViewById(R.id.tv_bn_account);

        ll_searchbar=findViewById(R.id.ll_searchbar);
        rv_searchlist=findViewById(R.id.rv_searchlist);
      //  rl_searchlist=findViewById(R.id.rl_searchlist);
        fragment_container=findViewById(R.id.fragment_container);

        ll_navigation=findViewById(R.id.ll_navigation);
        navigation = findViewById(R.id.navigation);
        ll_customised_toolbar=findViewById(R.id.ll_customised_toolbar);
        img_home=findViewById(R.id.img_home);
        ic_notification=findViewById(R.id.ic_notification);
        ic_menu=findViewById(R.id.ic_menu);
        searchview=findViewById(R.id.searchview);
        searhview_icon=findViewById(R.id.searhview_icon);

        ll_home=findViewById(R.id.ll_home);
        ll_categ=(LinearLayout) findViewById(R.id.ll_categories);
        ll_chat=findViewById(R.id.ll_chat);
        ll_cart=findViewById(R.id.ll_cart);
        ll_myprofile=findViewById(R.id.ll_myprofile);




        bn_home=findViewById(R.id.bn_home);
        bn_categories=findViewById(R.id.bn_categories);
        bn_chat=findViewById(R.id.bn_chat);
        bn_account=findViewById(R.id.bn_account);
        bn_cart=findViewById(R.id.bn_cart);

        bn_home_bg=findViewById(R.id.bn_home_bg);
        bn_categories_bg=findViewById(R.id.bn_categories_bg);
        bn_chat_bg=findViewById(R.id.bn_chat_bg);
        bn_account_bg=findViewById(R.id.bn_account_bg);
        bn_cart_bg=findViewById(R.id.bn_cart_bg);


        clicklistener();

    }


    private void setupDrawerContent(NavigationView navigationView) {

        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });

    }

    private boolean  loadFragment(Fragment fragment) {
        backstatename=fragment.getClass().getName();
        fragment_container.removeAllViews();

        //2 Method
        FragmentManager fragmentManager = getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate (backstatename, 0);

        if (!fragmentPopped){ //fragment not in back stack, create it.
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container,fragment);
            fragmentTransaction.addToBackStack(backstatename);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_NONE);
            fragmentTransaction.commit();

        }

//switching fragment
      /*  if (fragment != null) {
               getSupportFragmentManager().popBackStack();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, fragment)

                    .commit();

            return true;
        }
*/
        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        
    }




    @SuppressLint("RtlHardcoded")
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.img_home:
               if(!backstatename.equals("com.app.alliedmotor.view.Fragment.HomeFragment")){
                   loadFragment(new HomeFragment());
               }
                bn_home_bg.setVisibility(View.VISIBLE);
                bn_home.setVisibility(View.GONE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);
                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                break;
            case R.id.nav_account:
                loadFragment(new Account_Fragment_Linear());
                ll_customised_toolbar.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.dark_blue));
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case R.id.nav_notification:
                Intent tonotifify=new Intent(this,NotificationList_Activity.class);
                startActivity(tonotifify);
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case  R.id.nav_home:
                System.out.println("==Menu===");
                loadFragment(new HomeFragment());
                bn_home_bg.setVisibility(View.VISIBLE);
                bn_home.setVisibility(View.GONE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));

                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_favourite:
                loadFragment(new Cart_Fragment(0,st_ticketid));
                drawer.closeDrawer(Gravity.RIGHT);
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                bn_cart_bg.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);

                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_cart:
                loadFragment(new Cart_Fragment(1,st_ticketid));
                System.out.println("==Menu===");
                drawer.closeDrawer(Gravity.RIGHT);
                bn_cart_bg.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.GONE); ll_customised_toolbar.setVisibility(View.VISIBLE);
                bn_cart_bg.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);

                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_orders:
                loadFragment(new Cart_Fragment(2,st_ticketid));
                System.out.println("==Menu===");
                drawer.closeDrawer(Gravity.RIGHT);
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                bn_cart_bg.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);

                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case  R.id.nav_chat:
                loadFragment(new Chat_Fragment());
                System.out.println("==Menu===");
                drawer.closeDrawer(Gravity.RIGHT);
                bn_chat_bg.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);
                bn_categories_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                drawer.closeDrawer(Gravity.RIGHT);
                ll_customised_toolbar.setVisibility(View.GONE);

                break;

            case  R.id.nav_logout:
                System.out.println("==Menu===");
                Intent gotoLogin = new Intent(context, LoginActivity.class);
                System.out.println("===vc===" + sharedPref.getString(Constants.JWT_TOKEN));
                sharedPref.clearAll();
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoLogin);
                drawer.closeDrawer(Gravity.RIGHT);
               finish();
                break;

            case R.id.bt_signin:
                Intent tosignin=new Intent(this,LoginActivity.class);
                startActivity(tosignin);
                finish();
                case  R.id.ic_menu:
                System.out.println("==Menu===");
                drawer.openDrawer(GravityCompat.END);
                break;
            case  R.id.ic_notification:
                Intent tonotification=new Intent(this, NotificationList_Activity.class);
                startActivity(tonotification);
                break;
            case  R.id.ll_searchbar:
            case  R.id.searhview_icon:
            case  R.id.searchview:
            case R.id.cv_searchview:
                Intent toSearchpage2=new Intent(HomeActivity_searchimage_BottomButton.this,SearchedPageActivity.class);
                startActivity(toSearchpage2);
                break;
            case R.id.ll_home:
                if(!backstatename.equals("com.app.alliedmotor.view.Fragment.HomeFragment")){
                    loadFragment(new HomeFragment());
                }
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                bn_home_bg.setVisibility(View.VISIBLE);
                bn_home.setVisibility(View.GONE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                break;
            case R.id.ll_categories:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                bn_categories_bg.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);


                bn_home_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
               // dialog_addquote();
                fragmentTransaction(new Categories_Fragment());
                break;

            case R.id.ll_chat:
                ll_customised_toolbar.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.GONE);


                bn_home.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));

                fragmentTransaction(new Chat_Fragment());



                break;

            case R.id.ll_cart:
                ll_customised_toolbar.setVisibility(View.VISIBLE);
                bn_cart_bg.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.GONE);

                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.dark_blue));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));


                fragmentTransaction(new Cart_Fragment());
                break;
            case R.id.ll_myprofile:
                ll_customised_toolbar.setVisibility(View.GONE);
                bn_account_bg.setVisibility(View.VISIBLE);
                bn_account.setVisibility(View.GONE);

                bn_home.setVisibility(View.VISIBLE);
                bn_chat.setVisibility(View.VISIBLE);
                bn_cart.setVisibility(View.VISIBLE);
                bn_categories.setVisibility(View.VISIBLE);

                bn_categories_bg.setVisibility(View.GONE);
                bn_chat_bg.setVisibility(View.GONE);
                bn_cart_bg.setVisibility(View.GONE);
                bn_home_bg.setVisibility(View.GONE);
                tv_bn_home.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_categories.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_chat.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_cart.setTextColor(context.getResources().getColor(R.color.textcolor_catgeories));
                tv_bn_account.setTextColor(context.getResources().getColor(R.color.dark_blue));
                fragmentTransaction(new Account_Fragment_Linear());
                break;


        }
    }



    // fragment transaction
    private void fragmentTransaction(Fragment fragment){
       /* if (fragment != null) {
          //  fragment_container.removeAllViews();
            backstatename=fragment.getClass().getName();
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        }*/


        //2 method
        backstatename=fragment.getClass().getName();
         fragment_container.removeAllViews();

        FragmentManager fragmentManager = getSupportFragmentManager();
       // boolean fragmentPopped = fragmentManager.popBackStackImmediate (backstatename, 0);

       /* if (!fragmentPopped){*/ //fragment not in back stack, create it.
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container,fragment);
            fragmentTransaction.addToBackStack(backstatename);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_NONE);
            fragmentTransaction.commit();

       /* }*/
    }

    private void fragmentTransaction1(Fragment fragment){
       /* fragment_container.removeAllViews();
        if (fragment != null) {
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().
                    beginTransaction().
                    replace(R.id.fragment_container, fragment)
                    .commit();

        }*/


        //2method
        backstatename=fragment.getClass().getName();
         fragment_container.removeAllViews();

        FragmentManager fragmentManager = getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate (backstatename, 0);

        if (!fragmentPopped){ //fragment not in back stack, create it.
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container,fragment);
            fragmentTransaction.addToBackStack(backstatename);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_NONE); //newly added for avaoid reloading fragments -12-05-2021
            fragmentTransaction.commit();

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        Func_AppInfo();
        tv_notification_count.setText(notify_count);

        sidenav_notification_count.setText(notify_count);
        tv_accountname.setText(sharedPref.getString(Constants.PREF_USERNAME));
        LoginOrNot();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ArrayList<DataItem> data) {
        fragmentTransaction1(new Car_FullList_Fragment(data));
    }


    private void Method_Call() {

        homeActivityViewModel.getHomepagedata().observe(this, new Observer<Response_Homepage>() {
            @Override
            public void onChanged(Response_Homepage response_homepage) {
                System.out.println("===res===" + response_homepage.toString());
                categoriesItems = response_homepage.getData().getCategories();
            }

        });
    }


    private void dialog_notification(String message,String notifytype,String postid1,String ticketid1)

    {
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_notify_alert);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
        txttitle.setText("Allied Motors");
        txtInstructionDialog.setText(message);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
        CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
        btokay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notifytype.equals("general")) {
                    if (ticketid1 != null || ticketid1 != "") {
                        Intent tocart2 = new Intent(getApplicationContext(), HomeActivity_searchimage_BottomButton.class);
                        tocart2.putExtra("tabposition", "cart");
                        tocart2.putExtra("position_tabs", "2");
                        tocart2.putExtra("ticketid", ticketid1);
                        startActivity(tocart2);
                        fcmmessage_isread = "false";
                        alertDialog.dismiss();
                    }
                }
                else if(notifytype.equals("offer")){
                    OfferPage(postid1);
                    alertDialog.dismiss();
                }
            }
        });


        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fcmmessage_isread="false";
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing())
            alertDialog.show();
    }


    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FCMMessagePojo messagefirebse) {
        fcmmessage_isread=messagefirebse.getFcm_isread();
        Func_AppInfo();
        dialog_notification(messagefirebse.getFcmcontent(),messagefirebse.getType_notification(),messagefirebse.getPostid(),messagefirebse.getTicketid());
        }



    private void OfferPage(String post_ids)
    {
        HashMap<String,String>header=new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String,String>request=new HashMap<>();
        request.put("post_id",post_ids);
        new Service_FCMNotification(context, header, request, new ResponseInterface.FCMNotificationInterface() {
            @Override
            public void onFCMNotificationSuccess(Response_Notification response_notification) {
                if(response_notification.getStatus().equals("1"))
                {

                    Intent toofferpage=new Intent(getApplicationContext(), OfferPage_Activity.class);
                    toofferpage.putExtra("postid",post_ids);
                    toofferpage.putExtra("posttitle",response_notification.getData().getNotificationData().getPostTitle());
                    toofferpage.putExtra("imageurl",response_notification.getData().getNotificationData().getThumbnailId());
                    toofferpage.putExtra("buttontext",response_notification.getData().getNotificationData().getButtonText());
                    toofferpage.putExtra("buttonurl",response_notification.getData().getNotificationData().getButtonUrl());
                    toofferpage.putExtra("enablebutton",response_notification.getData().getNotificationData().getEnnableButton());
                    toofferpage.putExtra("postcontent",response_notification.getData().getNotificationData().getPostContent());
                    toofferpage.putExtra("shortdesc",response_notification.getData().getNotificationData().getShortDescription());
                    startActivity(toofferpage);
                }
                else {
                    System.out.println("===res"+response_notification.toString());
                }
            }
        });
    }

}