package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.GetQuote.QuoteDataItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GetQuote_Adapter extends RecyclerView.Adapter<GetQuote_Adapter.ViewHolder> {

    ArrayList<QuoteDataItem> dataItems = new ArrayList<>();
    RemoveQuote listener;

    SharedPref sharedPref;
    private Context mcontext;
    int qty;

    int count;


    public GetQuote_Adapter(Context context, ArrayList<QuoteDataItem> dataItems, RemoveQuote listener) {
        this.mcontext = context;
        this.dataItems = dataItems;
        this.listener = listener;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_add_to_quote_linear_new, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String imgurl = dataItems.get(position).getCarImage();
        String temp_Ecolor = dataItems.get(position).getExteriorColor();
        String temp_Icolor = dataItems.get(position).getInteriorColor();

        Picasso.get().load(imgurl).into(holder.ic_car);
        holder.tv_carname.setText((dataItems.get(position).getCarType()));
        holder.tv_cartitle.setText((dataItems.get(position).getCarTitle()));
        holder.tv_modelyear.setText(dataItems.get(position).getModelYear());
        holder.tv_varientcode.setText((dataItems.get(position).getVarientCode()));
        holder.tv_cartrans.setText((dataItems.get(position).getCarTransmission()));

        if (temp_Ecolor == null || temp_Ecolor.equals("")) {
            //holder.tv_tick1_bg.setVisibility(View.GONE);
            holder.ll_selection1.setVisibility(View.GONE);

        } else {
            Drawable unwrappedDrawable = holder.Ext_ll_inner.getBackground();
            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
            DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(dataItems.get(position).getExteriorColor())));
        }
        if (temp_Icolor == null || temp_Icolor.equals("")) {
            // holder.tv_tick2_bg.setVisibility(View.GONE);
            holder.ll_selection2.setVisibility(View.GONE);

        } else {
            Drawable unwrappedDrawable = holder.Int_ll_inner.getBackground();
            Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
            DrawableCompat.setTint(wrappedDrawable, (Color.parseColor(dataItems.get(position).getInteriorColor())));

        }


        holder.icon_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCarQuote(dataItems.get(position), position);
                notifyDataSetChanged();
            }
        });

        holder.tv_numbers.setText(dataItems.get(position).getQuantity());

        // qty=Integer.parseInt(holder.tv_numbers.getText().toString().trim());
        holder.tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String vals = holder.tv_numbers.getText().toString();
                count = Integer.parseInt(vals);

               // count = Integer.parseInt(dataItems.get(position).getQuantity());
                if (count == 1) {
                    holder.tv_numbers.setText("1");
                } else {
                    count -= 1;
                    holder.tv_numbers.setText("" + count);
                }

                String counts = String.valueOf(count);
                holder.tv_numbers.setText(counts);
                listener.updateQuote(dataItems.get(position), position, count);
//                notifyDataSetChanged();

            }
        });

        holder.tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String vals = holder.tv_numbers.getText().toString();
                if(count<99) {
                    count = Integer.parseInt(vals) + 1;
                    String counts = String.valueOf(count);
                    holder.tv_numbers.setText(counts);
                }
                listener.updateQuote(dataItems.get(position), position, count);
//                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public interface RemoveQuote {
        void onCarQuote(QuoteDataItem data, int position);

        void updateQuote(QuoteDataItem data, int position, int count);
    }

    public void removeItem(int position) {
        dataItems.remove(position);
    }

    public void SetValue(QuoteDataItem item, int position) {
        dataItems.set(position, item);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        ImageView ic_car, icon_delete;

        CustomTextViewSemiBold tv_carname;
        CustomTextViewSemiBold tv_cartitle;
        CustomTextViewRegular tv_modelyear, tv_varientcode, tv_cartrans;
        CustomRegularTextview tv_minus, tv_numbers, tv_plus;
      //  CardView tv_tick1_bg, tv_tick2_bg;
      LinearLayout Int_ll_inner,Ext_ll_inner;

        LinearLayout ll_selection1, ll_selection2;


        public ViewHolder(View itemView) {

            super(itemView);

            ll_selection1 = itemView.findViewById(R.id.ll_selection1);
            ll_selection2 = itemView.findViewById(R.id.ll_selection2);


            Int_ll_inner=itemView.findViewById(R.id.Int_ll_inner);
            Ext_ll_inner=itemView.findViewById(R.id.Ext_ll_inner);

            tv_carname = itemView.findViewById(R.id.tv_carname);
            ic_car = itemView.findViewById(R.id.ic_car);
            tv_cartitle = itemView.findViewById(R.id.tv_cartitle);
            tv_modelyear = itemView.findViewById(R.id.tv_modelyear);
            tv_varientcode = itemView.findViewById(R.id.tv_varientcode);
            tv_cartrans = itemView.findViewById(R.id.tv_cartrans);
            icon_delete = itemView.findViewById(R.id.icon_delete);


            tv_minus = itemView.findViewById(R.id.tv_minus);
            tv_numbers = itemView.findViewById(R.id.tv_numbers);
            tv_plus = itemView.findViewById(R.id.tv_plus);


        }
    }
}

