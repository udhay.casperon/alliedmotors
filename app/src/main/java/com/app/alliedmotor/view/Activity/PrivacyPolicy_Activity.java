package com.app.alliedmotor.view.Activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Privacy_Policy.PrivacyPolicy_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.viewmodel.PrivacyPolicyviewModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class PrivacyPolicy_Activity extends AppCompatActivity {


    ImageView back;
    WebView webview;
    Context context;
    PrivacyPolicyviewModel privacyPolicyviewModel;
    String content, font_size = "14", finalHtml;
    LinearLayout loading_ll;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        privacyPolicyviewModel = ViewModelProviders.of(this).get(PrivacyPolicyviewModel.class);
        context = this;
        back = findViewById(R.id.back);
        webview = findViewById(R.id.webview);
        loading_ll = findViewById(R.id.loading_ll);

        url = "https://alliedmotors.com/privacy/";


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
       // new MyAsynTask().execute();

        //  Commons.showProgress(context);
      /*  webview.requestFocus();
        webview.getSettings().

                setJavaScriptEnabled(true);

        WebSettings webSettings = webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webview.loadUrl("https://alliedmotors.com/privacy/");
        webview.setWebViewClient(new

                                         WebViewClient() {
                                             @Override
                                             public boolean shouldOverrideUrlLoading (WebView view, String url){
                                                 view.loadUrl(url);
                                                 Commons.hideProgress();
                                                 return true;
                                             }

                                             @Override
                                             public void onPageStarted (WebView view, String url, Bitmap favicon){
                                                 super.onPageStarted(view, url, favicon);
                                             }

                                             @Override
                                             public void onPageFinished (WebView view, String url){
                                                 super.onPageFinished(view, url);

                                                 webview.loadUrl("javascript:(function() { " +
                                                         "var head = document.getElementById('top_header inner_page"
                                                         + "head.parentNode.removeChild(head);" +
                                                         "})()");
                                             }




                                         });
        Commons.hideProgress();

        webview.setWebChromeClient(new WebChromeClient() {

        });*/


       loading_ll.setVisibility(View.VISIBLE);
        privacyPolicyviewModel.getprivacypolicydata().observe(this, new Observer<PrivacyPolicy_Response>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onChanged(PrivacyPolicy_Response privacyPolicy_response) {

                loading_ll.setVisibility(View.GONE);
                webview.setVisibility(View.VISIBLE);
                //Commons.hideProgress();
                System.out.println("===res=pp==" + privacyPolicy_response.toString());
                for (int j = 0; j < privacyPolicy_response.getData().size(); j++) {
                    content = privacyPolicy_response.getData().get(0).getPost_content();
                    font_size = "12";
                    finalHtml = "<html><head>"
                            + "<style type=\"text/css\">li{color: #fff} span {color: #000}"
                            + "</style></head>"
                            + "<body style='font-size:" + font_size + "px" + "'>" + "<font color='black'>"
                            + content + "</font>"
                            + "</body></html>";
                    webview.loadDataWithBaseURL(null, finalHtml, "text/html", "UTF-8", null);
                }
            }
        });

    }

  /*  private class MyAsynTask extends AsyncTask<Void, Void, Document> {
        @Override
        protected Document doInBackground(Void... voids) {

            Document document = null;
            try {
                document = Jsoup.connect(url).get();
                document.getElementsByClass("top_header inner_page").remove();
                document.getElementsByClass("footer_wrapper").remove();
                document.getElementsByClass("scroll_top").remove();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return document;
        }

        @Override
        protected void onPostExecute(Document document) {
            super.onPostExecute(document);
            webview.loadDataWithBaseURL(url, document.toString(), "text/html", "utf-8", "");
            webview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

            webview.loadUrl(url);
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    view.loadUrl(url);
                    return super.shouldOverrideUrlLoading(view, request);
                }
            });
                    }

}*/



}





