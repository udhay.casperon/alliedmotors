package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.HomePage.DataItem;
import com.app.alliedmotor.view.Adapter.FestivalList_Fullcar_Adapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class Car_FullList_Fragment extends Fragment {


    ImageView back_setting;
    CardView cv_generalchat;
    RecyclerView rv_fulllist;
    View root;
    private Context context;
    LinearLayout loading_ll, main_ll;
    ArrayList<DataItem> data = new ArrayList<>();
    FestivalList_Fullcar_Adapter festivalList_car_adapter;

    public Car_FullList_Fragment(ArrayList<DataItem> data) {
        this.data = data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_full_car_list, null);
            context = getActivity();
            findbyview();


            festivalList_car_adapter = new FestivalList_Fullcar_Adapter(context, data);
            rv_fulllist.setAdapter(festivalList_car_adapter);


        }
        return root;
    }

    private void findbyview() {
        loading_ll = root.findViewById(R.id.loading_ll);
        main_ll = root.findViewById(R.id.main_ll);
        rv_fulllist = root.findViewById(R.id.rv_fulllist);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
        rv_fulllist.setLayoutManager(gridLayoutManager);
        rv_fulllist.setHasFixedSize(true);
        loading_ll.setVisibility(View.GONE);
        main_ll.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(getActivity()))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(getActivity()))
            EventBus.getDefault().unregister(this);
    }

}
