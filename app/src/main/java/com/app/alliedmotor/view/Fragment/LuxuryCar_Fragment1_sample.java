package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Login12.Service_LuxurycarList;
import com.app.alliedmotor.model.LuxuryCar.CarListItem;
import com.app.alliedmotor.model.LuxuryCar.MakeListItem;
import com.app.alliedmotor.model.LuxuryCar.Response_LuxuryCar;
import com.app.alliedmotor.model.NewCar.NewCar_Response;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.LuxuryCar_CarList_Adapter;
import com.app.alliedmotor.view.Adapter.LuxuryCar_CarList_Adapter_homeBrand;
import com.app.alliedmotor.view.Adapter.LuxuryCar_MakerList_Adapter;
import com.app.alliedmotor.view.Adapter.NewCar_CarList_Adapter;
import com.app.alliedmotor.view.Adapter.NewCar_CarList_Adapter_homeBrand;
import com.app.alliedmotor.view.Adapter.NewCar_MakerList_Adapter;
import com.app.alliedmotor.viewmodel.LuxurycarviewModel;
import com.app.alliedmotor.viewmodel.NewCarviewModel;

import java.util.ArrayList;
import java.util.HashMap;

public class LuxuryCar_Fragment1_sample extends Fragment implements LuxuryCar_MakerList_Adapter.ILuxuryCarMakelist  {



    RecyclerView rv_carlist,rv_car_makerlist;
    LinearLayoutManager linearLayoutManager,linearLayoutManager1;
    View root;
    LuxurycarviewModel luxurycarviewModel;

    private Context context;
    LinearLayout loading_ll,main_ll;
    ImageView ic_vehicletype;
    CustomTextViewSemiBold tv_cartype;
    ArrayList<String> vehicle_typelist=new ArrayList<>();
    String slugname="luxury-car-details";
    String makeid="";
    String vehicle_type="";
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;
    CustomTextViewRegular tv_emptydata;
    
    ArrayList<MakeListItem>makelist_luxury=new ArrayList<>();
    ArrayList<CarListItem>carlist_luxury=new ArrayList<>();
    LuxuryCar_MakerList_Adapter makerList_adapter;
    LuxuryCar_CarList_Adapter luxuryCarCarListAdapter;

    LuxuryCar_CarList_Adapter_homeBrand luxuryCar_carList_adapter_homeBrand;

    public LuxuryCar_Fragment1_sample(String makeid) {
        this.makeid = makeid;
    }

    public LuxuryCar_Fragment1_sample() {
    }
    private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                makeid="";
                Func_FilterOption();
               // Method_LuxuryCar();
                Func_Selection();
                _hasLoadedOnce = true;
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(root==null) {

            root = inflater.inflate(R.layout.fragment_newcar, null);
            luxurycarviewModel = ViewModelProviders.of(this).get(LuxurycarviewModel.class);
            context=getActivity();
            rv_car_makerlist = root.findViewById(R.id.rv_car_makerlist);
            rv_carlist = root.findViewById(R.id.rv_carlist);
        //    loading_ll = root.findViewById(R.id.loading_ll);
            main_ll = root.findViewById(R.id.main_ll);

            tv_emptydata=root.findViewById(R.id.tv_emptydata);
            ic_vehicletype = root.findViewById(R.id.ic_vehicletype);
            tv_cartype = root.findViewById(R.id.tv_cartype);
            // Car List
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rv_carlist.setLayoutManager(linearLayoutManager);

            //makerlist (brand)
            linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rv_car_makerlist.setLayoutManager(linearLayoutManager1);


            if(makeid==null) {
               // Method_LuxuryCar();
                makeid="";
                tv_cartype.setText("All");
                Func_Selection();
            }
            else {
                Func_Selection();
            }

            Func_FilterOption();
        }

        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                Func_FilterOption();
                vehicle_type=tv_cartype.getText().toString().trim();
                if(makeid==null ||makeid=="" || makeid.equals(""))
                {
                  //  Method_LuxuryCar();
                    Func_Selection();
                }
                else {
                    Func_Selection();
                }
            }
        };

        return root;
    }

    private void Func_FilterOption() {

        ic_vehicletype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);

                //  popup.getMenuInflater().inflate(R.menu.popup_options, popup.getMenu());
                for(int i=0;i<vehicle_typelist.size();i++) {
                    popup.getMenu().add(vehicle_typelist.get(i));
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        tv_cartype.setText(item.getTitle());
                        vehicle_type=tv_cartype.getText().toString().trim();
                        Func_Selection();
                        return true;
                    }
                });
                popup.show();//showing popup menu
            }
        });
    }

    private void Func_Selection() {
       // Commons.showProgress(context);

        carlist_luxury.clear();
        if(makeid==null)
        {
            makeid="";
        }
        HashMap<String, String> request = new HashMap<>();
        request.put("make_id",makeid);
        request.put("vehicle_type",vehicle_type);

        String url= Constants.BASE_URL+"v1/"+slugname+"/";

        new Service_LuxurycarList(url, request, new ResponseInterface.LuxuryCarListInterface() {
            @Override
            public void onLuxuryCarListSuccess(Response_LuxuryCar response_luxuryCar) {
                Commons.hideProgress();
                System.out.println("---response-selected brand--"+response_luxuryCar.toString());
                if(response_luxuryCar.getStatus().equals("1")) {
                    carlist_luxury= response_luxuryCar.getData().getCarList();
                    makelist_luxury = response_luxuryCar.getData().getMakeList();
                    vehicle_typelist = response_luxuryCar.getData().getVehicleTypeList();
                    if(response_luxuryCar.getData().getCarList().size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",response_luxuryCar.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        if( !makeid.equals("")){
                            Func_Adapter1();}
                        else {
                            Func_Adapter();
                        }
                    }
                }
                else if(response_luxuryCar.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + response_luxuryCar.toString());
                    Log.i("res_newcar0-->",response_luxuryCar.toString());
                    Toast.makeText(context,response_luxuryCar.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }
        });


       /* luxurycarviewModel.getselectedMakeList(url,request).observe(getActivity(), new Observer<Response_LuxuryCar>() {
            @Override
            public void onChanged(Response_LuxuryCar newCarResponse) {

                Commons.hideProgress();
                System.out.println("---response-selected brand--"+newCarResponse.toString());
                if(newCarResponse.getStatus().equals("1")) {
                     carlist_luxury= newCarResponse.getData().getCarList();
                    makelist_luxury = newCarResponse.getData().getMakeList();
                    vehicle_typelist = newCarResponse.getData().getVehicleTypeList();
                    if(newCarResponse.getData().getCarList().size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",newCarResponse.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        Func_Adapter();
                    }
                }
                else if(newCarResponse.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + newCarResponse.toString());
                    Log.i("res_newcar0-->",newCarResponse.toString());
                    Toast.makeText(context,newCarResponse.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }
        });*/


    }

    private void Func_Adapter1() {

        makerList_adapter = new LuxuryCar_MakerList_Adapter(context, makelist_luxury,this,makeid);
        rv_car_makerlist.setAdapter(makerList_adapter);

        luxuryCar_carList_adapter_homeBrand = new LuxuryCar_CarList_Adapter_homeBrand(context, carlist_luxury);
        rv_carlist.setAdapter(luxuryCar_carList_adapter_homeBrand);


        rv_car_makerlist.scrollToPosition(getPosition(makeid));
    }

    private int getPosition(String makeid)
    {
        for(int i=0; i < makelist_luxury.size(); i++){
            if(makelist_luxury.get(i).getMkid().equals(makeid)){
                return i;
            }
        }
        return 0;
    }

    private void Method_LuxuryCar() {

      //  loading_ll.setVisibility(View.VISIBLE);
        luxurycarviewModel.getluxuryCardata().observe(getActivity(), new Observer<Response_LuxuryCar>() {
            @Override
            public void onChanged(Response_LuxuryCar newCar_response) {

               // loading_ll.setVisibility(View.GONE);
                main_ll.setVisibility(View.VISIBLE);
                Commons.hideProgress();
                if(newCar_response.getStatus().equals("1")) {
                    System.out.println("===res=new car=1=" + newCar_response.toString());
                    carlist_luxury = newCar_response.getData().getCarList();
                    makelist_luxury = newCar_response.getData().getMakeList();
                    vehicle_typelist = newCar_response.getData().getVehicleTypeList();

                    if(newCar_response.getData().getCarList().size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",newCar_response.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        Func_Adapter();
                    }

                }
                else if(newCar_response.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + newCar_response.toString());
                    Log.i("res_newcar0-->",newCar_response.toString());
                }
            }
        });

    }

    private void Func_Adapter() {

        makerList_adapter = new LuxuryCar_MakerList_Adapter(context, makelist_luxury,this,makeid);
        rv_car_makerlist.setAdapter(makerList_adapter);
        luxuryCarCarListAdapter = new LuxuryCar_CarList_Adapter(context, carlist_luxury);
        rv_carlist.setAdapter(luxuryCarCarListAdapter);
        rv_car_makerlist.scrollToPosition(getPosition(makeid));
        
    }


    @Override
    public void BrandSelected(MakeListItem data, int position) {
        makeid=data.getMkid();
        tv_cartype.setText("All");

        autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
        autoCompleteHandler.postDelayed(autoCompleteRunnable, 500);

    }

    @Override
    public void onResume() {
        super.onResume();
            }



}
