package com.app.alliedmotor.view.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.GetQuote.QuoteDataItem;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.OrderCreation.OrderCreation_Response;
import com.app.alliedmotor.model.QuoteDataItemDB;
import com.app.alliedmotor.model.Response_DeleteQuote;
import com.app.alliedmotor.model.Response_GuestMakeOrder;
import com.app.alliedmotor.model.Response_UpdateQuote;
import com.app.alliedmotor.model.sample_carlistPojo_Guest;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.CountryDialCode;
import com.app.alliedmotor.utility.GPSTracker;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.HomeActivity_searchimage_BottomButton;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Adapter.GetQuote_Adapter;
import com.app.alliedmotor.view.Adapter.GetQuote_Adapter_NonLogin;
import com.app.alliedmotor.viewmodel.GetQuote_viewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.android.material.tabs.TabLayout;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;


public class Fragment_AddtoQuote extends Fragment implements GetQuote_Adapter.RemoveQuote,GetQuote_Adapter_NonLogin.RemoveQuote{

    RecyclerView rv_addquote;
    LinearLayoutManager linearLayoutManager;
    CustomRegularButton bt_submit_remarks,bt_clearall;
    CustomRegularEditText et_firstname,et_company,et_mobile,et_email,et_remarks;
    GetQuote_viewModel getQuote_viewModel;
    SharedPref sharedPref;
    ArrayList<QuoteDataItem>itemArrayList=new ArrayList<>();
    GetQuote_Adapter getQuote_adapter;
    GetQuote_Adapter_NonLogin getQuote_adapter_nonLogin;
    Context context;
    LinearLayout loading_ll,ll_empty;
    RelativeLayout main_ll;
    View view;
    String s_fname,s_company,s_mobile,s_email,s_remarks;

    LinearLayout ll_loginfeature_text,ll_loginfeature,content_ll;
    ArrayList<QuoteDataItemDB> dataItemDBS=new ArrayList<>();
    String login_remarks="";
    CardView non_login;
    CustomRegularEditText et_remarks_loginuser;
    LinearLayout ll_nonloginuser;
    MyDataBase myDataBase;
    public ArrayList<sample_carlistPojo_Guest> sample_carlistPojo_guests1=new ArrayList<>();

    private LinearLayout myCountryCodeLAY;
    private ImageView myFlagIMG;
    private TextView myCountryCodeTXT;
    private CountryPicker myPicker;
    private String myFlagStr = "",st_full_mobile;
    private GPSTracker myGPSTracker;
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;

    Runnable autoCompleteRunnable1;
    Handler autoCompleteHandler1;
    String nonlogin_carid="";
    String cart_count="";
LinearLayout ll_button;
    ArrayList<String>carid_delete=new ArrayList<>();

    static Fragment_AddtoQuote fragment_addtoQuote;
    private boolean _hasLoadedOnce= false;

    public static Fragment_AddtoQuote getInstance() {
        if (fragment_addtoQuote == null)
            fragment_addtoQuote = new Fragment_AddtoQuote();
        return fragment_addtoQuote;
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);


        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Func_sample();
                _hasLoadedOnce = true;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view==null) {
            view = inflater.inflate(R.layout.fragment_add_to_quote_relative, container, false);
            getQuote_viewModel = ViewModelProviders.of(this).get(GetQuote_viewModel.class);
            context=getActivity();
            sharedPref = new SharedPref(context);
            myDataBase=new MyDataBase(context);
          //  bt_submit = view.findViewById(R.id.bt_submit);
            bt_submit_remarks = view.findViewById(R.id.bt_submit_remarks);
            bt_clearall = view.findViewById(R.id.bt_clearall);
            et_firstname = view.findViewById(R.id.et_firstname);
            et_company = view.findViewById(R.id.et_company);
            et_mobile = view.findViewById(R.id.et_mobile);
            et_email = view.findViewById(R.id.et_email);
            et_remarks = view.findViewById(R.id.et_remarks);
            rv_addquote = view.findViewById(R.id.rv_addquote);
            loading_ll=view.findViewById(R.id.loading_ll);
            main_ll=view.findViewById(R.id.main_ll);
            ll_empty=view.findViewById(R.id.ll_empty);
            ll_nonloginuser=view.findViewById(R.id.ll_nonloginuser);
            ll_loginfeature_text=view.findViewById(R.id.ll_loginfeature_text);
            ll_loginfeature=view.findViewById(R.id.ll_loginfeature);
            non_login=view.findViewById(R.id.non_login);
            et_remarks_loginuser=view.findViewById(R.id.et_remarks_loginuser);
            content_ll=view.findViewById(R.id.content_ll);
            ll_button=view.findViewById(R.id.ll_button);
            myGPSTracker = new GPSTracker(getActivity());
            myCountryCodeLAY = view.findViewById(R.id.activity_signup_mobile_no_LAY1);
            myFlagIMG =  view.findViewById(R.id.activity_signup_country_flag_IMG1);
            myCountryCodeTXT =  view.findViewById(R.id.activity_signup_country_code_TXT1);


            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rv_addquote.setLayoutManager(linearLayoutManager);
            rv_addquote.setHasFixedSize(true);


            ll_loginfeature_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tologin=new Intent(context, LoginActivity.class);
                    startActivity(tologin);
                    getActivity().finish();
                }
            });
            Func_sample();

            //Func1();
            bt_submit_remarks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                    autoCompleteHandler.postDelayed(autoCompleteRunnable, 1000);


                }
            });

            autoCompleteHandler = new Handler();
            autoCompleteRunnable = new Runnable() {
                public void run() {

                    if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
                    {
                        try {
                            Func_NonLoginSubitQuote();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Func_submitQuote();
                    }
                }
            };

            autoCompleteHandler1 = new Handler();
            autoCompleteRunnable1 = new Runnable() {
                public void run() {

                    if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
                    {
                        myDataBase.deletefRows();
                        getQuote_adapter_nonLogin.notifyDataSetChanged();
                        Func_sample();
                    }
                    else {
                        func_DeleteQuote();
                    }
                }
            };

            bt_clearall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    autoCompleteHandler1.removeCallbacks(autoCompleteRunnable1);
                    autoCompleteHandler1.postDelayed(autoCompleteRunnable1, 1000);


                }
            });

            myCountryCodeLAY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myPicker.show(getChildFragmentManager(), "COUNTRY_PICKER");
                }
            });
            classAndWidgetInitialize();
            TelephonyManager tm = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            String cod=tm.getNetworkCountryIso();
            System.out.println("==countrycode==="+countryCodeValue+"88==>"+cod);
            Log.e("coutrycod==>",countryCodeValue);
            Log.e("cpod==>",cod);

        }
        return view;
    }

    private void Func_sample() {

        if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {


            if(myDataBase!=null) {
                dataItemDBS = myDataBase.getAlldataforquote();
                if(dataItemDBS.size()==0)
                {
                    content_ll.setVisibility(View.GONE);
                    ll_empty.setVisibility(View.VISIBLE);
                    non_login.setVisibility(View.GONE);
                    ll_button.setVisibility(View.GONE);
                    et_remarks_loginuser.setVisibility(View.GONE);
                }
                else {
                    cart_count=String.valueOf(dataItemDBS.size());
                    sharedPref.setString(Constants.PREF_Cartcount,cart_count);
                    Func_NonLoginAdapter();
                    ll_empty.setVisibility(View.GONE);
                    content_ll.setVisibility(View.VISIBLE);
                    loading_ll.setVisibility(View.GONE);
                    ll_loginfeature.setVisibility(View.GONE);
                    non_login.setVisibility(View.VISIBLE);
                    ll_button.setVisibility(View.VISIBLE);
                    et_remarks_loginuser.setVisibility(View.GONE);
                }
            }
        }
        else
        {

            content_ll.setVisibility(View.VISIBLE);
            ApiCall_Addtoquote();
            non_login.setVisibility(View.GONE);
        }

    }

    private void func_DeleteQuote() {
        HashMap<String, String> header = new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String>request_delete_allquote=new HashMap<>();
        request_delete_allquote.put("car_id", "All");
        Commons.showProgress(context);
        getQuote_viewModel.deleteQuoteMutableLiveData(header,request_delete_allquote).observe(getActivity(), new Observer<Response_DeleteQuote>() {
            @Override
            public void onChanged(Response_DeleteQuote response_deleteQuote) {
                Commons.hideProgress();
                loading_ll.setVisibility(View.GONE);
                if (response_deleteQuote.status.equals("1")) {
                    System.out.println("===res=addquote=" + response_deleteQuote.toString());
                    Log.i("fav_frag-->", response_deleteQuote.toString());
                    if(getQuote_adapter!=null)
                    {
                        getQuote_adapter.notifyDataSetChanged();
                    }
                    getQuote_adapter.notifyDataSetChanged();
                    ApiCall_Addtoquote();


                } else if (response_deleteQuote.status.equals("0")) {
                    Log.i("-->res addquote==", response_deleteQuote.toString());
                    // Toast.makeText(context, response_addquote.message, Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    private void classAndWidgetInitialize() {

        myPicker = CountryPicker.newInstance("Select Country");
        myPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flag) {
                myPicker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                myFlagIMG.setVisibility(View.VISIBLE);
                myFlagStr = "" + flag;
                myFlagIMG.setImageResource(flag);
                String  exampleNumber= String.valueOf(PhoneNumberUtil.getInstance().getExampleNumber(code).getNationalNumber());
                et_mobile.setFilters( new InputFilter[] {new InputFilter.LengthFilter(exampleNumber.length())});  //mypicker.setlistener

            }
        });
        setCountryDataValues();

    }



    private void setCountryDataValues() {
        if (myGPSTracker.canGetLocation() && myGPSTracker.isgpsenabled()) {
            double MyCurrent_lat = myGPSTracker.getLatitude();
            double MyCurrent_long = myGPSTracker.getLongitude();
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String aCountryCodeStr = addresses.get(0).getCountryCode();
                    if (aCountryCodeStr.length() > 0 && !aCountryCodeStr.equals(null) && !aCountryCodeStr.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(aCountryCodeStr);
                        myCountryCodeTXT.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + aCountryCodeStr.toLowerCase(Locale.ENGLISH);
                        myFlagIMG.setImageResource(getResId(drawableName));
                        myFlagStr = "" + getResId(drawableName);
                        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(Str_countyCode));
                        String  exampleNumber= String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                        int phoneLength=exampleNumber.length();
                        et_mobile.setFilters( new InputFilter[] {
                                new InputFilter.LengthFilter(phoneLength)});
                        et_mobile.setEms(phoneLength);
                        System.out.println("==d==="+et_mobile.getText().toString().trim());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getResId(String drawableName) {
        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
        }
        return -1;
    }


    private void Func_NonLoginSubitQuote() throws JSONException {
        s_fname = et_firstname.getText().toString().trim();
        s_company = et_company.getText().toString().trim();
        s_email = et_email.getText().toString().trim();
        s_mobile = et_mobile.getText().toString().trim();
        s_remarks = et_remarks.getText().toString().trim();
        st_full_mobile=myCountryCodeTXT.getText().toString().trim()+""+et_mobile.getText().toString().trim();

        if (s_fname.isEmpty() && s_company.isEmpty() && s_email.isEmpty() && s_mobile.isEmpty() && s_remarks.isEmpty()) {
            Commons.Alert_custom(context,"Please Enter All Details");
        } else if (s_fname.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter name");
            et_firstname.requestFocus();
        } else if (s_company.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter Company name");
            et_company.requestFocus();
        } else if (s_mobile.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter mobile number");
            et_mobile.requestFocus();
        } else if (s_email.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter Emailid");
            et_email.requestFocus();
        } else if (!(validEmail(s_email))) {
            Commons.Alert_custom(context, "Please Enter Valid Emailid");
            et_email.requestFocus();
        } else if (s_remarks.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter remarks");
            et_remarks.requestFocus();
        }
        else
        {
            Api_guestMakeOrder();
        }
    }

    private void Api_guestMakeOrder() throws JSONException {

        String m_androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        HashMap<String, String> request = new HashMap<>();
        request.put("company_name", s_company);
        request.put("mobile_number", st_full_mobile);
        request.put("full_name", s_fname);
        request.put("remarks", s_remarks);
        request.put("user_email", s_email);
        request.put("device_id", m_androidId);
        request.put("device_type", "Android");


        JSONArray array=new JSONArray();
        JSONObject postData = new JSONObject();
        try {
            for (int j = 0; j < myDataBase.getAlldataforquote().size(); j++) {
                postData.put("car_cat", myDataBase.getAlldataforquote().get(j).getCarCat());
                postData.put("car_id", myDataBase.getAlldataforquote().get(j).getCarId());
                postData.put("car_info", myDataBase.getAlldataforquote().get(j).getCarInfo()+"s");
                postData.put("car_url", myDataBase.getAlldataforquote().get(j).getCarUrl());
                postData.put("exterior_color", myDataBase.getAlldataforquote().get(j).getExteriorColor());
                postData.put("interior_color", myDataBase.getAlldataforquote().get(j).getInteriorColor());
                postData.put("mkid", myDataBase.getAlldataforquote().get(j).getMakeid());
                postData.put("modid", myDataBase.getAlldataforquote().get(j).getModid());
                postData.put("quantity", myDataBase.getAlldataforquote().get(j).getQuantity());
            }
        }
           catch (JSONException e) {
            e.printStackTrace();
        }
        array.put(postData);
        request.put("car_lists",array.toString());


        getQuote_viewModel.guestmakeorder(request).observe(getActivity(), new Observer<Response_GuestMakeOrder>() {
            @Override
            public void onChanged(Response_GuestMakeOrder response_guestMakeOrder) {
                System.out.println("--guest make order--" + response_guestMakeOrder.message);
                if (response_guestMakeOrder.status.equals("1")) {
                   // Toast.makeText(context, response_guestMakeOrder.longMessage, Toast.LENGTH_SHORT).show();
                 //   Commons.Alert_custom_Title(context,"Allied Motors",response_guestMakeOrder.longMessage);
                  //  sharedPref.setString(Constants.JWT_TOKEN,response_guestMakeOrder.commonArr.token);
                    myDataBase.deletefRows();
                    Fun_MoveTab_NonLogin(response_guestMakeOrder.longMessage);
                  //  Fun_MoveTab(response_guestMakeOrder.longMessage);


                } else if ((response_guestMakeOrder.status.equals("0"))) {
                  //  Toast.makeText(context, response_guestMakeOrder.longMessage, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void Func_NonLoginAdapter() {
        getQuote_adapter_nonLogin=new GetQuote_Adapter_NonLogin(context,dataItemDBS,this);
        rv_addquote.setAdapter(getQuote_adapter_nonLogin);

    }

    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


    private void Func_submitQuote() {
            login_remarks=et_remarks_loginuser.getText().toString().trim();
           Apihit();
    }

        public void Apihit()
        {
            HashMap<String, String> header = new HashMap<>();
            header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
            HashMap<String, String> request = new HashMap<>();
            request.put("remarks", login_remarks);

            getQuote_viewModel.orderCreationResponseLiveData(header, request).observe(getActivity(), new Observer<OrderCreation_Response>() {
                @Override
                public void onChanged(OrderCreation_Response orderCreation_response) {
                    System.out.println("---add quote--" + orderCreation_response.getMessage());
                    if (orderCreation_response.getStatus().equals("1")) {
                      //  Toast.makeText(context, orderCreation_response.getLongMessage(), Toast.LENGTH_SHORT).show();
                        String content=orderCreation_response.getLongMessage();
                       // String content="Thank you for choosing us. Your request has been successfully submitted with ticket number . You can view the status details on the \"Your Activities\" page";
                        Fun_MoveTab(content);
                        ApiCall_Addtoquote();

                    } else if ((orderCreation_response.getStatus().equals("0"))) {
                      //  Toast.makeText(context, orderCreation_response.getLongMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }

    private void Fun_MoveTab(String content) {


        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_ios_singlebutton);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
        txttitle.setText("Allied Motors");
        txtInstructionDialog.setText(content);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);

        btokay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TabLayout tabs = (TabLayout)(getActivity()).findViewById(R.id.rv_categories);
                tabs.getTabAt(2).select();
                Func_AppInfo();
                alertDialog.dismiss();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();

    }

    private void Fun_MoveTab_NonLogin(String content) {


        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_ios_singlebutton);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
        txttitle.setText("Allied Motors");
        txtInstructionDialog.setText(content);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);
      //  CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
        btokay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tocart = new Intent(context, HomeActivity_searchimage_BottomButton.class);
                tocart.putExtra("tabposition", "categories");
                startActivity(tocart);
                getActivity().finish();
            }
        });




        if (!alertDialog.isShowing())
            alertDialog.show();

    }


    private void ApiCall_Addtoquote() {
        HashMap<String,String>header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        getQuote_viewModel.getquotedata(header).observe(getActivity(), new Observer<Response_GetQuote>() {
            @Override
            public void onChanged(Response_GetQuote response_getQuote) {
                loading_ll.setVisibility(View.GONE);
                if(response_getQuote.getStatus().equals("1")) {
                    if(response_getQuote.getData().getQuoteData().size()==0)
                    {
                        ll_empty.setVisibility(View.VISIBLE);
                        ll_button.setVisibility(View.GONE);
                        content_ll.setVisibility(View.GONE);
                    }
                    else {
                        rv_addquote.setVisibility(View.VISIBLE);
                        ll_empty.setVisibility(View.GONE);
                        content_ll.setVisibility(View.VISIBLE);
                        et_remarks_loginuser.setVisibility(View.VISIBLE);
                        ll_button.setVisibility(View.VISIBLE);
                        System.out.println("===res=my getquote=" + response_getQuote.toString());
                        itemArrayList = response_getQuote.getData().getQuoteData();
                        cart_count=String.valueOf(response_getQuote.getData().getQuoteData().size());
                        sharedPref.setString(Constants.PREF_Cartcount,cart_count);
                        Func_AdapterClass();
                    }
                }
                else if(response_getQuote.getStatus().equals("0"))
                {

                    System.out.println("==res-dff--sta==0=nnn=="+response_getQuote.getResponseCode());
                    Commons.hideProgress();

                    content_ll.setVisibility(View.VISIBLE);
                    non_login.setVisibility(View.VISIBLE);
                    et_remarks_loginuser.setVisibility(View.GONE);
                    ll_button.setVisibility(View.GONE);
                }
            }
        });
    }

    public void Func_AdapterClass()
    {
        getQuote_adapter = new GetQuote_Adapter(context, itemArrayList,this);
        rv_addquote.setAdapter(getQuote_adapter);

    }



    @Override
    public void onCarQuote(QuoteDataItem data, int position) {

        String temp_car_id=data.getId();
        HashMap<String,String>header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String,String>request=new HashMap<>();
        request.put("car_id",temp_car_id);
        Commons.showProgress(context);
        getQuote_viewModel.deleteQuoteMutableLiveData(header,request).observe(getActivity(), new Observer<Response_DeleteQuote>() {
            @Override
            public void onChanged(Response_DeleteQuote response_deleteQuote) {
                Commons.hideProgress();
                System.out.println("---delete quote--"+response_deleteQuote.message);
                if(response_deleteQuote.status.equals("1"))
                {
                   // Toast.makeText(context,response_deleteQuote.longMessage,Toast.LENGTH_SHORT).show();

                    if(getQuote_adapter!=null)
                    {
                            getQuote_adapter.notifyDataSetChanged();

                    }
                    else {
                        getQuote_adapter.notifyDataSetChanged();

                    }
                    ApiCall_Addtoquote();

                }
                else if((response_deleteQuote.status.equals("0")))
                {
                    Commons.Alert_custom(context,response_deleteQuote.longMessage);
                }
            }
        });
    }

    @Override
    public void updateQuote(QuoteDataItem data, int position, int count) {

            String car_id=data.getCarId();

       // Func_UpdateQuote(car_id,String.valueOf(count));
        HashMap<String,String>header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String,String>request=new HashMap<>();
        request.put("car_id",car_id);
        request.put("quantity",String.valueOf(count));

        getQuote_viewModel.updateQuoteLiveData(header,request).observe(getActivity(), new Observer<Response_UpdateQuote>() {
            @Override
            public void onChanged(Response_UpdateQuote updateQuote) {
                System.out.println("---update quote--"+updateQuote.message);
                if(updateQuote.status.equals("1"))
                {
                }
                else if((updateQuote.status.equals("0")))
                {
                    Commons.Alert_custom(context,updateQuote.longMessage);
                }
            }
        });
    }


    @Override
    public void onCarQuote1(QuoteDataItemDB data, int position) {
        autoCompleteHandler1.removeCallbacks(autoCompleteRunnable1);
        autoCompleteHandler1.postDelayed(autoCompleteRunnable1, 1000);
        nonlogin_carid=data.getCarId();


        myDataBase.deleteData(nonlogin_carid);

        if(getQuote_adapter_nonLogin!=null)
        {
            getQuote_adapter_nonLogin.removeItem(position);
            getQuote_adapter_nonLogin.notifyDataSetChanged();
            Func_NonLoginAdapter();
        }
        else {
            getQuote_adapter_nonLogin.notifyDataSetChanged();

        }

    }

    @Override
    public void updateQuote1(String data, int count) {
        String car_id=data;
        String qty=String.valueOf(count);
        myDataBase.updatequotelist1(car_id,qty);
    }


    @Override
    public void onResume() {
        super.onResume();
        System.out.println("==onreusme--add quote--");
        Func_sample();
        Func_AppInfo();
    }


    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        getQuote_viewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();


                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }


}