package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.viewmodel.FAQviewModel;

public class ContactUs_Activity extends AppCompatActivity {

    ImageView back;
    WebView webview;
    Context context;
    FAQviewModel faQviewModel;
    LinearLayout loading_ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us_);
        context=this;
        back=findViewById(R.id.back);
        webview=findViewById(R.id.webview);
        loading_ll=findViewById(R.id.loading_ll);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        Commons.showProgress(context);
        webview.requestFocus();
        webview.getSettings().

                setJavaScriptEnabled(true);

        WebSettings webSettings = webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webview.loadUrl("https://alliedmotors.com/contact-us/");
        webview.setWebViewClient(new

                                         WebViewClient() {
                                             @Override
                                             public boolean shouldOverrideUrlLoading (WebView view, String url){
                                                 view.loadUrl(url);
                                                 return true;
                                             }

                                             @Override
                                             public void onPageStarted (WebView view, String url, Bitmap favicon){
                                                 super.onPageStarted(view, url, favicon);
                                             }

                                             @Override
                                             public void onPageFinished (WebView view, String url){
                                                 super.onPageFinished(view, url);

                                             }

                                         });
        Commons.hideProgress();
    }
}