package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.app.alliedmotor.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Objects;

public class ImageBanner_Adapter_Static extends PagerAdapter {


    // Context object
    Context context;
    ArrayList<Integer> bannerDetailsItems;

    // Array of images
//    int[] images;

    // Layout Inflater
    LayoutInflater mLayoutInflater;


    // Viewpager Constructor
    public ImageBanner_Adapter_Static(Context context, ArrayList<Integer> bannerDetailsItems) {
        this.context = context;
        this.bannerDetailsItems = bannerDetailsItems;
//        this.images = images;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // return the number of images
        return bannerDetailsItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((CardView) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        // inflating the item.xml
        View itemView = mLayoutInflater.inflate(R.layout.banner_image_slider, container, false);

        // referencing the image view from the item.xml file
        ImageView imageView =  itemView.findViewById(R.id.img_banner);

        // setting the image in the imageView
//        imageView.setImageResource(images[position]);

       /* DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int pxWidth = displayMetrics.widthPixels;
        int dpWidth = (int) (pxWidth / displayMetrics.density);
        int pxHeight = displayMetrics.heightPixels;
        int dpHeight = (int) (pxHeight / displayMetrics.density);


        CardView.LayoutParams params = new CardView.LayoutParams(dpWidth, dpHeight);
        imageView.setLayoutParams(params);*/


        imageView.setImageResource(bannerDetailsItems.get(position));
        Glide.with(context).load(bannerDetailsItems.get(position)).into(imageView);

        // Adding the View
        Objects.requireNonNull(container).addView(itemView);



        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((CardView) object);
    }
    }



