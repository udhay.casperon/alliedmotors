package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.MyImageView;
import com.app.alliedmotor.utility.TouchImageView_sample;
import com.github.chrisbanes.photoview.PhotoView;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CarDetailImageBanner_Adapter1 extends PagerAdapter {
    Context context;
    ArrayList<String> cargallery_list;

    LayoutInflater mLayoutInflater;

    public CarDetailImageBanner_Adapter1(Context context, ArrayList<String> cargallery_list) {
        this.context = context;
        this.cargallery_list = cargallery_list;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // return the number of images
        return cargallery_list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        // inflating the item.xml
        View itemView = mLayoutInflater.inflate(R.layout.banner_image_slider_detail1, container, false);

        // referencing the image view from the item.xml file
     //   TouchImageView1 imageView = (TouchImageView1) itemView.findViewById(R.id.img_banner);

        LinearLayout ll_progress=itemView.findViewById(R.id.ll_progress);
        ZoomageView imageView = (ZoomageView) itemView.findViewById(R.id.img_banner);
        Picasso.get().load(cargallery_list.get(position)).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                ll_progress.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });
        // Adding the View
        (container).addView(itemView);

        if(context.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
        {
            imageView.setLayoutParams(
                    new RelativeLayout.LayoutParams(
                            // or ViewGroup.LayoutParams.WRAP_CONTENT
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            // or ViewGroup.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT ) );
        }
        else if(context.getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE)
        {
            imageView.setLayoutParams(
                    new RelativeLayout.LayoutParams(
                            // or ViewGroup.LayoutParams.WRAP_CONTENT
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            // or ViewGroup.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT ) );
        }

        return itemView;
    }




    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout) object);
    }



    }



