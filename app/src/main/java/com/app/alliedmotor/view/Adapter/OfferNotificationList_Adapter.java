package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Response_NotificationList.Offer_Notification_List;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.SwipeReveal.SwipeRevealLayout;
import com.app.alliedmotor.utility.SwipeReveal.ViewBinderHelper;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewBold;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.view.Activity.HomeActivity_searchimage_BottomButton;
import com.app.alliedmotor.view.Activity.OfferPage_Activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OfferNotificationList_Adapter extends RecyclerView.Adapter<OfferNotificationList_Adapter.ViewHolder> {

    ArrayList<Offer_Notification_List> dataItems = new ArrayList<>();
    ViewBinderHelper viewBinderHelper=new ViewBinderHelper();
    IOfferNotify listener;
    SharedPref sharedPref;
    private Context mcontext;
    String dateStr="";
    HomeActivity_searchimage_BottomButton obj;

    public OfferNotificationList_Adapter(Context context, ArrayList<Offer_Notification_List> dataItems, IOfferNotify listener) {
        this.mcontext = context;
        this.dataItems = dataItems;
        this.listener=listener;



    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_notification_new_offer, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

   String ticketid=String.valueOf(dataItems.get(position).getPost_id());
        holder.tv_title.setText(dataItems.get(position).getPost_title());

        dateStr = dataItems.get(position).getCreated_time();
        String resultDate = convertStringDateToAnotherStringDate(dateStr, "yyyy-MM-dd ", "dd MMM YYYY");

        holder.tv_date.setText(resultDate);
        holder.tv_descrption.setText(dataItems.get(position).getShort_description());

       /* if(ticketid.equals("null")|| ticketid.equals(null) || ticketid==null || ticketid.equals("")) {
            holder.tv_ticketno.setVisibility(View.INVISIBLE);
        }
        else {
            holder.tv_ticketno.setVisibility(View.VISIBLE);
            holder.tv_ticketno.setText("Ticket no:"+dataItems.get(position).getPost_id());
        }*/

        if(dataItems.get(position).getIs_unread().equals("0"))
        {
            holder.cv_cartindicator.setVisibility(View.GONE);
        }
        else if(dataItems.get(position).getIs_unread().equals("1"))
        {
            holder.cv_cartindicator.setVisibility(View.VISIBLE);
        }

  holder.rl_selected_data.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          listener.OfferNotifiy_Selected(dataItems.get(position).getPost_id());
            Intent toofferpage=new Intent(mcontext, OfferPage_Activity.class);
            toofferpage.putExtra("postid",dataItems.get(position).getPost_id());
            toofferpage.putExtra("posttitle",dataItems.get(position).getPost_title());
          toofferpage.putExtra("imageurl",dataItems.get(position).get_thumbnail_id());
          toofferpage.putExtra("buttontext",dataItems.get(position).getButton_text());
          toofferpage.putExtra("buttonurl",dataItems.get(position).getButton_url());
          toofferpage.putExtra("enablebutton",dataItems.get(position).getEnnable_button());
          toofferpage.putExtra("postcontent",dataItems.get(position).getPost_content());
          toofferpage.putExtra("shortdesc",dataItems.get(position).getShort_description());
          mcontext.startActivity(toofferpage);
      }
  });


        holder.tv_deletenotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.NotificationOffer_delete(dataItems.get(position).getPost_id());
            }
        });


    }




    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public interface IOfferNotify {
        void OfferNotifiy_Selected(String data);
        void NotificationOffer_delete(String postid);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewBold tv_title;
        CustomRegularTextview tv_date;
        CustomTextViewBold tv_ticketno;
        CustomTextViewRegular tv_descrption;
        LinearLayout rl_selected_data;
        CardView cv_cartindicator;
        SwipeRevealLayout cardview_swipe;
        CustomRegularTextview tv_deletenotification;


        public ViewHolder(View itemView) {

            super(itemView);

            cardview_swipe=itemView.findViewById(R.id.cardview_swipe);
            tv_deletenotification=itemView.findViewById(R.id.tv_deletenotification);

            cv_cartindicator=itemView.findViewById(R.id.cv_cartindicator);

            tv_title=itemView.findViewById(R.id.tv_title);
            tv_date=itemView.findViewById(R.id.tv_date);
            tv_descrption=itemView.findViewById(R.id.tv_descrption);
            tv_ticketno=itemView.findViewById(R.id.tv_ticketno);

            rl_selected_data=itemView.findViewById(R.id.rl_selected_data);

        }
    }

    public String convertStringDateToAnotherStringDate(String dateStr, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(dateStr);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }


}

