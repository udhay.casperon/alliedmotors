package com.app.alliedmotor.view.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.Car_CategoryList.DataItem;
import com.app.alliedmotor.model.Car_CategoryList.Response_CarCategoryList;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.QuoteDataItemDB;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.NonSwipeableViewPager;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomBoldTextview;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditTextBold;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.PagerAdapter_Favourite;
import com.app.alliedmotor.view.Fragment.LuxuryCar_Fragment1_sample;
import com.app.alliedmotor.view.Fragment.NewCarFragment;
import com.app.alliedmotor.view.Fragment.PreOwned__Fragment1;
import com.app.alliedmotor.view.Fragment.RHDCar_Fragment;
import com.app.alliedmotor.viewmodel.CarCategoryviewModel;
import com.app.alliedmotor.viewmodel.PreownedviewModel;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;

public class NewCar_Tablayout_Activity_static extends AppCompatActivity implements View.OnClickListener{

    CarCategoryviewModel carCategoryviewModel;
    Context context;
    TabLayout tab_categories;
    NonSwipeableViewPager viewpager;
    ArrayList<DataItem>dataItems_carcategory=new ArrayList<>();

    LinearLayout main_ll,loading_ll;
    ImageView img_home,ic_cart,ic_menu;
    EditText searchview;
    ImageView searhview_icon;
    String positionval;
    String slugname="";
    String brandselected="";
    int index=0;
    CardView cv_searchview;
    Dialog FilterDialog;
    private NavigationView navigationView;
    private DrawerLayout drawer;

    CustomRegularButton bt_signin;
    CustomTextViewSemiBold tv_accountname;
    CustomRegularTextview nav_home,nav_favourite,nav_cart,nav_orders,nav_chat,nav_logout,nav_account,nav_notification;
    int position_tab=0;
    LinearLayout ll_logout;
    SharedPref sharedPref;
    public static final int REQUEST_CODE = 1;
    CustomRegularEditTextBold et_make,et_model,et_fuel,et_transmission,et_year,et_maxyear,et_mileage,et_body_condition,et_dealer_warranty,et_exteriorcolor,et_interior_color,et_descrption, et_engine,et_varient,et_door;

    ConstraintLayout ll_make,ll_model,ll_fueltype,ll_transmission,ll_select_year,ll_select_maxyear;
    ConstraintLayout ll_mileage,ll_body_condition,ll_dealer_warranty,ll_exteriorcolor,ll_interior_color,ll_descrption;
    LinearLayout ll_enginetype,ll_varienttype,ll_door;
    int makelistsize,modellistsize;
    String[] str_makedid;
    String[] strmodellist;
    String ip_makeid,ipmodelid;
    ArrayList<Response_CarMakeList.DataItem>carmakelist=new ArrayList<>();
    ArrayList<Response_CarModelList.DataItem>carmodellist=new ArrayList<>();
    String temp_makelist="",temp_model="",temp_fuel="",temp_tranmission="",temp_year="",temp_maxyear="",temp_mileage="",temp_extcolor="",temp_intcolor="";
    String temp_engine="",tempvarient="",tempdoors="";
    PreownedviewModel preownedviewModel;
    String fromactiviyy="";

    CustomBoldTextview tv_notification_count;
    CardView cv_cartindicator,cv_notificationcount;
    String notify_count="";
    CustomBoldTextview sidenav_notification_count;
    ArrayList<QuoteDataItemDB> dataItemDBS=new ArrayList<>();
    MyDataBase myDataBase;

    CardView cv_nav_cartcount;
    CustomBoldTextview sidenav_cart_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_car_tablayout);
        carCategoryviewModel = ViewModelProviders.of(this).get(CarCategoryviewModel.class);
        preownedviewModel = ViewModelProviders.of(this).get(PreownedviewModel.class);
        context=this;
        sharedPref=new SharedPref(context);
        myDataBase = new MyDataBase(context);
        findbyviews();
        clicklistener();
        Intent getIntent=getIntent();
        positionval=getIntent.getStringExtra("tab_position");
        brandselected=getIntent.getStringExtra("brand_selected");
        fromactiviyy=getIntent.getStringExtra("FromActivity");
        Log.e("tab_postion-->",positionval);
        Method_CarCategoryList();

       WebServiceCall_MakeList();

       LoginOrNot();
        Func_AppInfo();
        if(fromactiviyy!=null && positionval.equals("3"))
        {
            temp_makelist=getIntent.getStringExtra("makeid");
            temp_model=getIntent.getStringExtra("modelid");
            temp_year=getIntent.getStringExtra("minyear");
            temp_maxyear=getIntent.getStringExtra("maxyear");
            temp_mileage=getIntent.getStringExtra("mileage");
            temp_engine=getIntent.getStringExtra("engine");
            temp_tranmission=getIntent.getStringExtra("transmission");
            temp_fuel=getIntent.getStringExtra("fuel");
            tempvarient=getIntent.getStringExtra("varient");
            tempdoors=getIntent.getStringExtra("doors");
            temp_extcolor=getIntent.getStringExtra("exteriorcolor");
            temp_intcolor=getIntent.getStringExtra("interiorcolor");
            if(PreOwned__Fragment1.getInstance() != null) {

                PreOwned__Fragment1.getInstance().WebServiceCall(temp_makelist, temp_model, temp_year, temp_maxyear, temp_mileage, temp_engine, temp_tranmission, temp_fuel, tempvarient, tempdoors, temp_extcolor, temp_intcolor);
            }
        }

    }

    private void LoginOrNot() {
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN)=="")
        {
            bt_signin.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.GONE);
            ll_logout.setVisibility(View.GONE);
            cv_notificationcount.setVisibility(View.GONE);
            if(myDataBase!=null) {
                dataItemDBS = myDataBase.getAlldataforquote();
                if(dataItemDBS.size()==0)
                {
                    cv_cartindicator.setVisibility(View.GONE);
                    cv_nav_cartcount.setVisibility(View.GONE);

                }
                else {
                    cv_cartindicator.setVisibility(View.VISIBLE);
                    cv_nav_cartcount.setVisibility(View.VISIBLE);
                    String cart_count=String.valueOf(dataItemDBS.size());
                    sharedPref.setString(Constants.PREF_Cartcount,cart_count);
                    tv_notification_count.setText(cart_count);
                    sidenav_cart_count.setText(cart_count);

                }
            }



        }
        else
        {
            bt_signin.setVisibility(View.GONE);
            ll_logout.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.VISIBLE);
            tv_accountname.setText(sharedPref.getString(Constants.PREF_USERNAME));
            cv_cartindicator.setVisibility(View.VISIBLE);
            cv_nav_cartcount.setVisibility(View.VISIBLE);
            cv_notificationcount.setVisibility(View.VISIBLE);
            notify_count=sharedPref.getString(Constants.PREF_UNREAD_NOTIFICATION);

            sidenav_notification_count.setText(notify_count);
            getquote_func();
        }
    }

    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        carCategoryviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                    if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());
                        cv_notificationcount.setVisibility(View.GONE);
                    }
                    else {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());
                        String count = response_appInfo.getData().getUnreadNotificationCount();
                        if(count.equals("0")) {
                            cv_notificationcount.setVisibility(View.GONE);
                        }
                        else
                        {
                            cv_notificationcount.setVisibility(View.VISIBLE);
                            sidenav_notification_count.setText(count);
                        }
                    }

                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }


    private void getquote_func() {
        HashMap<String,String>header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        carCategoryviewModel.getquotedata(header).observe(this, new Observer<Response_GetQuote>() {
            @Override
            public void onChanged(Response_GetQuote response_getQuote) {

                if(response_getQuote.getStatus().equals("1")) {
                    String cart_count=String.valueOf(response_getQuote.getData().getQuoteData().size());
                    if (Integer.parseInt(cart_count)==0)
                    {
                        cv_cartindicator.setVisibility(View.GONE);
                        cv_nav_cartcount.setVisibility(View.GONE);
                    }
                    else {
                        cv_cartindicator.setVisibility(View.VISIBLE);
                        cv_nav_cartcount.setVisibility(View.VISIBLE);
                        sharedPref.setString(Constants.PREF_Cartcount, cart_count);
                        cv_nav_cartcount.setVisibility(View.VISIBLE);
                        tv_notification_count.setText((cart_count));
                        sidenav_cart_count.setText(sharedPref.getString(Constants.PREF_Cartcount));
                    }
                }
            }

        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        Func_AppInfo();

        sidenav_notification_count.setText(notify_count);

    }

    private void Method_CarCategoryList() {


        tab_categories.addTab(tab_categories.newTab().setText("New Cars"));
        tab_categories.addTab(tab_categories.newTab().setText("Luxury Cars"));
        tab_categories.addTab(tab_categories.newTab().setText("RHD Cars"));
        tab_categories.addTab(tab_categories.newTab().setText("PreOwned Cars"));
       // tab_categories.addTab(tab_categories.newTab().setText("Heavy Vehicles"));


        tab_categories.setTabGravity(TabLayout.GRAVITY_FILL);
        //rv_categories.setupWithViewPager(viewpager);
        Tablayout_Category_Adapter adapter = new Tablayout_Category_Adapter(getSupportFragmentManager(),context, tab_categories.getTabCount());
        //Adding adapter to pager
        viewpager.setAdapter(adapter);
        //viewpager.setOffscreenPageLimit(1);
        //Adding onTabSelectedListener to swipe views

        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_categories));
        viewpager.setCurrentItem(Integer.valueOf(positionval));
        viewpager.setOffscreenPageLimit(4);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_categories));
        tab_categories.setupWithViewPager(viewpager);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {

            case R.id.ic_menu:
                drawer.openDrawer(GravityCompat.END);
                break;

            case R.id.nav_account:
                System.out.println("==Menu===");
                Intent tohomes = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohomes.putExtra("tabposition", "useraccount");
                startActivity(tohomes);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case R.id.nav_notification:
                drawer.closeDrawer(Gravity.RIGHT);
                Intent tonotifify=new Intent(this,NotificationList_Activity.class);
                startActivity(tonotifify);
                break;

            case  R.id.nav_home:
                System.out.println("==Menu===");
                Intent tohome = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohome.putExtra("tabposition", "home");
                startActivity(tohome);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_favourite:
                Intent tocart = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart.putExtra("tabposition", "cart");
                tocart.putExtra("position_tabs","0");
                startActivity(tocart);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);

                break;

            case  R.id.nav_cart:
                Intent tocart1 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart1.putExtra("tabposition", "cart");
                tocart1.putExtra("position_tabs","1");
                startActivity(tocart1);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_orders:
                Intent tocart2 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart2.putExtra("tabposition", "cart");
                tocart2.putExtra("position_tabs","2");
                startActivity(tocart2);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case  R.id.nav_chat:
                Intent tocart3 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart3.putExtra("tabposition", "chat");
                startActivity(tocart3);
                finish();
                System.out.println("==Menu===");
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_logout:
                System.out.println("==Menu===");
                drawer.closeDrawer(Gravity.RIGHT);
                Intent gotoLogin = new Intent(context, LoginActivity.class);
                System.out.println("===vc===" + sharedPref.getString(Constants.JWT_TOKEN));
                sharedPref.clearAll();
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoLogin);
                finish();
                break;

            case R.id.bt_signin:
                Intent tosignin=new Intent(this,LoginActivity.class);
                startActivity(tosignin);
                drawer.closeDrawer(Gravity.RIGHT);
                finish();
                break;

            case R.id.ic_cart:
                Intent tocartpage = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocartpage.putExtra("tabposition", "cart");
                tocartpage.putExtra("position_tabs","1");
                startActivity(tocartpage);
                finish();
                break;
            case R.id.img_home:
                 onBackPressed();
                break;

            case R.id.searchview:
            case R.id.searhview_icon:
            case R.id.cv_searchview:
                if(viewpager.getCurrentItem()==3)
                {

                    ip_makeid="";
                    ipmodelid="";
                    temp_makelist="";
                    temp_model="";
                    temp_maxyear="";
                    temp_year="";
                    Dialog_Filter_preowned();
                }
                else {
                    Intent toSearchpage2 = new Intent(NewCar_Tablayout_Activity_static.this, SearchedPageActivity.class);
                    startActivity(toSearchpage2);
                    break;
                }
        }
    }

        private void Dialog_Filter_preowned()

        {
            FilterDialog = new Dialog(context);
            FilterDialog.setCanceledOnTouchOutside(true);
            FilterDialog.setCancelable(true);

            FilterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            FilterDialog.setContentView(R.layout.dialog_preowned_filter);
            FilterDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            FilterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            Window window = FilterDialog.getWindow();
            window.setGravity(Gravity.CENTER);
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


            CustomRegularButton bt_go,bt_advanced;
            LinearLayout ll_advanced;

            ImageView img_home_back;

            img_home_back=FilterDialog.findViewById(R.id.img_home_back);
         //   bt_advanced=FilterDialog.findViewById(R.id.bt_advanced);
            bt_go=FilterDialog.findViewById(R.id.bt_go);
            ll_advanced=FilterDialog.findViewById(R.id.ll_advanced);


            et_make=FilterDialog.findViewById(R.id.et_make);
            et_model=FilterDialog.findViewById(R.id.et_model);
            et_year=FilterDialog.findViewById(R.id.et_year);
            et_maxyear=FilterDialog.findViewById(R.id.et_maxyear);
            et_mileage=FilterDialog.findViewById(R.id.et_mileage);
            et_engine=FilterDialog.findViewById(R.id.et_engine);
            et_varient=FilterDialog.findViewById(R.id.et_varient);
            et_transmission=FilterDialog.findViewById(R.id.et_transmission);
            et_fuel=FilterDialog.findViewById(R.id.et_fuel);
            et_door=FilterDialog.findViewById(R.id.et_door);
            et_exteriorcolor=FilterDialog.findViewById(R.id.et_exteriorcolor);
            et_interior_color=FilterDialog.findViewById(R.id.et_interior_color);




            ll_make=FilterDialog.findViewById(R.id.ll_make);
            ll_model=FilterDialog.findViewById(R.id.ll_model);
            ll_fueltype=FilterDialog.findViewById(R.id.ll_fueltype);
            ll_transmission=FilterDialog.findViewById(R.id.ll_transmission);
            ll_select_year=FilterDialog.findViewById(R.id.ll_select_year);
            ll_select_maxyear=FilterDialog.findViewById(R.id.ll_select_maxyear);
            ll_mileage=FilterDialog.findViewById(R.id.ll_mileage);
            ll_varienttype=FilterDialog.findViewById(R.id.ll_varienttype);
            ll_enginetype=FilterDialog.findViewById(R.id.ll_enginetype);
            ll_door=FilterDialog.findViewById(R.id.ll_door);
            ll_exteriorcolor=FilterDialog.findViewById(R.id.ll_exteriorcolor);
            ll_interior_color=FilterDialog.findViewById(R.id.ll_interior_color);

            temp_engine=et_engine.getText().toString().trim();
            tempvarient=et_varient.getText().toString().trim();
            tempdoors=et_door.getText().toString().trim();

            ll_make.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog_NumberPicker_MakeList();
                }
            });

            ll_model.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(temp_makelist.isEmpty())
                    {
                        Commons.Alert_custom(context,"Please select the make options");
                    }
                    else {
                        Dialog_NumberPicker_ModelList();
                    }

                }
            });
            ll_select_year.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Dialog_Year();
                }
            });
            ll_select_maxyear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Dialog_Year_max();
                }
            });
           /* ll_mileage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Dialog_Mileage();
                }
            });
            ll_transmission.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog_Transmission();
                }
            });
            ll_fueltype.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog_Fuel();
                }
            });
            ll_exteriorcolor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Dialog_ExteriorSell();
            }
        });
            ll_interior_color.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Dialog_InteriorSell();
                }
            });
*/
            img_home_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   FilterDialog.dismiss();
                }
            });

            bt_go.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(PreOwned__Fragment1.getInstance() != null)
                        PreOwned__Fragment1.getInstance().WebServiceCall(ip_makeid,ipmodelid,temp_year,temp_maxyear,temp_mileage,temp_engine,temp_tranmission,temp_fuel,tempvarient,tempdoors,temp_extcolor,temp_intcolor);
                    else {
                       // loadFragment(getit)
                    }
                    FilterDialog.dismiss();
                }
            });

/*

            bt_advanced.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ll_advanced.isShown()) {
                        bt_advanced.setText("Advanced Options");
                        ll_advanced.setVisibility(View.GONE);
                    }
                    else {

                        bt_advanced.setText("-");
                        ll_advanced.setVisibility(View.VISIBLE);
                    }
                }
            });
*/

            if(!FilterDialog.isShowing()) {
                FilterDialog.show();
            }

        }


    private void WebServiceCall_MakeList() {

        HashMap<String,String>request=new HashMap<>();
        request.put("isPreowned","yes");

        carCategoryviewModel.getcarmakelist(request).observe(this, new Observer<Response_CarMakeList>() {
            @Override
            public void onChanged(Response_CarMakeList response_carMakeList) {


                main_ll.setVisibility(View.VISIBLE);
                if(response_carMakeList.status.equals("1")) {
                    System.out.println("===res=preowned=makelist=" + response_carMakeList.data.toString());
                    makelistsize=response_carMakeList.data.size();
                    carmakelist=response_carMakeList.data;
                    Log.i("res-=preownr maklist0->",response_carMakeList.message);

                }
                else if(response_carMakeList.status.equals("0"))
                {
                    Log.i("r=preownr maklist->",response_carMakeList.message);
                    System.out.println("===r=preownr maklist=" + response_carMakeList.data);
                    Toast.makeText(context,response_carMakeList.message,Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void WebServiceCall_ModelList() {

        Commons.showProgress(context);
        HashMap<String,String> request=new HashMap<>();
        request.put("make_id",ip_makeid);
        request.put("isPreowned","yes");

        System.out.println("url-modellist->"+request);
        Log.e("--modellist--->",request.toString());
        carCategoryviewModel.getCarModeldata(request).observe(this, new Observer<Response_CarModelList>() {
            @Override
            public void onChanged(Response_CarModelList response_carModelList) {

                Commons.hideProgress();
                System.out.println("===res=car model list=" + response_carModelList.message);
                if(response_carModelList.status.equals("1"))
                {
                    carmodellist=response_carModelList.data;
                    modellistsize=carmodellist.size();
                }
                else if(response_carModelList.status.equals("0"))
                {
                    Toast.makeText(context,response_carModelList.message,Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    private void Dialog_NumberPicker_MakeList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(makelistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmakelist.size()];
        str_makedid=new String[carmakelist.size()];
        for (int i=0;i<carmakelist.size();i++)
        {
            str[i]=carmakelist.get(i).makename;
            str_makedid[i]=carmakelist.get(i).mkid;
        }
        np.setDisplayedValues(str);
        temp_makelist=str[index];
        ip_makeid=str_makedid[index];
        button_addquote.setOnClickListener(v -> {
            et_make.setText(temp_makelist);
          //  PreOwned__Fragment1.res_make=temp_makelist;
            dialog.dismiss();
            WebServiceCall_ModelList();
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_makelist=str[newVal];
                ip_makeid=str_makedid[newVal];
                // Toast.makeText(getActivity(), temp_makelist, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_NumberPicker_ModelList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(modellistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmodellist.size()];
        strmodellist=new String[carmodellist.size()];
        for (int i=0;i<carmodellist.size();i++)
        {
            str[i]=carmodellist.get(i).modelName;
            strmodellist[i]=carmodellist.get(i).modid;
        }
        np.setDisplayedValues(str);
        temp_model=str[index];
        ipmodelid=strmodellist[index];


        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_model.setText(temp_model);
               // PreOwned__Fragment1.res_make=temp_model;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_model=str[newVal];
                ipmodelid=strmodellist[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Fuel() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] fuel_array = getResources().getStringArray(R.array.Fuel);
        np.setMaxValue(fuel_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(fuel_array);
        temp_fuel=fuel_array[index];


        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_fuel.setText(temp_fuel);
               // PreOwned__Fragment1.res_fueltype=temp_fuel;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_fuel=fuel_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Transmission() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Transmission);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_tranmission=transmission_array[index];


        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_transmission.setText(temp_tranmission);
             //   PreOwned__Fragment1.res_transmission=temp_tranmission;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_tranmission=transmission_array[newVal];
                // Toast.makeText(getActivity(), temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Year() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Min_Year_Preowned);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_year=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_year.setText(temp_year);
               // PreOwned__Fragment1.res_minyear=temp_year;
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_year=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Year_max() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Year_Preowned);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_maxyear=transmission_array[index];

        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_maxyear.setText(temp_maxyear);


                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_maxyear=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Mileage() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Mileage);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_mileage=transmission_array[index];

        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_mileage.setText(temp_mileage);
              //  PreOwned__Fragment1.res_mileage=temp_mileage;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_mileage=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_ExteriorSell() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.exterior_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_extcolor=transmission_array[index];


        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_exteriorcolor.setText(temp_extcolor);
               // PreOwned__Fragment1.res_extcolor=temp_extcolor;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_extcolor=transmission_array[newVal];
            }
        });
        dialog.show();
    }

    private void Dialog_InteriorSell() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);

        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.interior_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_intcolor=transmission_array[index];


        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_interior_color.setText(temp_intcolor);
             //   PreOwned__Fragment1.res_intcolor=temp_intcolor;
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_intcolor=transmission_array[newVal];

            }
        });
        dialog.show();
    }





    public class Tablayout_Category_Adapter  extends FragmentPagerAdapter {
        private Context myContext;
        int totalTabs;

        public Tablayout_Category_Adapter(FragmentManager fm, Context myContext, int totalTabs) {
            super(fm);
            this.myContext = (Context) myContext;
            this.totalTabs = totalTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    searchview.setHint("Search (make, model, varient id)");
                    NewCarFragment newCarFragment = new NewCarFragment(brandselected);
                    return newCarFragment;
                case 1:
                    searchview.setHint("Search (make, model, varient id)");
                    LuxuryCar_Fragment1_sample luxuryCar_fragment = new LuxuryCar_Fragment1_sample(brandselected);
                    return luxuryCar_fragment;
                case 2:
                    searchview.setHint("Search (make, model, varient id)");
                    RHDCar_Fragment rhdCar_fragment = new RHDCar_Fragment(brandselected);
                    return rhdCar_fragment;
                case 3:
                    searchview.setHint("Search (make, model, year)");
                    PreOwned__Fragment1 preOwned__fragment = new PreOwned__Fragment1();
                    return preOwned__fragment;

              /*  case 4:
                    searchview.setHint("Search (make, model, varient id)");
                    HeavyVehicle_Fragment heavyVehicle_fragment = new HeavyVehicle_Fragment();
                    return heavyVehicle_fragment;*/
                default:
                    return null;
            }
        }


        @Override
        public int getCount() {
            return totalTabs;
        }


       @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "New Cars";
                case 1:
                    return "Luxury Cars";
                case 2:
                    return "RHD Cars";
                case 3:
                    return "Pre Owned Cars";
                /*case 4:
                    return "Heavy Vehicle";*/
            }
            return null;
        }
    }
    private void setupDrawerContent(NavigationView navigationView) {

        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });
    }

    public void setSearchTitle(String title){
        searchview.setText(title);
    }



    private void findbyviews() {



        sidenav_cart_count=findViewById(R.id.sidenav_cart_count);
        cv_nav_cartcount=findViewById(R.id.cv_nav_cartcount);

        sidenav_notification_count=findViewById(R.id.sidenav_notification_count);
        cv_cartindicator=findViewById(R.id.cv_cartindicator);
        cv_notificationcount=findViewById(R.id.cv_notificationcount);

        tv_notification_count=findViewById(R.id.tv_notification_count);

        ll_logout=findViewById(R.id.ll_logout);
        nav_logout=findViewById(R.id.nav_logout);
        nav_home=findViewById(R.id.nav_home);
        nav_cart=findViewById(R.id.nav_cart);
        nav_favourite=findViewById(R.id.nav_favourite);
        nav_orders=findViewById(R.id.nav_orders);
        nav_chat=findViewById(R.id.nav_chat);
        bt_signin=findViewById(R.id.bt_signin);
        tv_accountname=findViewById(R.id.tv_accountname);
        nav_account=findViewById(R.id.nav_account);
        nav_notification=findViewById(R.id.nav_notification);





        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }


        tab_categories=findViewById(R.id.tab_categories);
        viewpager=findViewById(R.id.viewpager);

        main_ll=findViewById(R.id.main_ll);
        img_home=findViewById(R.id.img_home);
        searchview=findViewById(R.id.searchview);
        ic_cart=findViewById(R.id.ic_cart);
        ic_menu=findViewById(R.id.ic_menu);
        searhview_icon=findViewById(R.id.searhview_icon);
        cv_searchview=findViewById(R.id.cv_searchview);

    }

    private void clicklistener() {
        img_home.setOnClickListener(this);
        ic_menu.setOnClickListener(this);
        ic_cart.setOnClickListener(this);
        cv_searchview.setOnClickListener(this);
        searchview.setOnClickListener(this);
        searhview_icon.setOnClickListener(this);





        nav_home.setOnClickListener(this);
        nav_cart.setOnClickListener(this);
        nav_favourite.setOnClickListener(this);
        nav_orders.setOnClickListener(this);
        nav_chat.setOnClickListener(this);
        bt_signin.setOnClickListener(this);
        nav_logout.setOnClickListener(this);
        tv_accountname.setOnClickListener(this);

        nav_account.setOnClickListener(this);
        nav_notification.setOnClickListener(this);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode != RESULT_OK && data != null && requestCode==REQUEST_CODE) {
            temp_makelist = data.getStringExtra("makeid");
            temp_model = data.getStringExtra("modelid");
            temp_year = data.getStringExtra("minyear");
            temp_maxyear = data.getStringExtra("maxyear");
            temp_mileage = data.getStringExtra("mileage");
            temp_engine = data.getStringExtra("engine");
            temp_tranmission = data.getStringExtra("transmission");
            temp_fuel = data.getStringExtra("fuel");
            tempvarient = data.getStringExtra("varient");
            tempdoors = data.getStringExtra("doors");
            temp_extcolor = data.getStringExtra("exteriorcolor");
            temp_intcolor = data.getStringExtra("interiorcolor");
            if (PreOwned__Fragment1.getInstance() != null) {
                PreOwned__Fragment1.getInstance().WebServiceCall(temp_makelist, temp_model, temp_year, temp_maxyear, temp_mileage, temp_engine, temp_tranmission, temp_fuel, tempvarient, tempdoors, temp_extcolor, temp_intcolor);
            }
        }
        }

}