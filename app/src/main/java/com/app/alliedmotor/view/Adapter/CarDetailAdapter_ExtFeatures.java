package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.util.ArrayList;

public class CarDetailAdapter_ExtFeatures extends RecyclerView.Adapter<CarDetailAdapter_ExtFeatures.ViewHolder> {

    ArrayList<String> features;

    int limit=8;
    SharedPref sharedPref;
    private Context mcontext;


    public CarDetailAdapter_ExtFeatures(Context context, ArrayList<String> features) {
        this.mcontext = context;
        this.features = features;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.features_rv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_points.setText(AppInstalled.toTitleCase(features.get(position).toLowerCase()));
          }

    @Override
    public int getItemCount() {
            return features.size();

    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewSemiBold tv_points;

        public ViewHolder(View itemView) {

            super(itemView);

            tv_points=itemView.findViewById(R.id.tv_points);
          ;
        }
    }
}

