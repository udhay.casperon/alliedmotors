package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Car_CategoryList.DataItem;
import com.app.alliedmotor.utility.widgets.CustomTextView;

import java.util.ArrayList;

public class Category_tab_Adapter extends RecyclerView.Adapter<Category_tab_Adapter.ViewHolder> {

    Context context;
    private OnItemClickListener listener;
    private int currentSelected = 0,row_index=0;
    ArrayList<DataItem> dataItems_carCategory;
    private LinearLayoutManager layoutManager;

    FragmentActivity fragmentActivity;


    public Category_tab_Adapter(FragmentActivity activity, ArrayList<DataItem> dataItems_carCategory, LinearLayoutManager linearLayoutManager, OnItemClickListener listener1) {
        this.fragmentActivity=activity;
        this.dataItems_carCategory = dataItems_carCategory;
        this.layoutManager=linearLayoutManager;
        this.listener=listener1;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.subject_chapter_rv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CustomTextView title = holder.points_title;
        LinearLayout ll_tabpoints=holder.ll_tabpoints;
        title.setText(dataItems_carCategory.get(position).getPostTitle());

        DataItem currmodel=dataItems_carCategory.get(position);
        if(position==0)
        {

            title.setTextColor(fragmentActivity.getResources().getColor(R.color.dark_blue));
        }
        else
        {
            title.setTextColor(fragmentActivity.getResources().getColor(R.color.light_gray));
        }
        ll_tabpoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
    }




    private View getViewByPosition(int pos) {
        if (layoutManager == null) {
            return null;
        }
        final int firstListItemPosition = layoutManager.findFirstVisibleItemPosition();
        final int lastListItemPosition = firstListItemPosition + layoutManager.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return null;
        } else {
            final int childIndex = pos - firstListItemPosition;
            return layoutManager.getChildAt(childIndex);
        }
    }


    private void select(int position) {
        dataItems_carCategory.get(position); //updating dataset
        if (currentSelected >= 0) {
            deselect(currentSelected);
        }

        View targetView = getViewByPosition(position);
        if (targetView != null) {
            // change the appearance
            TextView title = targetView.findViewById(R.id.tv_points);
            title.setTextColor(fragmentActivity.getResources().getColor(R.color.dark_blue));

        }

        if (listener != null) {
            listener.onItemClick(position);
        }

        currentSelected = position;

    }


    private void deselect(int position) {
        dataItems_carCategory.get(position); //updating dataset
        if (getViewByPosition(position) != null) {
            View targetView = getViewByPosition(position);
            if (targetView != null) {
                // change the appearance
                TextView title = targetView.findViewById(R.id.tv_points);
                title.setTextColor(fragmentActivity.getResources().getColor(R.color.light_gray));

            }
        }

        currentSelected = -1;
    }


    //public method to set the current selected tab item.
    public void setCurrentSelected(int i) {
        select(i);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }
    public interface OnItemClickListener {
        void onItemClick(int position);
    }


    @Override
    public int getItemCount() {
        return dataItems_carCategory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      //  CardView cardView;
        LinearLayout ll_tabpoints;
        CustomTextView points_title;

        public ViewHolder(View itemView) {

            super(itemView);

          //  this.cardView = (CardView) itemView.findViewById(R.id.cardView);
            this.points_title = (CustomTextView) itemView.findViewById(R.id.tv_points);
            this.ll_tabpoints = (LinearLayout) itemView.findViewById(R.id.ll_tabpoints);
        }
    }
}
