package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;

import com.app.alliedmotor.model.LuxuryCar.CarDatasItem;
import com.app.alliedmotor.model.LuxuryCar.CarListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.FullLuxuryCarList_Category;

import java.util.ArrayList;

public class LuxuryCar_CarList_Adapter extends RecyclerView.Adapter<LuxuryCar_CarList_Adapter.ViewHolder> {

    ArrayList<CarListItem> carListItems = new ArrayList<>();

    private final int limit = 4;
    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;
    LuxuryCar_Dataitems_Adapter luxuryCarDataitemsAdapter;
    ArrayList<CarDatasItem> dataItems = new ArrayList<>();

    public LuxuryCar_CarList_Adapter(Context context, ArrayList<CarListItem> listdata) {
        this.mcontext = context;
        this.carListItems = listdata;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.festival_rv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        dataItems=carListItems.get(position).getCarDatas();
        holder.textview1.setText(carListItems.get(position).getTitle());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mcontext,2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
        holder.rv_list_festivals.setLayoutManager(gridLayoutManager);
        holder.rv_list_festivals.setHasFixedSize(true);


        if(dataItems.size()>4) {
            holder.viewall.setVisibility(View.VISIBLE);
            luxuryCarDataitemsAdapter = new LuxuryCar_Dataitems_Adapter(mcontext, dataItems);
            holder.rv_list_festivals.setAdapter(luxuryCarDataitemsAdapter);
        }
        else
        {
            holder.viewall.setVisibility(View.INVISIBLE);
            luxuryCarDataitemsAdapter = new LuxuryCar_Dataitems_Adapter(mcontext, dataItems);
            holder.rv_list_festivals.setAdapter(luxuryCarDataitemsAdapter);
        }

        holder.viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toIntent=new Intent(mcontext, FullLuxuryCarList_Category.class);
                toIntent.putExtra("festive_title",carListItems.get(position).getTitle());
                toIntent.putExtra("festive_datalist",carListItems.get(position).getCarDatas());
                mcontext.startActivity(toIntent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return carListItems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewSemiBold textview1;
        RecyclerView rv_list_festivals;
        CustomRegularButton viewall;


        public ViewHolder(View itemView) {

            super(itemView);

            this.textview1 = itemView.findViewById(R.id.textview1);
            this.rv_list_festivals = itemView.findViewById(R.id.rv_list_festivals);

            this.viewall = itemView.findViewById(R.id.textView23);
        }
    }
}

