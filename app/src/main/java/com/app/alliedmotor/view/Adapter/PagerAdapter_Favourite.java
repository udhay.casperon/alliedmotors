package com.app.alliedmotor.view.Adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.app.alliedmotor.view.Fragment.Fragment_AddtoQuote;
import com.app.alliedmotor.view.Fragment.Fragment_Favourite;
import com.app.alliedmotor.view.Fragment.Fragment_YourOrder;

public class PagerAdapter_Favourite extends FragmentPagerAdapter {

        //integer to count number of tabs
        int tabCount;
        String tickid="";

        //Constructor to the class
        public PagerAdapter_Favourite(FragmentManager fm, int tabCount,String ticketid) {
            super(fm);
            //Initializing tab count
            this.tabCount= tabCount;
            this.tickid=ticketid;
        }

        //Overriding method getItem
        @Override
        public Fragment getItem(int position) {
            //Returning the current tabs
            switch (position) {
                case 0:
                    Fragment_Favourite tab1 = new Fragment_Favourite();
                    return tab1;
                case 1:
                    Fragment_AddtoQuote tab2 = new Fragment_AddtoQuote();
                    return tab2;
                case 2:
                    Fragment_YourOrder tab3 = new Fragment_YourOrder(tickid);
                    return tab3;
                default:
                    return null;
            }
        }

        //Overriden method getCount to get the number of tabs
        @Override
        public int getCount() {
            return tabCount;
        }
    }


