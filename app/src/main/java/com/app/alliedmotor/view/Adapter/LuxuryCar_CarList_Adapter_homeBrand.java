package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;

import com.app.alliedmotor.model.LuxuryCar.CarDatasItem;
import com.app.alliedmotor.model.LuxuryCar.CarListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.util.ArrayList;

public class LuxuryCar_CarList_Adapter_homeBrand extends RecyclerView.Adapter<LuxuryCar_CarList_Adapter_homeBrand.ViewHolder> {

    ArrayList<CarListItem> carListItems = new ArrayList<>();
    private final int limit = 4;

    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;
    LuxuryCar_Dataitems_Adapter_HomeBrand luxuryCarDataitemsAdapter;
    ArrayList<CarDatasItem> dataItems = new ArrayList<>();

    public LuxuryCar_CarList_Adapter_homeBrand(Context context, ArrayList<CarListItem> listdata) {
        this.mcontext = context;
        this.carListItems = listdata;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.festival_rv_homepagebrandslection, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        dataItems=carListItems.get(position).getCarDatas();
        holder.textview1.setText(carListItems.get(position).getTitle());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mcontext,2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
        holder.rv_list_festivals.setLayoutManager(gridLayoutManager);
        holder.rv_list_festivals.setHasFixedSize(true);

       /* for (int i=0;i<dataItems.size();i++)
        {
            if(dataItems.get(i).getMoId().equals("0"))
            {
                dataItems.remove(dataItems.get(i));
            }
            else
            {
                dataItems=dataItems;
            }
        }
*/

        luxuryCarDataitemsAdapter = new LuxuryCar_Dataitems_Adapter_HomeBrand(mcontext, dataItems);
        holder.rv_list_festivals.setAdapter(luxuryCarDataitemsAdapter);
            }

    @Override
    public int getItemCount() {
        return carListItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewSemiBold textview1;
        RecyclerView rv_list_festivals;
       // CustomRegularButton viewall;


        public ViewHolder(View itemView) {

            super(itemView);

            this.textview1 = itemView.findViewById(R.id.textview1);
            this.rv_list_festivals = itemView.findViewById(R.id.rv_list_festivals);


        }
    }
}

