package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.CarDetail.FuelTypeListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.util.ArrayList;

public class CarDetailAdapter_FuelTypeList extends RecyclerView.Adapter<CarDetailAdapter_FuelTypeList.ViewHolder> {

    ArrayList<FuelTypeListItem> features;
    SharedPref sharedPref;
    private Context mcontext;
    int current_position = -1;
    int previousposition = 0;
    Listener_Fuelid listener;
    String name_fuel = "";


    public interface Listener_Fuelid {
        void onfuelSelected(String data);

    }


    public CarDetailAdapter_FuelTypeList(Context context, ArrayList<FuelTypeListItem> features, Listener_Fuelid listener, String fuelname) {
        this.mcontext = context;
        this.features = features;
        this.listener = listener;
        this.name_fuel = fuelname;
        for (int k = 0; k < features.size(); k++) {
            if (name_fuel.equals(features.get(k).getEfuelType())) {
                current_position = k;
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_filter, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        holder.tv_value.setText((features.get(position).getEfuelType()));


        if (current_position == position) {
            holder.ll_filter.setBackgroundResource(R.drawable.edittext_roundshadow);
            holder.tv_value.setTextColor(mcontext.getResources().getColor(R.color.white));
        } else {
            holder.ll_filter.setBackgroundResource(R.drawable.edittext_roundshadow_outline_grey);
            holder.tv_value.setTextColor(mcontext.getResources().getColor(R.color.black));
        }

        holder.ll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onfuelSelected(features.get(position).getEftId());
                current_position = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {

        return features.size();


    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewSemiBold tv_value;
        LinearLayout ll_filter;


        public ViewHolder(View itemView) {

            super(itemView);

            tv_value = itemView.findViewById(R.id.tv_value);
            ll_filter = itemView.findViewById(R.id.ll_filter);

        }
    }
}

