package com.app.alliedmotor.view.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.GetQuote.Response_GetQuote;
import com.app.alliedmotor.model.QuoteDataItemDB;
import com.app.alliedmotor.model.Response_SearchbyKeywords;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomBoldTextview;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.SearchListAdapter;
import com.app.alliedmotor.viewmodel.HomeActivityViewModel;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchedPageActivity extends AppCompatActivity implements View.OnClickListener {

    String St_searchedstring;
    private Context context;
    SharedPref sharedPref;
    HomeActivityViewModel homeActivityViewModel;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rv_searchlist;
    SearchListAdapter searchListAdapter;
    ArrayList<Response_SearchbyKeywords.CarsItem>arrayList=new ArrayList<>();


    ImageView img_home,ic_menu;
    LinearLayout ll_searchbar;
    EditText searchview;
    ImageView searhview_icon;
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;
    MyDataBase myDataBase;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    CustomRegularButton bt_signin;
    CustomTextViewSemiBold tv_accountname;
    CustomRegularTextview nav_home,nav_favourite,nav_cart,nav_orders,nav_chat,nav_logout,nav_account,nav_notification;
    CustomBoldTextview sidenav_notification_count;
    CardView cv_notificationcount;
    LinearLayout ll_logout;
    ArrayList<QuoteDataItemDB> dataItemDBS=new ArrayList<>();
    androidx.cardview.widget.CardView cv_notification_count,cv_nav_cartcount;
    CustomBoldTextview sidenav_cart_count;
    String notify_count="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview_layout);
        homeActivityViewModel = ViewModelProviders.of(this).get(HomeActivityViewModel.class);
        context = this;
        sharedPref = new SharedPref(context);
        myDataBase = new MyDataBase(context);
        findbyviews();
        clicklistener();


        Func_AppInfo();
         searchview.requestFocus();
            Func_loginorNot();

        searchview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                St_searchedstring = s.toString();
                autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                autoCompleteHandler.postDelayed(autoCompleteRunnable, 1000);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                if(St_searchedstring.length()<1)
                {
                }
                else
                {
                     Service_SearchKeyword();
                }

            }
        };
    }

    private void Func_loginorNot() {

        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN)=="")
        {
            bt_signin.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.GONE);
            ll_logout.setVisibility(View.GONE);
            cv_notificationcount.setVisibility(View.GONE);
            if(myDataBase!=null) {
                dataItemDBS = myDataBase.getAlldataforquote();
                if(dataItemDBS.size()==0)
                {

                    cv_nav_cartcount.setVisibility(View.GONE);
                }
                else {
                    cv_nav_cartcount.setVisibility(View.VISIBLE);
                    String cart_count=String.valueOf(dataItemDBS.size());
                    sharedPref.setString(Constants.PREF_Cartcount,cart_count);
                    sidenav_cart_count.setText(cart_count);

                }
            }

        }
        else
        {
            bt_signin.setVisibility(View.GONE);
            ll_logout.setVisibility(View.VISIBLE);
            tv_accountname.setVisibility(View.VISIBLE);
            tv_accountname.setText(sharedPref.getString(Constants.PREF_USERNAME));
            notify_count=sharedPref.getString(Constants.PREF_UNREAD_NOTIFICATION);
            cv_notificationcount.setVisibility(View.VISIBLE);
            sidenav_notification_count.setText(notify_count);

            getquote_func();

        }
    }



    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        homeActivityViewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                    if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                        cv_notificationcount.setVisibility(View.GONE);
                    }
                    else {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                        String count = response_appInfo.getData().getUnreadNotificationCount();
                        if(count.equals("0")) {

                            cv_notificationcount.setVisibility(View.GONE);

                        }
                        else
                        {

                            cv_notificationcount.setVisibility(View.VISIBLE);
                            sidenav_notification_count.setText(count);
                        }
                    }

                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }



    private void getquote_func() {

        HashMap<String,String>header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        homeActivityViewModel.getquotedata(header).observe(this, new Observer<Response_GetQuote>() {
            @Override
            public void onChanged(Response_GetQuote response_getQuote) {

                if(response_getQuote.getStatus().equals("1")) {

                    String cart_count=String.valueOf(response_getQuote.getData().getQuoteData().size());
                    if (Integer.parseInt(cart_count)==0)
                    {

                        cv_nav_cartcount.setVisibility(View.GONE);
                    }
                    else {
                        cv_nav_cartcount.setVisibility(View.VISIBLE);
                        sharedPref.setString(Constants.PREF_Cartcount, cart_count);
                        sidenav_cart_count.setText(sharedPref.getString(Constants.PREF_Cartcount));
                    }
                }
            }

        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        sidenav_notification_count.setText(notify_count);
        Func_AppInfo();
    }

    private void findbyviews() {

        sidenav_cart_count=findViewById(R.id.sidenav_cart_count);
        cv_nav_cartcount=findViewById(R.id.cv_nav_cartcount);
        cv_notificationcount=findViewById(R.id.cv_notificationcount);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation);

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        ll_logout=findViewById(R.id.ll_logout);
        nav_logout=findViewById(R.id.nav_logout);
        nav_home=findViewById(R.id.nav_home);
        nav_cart=findViewById(R.id.nav_cart);
        nav_favourite=findViewById(R.id.nav_favourite);
        nav_orders=findViewById(R.id.nav_orders);
        nav_chat=findViewById(R.id.nav_chat);
        bt_signin=findViewById(R.id.bt_signin);
        tv_accountname=findViewById(R.id.tv_accountname);


        nav_account=findViewById(R.id.nav_account);
        nav_notification=findViewById(R.id.nav_notification);


        sidenav_notification_count=findViewById(R.id.sidenav_notification_count);

        rv_searchlist=findViewById(R.id.rv_searchlist);
        img_home=findViewById(R.id.img_home);
        ll_searchbar=findViewById(R.id.ll_searchbar);
        ic_menu=findViewById(R.id.ic_menu);
        searchview=findViewById(R.id.searchview);
        searhview_icon=findViewById(R.id.searhview_icon);

        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_searchlist.setLayoutManager(linearLayoutManager);
        rv_searchlist.setHasFixedSize(true);
    }

    private void clicklistener() {
        img_home.setOnClickListener(this);
        ic_menu.setOnClickListener(this);
        ll_searchbar.setOnClickListener(this);
        searhview_icon.setOnClickListener(this);
        searchview.setOnClickListener(this);

        nav_account.setOnClickListener(this);
        nav_notification.setOnClickListener(this);
        tv_accountname=findViewById(R.id.tv_accountname);

        nav_home.setOnClickListener(this);
        nav_cart.setOnClickListener(this);
        nav_favourite.setOnClickListener(this);
        nav_orders.setOnClickListener(this);
        nav_chat.setOnClickListener(this);
        bt_signin.setOnClickListener(this);
        nav_logout.setOnClickListener(this);

    }

    private void setupDrawerContent(NavigationView navigationView) {

        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });

    }



    private void Service_SearchKeyword() {
        Commons.showProgress(context);
        HashMap<String, String> request = new HashMap<>();
        request.put("search_keywords", St_searchedstring);


        homeActivityViewModel.getSearcheddata(request).observe(this, new Observer<Response_SearchbyKeywords>() {
            @Override
            public void onChanged(Response_SearchbyKeywords response_searchbyKeywords) {
                Commons.hideProgress();
                if (response_searchbyKeywords.status.equals("1")) {
                    System.out.println("==response==delet fav===" + response_searchbyKeywords.toString());
                    arrayList=response_searchbyKeywords.data.cars;
                    searchListAdapter=new SearchListAdapter(context,arrayList);
                    rv_searchlist.setAdapter(searchListAdapter);
                }
                else if(response_searchbyKeywords.status.equals("0"))
                {
                  //  Commons.Alert_custom(context,response_searchbyKeywords.message);
                }
            }
        });
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.img_home:
                onBackPressed();
                break;
            case R.id.ll_searchbar:
                searchview.setCursorVisible(true);
                break;
            case R.id.searhview_icon:
                searchview.setCursorVisible(true);
                break;
            case R.id.searchview:
                searchview.setCursorVisible(true);
                break;
            case R.id.nav_account:
                System.out.println("==Menu===");
                Intent tohomes = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohomes.putExtra("tabposition", "useraccount");
                startActivity(tohomes);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case R.id.nav_notification:
                drawer.closeDrawer(Gravity.RIGHT);
                Intent tonotifify=new Intent(this,NotificationList_Activity.class);
                startActivity(tonotifify);
                break;


            case R.id.ic_menu:
                drawer.openDrawer(GravityCompat.END);
                closeKeyboard();
                break;
            case  R.id.nav_home:
                System.out.println("==Menu===");
                Intent tohome = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tohome.putExtra("tabposition", "home");
                startActivity(tohome);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_favourite:
                Intent tocart = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart.putExtra("tabposition", "cart");
                tocart.putExtra("position_tabs","0");
                startActivity(tocart);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);

                break;

            case  R.id.nav_cart:
                Intent tocart1 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart1.putExtra("tabposition", "cart");
                tocart1.putExtra("position_tabs","1");
                startActivity(tocart1);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_orders:
                Intent tocart2 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart2.putExtra("tabposition", "cart");
                tocart2.putExtra("position_tabs","2");
                startActivity(tocart2);
                finish();
                drawer.closeDrawer(Gravity.RIGHT);
                break;
            case  R.id.nav_chat:
                Intent tocart3 = new Intent(this, HomeActivity_searchimage_BottomButton.class);
                tocart3.putExtra("tabposition", "chat");
                startActivity(tocart3);
                finish();
                System.out.println("==Menu===");
                drawer.closeDrawer(Gravity.RIGHT);
                break;

            case  R.id.nav_logout:
                System.out.println("==Menu===");
                Intent gotoLogin = new Intent(context, LoginActivity.class);
                System.out.println("===vc===" + sharedPref.getString(Constants.JWT_TOKEN));
                sharedPref.clearAll();
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoLogin);
                drawer.closeDrawer(Gravity.RIGHT);
                finish();
                break;

            case R.id.bt_signin:
                Intent tosignin=new Intent(this,LoginActivity.class);
                startActivity(tosignin);
                drawer.closeDrawer(Gravity.RIGHT);
                finish();
                break;






        }

    }
}
