package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.NonSwipeableViewPager;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.view.Activity.HomeActivity_searchimage_BottomButton;
import com.app.alliedmotor.view.Adapter.PagerAdapter_Favourite;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;

public class Cart_Fragment extends Fragment {


    TabLayout rv_categories;
    NonSwipeableViewPager viewpager;
    View root;
    SharedPref sharedPref;
    Context context;
    int tabposition=0;
    String ticketid="";

    public Cart_Fragment(int tabposition,String ticketid) {
        this.tabposition=tabposition;
        this.ticketid=ticketid;
    }
    public Cart_Fragment() {
    }

   /* private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                _hasLoadedOnce = true;
            }
        }
    }*/



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (root == null)
        {
            root = inflater.inflate(R.layout.fragment_cart, container,false);
            context = getActivity();
            sharedPref = new SharedPref(context);

            findbyviews();
            clicklistener();
            Method_CarCategoryList();
            rv_categories.getTabAt(tabposition).select();
        }

       else {
            ((ViewGroup) root.getParent()).removeView(root);
        }

        return root;
    }


   /* @Override
    public void onResume() {
        super.onResume();
        System.out.println("====>OnResumecart call---->");
          }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("====>cartpausecall---->");
    }*/


    private void findbyviews() {
        rv_categories = root.findViewById(R.id.rv_categories);
        viewpager = root.findViewById(R.id.viewpager);

    }

    private void clicklistener() {

    }

    private void Method_CarCategoryList() {

        rv_categories.addTab(rv_categories.newTab().setText("Favourites"));
        rv_categories.addTab(rv_categories.newTab().setText("Add to Quote"));
        rv_categories.addTab(rv_categories.newTab().setText("Your Activities"));


        rv_categories.setTabGravity(TabLayout.GRAVITY_FILL);
        //rv_categories.setupWithViewPager(viewpager);
        PagerAdapter_Favourite adapter = new PagerAdapter_Favourite(getChildFragmentManager(), rv_categories.getTabCount(),ticketid);
        //Adding adapter to pager
        viewpager.setAdapter(adapter);
        //viewpager.setOffscreenPageLimit(1);
        //Adding onTabSelectedListener to swipe views

        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(rv_categories));
        onTabSelectedListener(viewpager);
        rv_categories.setOnTabSelectedListener(onTabSelectedListener(viewpager));


    }


    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
}
