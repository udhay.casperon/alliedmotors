package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.DummyStringPojo;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;

import java.util.ArrayList;

public class CarDetailAdapter_IFeatures_sample extends RecyclerView.Adapter {


   public interface Callbacks {
        public void onClickLoadMore();
    }

    private Callbacks mCallbacks;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean mWithHeader = false;
    private boolean mWithFooter = false;


  //  ArrayList<String> features;

    int limit=8;
    SharedPref sharedPref;
    private Context mcontext;
    LinearLayoutManager linearLayoutManager;

    private ArrayList<DummyStringPojo> features;

    public CarDetailAdapter_IFeatures_sample(Context context, ArrayList<DummyStringPojo> features) {
        this.mcontext = context;
        this.features = features;


    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;

        if (viewType == TYPE_FOOTER) {

            itemView = View.inflate(parent.getContext(), R.layout.load_more, null);
            return new LoadMoreViewHolder(itemView);

        } else {

            itemView = View.inflate(parent.getContext(), R.layout.features_rv, null);
            return new ViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof LoadMoreViewHolder) {

            LoadMoreViewHolder loadMoreViewHolder = (LoadMoreViewHolder) holder;

            loadMoreViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mCallbacks!=null)
                        mCallbacks.onClickLoadMore();
                }
            });

        } else {
            ViewHolder elementsViewHolder = (ViewHolder) holder;

            ((ViewHolder) holder).tv_points.setText(AppInstalled.toTitleCase(features.get(position).getStringvalue().toLowerCase()));
        }

    }

    @Override
    public int getItemCount() {
        int itemCount = features.size();
        if (mWithHeader)
            itemCount++;
        if (mWithFooter)
            itemCount++;
        return itemCount;
        /*if(features.size() > limit){

            return limit;
        }
        else
        {
            return features.size();
        }
*/

    }
    @Override
    public int getItemViewType(int position) {
        if (mWithHeader && isPositionHeader(position))
            return TYPE_HEADER;
        if (mWithFooter && isPositionFooter(position))
            return TYPE_FOOTER;
        return TYPE_ITEM;
    }

    public boolean isPositionHeader(int position) {
        return position == 0 && mWithHeader;
    }

    public boolean isPositionFooter(int position) {
        return position == getItemCount() - 1 && mWithFooter;
    }

    public void setWithHeader(boolean value){
        mWithHeader = value;
    }

    public void setWithFooter(boolean value){
        mWithFooter = value;
    }

    public void setCallback(Callbacks callbacks){
        mCallbacks = callbacks;
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

       CustomRegularTextview tv_points;
        CustomRegularTextview tv_showmore;

        public ViewHolder(View itemView) {

            super(itemView);

            tv_points=itemView.findViewById(R.id.tv_points);


        }
    }

    public class LoadMoreViewHolder  extends RecyclerView.ViewHolder {

        public LoadMoreViewHolder(View itemView) {
            super(itemView);
        }
    }
}

