package com.app.alliedmotor.view.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Signup.Response_SignupResponse;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.CountryDialCode;
import com.app.alliedmotor.utility.GPSTracker;
import com.app.alliedmotor.utility.PasswordValidator;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.viewmodel.SignupviewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class Signup_Activity extends AppCompatActivity implements View.OnClickListener {

    private Context context;

    String FCM_token="";
    RadioButton rb_company, rb_indivdual;
    EditText et_firstname, et_lastname,et_mobile, et_email, et_company, et_role, et_password, et_confirm_password;
    CheckBox checkbox1;
    Button bt_submit;
    RadioGroup rg_selection;
    CustomRegularTextview tv_guestuser;
    String st_firstname, st_lastname, st_email, st_mobile, st_company, st_role, st_password, st_confirm_password, st_usertype, st_enable_newsletter;
    LinearLayout ll_company_role;
    PasswordValidator passwordValidator;
    private SignupviewModel signupviewModel;
    SharedPref sharedPref;
 String st_selectionphone,st_selectionemail,st_otptype;

    private LinearLayout myCountryCodeLAY;
    private ImageView myFlagIMG;
    private TextView myCountryCodeTXT;
    private CountryPicker myPicker;
    private String myFlagStr = "",st_full_mobile;
    private GPSTracker myGPSTracker;
    CustomTextViewSemiBold tv_learnmore;
    CustomRegularButton bt_signin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signupviewModel = ViewModelProviders.of(this).get(SignupviewModel.class);
        context = this;
        passwordValidator = new PasswordValidator();
        sharedPref = new SharedPref(context);

        findbyviews();
        clicklistener();
        classAndWidgetInitialize();
        st_enable_newsletter="no";
        final Animation slide_up_in = AnimationUtils.loadAnimation(
                getApplicationContext(), R.anim.bottom_up);
        final Animation slide_up_out = AnimationUtils.loadAnimation(
                getApplicationContext(), R.anim.bottom_down);
        st_usertype = "company";
        rg_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rb_company) {
                    ll_company_role.startAnimation(slide_up_in);
                    ll_company_role.setVisibility(View.VISIBLE);
                    st_usertype = "company";
                } else if (checkedId == R.id.rb_indivdual) {
                    ll_company_role.startAnimation(slide_up_out);
                    ll_company_role.setVisibility(View.GONE);
                    st_usertype = "customer";
                }
            }
        });


        checkbox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkbox1.isChecked()) {
                    st_enable_newsletter = "yes";
                } else {
                    st_enable_newsletter = "No";
                }
            }
        });



        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        String cod=tm.getNetworkCountryIso();
        System.out.println("==countrycode==="+countryCodeValue+"88==>"+cod);
        Log.e("coutrycod==>",countryCodeValue);
        Log.e("cpod==>",cod);

    }

    private void classAndWidgetInitialize() {

        myPicker = CountryPicker.newInstance("Select Country");
        myPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flag) {
                myPicker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                myFlagIMG.setVisibility(View.VISIBLE);
                myFlagStr = "" + flag;
                myFlagIMG.setImageResource(flag);
                String  exampleNumber= String.valueOf(PhoneNumberUtil.getInstance().getExampleNumber(code).getNationalNumber());
                et_mobile.setFilters( new InputFilter[] {new InputFilter.LengthFilter(exampleNumber.length())});  //mypicker.setlistener

            }
        });
        setCountryDataValues();

    }

    private void findbyviews() {

        tv_learnmore = findViewById(R.id.tv_learnmore);
        bt_signin = findViewById(R.id.bt_signin);


        ll_company_role = findViewById(R.id.ll_company_role);
        rb_company = findViewById(R.id.rb_company);
        rb_indivdual = findViewById(R.id.rb_indivdual);
        et_firstname = findViewById(R.id.et_firstname);
        et_lastname = findViewById(R.id.et_lastname);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_role = findViewById(R.id.et_role);
        et_company = findViewById(R.id.et_company);
        checkbox1 = findViewById(R.id.checkbox1);
        bt_submit = findViewById(R.id.bt_submit);
        rg_selection = findViewById(R.id.rg_selection);
        et_password = findViewById(R.id.et_password);
        et_confirm_password = findViewById(R.id.et_confirm_password);
        tv_guestuser = findViewById(R.id.tv_guestuser);
        myGPSTracker = new GPSTracker(Signup_Activity.this);
        myCountryCodeLAY = (LinearLayout)findViewById(R.id.activity_signup_mobile_no_LAY1);
        myFlagIMG = (ImageView) findViewById(R.id.activity_signup_country_flag_IMG1);
        myCountryCodeTXT = findViewById(R.id.activity_signup_country_code_TXT1);

    }

    private void clicklistener() {
        bt_submit.setOnClickListener(this);
        tv_guestuser.setOnClickListener(this);
        myCountryCodeLAY.setOnClickListener(this);
        tv_learnmore.setOnClickListener(this);
        bt_signin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.bt_submit:
                Validation();
                break;

            case R.id.tv_learnmore:
                Intent guestuserIntent2=new Intent(Signup_Activity.this, PrivacyPolicy_Activity.class);
                startActivity(guestuserIntent2);
                break;

            case R.id.bt_signin:
                Intent guestuserIntent1=new Intent(Signup_Activity.this, LoginActivity.class);
                startActivity(guestuserIntent1);
                finish();
                break;


            case R.id.tv_guestuser:
                Intent guestuserIntent=new Intent(Signup_Activity.this, HomeActivity_searchimage_BottomButton.class);
                sharedPref.setString(Constants.JWT_TOKEN,"");
                startActivity(guestuserIntent);
                finish();
                break;

            case R.id.activity_signup_mobile_no_LAY1:
                myPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;

        }
    }

    private void Validation() {
        st_firstname = et_firstname.getText().toString().trim();
        st_lastname = et_lastname.getText().toString().trim();
        st_email = et_email.getText().toString().trim();
        st_mobile = et_mobile.getText().toString().trim();
        st_company = et_company.getText().toString().trim();
        st_role = et_role.getText().toString().trim();
        st_password = et_password.getText().toString().trim();
        st_confirm_password = et_confirm_password.getText().toString().trim();
        st_full_mobile=myCountryCodeTXT.getText().toString().trim()+""+et_mobile.getText().toString().trim();
        System.out.println("===string--fullmobile---"+st_full_mobile);
        if (st_firstname.isEmpty() && st_lastname.isEmpty() && st_email.isEmpty() && st_full_mobile.isEmpty() && st_company.isEmpty() && st_role.isEmpty() && st_password.isEmpty() && st_confirm_password.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter the all Details");
        } else if (st_firstname.isEmpty()) {
            et_firstname.requestFocus();
            Commons.Alert_custom(context, "Please Enter Firstname");
        } /*else if (st_lastname.isEmpty()) {
            et_lastname.requestFocus();
            Commons.Alert_custom(context, "Please Enter Lastname");
        }*/ else if (st_email.isEmpty()) {
            et_email.requestFocus();
            Commons.Alert_custom(context, "Please enter Email id");
        } else if (!validEmail(st_email)) {
            Commons.Alert_custom(context, "Please enter Valid Email id");
            et_email.requestFocus();
        }
        else if (st_password.isEmpty()) {

            Commons.Alert_custom(context, "Please Enter Password");
            et_password.requestFocus();
        } else if (!passwordValidator.validate(st_password)) {

            Commons.Alert_custom(context,"Minimum of 8 characters in length, Should contain atleast one uppercase,lowercase,number and special Character");
            //  et_password.setError("Must contains atleast one digit, one lowercase,one Uppercase , special character  and at least 8 characters");
            et_password.requestFocus();
        } else if (st_confirm_password.isEmpty()) {
            Commons.Alert_custom(context, "Please Enter confirm password");
            et_confirm_password.requestFocus();
        } else if (!passwordValidator.validate(st_confirm_password)) {
            Commons.Alert_custom(context,"Minimum of 8 characters in length, Should contain atleast one uppercase,lowercase,number and special Character");
            et_password.requestFocus();
        } else if (!st_password.equals(st_confirm_password)) {
            et_confirm_password.requestFocus();
            Commons.Alert_custom(context, "The Password and confirm password not same");
        }
        else if (st_mobile.isEmpty()) {

            System.out.println("===string--mobile-vvv--"+st_full_mobile);
            Commons.Alert_custom(context, "Please Enter Mobile number");
            et_mobile.requestFocus();
        }
       else if(rb_company.isChecked())
       {
           if(st_company.isEmpty() && st_role.isEmpty())
           {
               Commons.Alert_custom(context, "Please Enter company name and role");
               et_company.requestFocus();
           }
           else if(st_company.isEmpty())
           {
               Commons.Alert_custom(context,"Please Enter Company name");
               et_company.requestFocus();
           }
           else if(st_role.isEmpty())
           {
               Commons.Alert_custom(context,"Please Enter Role");
               et_role.requestFocus();
           }
           else
           {
               dialog_Verify();
           }
       }
       else if(rb_indivdual.isChecked())
        {
            st_company="";
            st_role="";
            dialog_Verify();
        }

    }

    private void setCountryDataValues() {
        if (myGPSTracker.canGetLocation() && myGPSTracker.isgpsenabled()) {
            double MyCurrent_lat = myGPSTracker.getLatitude();
            double MyCurrent_long = myGPSTracker.getLongitude();
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String aCountryCodeStr = addresses.get(0).getCountryCode();
                    if (aCountryCodeStr.length() > 0 && !aCountryCodeStr.equals(null) && !aCountryCodeStr.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(aCountryCodeStr);
                        myCountryCodeTXT.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + aCountryCodeStr.toLowerCase(Locale.ENGLISH);
                        myFlagIMG.setImageResource(getResId(drawableName));
                        myFlagStr = "" + getResId(drawableName);
                        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(Str_countyCode));
                        String  exampleNumber= String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                        int phoneLength=exampleNumber.length();
                        et_mobile.setFilters( new InputFilter[] {
                                new InputFilter.LengthFilter(phoneLength)});
                        et_mobile.setEms(phoneLength);
                        System.out.println("==d==="+et_mobile.getText().toString().trim());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getResId(String drawableName) {
        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
        }
        return -1;
    }


    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void Method_signup() {
        Commons.showProgress(context);
        HashMap<String, String> request_signup = new HashMap<>();
        request_signup.put("first_name", st_firstname);
        request_signup.put("last_name", st_lastname);
        request_signup.put("user_email", st_email);
        request_signup.put("mobile_number", st_full_mobile);
        request_signup.put("user_type", st_usertype);
        request_signup.put("company_name", st_company);
        request_signup.put("company_role", st_role);
        request_signup.put("enable_newsletter", st_enable_newsletter);
        request_signup.put("password", st_password);
        request_signup.put("otp_type",st_otptype);


        System.out.println("==req===signup===" + request_signup);

        signupviewModel.getSignupData(request_signup).observe(this, new Observer<Response_SignupResponse>() {
            @Override
            public void onChanged(Response_SignupResponse signupResponse) {
                Commons.hideProgress();
                System.out.println("===res=signup==" + signupResponse.toString());
                if(signupResponse.getStatus().equals("1")) {
                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getCode());

                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getMobileNumber());
                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getUserEmail());
                    System.out.println("gdgd===="+signupResponse.getData().getOtpData().getOtpType());
                    Intent tomain = new Intent(Signup_Activity.this, OTPVerification_SignupActivity.class);
                    tomain.putExtra("fname",signupResponse.getData().getFirstName());
                    tomain.putExtra("lname",signupResponse.getData().getLastName());
                    tomain.putExtra("useremail",signupResponse.getData().getUserEmail());
                    tomain.putExtra("mobilenumber",signupResponse.getData().getMobileNumber());
                    tomain.putExtra("usertype",signupResponse.getData().getUserType());
                    tomain.putExtra("companyname",signupResponse.getData().getCompanyName());
                    tomain.putExtra("companyuserole",signupResponse.getData().getCompanyUserRole());
                    tomain.putExtra("useletter",signupResponse.getData().getEnableNewsletter());
                    tomain.putExtra("password",signupResponse.getData().getPassword());
                    tomain.putExtra("otptype",signupResponse.getData().getOtpType());
                    tomain.putExtra("contentmessage",signupResponse.getLongMessage());
                    tomain.putExtra("otpdevmode",String.valueOf(signupResponse.getData().getOtpData().getDevMode()));
                    tomain.putExtra("otptoken",signupResponse.getData().getOtpData().getCode());
                    startActivity(tomain);
                    finish();

                }

                else if(signupResponse.getStatus().equals("0"))
                {
                    if(signupResponse.getErrors().getMobile_number()!=null)
                    {
                        Commons.Alert_custom(context,signupResponse.getErrors().getMobile_number());
                    }
                    else if(signupResponse.getErrors().getUserEmail()!=null) {
                        Commons.Alert_custom(context, signupResponse.getErrors().getUserEmail());
                    }


                }
            }
        });
    }


    private void dialog_Verify() {
        final Dialog dialog = new Dialog(Signup_Activity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        dialog.setContentView(R.layout.dialog_mobileverfiy);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height

        ImageView imgclose;
        RadioButton rb_emailverify,rb_phoneverify;
        CustomRegularButton bt_validateotp;

        imgclose=dialog.findViewById(R.id.close_img);
        rb_emailverify=dialog.findViewById(R.id.rb_emailverify);
        rb_phoneverify=dialog.findViewById(R.id.rb_phoneverify);
        bt_validateotp=dialog.findViewById(R.id.bt_validateotp);

        //rb_emailverify.setChecked(true);
       // st_otptype="email";
        rb_emailverify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_emailverify.setChecked(true);
                rb_phoneverify.setChecked(false);
                st_otptype="email";

            }
        });

        rb_phoneverify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_phoneverify.setChecked(true);
                rb_emailverify.setChecked(false);
                st_otptype="phone";
            }
        });


        bt_validateotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!rb_emailverify.isChecked()&& !rb_phoneverify.isChecked())
                {
                    Commons.Alert_custom(context,"Please select verification type");
                }
                else {
                    Method_signup();
                    dialog.dismiss();
                }
                          }
        });

        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Commons.hideProgress();
            }
        });





        dialog.show();

    }




}