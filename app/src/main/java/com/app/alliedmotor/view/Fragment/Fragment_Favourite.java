package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.DeleteFavourite.Response_DeleteFavourite;
import com.app.alliedmotor.model.MyFavourite.DataItem;
import com.app.alliedmotor.model.MyFavourite.Response_MyFavourite;
import com.app.alliedmotor.model.Response_Addquote;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Adapter.MyFavourite_Adapter;
import com.app.alliedmotor.viewmodel.MyFavoouriteviewModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;


public class Fragment_Favourite extends Fragment implements MyFavourite_Adapter.I_MyFavourite {

    CheckBox checkbox;

    RecyclerView rv_favourite;
    LinearLayout ll_product_no_found;
    LinearLayoutManager linearLayoutManager;
    View view;
    CustomRegularButton bt_delete, bt_addtoquote;
    MyFavoouriteviewModel myFavoouriteviewModel;
    MyFavourite_Adapter myFavourite_adapter;
    ArrayList<DataItem> dataItemArrayList = new ArrayList<>();
    SharedPref sharedPref;
    Context context;
    LinearLayout loading_ll, ll_empty,button_ll,ll_loginfeature;
    RelativeLayout main_ll;
    ArrayList<String>selected_data=new ArrayList<>();
    int []list_fav;
    CustomTextViewSemiBold tv_loginfeature;
    MyDataBase myDataBase;
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;
    LinearLayout ll_loginfeature_text;
    ArrayList<Boolean>isslectedvalue=new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_favourite_relative, container, false);
            myFavoouriteviewModel = ViewModelProviders.of(this).get(MyFavoouriteviewModel.class);
            context = getActivity();
            sharedPref = new SharedPref(context);

            findviews();

            if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
            {
                loading_ll.setVisibility(View.GONE);
                ll_loginfeature.setVisibility(View.VISIBLE);
                ll_empty.setVisibility(View.GONE);
                main_ll.setVisibility(View.GONE);

            }
            else
            {
                ApiCall_MyFavourite();
            }


            ll_loginfeature_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tologin=new Intent(context, LoginActivity.class);
                    startActivity(tologin);
                    getActivity().finish();
                }
            });



            bt_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                    autoCompleteHandler.postDelayed(autoCompleteRunnable, 1000);
                }
            });


            autoCompleteHandler = new Handler();
            autoCompleteRunnable = new Runnable() {
                public void run() {
                    if(selected_data.size()==0)
                    {
                        Commons.Alert_custom(context,"Please Select data");
                    }
                    else {
                        Apicall_DeleteFavourite(selected_data);
                    }
                }
            };

            bt_addtoquote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(selected_data.size()==0)
                    {
                        Commons.Alert_custom(context,"Please Select the data");
                    }
                    else {
                        Api_AddQuote(selected_data);
                    }
                }
            });

        }


        tv_loginfeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tologin=new Intent(context, LoginActivity.class);
                startActivity(tologin);
                getActivity().finish();
            }
        });
        return view;
    }

    private void Api_AddQuote(ArrayList<String> data) {
        HashMap<String, String> header = new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        for(int k=0;k<dataItemArrayList.size();k++)
        {
            if(dataItemArrayList.get(k).isSelected()==true) {
                selected_data.add(dataItemArrayList.get(k).getFav_id());
            }
        }
        System.out.println("===ss===="+selected_data.size());

        HashMap<String, String> request = new HashMap<>();
        for(int i=0;i<data.size();i++) {
            request.put("car_id[" + i + "]", selected_data.get(i));
        }
        request.put("type", "favorite");
        request.put("quantity", "1");
        request.put("exterior_color", "");
        request.put("interior_color", "");

            Commons.showProgress(context);
        myFavoouriteviewModel.addquoteLiveData(header,request).observe(getActivity(), new Observer<Response_Addquote>() {
            @Override
            public void onChanged(Response_Addquote response_addquote) {
                Commons.hideProgress();
                loading_ll.setVisibility(View.GONE);
                if (response_addquote.status.equals("1")) {
                    System.out.println("===res=addquote=" + response_addquote.toString());
                    Log.i("fav_frag-->", response_addquote.toString());
                    ApiCall_MyFavourite();

                    TabLayout tabs = (TabLayout)(getActivity()).findViewById(R.id.rv_categories);
                    tabs.getTabAt(1).select();
                  //  Commons.Alert_custom_Title(context,"Allied Motors","Selected Favourite item added to Quote List successfully");
                       String content="Your favourite's cars is added to the quotation list";
                       Commons.Alert_custom(context,content);
                    //Fun_MoveTab(content);


                } else if (response_addquote.status.equals("0")) {
                    Log.i("-->res addquote==", response_addquote.toString());
                   // Toast.makeText(context, response_addquote.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void findviews() {

        checkbox = view.findViewById(R.id.checkbox);
        rv_favourite = view.findViewById(R.id.rv_favourite);
        bt_delete = view.findViewById(R.id.bt_delete);
        bt_addtoquote = view.findViewById(R.id.bt_addtoquote);
        loading_ll = view.findViewById(R.id.loading_ll);
        main_ll = view.findViewById(R.id.main_ll);
        tv_loginfeature=view.findViewById(R.id.tv_loginfeature);
        ll_loginfeature_text=view.findViewById(R.id.ll_loginfeature_text);
        ll_loginfeature=view.findViewById(R.id.ll_loginfeature);
        ll_empty = view.findViewById(R.id.ll_empty);
        button_ll=view.findViewById(R.id.button_ll);
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_favourite.setLayoutManager(linearLayoutManager);
        rv_favourite.setHasFixedSize(true);

    }

    private void ApiCall_MyFavourite() {


        HashMap<String, String> header = new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));


        loading_ll.setVisibility(View.VISIBLE);
        myFavoouriteviewModel.getmyfavouritedata(header).observe(getActivity(), new Observer<Response_MyFavourite>() {
            @Override
            public void onChanged(Response_MyFavourite response_myFavourite) {
                Commons.hideProgress();
                loading_ll.setVisibility(View.GONE);
                if (response_myFavourite.getStatus().equals("1")) {
                    System.out.println("===res=my favouirte=" + response_myFavourite.toString());
                    Log.i("fav_frag-->", response_myFavourite.toString());
                    dataItemArrayList = response_myFavourite.getData();
                        main_ll.setVisibility(View.VISIBLE);
                        ll_empty.setVisibility(View.GONE);
                        button_ll.setVisibility(View.VISIBLE);
                        func_adpater();

                } else if (response_myFavourite.getStatus().equals("0")) {
                    Log.i("-->res onstaus0==", response_myFavourite.toString());
                    Commons.hideProgress();
                    ll_empty.setVisibility(View.VISIBLE);
                    main_ll.setVisibility(View.GONE);
                    button_ll.setVisibility(View.GONE);
                  //  Toast.makeText(context, response_myFavourite.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void func_adpater() {

        myFavourite_adapter = new MyFavourite_Adapter(context, dataItemArrayList,this);
        rv_favourite.setAdapter(myFavourite_adapter);

        for (DataItem model : dataItemArrayList) {
            if(model.isSelected())
                isslectedvalue.add(model.isSelected());
        }
        checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkbox.isChecked()) {

                    for (DataItem model : dataItemArrayList) {
                        model.setSelected(true);

                        for(int k=0;k<dataItemArrayList.size();k++)
                        {
                            selected_data.add(dataItemArrayList.get(k).getFav_id());
                        }
                    }
                } else {

                    for (DataItem model : dataItemArrayList) {
                        model.setSelected(false);
                    }
                }
                myFavourite_adapter.notifyDataSetChanged();
            }

        });




    }


    @Override
    public void on_CarSelected(DataItem dataItem, int position) {
        Log.i("CarSelected==>",dataItem.getFav_id());

        System.out.println("---data---"+selected_data+"==="+position);

        for(int k=0;k<dataItemArrayList.size();k++)
        {
            if(dataItemArrayList.get(k).isSelected()==true) {
                if(selected_data.size()==0) {
                    selected_data.add(dataItemArrayList.get(k).getFav_id());
                }
                else {
                    for(int z=0;z<selected_data.size();z++) {
                        if (dataItemArrayList.get(k).getFav_id() != selected_data.get(z)) {
                           if(!selected_data.contains(dataItemArrayList.get(k).getFav_id()))
                                selected_data.add(dataItemArrayList.get(k).getFav_id());
                        }
                    }
                }
            }
            else if(dataItemArrayList.get(k).isSelected()==false)
            {
                for(int x=0;x<selected_data.size();x++)
                {
                    if(dataItemArrayList.get(k).getFav_id()==selected_data.get(x))
                    {
                        selected_data.remove(dataItemArrayList.get(k).getFav_id());
                    }
                }
            }
        }


        if(selected_data.size()==dataItemArrayList.size())
        {
            checkbox.setChecked(true);

        }
        else {
            checkbox.setChecked(false);
        }



    }



    private void Apicall_DeleteFavourite(ArrayList<String> data) {

        HashMap<String, String> header = new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        Log.i("fav_frag-req->", sharedPref.getString(Constants.JWT_TOKEN));


        for(int k=0;k<dataItemArrayList.size();k++)
        {
            if(dataItemArrayList.get(k).isSelected()==true) {
                selected_data.add(dataItemArrayList.get(k).getFav_id());
            }
        }
        System.out.println("===ss===="+selected_data.size());
        HashMap<String, String> request = new HashMap<>();
        for(int i=0;i<data.size();i++) {
            request.put("car_id[" + i + "]", selected_data.get(i));
        }
        Commons.showProgress(context);
        myFavoouriteviewModel.deleteFavourite(header,request).observe(getActivity(), new Observer<Response_DeleteFavourite>() {
            @Override
            public void onChanged(Response_DeleteFavourite response_deleteFavourite) {
                Commons.hideProgress();
              //  loading_ll.setVisibility(View.GONE);
                if (response_deleteFavourite.getStatus().equals("1")) {
                    //Commons.Alert_custom(context,response_deleteFavourite.getMessage());
                    Commons.Alert_custom(context,"Favourite item removed successfully");
                    System.out.println("==response==delet fav===" + response_deleteFavourite.toString());
                    if(myFavourite_adapter!=null)
                    {
                        myFavourite_adapter.notifyDataSetChanged();
                    }

                }
                else if(response_deleteFavourite.getStatus().equals("0"))
                {
                    Commons.Alert_custom(context,response_deleteFavourite.getMessage());
                    System.out.println("==response==delet fav===" + response_deleteFavourite.toString());
                }
                ApiCall_MyFavourite();
            }
        });


    }





    @Override
    public void onResume() {
        super.onResume();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            loading_ll.setVisibility(View.GONE);
            ll_loginfeature.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
            main_ll.setVisibility(View.GONE);

        }
        else
        {
            ApiCall_MyFavourite();
        }

    }
}