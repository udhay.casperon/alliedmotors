package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.ChangePassword.Response_ChangePassword;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.PasswordValidator;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.viewmodel.ChangePasswordviewModel;

import java.util.HashMap;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener{

    private Context context;
    ImageView back_setting;
    CustomRegularEditText et_currentpwd,et_newpwd,et_confirmpwd;
    CustomRegularButton bt_submit;
    String st_currentpwd,st_newpwd,st_confirmpwd;
    PasswordValidator passwordValidator;
    SharedPref sharedPref;
    ChangePasswordviewModel changePasswordviewModel;
    LinearLayout loading_ll;
    MyDataBase myDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        changePasswordviewModel = ViewModelProviders.of(this).get(ChangePasswordviewModel.class);
        context=this;
            myDataBase=new MyDataBase(context);
        sharedPref=new SharedPref(context);
        passwordValidator=new PasswordValidator();
        finbyviews();
        clicklistener();
    }

    private void clicklistener() {
        back_setting.setOnClickListener(this);
        bt_submit.setOnClickListener(this);
    }

    private void finbyviews() {
        back_setting=findViewById(R.id.back_setting);
        et_currentpwd=findViewById(R.id.et_currentpwd);
        et_newpwd=findViewById(R.id.et_newpwd);
        et_confirmpwd=findViewById(R.id.et_confirmpwd);
        bt_submit=findViewById(R.id.bt_submit);
        loading_ll=findViewById(R.id.loading_ll);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back_setting:
                onBackPressed();
                finish();
                break;

            case R.id.bt_submit:
                Validation();
                break;
        }
    }

    private void Validation() {
        st_currentpwd=et_currentpwd.getText().toString().trim();
        st_newpwd=et_newpwd.getText().toString().trim();
        st_confirmpwd=et_confirmpwd.getText().toString().trim();

        if(st_currentpwd.isEmpty() && st_newpwd.isEmpty() && st_confirmpwd.isEmpty())
        {
            Commons.showAlertDialog1(context,"Please Enter all Details");
        }
        else if(st_newpwd.isEmpty())
        {
            Commons.showAlertDialog1(context,"Please Enter Password");
            et_newpwd.requestFocus();
        }
        else if (!passwordValidator.validate(st_newpwd)) {
            et_newpwd.setError("Must contains atleast one digit, one lowercase,one Uppercase , special character  and at least 8 characters");
            et_newpwd.requestFocus();
        } else if (st_confirmpwd.isEmpty()) {
            et_confirmpwd.requestFocus();
            Commons.showAlertDialog1(context, "Please Enter confirm password");
        } else if (!passwordValidator.validate(st_confirmpwd)) {
            et_confirmpwd.setError("Must contains atleast one digit, one lowercase,one Uppercase , special character  and at least 8 characters");
            et_confirmpwd.requestFocus();
        } else if (!st_newpwd.equals(st_confirmpwd)) {
            et_confirmpwd.requestFocus();
            Commons.showAlertDialog1(context, "Please confirm password");
        } else {
            Method_ChanePassword();
        }
    }

    private void Method_ChanePassword() {

        HashMap<String, String> request_changepassword = new HashMap<>();
        request_changepassword.put("current_user_password",st_currentpwd);
        request_changepassword.put("user_password",st_newpwd);
        request_changepassword.put("confirm_user_password",st_confirmpwd);


        HashMap<String,String>header1=new HashMap<>();
        header1.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));


        System.out.println("==req---login---"+"==="+request_changepassword+"---"+header1);
        //Commons.showProgress(context);
        loading_ll.setVisibility(View.VISIBLE);
        changePasswordviewModel.getChangepasswordData(request_changepassword,header1).observe(this, new Observer<Response_ChangePassword>() {
            @Override
            public void onChanged(Response_ChangePassword response_changePassword) {
                //Commons.hideProgress();
                loading_ll.setVisibility(View.GONE);
                if(response_changePassword.status.equals("1"))
                {
                   // Commons.Alert_custom(context,response_changePassword.longMessage);
                    System.out.println("===res=change password==" + response_changePassword.toString());
                    Dialog_Confirm(response_changePassword.longMessage);
                }
                else if(response_changePassword.status.equals("0"))
                {
                    if(response_changePassword.longMessage!=null)
                    {
                        Commons.Alert_custom(context, response_changePassword.longMessage);
                    }
                    if(response_changePassword.errors!=null) {
                        Commons.Alert_custom(context, response_changePassword.errors.user_password);
                    }
                }
            }
        });
    }

    private void Dialog_Confirm(String content) {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_ios_singlebutton);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
        txttitle.setText("Allied Motors");
        txtInstructionDialog.setText(content);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);
        CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
        btokay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onBackPressed();
                dialog_savepassword();
                alertDialog.dismiss();
            }
        });



        if (!alertDialog.isShowing())
            alertDialog.show();

        //dialog.dismiss();

    }



    private void dialog_savepassword() {

        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.bottomsheet_changepassword);
            alertDialog.setCancelable(true);
            Window window = alertDialog.getWindow();
            window.setGravity(Gravity.BOTTOM);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Your Password has been changed, do you want to save the password?");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        myDataBase.update(sharedPref.getString(Constants.PREF_USERMAIL),st_newpwd);
                    Intent tocartpage = new Intent(ChangePassword.this, HomeActivity_searchimage_BottomButton.class);
                    tocartpage.putExtra("tabposition", "home");
                    tocartpage.putExtra("position_tabs","1");
                    startActivity(tocartpage);
                    finish();
                    alertDialog.dismiss();

                   // onBackPressed();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();


        }
    }


}