package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewMedium;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.view.Activity.Faq_Activity;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Activity.MyProfile_Activity;
import com.app.alliedmotor.view.Activity.PrivacyPolicy_Activity;
import com.app.alliedmotor.view.Activity.Signup_Activity;
import com.app.alliedmotor.view.Activity.TermsCondition;
import com.app.alliedmotor.viewmodel.UserProfileviewModel;

import java.util.HashMap;
import java.util.Objects;

public class Account_Fragment extends Fragment implements View.OnClickListener {

    CustomTextViewMedium tv_user_name, tv_default_title;
    CustomTextViewRegular tv_user_mail;
    CustomRegularTextview tv_profile_forward_ic, tv_profile, tv_login, tv_signup, tv_logout, tv_notification, tv_tc, tv_privacy_policy, tv_faq, tv_mobil, tv_whatsapp, tv_mobile_value, tv_whatsapp_value;
    Context context;
    Switch seekBar;
    View root;
    UserProfileviewModel userProfileviewModel;
    String res_fname, res_lname, res_email, res_mobile, res_workno, res_comp_name, res_usertype;
    SharedPref sharedPref;
    ConstraintLayout cl_login, cl_signup, cl_logout, cl_myprofile;
    String st_mobno, st_whatsapno;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (root == null) {
            root = inflater.inflate(R.layout.fragment_user_account, container,false);
            userProfileviewModel = ViewModelProviders.of(this).get(UserProfileviewModel.class);
            context = getActivity();
            sharedPref = new SharedPref(context);
            findviews();
            clicklistener();
            System.out.println("==shares===" + sharedPref.getString(Constants.JWT_TOKEN));
            if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
            } else {
                Method_UserProfile();
            }
            st_mobno = tv_mobile_value.getText().toString().trim();
            st_whatsapno = tv_whatsapp_value.getText().toString().trim();
        }
        return root;
    }


    private void clicklistener() {
        tv_profile_forward_ic.setOnClickListener(this);
        tv_profile.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        tv_notification.setOnClickListener(this);
        tv_tc.setOnClickListener(this);
        tv_privacy_policy.setOnClickListener(this);
        tv_faq.setOnClickListener(this);
        tv_profile_forward_ic.setOnClickListener(this);
        tv_mobile_value.setOnClickListener(this);
        tv_whatsapp_value.setOnClickListener(this);
    }

    private void findviews() {
        tv_default_title = root.findViewById(R.id.tv_default_title);
        cl_login = root.findViewById(R.id.cl_login);
        cl_signup = root.findViewById(R.id.cl_signup);
        cl_logout = root.findViewById(R.id.cl_logout);
        cl_myprofile = root.findViewById(R.id.cl_myprofile);
        tv_user_name = root.findViewById(R.id.tv_user_name);
        tv_user_mail = root.findViewById(R.id.tv_user_mail);
        tv_profile_forward_ic = root.findViewById(R.id.tv_profile_forward_ic);
        tv_profile = root.findViewById(R.id.tv_profile);
        tv_login = root.findViewById(R.id.tv_login);
        tv_signup = root.findViewById(R.id.tv_signup);
        tv_logout = root.findViewById(R.id.tv_logout);
        tv_notification = root.findViewById(R.id.tv_notification);
        tv_tc = root.findViewById(R.id.tv_tc);
        tv_privacy_policy = root.findViewById(R.id.tv_privacy_policy);
        tv_faq = root.findViewById(R.id.tv_faq);
        tv_mobile_value = root.findViewById(R.id.tv_mobile_value);
        tv_whatsapp_value = root.findViewById(R.id.tv_whatsapp_value);
        seekBar = root.findViewById(R.id.seekBar);

    }

    private void Method_UserProfile() {

        HashMap<String ,String> header=new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
        userProfileviewModel.getuserprofiledata(header).observe(Objects.requireNonNull(getActivity()), new Observer<Response_UserProfile>() {
            @Override
            public void onChanged(Response_UserProfile response_userProfile) {
                System.out.println("===res=user_profile==" + response_userProfile.toString());
                tv_whatsapp_value.setText(response_userProfile.getData().getSupport().getWhatsapp());
                tv_mobile_value.setText(response_userProfile.getData().getSupport().getMobile());
                res_fname = response_userProfile.getData().getUserDetails().getDisplayName();

                res_email = response_userProfile.getData().getUserDetails().getUserEmail();
                res_mobile = response_userProfile.getData().getUserDetails().getMobileNumber();

                res_comp_name = response_userProfile.getData().getUserDetails().getCompanyName();

                tv_default_title.setVisibility(View.GONE);
                tv_user_name.setVisibility(View.VISIBLE);
                tv_user_mail.setVisibility(View.VISIBLE);
                tv_user_mail.setText(res_email);
                tv_user_name.setText(res_fname);
                cl_myprofile.setVisibility(View.VISIBLE);
                cl_logout.setVisibility(View.VISIBLE);
                cl_login.setVisibility(View.GONE);
                cl_signup.setVisibility(View.GONE);
            }
        });

    }


    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    public void onStop() {
        super.onStop();
        //  ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.tv_profile:
            case R.id.tv_profile_forward_ic:
                Intent tomyprofile = new Intent(getActivity(), MyProfile_Activity.class);
                tomyprofile.putExtra("user_fname", res_fname);
                tomyprofile.putExtra("user_lname", res_lname);
                tomyprofile.putExtra("user_email", res_email);
                tomyprofile.putExtra("user_mobile", res_mobile);
                tomyprofile.putExtra("user_workno", res_workno);
                tomyprofile.putExtra("user_comp_name", res_comp_name);
                tomyprofile.putExtra("user_fname", res_fname);
                tomyprofile.putExtra("usertype", res_usertype);
                Objects.requireNonNull(getActivity()).startActivity(tomyprofile);
                break;
            case R.id.tv_faq:
                Intent toIntent1 = new Intent(getActivity(), Faq_Activity.class);
                Objects.requireNonNull(getActivity()).startActivity(toIntent1);
                break;
            case R.id.tv_login:
                Intent toIntent2 = new Intent(getActivity(), LoginActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(toIntent2);
                break;
            case R.id.tv_signup:
                Intent toIntent3 = new Intent(getActivity(), Signup_Activity.class);
                Objects.requireNonNull(getActivity()).startActivity(toIntent3);
                break;
            case R.id.tv_tc:
                Intent toIntent4 = new Intent(getActivity(), TermsCondition.class);
                Objects.requireNonNull(getActivity()).startActivity(toIntent4);
                break;
            case R.id.tv_privacy_policy:
                Intent toIntent5 = new Intent(getActivity(), PrivacyPolicy_Activity.class);
                Objects.requireNonNull(getActivity()).startActivity(toIntent5);
                break;
            case R.id.tv_logout:
                Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                System.out.println("===vc===" + sharedPref.getString(Constants.JWT_TOKEN));
                sharedPref.clearAll();
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoLogin);
                getActivity().finish();
                break;

            case R.id.tv_mobile_value:
                Intent intent = new Intent(Intent.ACTION_CALL_BUTTON);
                //  intent.setData(Uri.parse(st_mobno));
                startActivity(intent);
                break;
            case R.id.tv_whatsapp_value:
                AppInstalled.isAppInstalled("com.whatsapp", context);
                break;

        }

    }
}
