package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.NewCar.MakeListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NewCar_MakerList_Adapter extends RecyclerView.Adapter<NewCar_MakerList_Adapter.ViewHolder> {

    ArrayList<MakeListItem> makeListItems;

    INewCarMakelist listener;
    SharedPref sharedPref;
    private Context mcontext;
    String imageurl;
    int curr_position=0;
    String makeid="";
   int makeidpisotion=0;

    public NewCar_MakerList_Adapter(Context context, ArrayList<MakeListItem> listdata, INewCarMakelist listener,String  makeid) {
        this.mcontext = context;
        this.makeListItems = listdata;
        this.listener = listener;
        this.makeid = makeid;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_newcar_makerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        imageurl=makeListItems.get(position).getIcon();
        if(makeListItems.get(position).getMakename().equals("All Cars"))
        {
            Picasso.get().load(R.drawable.all_car).into(holder.imageView);
        }
        else {

            Picasso.get().load(imageurl).into(holder.imageView);
        }

        if(makeid==null || makeid=="")
        {
            if(curr_position==position)
            {
                holder.cardview.setBackgroundResource(R.drawable.cardview_round_plain_blue);
            }
            else {
                holder.cardview.setBackgroundResource(R.drawable.cardview_round_plain_white);
            }
        }
        else
        {
            if(makeid.equals(makeListItems.get(position).getMkid()))
            {
                holder.cardview.setBackgroundResource(R.drawable.cardview_round_plain_blue);
            }
            else {
                holder.cardview.setBackgroundResource(R.drawable.cardview_round_plain_white);
            }
        }


        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBrandSelected(makeListItems.get(position),position);
                curr_position=position;
                notifyDataSetChanged();
            }
        });
    }





    @Override
    public int getItemCount() {
        return makeListItems.size();
    }

    public interface INewCarMakelist  {
        void onBrandSelected(MakeListItem data, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ImageView imageView;
        CardView cardview;


        public ViewHolder(View itemView) {

            super(itemView);

            this.imageView =  itemView.findViewById(R.id.imageview1);
            this.cardview =  itemView.findViewById(R.id.ll_imagecontainr);

        }
    }
}

