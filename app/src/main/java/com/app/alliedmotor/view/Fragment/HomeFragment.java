package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.HomePage.BannerDetailsItem;
import com.app.alliedmotor.model.HomePage.BrandsItem;
import com.app.alliedmotor.model.HomePage.CategoriesItem;
import com.app.alliedmotor.model.HomePage.DataItem;
import com.app.alliedmotor.model.HomePage.FestivalOffersItem;
import com.app.alliedmotor.model.HomePage.Response_Homepage;
import com.app.alliedmotor.view.Adapter.Festival_Adapter;
import com.app.alliedmotor.view.Adapter.HomePage_Brand_Adapter;
import com.app.alliedmotor.view.Adapter.HomePage_Categories_Adapter;
import com.app.alliedmotor.view.Adapter.ImageBanner_Adapter_Static;
import com.app.alliedmotor.viewmodel.HomepageviewModel;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;


public class HomeFragment extends Fragment implements Festival_Adapter.Categoires {

    boolean fragmentAlreadyLoaded = false;
    RecyclerView grid_categories, rv_brands, rv_festival;
    HomepageviewModel homepageviewModel;
    LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    View root;
   static ArrayList<BannerDetailsItem> bannerDetailsItems = new ArrayList<>();
    ArrayList<CategoriesItem> categoriesItems = new ArrayList<>();
    ArrayList<BrandsItem> brandsItems = new ArrayList<>();
    ArrayList<FestivalOffersItem> festivalOffersItems = new ArrayList<>();
    HomePage_Categories_Adapter homePageCategories_adapter;
    HomePage_Brand_Adapter homePageBrand_adapter;
    Festival_Adapter festival_adapter;
    PagerAdapter imageBanner_adapter;
    ViewPager photos_viewpager;
    TabLayout tabindiactor;
    private static int currentPage = 0;
    Timer timer;
    private int NUM_PAGES;
    final long DELAY_MS = 1000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    LinearLayout loading_ll, main_ll;
    private Context context;
    static HomeFragment home_fragment;

    private Integer[] IMAGES= {R.drawable.banner1,R.drawable.banner2,R.drawable.banner3,R.drawable.banner4,R.drawable.banner5,R.drawable.banner6};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

    public static HomeFragment getInstance() {
        if (home_fragment == null)
            home_fragment = new HomeFragment();
        return home_fragment;
    }

    LinearSnapHelper linearSnapHelper;


   /* private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                _hasLoadedOnce = true;
            }
        }
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_home1, container, false);
            homepageviewModel = ViewModelProviders.of(this).get(HomepageviewModel.class);
            context = getActivity();
            home_fragment = this;
            grid_categories = root.findViewById(R.id.grid_categories);
            rv_brands = root.findViewById(R.id.rv_brands);
            rv_festival = root.findViewById(R.id.rv_festival);
            loading_ll = root.findViewById(R.id.loading_ll);
            main_ll = root.findViewById(R.id.main_ll);
            linearSnapHelper=new LinearSnapHelper();

            for(int i=0;i<IMAGES.length;i++)
                ImagesArray.add(IMAGES[i]);

            System.out.println("====>HomeOncreateview call---->");
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rv_brands.setLayoutManager(linearLayoutManager);
            linearSnapHelper.attachToRecyclerView(rv_brands);
            rv_brands.setHasFixedSize(true);

            linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rv_festival.setLayoutManager(linearLayoutManager1);
            linearSnapHelper.attachToRecyclerView(rv_festival);
            rv_festival.setHasFixedSize(true);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 4);
            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
            grid_categories.setLayoutManager(gridLayoutManager);
            grid_categories.setHasFixedSize(true);

            photos_viewpager = root.findViewById(R.id.photos_viewpager);
            tabindiactor = root.findViewById(R.id.tabindiactor);
            func();
            Method_Call();
        }
       /* else {
            ((ViewGroup) root.getParent()).removeView(root);
        }*/
        return root;
    }

    private void Method_Call() {
        loading_ll.setVisibility(View.VISIBLE);
        homepageviewModel.getHomepagedata().observe(Objects.requireNonNull(getActivity()), new Observer<Response_Homepage>() {
            @Override
            public void onChanged(Response_Homepage response_homepage) {
                //Commons.hideProgress();
                loading_ll.setVisibility(View.GONE);
                main_ll.setVisibility(View.VISIBLE);
                System.out.println("===res===" + response_homepage.toString());
                categoriesItems = response_homepage.getData().getCategories();
                brandsItems = response_homepage.getData().getBrands();
                festivalOffersItems = response_homepage.getData().getFestivalOffers();
                // bannerDetailsItems = response_homepage.getData().getBannerDetails();

                setAdapter();
            }

        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (root != null) {
            ViewGroup parentViewGroup = (ViewGroup) root.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(getActivity()))
        EventBus.getDefault().register(this);
        System.out.println("====>HomeOnResume call---->");
       // Method_Call();

    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("====>Homepausecall---->");
       // Method_Call();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
          if(EventBus.getDefault().isRegistered(getActivity()))
        EventBus.getDefault().unregister(this);
    }

    public void  func()
    {


        //NUM_PAGES = bannerDetailsItems.size();
        NUM_PAGES = IMAGES.length;
        imageBanner_adapter = new ImageBanner_Adapter_Static(context, ImagesArray);
        photos_viewpager.setAdapter(imageBanner_adapter);
        photos_viewpager.setPageMargin(50);
        photos_viewpager.setClipToPadding(false);
        photos_viewpager.setPadding(5, 5, 5, 5);
        tabindiactor.setupWithViewPager(photos_viewpager, true);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                photos_viewpager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    private void setAdapter() {
        festival_adapter = new Festival_Adapter(context, festivalOffersItems, this::onCategorySelected);
        rv_festival.setAdapter(festival_adapter);
        homePageCategories_adapter = new HomePage_Categories_Adapter(context, categoriesItems);
        grid_categories.setAdapter(homePageCategories_adapter);

        homePageBrand_adapter = new HomePage_Brand_Adapter(context, brandsItems);
        rv_brands.setAdapter(homePageBrand_adapter);

       /* NUM_PAGES = IMAGES.length;
        imageBanner_adapter = new ImageBanner_Adapter_Static(context, ImagesArray);
        photos_viewpager.setAdapter(imageBanner_adapter);
        photos_viewpager.setPageMargin(50);
        photos_viewpager.setClipToPadding(false);
        photos_viewpager.setPadding(5, 5, 5, 5);
        tabindiactor.setupWithViewPager(photos_viewpager, true);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0;
                }
                photos_viewpager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);*/
        }

    @Override
    public void onCategorySelected(ArrayList<DataItem> data, int position) {
        EventBus.getDefault().post(data);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ArrayList<DataItem> data,int position) {


    }
}
