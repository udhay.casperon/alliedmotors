package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.Notification.Response_Notification;
import com.app.alliedmotor.model.Notification.Service_FCMNotification;
import com.app.alliedmotor.model.NotificationDelete.Response_NotifyDelete;
import com.app.alliedmotor.model.OfferNotify_change.Response_OfferNotify;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.viewmodel.AppInfoviewModel;

import java.util.HashMap;

public class SplashScreen extends AppCompatActivity {

    private View background;
    AppInfoviewModel appInfoviewModel;
    private Context context;
    SharedPref sharedPref;

  String postid="",st_ticketid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen1);
        appInfoviewModel = ViewModelProviders.of(this).get(AppInfoviewModel.class);
        context = this;
        background = findViewById(R.id.background);
        sharedPref = new SharedPref(context);
        if (savedInstanceState == null) {
            background.setVisibility(View.INVISIBLE);

            final ViewTreeObserver viewTreeObserver = background.getViewTreeObserver();

            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        circularRevealActivity();
                        background.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        background.setVisibility(View.VISIBLE);
                    }

                });
            }
        }
        if(getIntent()!=null) {
            postid=getIntent().getStringExtra("postid");
            st_ticketid=getIntent().getStringExtra("ticketid");
            if(postid!=null || postid=="")
            {
                OfferPage();
            }
            else if(st_ticketid!=null || st_ticketid=="")
            {
                Intent tocart2 = new Intent(getApplicationContext(), HomeActivity_searchimage_BottomButton.class);
                tocart2.putExtra("tabposition", "cart");
                tocart2.putExtra("position_tabs", "2");
                tocart2.putExtra("ticketid", st_ticketid);
                startActivity(tocart2);
            }
            else {
                handleIntent(getIntent());
            }
        }
    }


    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            String recipeId = appLinkData.getLastPathSegment();
            System.out.println("--red==="+recipeId);


            Uri appData = Uri.parse(appLinkData.toString()).buildUpon()
                    .appendPath(recipeId).build();
            // full link--->  Toast.makeText(context,appData.toString(),Toast.LENGTH_SHORT).show();
            String linkdata=appData.getQuery();
            // last params --->  Toast.makeText(context,linkdata.toString(),Toast.LENGTH_SHORT).show();

            String separator ="=";
            int sepPos = linkdata.indexOf(separator);
            if (sepPos == -1) {
                System.out.println("");
            }
            String begore= linkdata.substring(0 , sepPos); //this will give abc
            System.out.println("Substring----"+begore);
            String carid=linkdata.substring(sepPos +       separator.length());
            System.out.println("Substring after separator = "+linkdata.substring(sepPos +       separator.length()));


            if(begore.equals("carId"))
            {
                Intent tocardetail=new Intent(SplashScreen.this,Car_Detail__Constarint_Activity1.class);
                tocardetail.putExtra("carid",carid);
                startActivity(tocardetail);
                finish();
            }
            else if(begore.equals("preownedId"))
            {
                Intent pretocardetail=new Intent(SplashScreen.this,PreOwnedCar_Detail_Activity.class);
                pretocardetail.putExtra("carid",carid);
                startActivity(pretocardetail);
                finish();
            }


            // particular id alone---  Toast.makeText(context,linkdata.substring(sepPos +       separator.length()),Toast.LENGTH_SHORT).show();



        }
    }


    private void OfferPage()
    {
        HashMap<String,String>header=new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String,String>request=new HashMap<>();
        request.put("post_id",postid);
        new Service_FCMNotification(context, header, request, new ResponseInterface.FCMNotificationInterface() {
            @Override
            public void onFCMNotificationSuccess(Response_Notification response_notification) {
                if(response_notification.getStatus().equals("1"))
                {

                    Intent toofferpage=new Intent(getApplicationContext(), OfferPage_Activity.class);
                    toofferpage.putExtra("postid",postid);
                    toofferpage.putExtra("posttitle",response_notification.getData().getNotificationData().getPostTitle());
                    toofferpage.putExtra("imageurl",response_notification.getData().getNotificationData().getThumbnailId());
                    toofferpage.putExtra("buttontext",response_notification.getData().getNotificationData().getButtonText());
                    toofferpage.putExtra("buttonurl",response_notification.getData().getNotificationData().getButtonUrl());
                    toofferpage.putExtra("enablebutton",response_notification.getData().getNotificationData().getEnnableButton());
                    toofferpage.putExtra("postcontent",response_notification.getData().getNotificationData().getPostContent());
                    toofferpage.putExtra("shortdesc",response_notification.getData().getNotificationData().getShortDescription());
                    startActivity(toofferpage);
                }
                else {
                    System.out.println("===res"+response_notification.toString());
                }
            }
        });
    }



    private void Func_AppInfo() {
        HashMap<String,String> header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else if(sharedPref.getString(Constants.JWT_TOKEN)!=null)
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");
            Method_UserProfile();
        }

        appInfoviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                            if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());

                    sharedPref.setString(Constants.PREF_USERNAME, response_appInfo.getData().getUserName());
                    sharedPref.setString(Constants.PREF_USERMAIL, response_appInfo.getData().getUserEmail());


                    sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }

    private int getDips(int dps) {
        Resources resources = getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dps,
                resources.getDisplayMetrics());
    }


    private void circularRevealActivity() {
        int cx = background.getRight() - getDips(44);
        int cy = background.getBottom() - getDips(44);

        float finalRadius = Math.max(background.getWidth(), background.getHeight());

        Animator circularReveal = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            circularReveal = ViewAnimationUtils.createCircularReveal(
                    background,
                    cx,
                    cy,
                    0,
                    finalRadius);
        }

        circularReveal.setDuration(1500);
        background.setVisibility(View.VISIBLE);
        circularReveal.start();

    }

    private void Method_UserProfile() {

        HashMap<String ,String> header=new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
        System.out.println("dhh---------"+sharedPref.getString(Constants.JWT_TOKEN));
        appInfoviewModel.getuserprofiledata(header).observe(this, new Observer<Response_UserProfile>() {
            @Override
            public void onChanged(Response_UserProfile response_userProfile) {
                System.out.println("===res=user_profile==" + response_userProfile.toString());
                sharedPref.setString(Constants.PREF_USERMAIL,response_userProfile.getData().getUserDetails().getUserEmail());
                sharedPref.setString(Constants.PREF_USERNAME,response_userProfile.getData().getUserDetails().getDisplayName());
            }
        });

    }


}