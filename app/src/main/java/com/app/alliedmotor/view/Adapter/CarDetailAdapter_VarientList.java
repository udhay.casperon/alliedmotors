package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.CarDetail.VarientListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.Car_Detail__Constarint_Activity1;

import java.util.ArrayList;

public class CarDetailAdapter_VarientList extends RecyclerView.Adapter<CarDetailAdapter_VarientList.ViewHolder> {


    ArrayList<VarientListItem> features;

    SharedPref sharedPref;
    private Context mcontext;
    int current_position=-1;
    String name_varient;

    VarientOption listener;

    public interface VarientOption
    {
        void onVarientOption(String data);
    }

    public CarDetailAdapter_VarientList(Context context, ArrayList<VarientListItem> features,VarientOption listener,String namevarient) {
        this.mcontext = context;
        this.features = features;
        this.listener=listener;
        this.name_varient=namevarient;
        for (int k = 0; k < features.size(); k++) {
            if (name_varient.equals(features.get(k).getVarientCode())) {
                current_position = k;
            }
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_filter, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_value.setText((features.get(position).getVarientCode()));
        holder.ll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onVarientOption(features.get(position).getMainId());
                current_position=position;
                notifyDataSetChanged();
            }
        });

        if(current_position==position)
        {
            holder.ll_filter.setBackgroundResource(R.drawable.edittext_roundshadow);
            holder.tv_value.setTextColor(mcontext.getResources().getColor(R.color.white));
        }
        else {
            holder.ll_filter.setBackgroundResource(R.drawable.edittext_roundshadow_outline_grey);
            holder.tv_value.setTextColor(mcontext.getResources().getColor(R.color.black));
        }
    }

    @Override
    public int getItemCount() {
      return features.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewSemiBold tv_value;
        LinearLayout ll_filter;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_value=itemView.findViewById(R.id.tv_value);
            ll_filter=itemView.findViewById(R.id.ll_filter);

        }
    }
}

