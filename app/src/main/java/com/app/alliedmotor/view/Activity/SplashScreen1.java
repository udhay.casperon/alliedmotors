package com.app.alliedmotor.view.Activity;

import android.Manifest;
import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.viewmodel.AppInfoviewModel;

import java.util.HashMap;

public class SplashScreen1 extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final String TAG = "102";
    SharedPref sharedPref;
    private long pressedTime;
    private Context context;
    private View background;

    AppInfoviewModel appInfoviewModel;
    SharedPreferences sp;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen1);
        appInfoviewModel = ViewModelProviders.of(this).get(AppInfoviewModel.class);

        context = this;
        sharedPref = new SharedPref(context);

        Func_AppInfo();





        background = findViewById(R.id.background);

        if (savedInstanceState == null) {
            background.setVisibility(View.INVISIBLE);

            final ViewTreeObserver viewTreeObserver = background.getViewTreeObserver();

            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        circularRevealActivity();
                        background.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        background.setVisibility(View.VISIBLE);
                    }

                });
            }
            splashIntent();

        }

        if(getIntent()!=null) {
            handleIntent(getIntent());
        }

    }



    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            String recipeId = appLinkData.getLastPathSegment();
            System.out.println("--red==="+recipeId);


            Uri appData = Uri.parse(appLinkData.toString()).buildUpon()
                    .appendPath(recipeId).build();
            // full link--->  Toast.makeText(context,appData.toString(),Toast.LENGTH_SHORT).show();
            String linkdata=appData.getQuery();
            // last params --->  Toast.makeText(context,linkdata.toString(),Toast.LENGTH_SHORT).show();

            String separator ="=";
            int sepPos = linkdata.indexOf(separator);
            if (sepPos == -1) {
                System.out.println("");
            }
            String begore= linkdata.substring(0 , sepPos); //this will give abc
            System.out.println("Substring----"+begore);
            String carid=linkdata.substring(sepPos +       separator.length());
            System.out.println("Substring after separator = "+linkdata.substring(sepPos +       separator.length()));


            if(begore.equals("carId"))
            {
                Intent tocardetail=new Intent(SplashScreen1.this,Car_Detail__Constarint_Activity1.class);
                tocardetail.putExtra("carid",carid);
                startActivity(tocardetail);
                finish();
            }
            else if(begore.equals("preownedId"))
            {
                Intent pretocardetail=new Intent(SplashScreen1.this,PreOwnedCar_Detail_Activity.class);
                pretocardetail.putExtra("carid",carid);
                startActivity(pretocardetail);
                finish();
            }


            // particular id alone---  Toast.makeText(context,linkdata.substring(sepPos +       separator.length()),Toast.LENGTH_SHORT).show();



        }
    }



    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else if(sharedPref.getString(Constants.JWT_TOKEN)!=null)
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");
            Method_UserProfile();
        }

        appInfoviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();


                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());

                    sharedPref.setString(Constants.PREF_USERNAME, response_appInfo.getData().getUserName());
                    sharedPref.setString(Constants.PREF_USERMAIL, response_appInfo.getData().getUserEmail());


                    sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }


    private void circularRevealActivity() {
        int cx = background.getRight() - getDips(44);
        int cy = background.getBottom() - getDips(44);

        float finalRadius = Math.max(background.getWidth(), background.getHeight());

        Animator circularReveal = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            circularReveal = ViewAnimationUtils.createCircularReveal(
                    background,
                    cx,
                    cy,
                    0,
                    finalRadius);
        }

        circularReveal.setDuration(1500);
        background.setVisibility(View.VISIBLE);
        circularReveal.start();

    }

    private int getDips(int dps) {
        Resources resources = getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dps,
                resources.getDisplayMetrics());
    }



    public void splashIntent() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (Exception e) {

                } finally {
                    System.out.println("--token==nstatus=" + sharedPref.getString(Constants.PREF_LOGIN_STATUS));
                        if (sharedPref.getString(Constants.PREF_LOGIN_STATUS) == null) {
                            final Intent i = new Intent(SplashScreen1.this, LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else if(sharedPref.getString(Constants.PREF_LOGIN_STATUS).equals("1")){
                            Intent mainIntent = new Intent(SplashScreen1.this, HomeActivity_searchimage_BottomButton.class);
                            startActivity(mainIntent);
                            finish();
                        }

                }
            }
        };
        timer.start();
    }


    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cx = background.getWidth() - getDips(44);
            int cy = background.getBottom() - getDips(44);

            float finalRadius = Math.max(background.getWidth(), background.getHeight());
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(background, cx, cy, finalRadius, 0);

            circularReveal.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    background.setVisibility(View.INVISIBLE);
                    finish();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            circularReveal.setDuration(700);
            circularReveal.start();
        }
        else {
            super.onBackPressed();
        }
    }


    private void Method_UserProfile() {

        HashMap<String ,String> header=new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
        System.out.println("dhh---------"+sharedPref.getString(Constants.JWT_TOKEN));
        appInfoviewModel.getuserprofiledata(header).observe(this, new Observer<Response_UserProfile>() {
            @Override
            public void onChanged(Response_UserProfile response_userProfile) {
                System.out.println("===res=user_profile==" + response_userProfile.toString());
               sharedPref.setString(Constants.PREF_USERMAIL,response_userProfile.getData().getUserDetails().getUserEmail());
               sharedPref.setString(Constants.PREF_USERNAME,response_userProfile.getData().getUserDetails().getDisplayName());
            }
        });

    }


}