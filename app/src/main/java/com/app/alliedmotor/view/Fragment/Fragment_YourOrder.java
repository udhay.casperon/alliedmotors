package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.MyOrderLatest.OrderDataItem;
import com.app.alliedmotor.model.MyOrderLatest.Response_MyOrderLatest;
import com.app.alliedmotor.model.NotificationStatusChange.Response_NotificationStatuschange;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.HomeActivity_searchimage_BottomButton;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Adapter.YourOrder_Adapter;
import com.app.alliedmotor.viewmodel.MyOrderviewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;


public class Fragment_YourOrder extends Fragment {

    RecyclerView rv_yourorder;
    LinearLayoutManager linearLayoutManager;
    MyOrderviewModel myOrderviewModel;
    ArrayList<OrderDataItem> ordersItemArrayList = new ArrayList<>();
    YourOrder_Adapter yourOrder_adapter;
    private Context context;
    SharedPref sharedPref;
    LinearLayout loading_ll, main_ll, ll_empty,ll_loginfeature;
    View view;
    CustomTextViewSemiBold tv_loginfeature;
    private boolean _hasLoadedOnce= false;
LinearLayout ll_loginfeature_text;
SwipeRefreshLayout swipelayout;
    String ticketid="";

    public Fragment_YourOrder(String ticketid) {
        this.ticketid=ticketid;
    }
    public Fragment_YourOrder() {
    }


    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Func_1();
                _hasLoadedOnce = true;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_your_order, container, false);
            myOrderviewModel = ViewModelProviders.of(this).get(MyOrderviewModel.class);
            context = getActivity();
            sharedPref = new SharedPref(context);

            ll_loginfeature=view.findViewById(R.id.ll_loginfeature);
            rv_yourorder = view.findViewById(R.id.rv_yourorder);
            loading_ll = view.findViewById(R.id.loading_ll);
            main_ll = view.findViewById(R.id.main_ll);
            tv_loginfeature=view.findViewById(R.id.tv_loginfeature);
            ll_empty = view.findViewById(R.id.ll_empty);
            ll_loginfeature_text=view.findViewById(R.id.ll_loginfeature_text);
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rv_yourorder.setLayoutManager(linearLayoutManager);
            rv_yourorder.setHasFixedSize(true);

            swipelayout=view.findViewById(R.id.swipelayout);

           Func_1();


            swipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipelayout.setRefreshing(false);
                    Func_AppInfo();
                }
            });

            if(ticketid!=null || ticketid!="")
            {
                notifyselect();
            }

            ll_loginfeature_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tologin=new Intent(context, LoginActivity.class);
                    startActivity(tologin);
                    getActivity().finish();
                }
            });
        }
        return view;
    }

    private void Func_1() {
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            ll_loginfeature.setVisibility(View.VISIBLE);
            ll_empty.setVisibility(View.GONE);
            main_ll.setVisibility(View.GONE);
        }
        else if(sharedPref.getString(Constants.JWT_TOKEN)!=null)
        {
            Apicall_YourOrder();
        }
    }

    private void Apicall_YourOrder() {
        loading_ll.setVisibility(View.VISIBLE);
        HashMap<String, String> header = new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
        Log.i("req-ur order-->", String.valueOf(header));
        System.out.println("==jwt---toke----"+sharedPref.getString(Constants.JWT_TOKEN));

        myOrderviewModel.getmyorderdata(header).observe(getActivity(), new Observer<Response_MyOrderLatest>() {
            @Override
            public void onChanged(Response_MyOrderLatest myOrder_response) {
                System.out.println("===res=my order=" + myOrder_response.toString());
                loading_ll.setVisibility(View.GONE);

                if (myOrder_response.getStatus().equals("1")) {

                    if (myOrder_response.getData().getOrderData().size()==0) {
                        ll_empty.setVisibility(View.VISIBLE);
                        main_ll.setVisibility(View.GONE);
                    } else {
                        ordersItemArrayList = myOrder_response.getData().getOrderData();
                        main_ll.setVisibility(View.VISIBLE);
                        ll_empty.setVisibility(View.GONE);

                        Collections.sort(ordersItemArrayList, new Comparator<OrderDataItem>() {
                            @Override
                            public int compare(OrderDataItem lhs, OrderDataItem rhs) {
                                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                                return Integer.parseInt(lhs.getTicketId()) > Integer.parseInt(rhs.getTicketId()) ? -1 : (Integer.parseInt(lhs.getTicketId()) < Integer.parseInt(rhs.getTicketId()) ) ? 1 : 0;
                            }
                        });

                        yourOrder_adapter = new YourOrder_Adapter(context, ordersItemArrayList,ticketid);
                        rv_yourorder.setAdapter(yourOrder_adapter);
                        linearLayoutManager.scrollToPosition(getcurrentview(ticketid));
                    }
                } else if (myOrder_response.getStatus().equals("0")) {
                    Log.i("-->res onstaus0==", myOrder_response.toString());
                    Commons.hideProgress();
                    ll_empty.setVisibility(View.VISIBLE);
                    main_ll.setVisibility(View.GONE);
                }
            }
        });
    }


    private int getcurrentview(String chosen1)
    {
        for(int i=0; i < ordersItemArrayList.size(); i++){
            if(ordersItemArrayList.get(i).getTicketId().equals(chosen1)){
                return i;
            }
        }
        return 0;
    }


    public void notifyselect()
    {
        HashMap<String,String> header=new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        HashMap<String,String> request=new HashMap<>();
        request.put("notification_id", ticketid);
        request.put("is_unread", "0");

        myOrderviewModel.statuschange_notification(header,request).observe(this, new Observer<Response_NotificationStatuschange>() {
            @Override
            public void onChanged(Response_NotificationStatuschange notificationStatuschange) {
                //  loading_ll.setVisibility(View.GONE);
                Commons.hideProgress();
                System.out.println("---update quote--"+notificationStatuschange.message);
                if(notificationStatuschange.status.equals("1"))
                {
                    Func_AppInfo();
                }
                else if((notificationStatuschange.status.equals("0")))
                {
                }
            }
        });
    }


    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        myOrderviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                    if(sharedPref.getString(Constants.JWT_TOKEN)!=null) {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                        String count = response_appInfo.getData().getUnreadNotificationCount();
                        HomeActivity_searchimage_BottomButton.object.tv_notification_count.setText(count);
                        HomeActivity_searchimage_BottomButton.object.sidenav_notification_count.setText(count);
                    }
                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }

}