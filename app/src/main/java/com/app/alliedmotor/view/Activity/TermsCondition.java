package com.app.alliedmotor.view.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.TermsCondition.Response_Terms;
import com.app.alliedmotor.viewmodel.TermsviewModel;

public class TermsCondition extends AppCompatActivity {

    ImageView back;
    WebView webview;
    Context context;
    TermsviewModel termsviewModel;
    LinearLayout loading_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);

        termsviewModel = ViewModelProviders.of(this).get(TermsviewModel.class);
        context = this;
        back = findViewById(R.id.back);
        loading_ll=findViewById(R.id.loading_ll);
        webview = findViewById(R.id.webview);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        loading_ll.setVisibility(View.VISIBLE);


        // Commons.showProgress(context);
        /*webview.requestFocus();
        webview.getSettings().

                setJavaScriptEnabled(true);

        WebSettings webSettings = webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webview.loadUrl("https://alliedmotors.com/privacy/");
        webview.setWebViewClient(new

                                         WebViewClient() {
                                             @Override
                                             public boolean shouldOverrideUrlLoading (WebView view, String url){
                                                 view.loadUrl(url);
                                                 Commons.hideProgress();
                                               //  loading_ll.setVisibility(View.GONE);
                                                 return true;
                                             }

                                             @Override
                                             public void onPageStarted (WebView view, String url, Bitmap favicon){
                                                 super.onPageStarted(view, url, favicon);
                                             }

                                             @Override
                                             public void onPageFinished (WebView view, String url){
                                                 super.onPageFinished(view, url);

                                             }

                                         });
        //Commons.hideProgress();
      //  loading_ll.setVisibility(View.GONE);
        webview.setWebChromeClient(new WebChromeClient() {

        });*/

        termsviewModel.getTermsdata().observe(this, new Observer<Response_Terms>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onChanged(Response_Terms response_terms) {
                loading_ll.setVisibility(View.GONE);
                webview.setVisibility(View.VISIBLE);
                //Commons.hideProgress();
                System.out.println("===res===" + response_terms.toString());
                for (int j = 0; j < response_terms.getData().size(); j++) {

                    String content = response_terms.getData().get(0).getPost_content();

                    String font_size = "12";
                    String finalHtml = "<html><head>"
                            + "<style type=\"text/css\">li{color: #fff} span {color: #000}"
                            + "</style></head>"
                            + "<body style='font-size:" + font_size + "px" + "'>" + "<font color='black'>"
                            + content + "</font>"
                            + "</body></html>";

                    webview.loadDataWithBaseURL(null, finalHtml, "text/html", "UTF-8", null);

                    // webview.loadData(Base64.encodeToString(ht), "text/html; charset=UTF-8", null);
                }
            }
        });

    }


}