package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.MyOrderLatest.OrdersItem;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.util.ArrayList;

public class MyOrder_rowsAdpater extends RecyclerView.Adapter<MyOrder_rowsAdpater.ViewHolder> {

    ArrayList<OrdersItem> adata = new ArrayList<>();
    private Context mcontext;


    public MyOrder_rowsAdpater(Context context, ArrayList<OrdersItem> adata) {
        this.mcontext = context;
        this.adata = adata;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_order_rows, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
              String enqStatus=adata.get(position).getEnqStatus();

              if(enqStatus.equalsIgnoreCase("Pending"))
              {
                  holder.tv_status.setText("RFQ Submitted");
              }
              else
              {
                  holder.tv_status.setText(enqStatus);
              }

        try {
            holder.tv_carbrand.setText(adata.get(position).getMakename());
            holder.tv_carname.setText(AppInstalled.toTitleCase(adata.get(position).getModelName()));
            holder.tv_modelyear.setText(adata.get(position).getModelyear());
            holder.tv_tolc.setText(adata.get(position).getFueltype());
            holder.tv_gx.setText(adata.get(position).getCarvarient());
            holder.tv_qty.setText(adata.get(position).getQuantity());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return adata.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomTextViewRegular tv_carbrand, tv_modelyear, tv_tolc, tv_gx, tv_qty, tv_status;
        CustomTextViewSemiBold tv_carname;
        public ViewHolder(View itemView) {

            super(itemView);
            tv_carbrand = itemView.findViewById(R.id.tv_carbrand);
            tv_carname = itemView.findViewById(R.id.tv_carname);
            tv_modelyear = itemView.findViewById(R.id.tv_modelyear);
            tv_tolc = itemView.findViewById(R.id.tv_tolc);
            tv_gx = itemView.findViewById(R.id.tv_gx);
            tv_qty = itemView.findViewById(R.id.tv_qty);
            tv_status = itemView.findViewById(R.id.tv_status);


        }
    }
}

