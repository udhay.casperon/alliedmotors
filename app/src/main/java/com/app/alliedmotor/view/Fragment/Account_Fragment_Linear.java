package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Sample1.Response_UserNotificationstatuschange;
import com.app.alliedmotor.model.UserProfile.Response_UserProfile;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewMedium;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.view.Activity.Faq_Activity;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Activity.MyProfile_Activity;
import com.app.alliedmotor.view.Activity.PrivacyPolicy_Activity;
import com.app.alliedmotor.view.Activity.Signup_Activity;
import com.app.alliedmotor.view.Activity.TermsCondition;
import com.app.alliedmotor.viewmodel.UserProfileviewModel;
import com.google.android.material.switchmaterial.SwitchMaterial;


import java.util.HashMap;
import java.util.Objects;

public class Account_Fragment_Linear extends Fragment implements View.OnClickListener {

    CustomTextViewMedium tv_user_name, tv_default_title;
    CustomTextViewRegular tv_user_mail;
    CustomRegularTextview tv_profile_forward_ic, tv_profile, tv_login, tv_signup, tv_logout, tv_notification, tv_tc, tv_privacy_policy, tv_faq, tv_mobil, tv_whatsapp, tv_mobile_value, tv_whatsapp_value;
    Context context;
    public SwitchMaterial seekBar;
    View root;
    UserProfileviewModel userProfileviewModel;
    String res_fname, res_lname, res_email, res_mobile, res_workno, res_comp_name, res_usertype, res_ismobileverified = "";
    SharedPref sharedPref;
    LinearLayout ll_login, ll_signup, ll_myprofile, ll_logout, ll_terms, ll_privacypolicy, ll_faq, ll_mobile, ll_whatsapp;
    String st_mobno, st_whatsapno;
    SharedPreferences sp;
    String st_notifystatus = "", st_contact = "";

    private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                _hasLoadedOnce = true;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (root == null) {
            root = inflater.inflate(R.layout.fragment_user_account_linear, container,false);
            userProfileviewModel = ViewModelProviders.of(this).get(UserProfileviewModel.class);
            context = getActivity();
            sharedPref = new SharedPref(context);

            findviews();
            clicklistener();


            if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
                seekBar.setVisibility(View.INVISIBLE);
            } else {
                Method_UserProfile();
                seekBar.setVisibility(View.VISIBLE);
            }


            seekBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Log.v("Switch State=", "" + isChecked);
                        Method_notificationchange();


                }
            });

            st_contact = sharedPref.getString(Constants.MOBILENO);




            System.out.println("==shares===" + sharedPref.getString(Constants.JWT_TOKEN));
            if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
                tv_mobile_value.setText(st_contact);
                tv_whatsapp_value.setText(st_contact);
            } else if (sharedPref.getString(Constants.JWT_TOKEN) != null) {
                tv_user_mail.setText(sharedPref.getString(Constants.PREF_USERMAIL));
                tv_user_name.setText(sharedPref.getString(Constants.PREF_USERNAME));
                tv_user_name.setVisibility(View.VISIBLE);
                tv_user_mail.setVisibility(View.VISIBLE);
                ll_myprofile.setVisibility(View.VISIBLE);
                ll_logout.setVisibility(View.VISIBLE);
                ll_login.setVisibility(View.GONE);
                ll_signup.setVisibility(View.GONE);
                tv_default_title.setVisibility(View.GONE);

                tv_mobile_value.setText(sharedPref.getString(Constants.tv_whatsapp_value));
                tv_whatsapp_value.setText(sharedPref.getString(Constants.tv_whatsapp_value));
            }


            st_mobno = tv_mobile_value.getText().toString().trim();
            st_whatsapno = tv_whatsapp_value.getText().toString().trim();


        } else {
            ((ViewGroup) root.getParent()).removeView(root);
        }
        return root;
    }




    @Override
    public void onResume() {
        super.onResume();
        System.out.println("====>HomeOnResume call---->");
        Method_UserProfile();
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("====>Homepausecall---->");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (root != null) {
            ViewGroup parentViewGroup = (ViewGroup) root.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    private void clicklistener() {
        tv_profile_forward_ic.setOnClickListener(this);
        tv_profile.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        tv_notification.setOnClickListener(this);
        tv_tc.setOnClickListener(this);
        tv_privacy_policy.setOnClickListener(this);
        tv_faq.setOnClickListener(this);
        tv_profile_forward_ic.setOnClickListener(this);
        tv_mobile_value.setOnClickListener(this);
        tv_whatsapp_value.setOnClickListener(this);
        ll_login.setOnClickListener(this);
        ll_signup.setOnClickListener(this);
        ll_myprofile.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
        ll_terms.setOnClickListener(this);
        ll_privacypolicy.setOnClickListener(this);
        ll_faq.setOnClickListener(this);
        ll_mobile.setOnClickListener(this);
        ll_whatsapp.setOnClickListener(this);
    }

    private void findviews() {

        ll_terms = root.findViewById(R.id.ll_terms);
        ll_privacypolicy = root.findViewById(R.id.ll_privacypolicy);
        ll_faq = root.findViewById(R.id.ll_faq);
        ll_mobile = root.findViewById(R.id.ll_mobile);
        ll_whatsapp = root.findViewById(R.id.ll_whatsapp);


        tv_default_title = root.findViewById(R.id.tv_default_title);
        ll_login = root.findViewById(R.id.ll_login);
        ll_signup = root.findViewById(R.id.ll_signup);
        ll_logout = root.findViewById(R.id.ll_logout);
        ll_myprofile = root.findViewById(R.id.ll_myprofile);
        tv_user_name = root.findViewById(R.id.tv_user_name);
        tv_user_mail = root.findViewById(R.id.tv_user_mail);
        tv_profile_forward_ic = root.findViewById(R.id.tv_profile_forward_ic);
        tv_profile = root.findViewById(R.id.tv_profile);
        tv_login = root.findViewById(R.id.tv_login);
        tv_signup = root.findViewById(R.id.tv_signup);
        tv_logout = root.findViewById(R.id.tv_logout);
        tv_notification = root.findViewById(R.id.tv_notification);
        tv_tc = root.findViewById(R.id.tv_tc);
        tv_privacy_policy = root.findViewById(R.id.tv_privacy_policy);
        tv_faq = root.findViewById(R.id.tv_faq);
        tv_mobile_value = root.findViewById(R.id.tv_mobile_value);
        tv_whatsapp_value = root.findViewById(R.id.tv_whatsapp_value);
        seekBar = root.findViewById(R.id.seekBar);

    }

    private void Method_UserProfile() {

        HashMap<String, String> header = new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));
        System.out.println("dhh---------" + sharedPref.getString(Constants.JWT_TOKEN));
        userProfileviewModel.getuserprofiledata(header).observe(Objects.requireNonNull(getActivity()), new Observer<Response_UserProfile>() {
            @Override
            public void onChanged(Response_UserProfile response_userProfile) {
                System.out.println("===res=user_profile=11111-----" + response_userProfile.toString());
                sharedPref.setString(Constants.tv_mobile_value,response_userProfile.getData().getSupport().getMobile());
                sharedPref.setString(Constants.tv_whatsapp_value,response_userProfile.getData().getSupport().getWhatsapp());
                sharedPref.setString(Constants.PREF_USERNAME,response_userProfile.getData().getUserDetails().getDisplayName());
                sharedPref.setString(Constants.PREF_USERMAIL,response_userProfile.getData().getUserDetails().getUserEmail());
                st_notifystatus = response_userProfile.getData().getUserDetails().getNotificationStatus();
                if (st_notifystatus.equals("0")) {
                    seekBar.setChecked(false);
                } else if (st_notifystatus.equals("1")) {
                    seekBar.setChecked(true);
                }
            }
        });

    }


    private void Method_notificationchange() {
                 if(seekBar.isChecked())
                 {
                     st_notifystatus="1";
                 }
                 else if(!seekBar.isChecked())
                 {
                     st_notifystatus="0";
                 }

        HashMap<String, String> header = new HashMap<>();
        header.put("Auth", sharedPref.getString(Constants.JWT_TOKEN));

        HashMap<String, String> request = new HashMap<>();
        request.put("notification_status", st_notifystatus);

        userProfileviewModel.getnotifysatauschangedata(header, request).observe(getActivity(), new Observer<Response_UserNotificationstatuschange>() {
            @Override
            public void onChanged(Response_UserNotificationstatuschange response_User_notificationstatuschange) {
                System.out.println("===res=user_profile==" + response_User_notificationstatuschange.toString());

                if (response_User_notificationstatuschange.status.equals("1")) {
                    Commons.Alert_custom(context, response_User_notificationstatuschange.longMessage);
                    Method_UserProfile();
                } else {
                    Commons.Alert_custom(context, response_User_notificationstatuschange.longMessage);
                }

            }
        });

    }


    @Override
    public void onStop() {
        super.onStop();
        //  ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.tv_profile:
            case R.id.ll_myprofile:
            case R.id.tv_profile_forward_ic:
                Intent tomyprofile = new Intent(getActivity(), MyProfile_Activity.class);
                context.startActivity(tomyprofile);
                break;
            case R.id.ll_faq:
            case R.id.tv_faq:
                Intent toIntent1 = new Intent(getActivity(), Faq_Activity.class);
                context.startActivity(toIntent1);
                break;
            case R.id.ll_login:
            case R.id.tv_login:
                Intent toIntent2 = new Intent(getActivity(), LoginActivity.class);
                context.startActivity(toIntent2);
                break;
            case R.id.ll_signup:
            case R.id.tv_signup:
                Intent toIntent3 = new Intent(getActivity(), Signup_Activity.class);
                context.startActivity(toIntent3);
                break;
            case R.id.ll_terms:
            case R.id.tv_tc:
                Intent toIntent4 = new Intent(getActivity(), TermsCondition.class);
                context.startActivity(toIntent4);
                break;
            case R.id.ll_privacypolicy:
            case R.id.tv_privacy_policy:
                Intent toIntent5 = new Intent(getActivity(), PrivacyPolicy_Activity.class);
                context.startActivity(toIntent5);
                break;
            case R.id.ll_logout:
            case R.id.tv_logout:
                Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                System.out.println("===vc===" + sharedPref.getString(Constants.JWT_TOKEN));
                sharedPref.clearAll();
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                gotoLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gotoLogin);
                getActivity().finish();
                break;
            case R.id.ll_mobile:
            case R.id.tv_mobile_value:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + st_mobno));
                startActivity(intent);
                break;
            case R.id.ll_whatsapp:
            case R.id.tv_whatsapp_value:
                // AppInstalled.isAppInstalled("com.whatsapp", context);
                //  openWhatsApp();

                openWhatsApp1();
                break;

        }

    }

    private void openWhatsApp1() {
        try {
            Uri uri = Uri.parse("smsto:" + st_whatsapno);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Welcome");
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.setPackage("com.whatsapp");
            startActivity(i);
    }
        catch(Exception e)

    {
        e.printStackTrace();
        Commons.Alert_custom(context, "Please Install WhatsApp in your phone");
    }
}


    public void openWhatsApp(){
      //  PackageManager pm=getPackageManager();
        try {
            String toNumber = st_whatsapno; // Replace with mobile phone number without +Sign or leading zeros, but with country code.
          /*  //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:" + "" + toNumber ));
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);*/
            String url = "https://api.whatsapp.com/send?phone=" + toNumber;
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        catch (Exception e){
            e.printStackTrace();
           // Toast.makeText(context,"it may be you dont have whats app",Toast.LENGTH_LONG).show();
            Commons.Alert_custom(context,"Please Install WhatsApp in your phone");
        }
    }
}
