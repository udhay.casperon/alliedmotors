package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Login12.Service_NewcarList;
import com.app.alliedmotor.model.NewCar.CarListItem;
import com.app.alliedmotor.model.NewCar.MakeListItem;
import com.app.alliedmotor.model.NewCar.NewCar_Response;

import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Adapter.NewCar_CarList_Adapter;
import com.app.alliedmotor.view.Adapter.NewCar_CarList_Adapter_homeBrand;
import com.app.alliedmotor.view.Adapter.NewCar_MakerList_Adapter;
import com.app.alliedmotor.viewmodel.NewCarviewModel;

import java.util.ArrayList;
import java.util.HashMap;

public class NewCarFragment extends Fragment implements NewCar_MakerList_Adapter.INewCarMakelist {

    public NewCarFragment() {

    }

    public NewCarFragment(String makeid) {
        this.makeid = makeid;
    }

    RecyclerView rv_carlist,rv_car_makerlist;
    LinearLayoutManager linearLayoutManager,linearLayoutManager1;
    View root;
    NewCarviewModel newCarviewModel;
    ArrayList<CarListItem>newcar_List=new ArrayList<>();
    ArrayList<MakeListItem>newcar_makeList=new ArrayList<>();
    NewCar_MakerList_Adapter newCar_makerList_adapter;
    NewCar_CarList_Adapter car_carList_adapter;
    NewCar_CarList_Adapter_homeBrand list_adapter_homeBrand;
    LinearLayout loading_ll,main_ll;
    private Context context;
    ImageView ic_vehicletype;
    CustomTextViewSemiBold tv_cartype;
    ArrayList<String> vehicle_typelist=new ArrayList<>();
    String slugname="new-car-details";
    String makeid="";
    String vehicle_type="";
    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;
    CustomTextViewRegular tv_emptydata;

    static  NewCarFragment newCarFragment;

    public static NewCarFragment getInstance() {
        if (newCarFragment == null)
            newCarFragment = new NewCarFragment();
        return newCarFragment;
    }


    private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {

                makeid="";
                Func_FilterOption();
                Func_Selection();
              //  Method_NewCar();
                _hasLoadedOnce = true;
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(root==null) {

            root = inflater.inflate(R.layout.fragment_newcar, null);
            newCarviewModel = ViewModelProviders.of(this).get(NewCarviewModel.class);
            context=getActivity();
            rv_car_makerlist = root.findViewById(R.id.rv_car_makerlist);
            rv_carlist = root.findViewById(R.id.rv_carlist);
          //  loading_ll = root.findViewById(R.id.loading_ll);
            main_ll = root.findViewById(R.id.main_ll);

            tv_emptydata=root.findViewById(R.id.tv_emptydata);
            ic_vehicletype = root.findViewById(R.id.ic_vehicletype);
            tv_cartype = root.findViewById(R.id.tv_cartype);
            // Car List
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rv_carlist.setLayoutManager(linearLayoutManager);

            //makerlist (brand)
            linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rv_car_makerlist.setLayoutManager(linearLayoutManager1);


            if(makeid==null) {
               // Method_NewCar();
                makeid="";
                tv_cartype.setText("All");
                Func_Selection();
            }
            else {
                Func_Selection();
            }

                Func_FilterOption();
        }


        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                Func_FilterOption();
                vehicle_type=tv_cartype.getText().toString().trim();
                if(makeid==null ||makeid=="" || makeid.equals(""))
                {
                  //  Method_NewCar();

                    Func_Selection();
                }
                else {
                    Func_Selection();
                }
            }
        };


                return root;
    }

    private void Func_FilterOption() {
        ic_vehicletype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);

                //  popup.getMenuInflater().inflate(R.menu.popup_options, popup.getMenu());
                for(int i=0;i<vehicle_typelist.size();i++) {
                    popup.getMenu().add(vehicle_typelist.get(i));
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        tv_cartype.setText(item.getTitle());
                        vehicle_type=tv_cartype.getText().toString().trim();
                        Func_Selection();
                        return true;
                    }
                });
                popup.show();//showing popup menu
            }
        });
    }

    private void Method_NewCar() {
      //  loading_ll.setVisibility(View.VISIBLE);
        Commons.showProgress(context);
        newCarviewModel.getnewCardata().observe(getActivity(), new Observer<NewCar_Response>() {
            @Override
            public void onChanged(NewCar_Response newCar_response) {

            //    loading_ll.setVisibility(View.GONE);
                main_ll.setVisibility(View.VISIBLE);
                Commons.hideProgress();

                if(newCar_response.getStatus().equals("1")) {
                    System.out.println("===res=new car=1=" + newCar_response.toString());
                    newcar_List = newCar_response.getData().getCarList();
                    newcar_makeList = newCar_response.getData().getMakeList();
                    vehicle_typelist = newCar_response.getData().getVehicleTypeList();

                    if(newCar_response.getData().getCarList().size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",newCar_response.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        Func_Adapter();
                    }

                }
                else if(newCar_response.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + newCar_response.toString());
                    Log.i("res_newcar0-->",newCar_response.toString());
                }
            }
        });



    }

    private void Func_Adapter() {

        newCar_makerList_adapter = new NewCar_MakerList_Adapter(context, newcar_makeList,this,makeid);
        rv_car_makerlist.setAdapter(newCar_makerList_adapter);
        car_carList_adapter = new NewCar_CarList_Adapter(context, newcar_List);
        rv_carlist.setAdapter(car_carList_adapter);
        rv_car_makerlist.scrollToPosition(getPosition(makeid));
    }

    @Override
    public void onBrandSelected(MakeListItem data, int position) {

        makeid=data.getMkid();
        tv_cartype.setText("All");

        autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
        autoCompleteHandler.postDelayed(autoCompleteRunnable, 500);

    }

    private void Func_Selection() {
        Commons.showProgress(context);
        newcar_List.clear();
        if(makeid==null)
        {
            makeid="";
        }
        HashMap<String, String> request = new HashMap<>();
        request.put("make_id",makeid);
        request.put("vehicle_type",vehicle_type);

        String url= Constants.BASE_URL+"v1/"+slugname+"/";
        new Service_NewcarList(url, request, new ResponseInterface.NewCarListInterface() {
            @Override
            public void onNewCarListSuccess(NewCar_Response newCar_response) {
                Commons.hideProgress();
                System.out.println("---response-selected brand--"+newCar_response.toString());
                if(newCar_response.getStatus().equals("1")) {
                    newcar_List = newCar_response.getData().getCarList();
                    newcar_makeList = newCar_response.getData().getMakeList();
                    vehicle_typelist = newCar_response.getData().getVehicleTypeList();
                    if(newCar_response.getData().getCarList().size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",newCar_response.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        if( !makeid.equals("")){
                            Func_Adapter1();}
                        else {
                            Func_Adapter();
                        }
                    }
                }
                else if(newCar_response.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + newCar_response.toString());
                    Log.i("res_newcar0-->",newCar_response.toString());
                    Toast.makeText(context,newCar_response.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }

        });

       /* newCarviewModel.getselectedMakeList(url,request).observe(getActivity(), new Observer<NewCar_Response>() {
            @Override
            public void onChanged(NewCar_Response newCarResponse) {

                Commons.hideProgress();
                System.out.println("---response-selected brand--"+newCarResponse.toString());
                if(newCarResponse.getStatus().equals("1")) {
                    newcar_List = newCarResponse.getData().getCarList();
                    newcar_makeList = newCarResponse.getData().getMakeList();
                    vehicle_typelist = newCarResponse.getData().getVehicleTypeList();
                    if(newCarResponse.getData().getCarList().size()==0)
                    {
                        rv_carlist.setVisibility(View.GONE);
                        tv_emptydata.setVisibility(View.VISIBLE);
                        tv_emptydata.setText("Car not Found");
                        Log.e("Str_messgae--->",newCarResponse.getMessage());
                    }
                    else {
                        rv_carlist.setVisibility(View.VISIBLE);
                        tv_emptydata.setVisibility(View.GONE);
                        Func_Adapter();
                    }
                }
                else if(newCarResponse.getStatus().equals("0"))
                {
                    System.out.println("===res=new car=0=" + newCarResponse.toString());
                    Log.i("res_newcar0-->",newCarResponse.toString());
                    Toast.makeText(context,newCarResponse.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }
        });

*/
    }


    private int getPosition(String makeid)
    {
        for(int i=0; i < newcar_makeList.size(); i++){
            if(newcar_makeList.get(i).getMkid().equals(makeid)){
                return i;
            }
        }
        return 0;
    }


    private void Func_Adapter1() {

        newCar_makerList_adapter = new NewCar_MakerList_Adapter(context, newcar_makeList,this,makeid);
        rv_car_makerlist.setAdapter(newCar_makerList_adapter);

        list_adapter_homeBrand = new NewCar_CarList_Adapter_homeBrand(context, newcar_List);
        rv_carlist.setAdapter(list_adapter_homeBrand);


        rv_car_makerlist.scrollToPosition(getPosition(makeid));
    }


    @Override
    public void onResume() {
        super.onResume();


    }
}
