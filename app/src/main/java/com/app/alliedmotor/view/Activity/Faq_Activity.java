package com.app.alliedmotor.view.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.FAQ_Page.Response_Faq;
import com.app.alliedmotor.viewmodel.FAQviewModel;

public class Faq_Activity extends AppCompatActivity {


    ImageView back;
    WebView webview;
    Context context;
    FAQviewModel faQviewModel;
    LinearLayout loading_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_);
        faQviewModel = ViewModelProviders.of(this).get(FAQviewModel.class);
        context=this;
        back=findViewById(R.id.back);
        webview=findViewById(R.id.webview);
        loading_ll=findViewById(R.id.loading_ll);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
                //Commons.showProgress(context);
        loading_ll.setVisibility(View.VISIBLE);
        faQviewModel.getFaqdata().observe(this, new Observer<Response_Faq>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onChanged(Response_Faq response_faq) {
                loading_ll.setVisibility(View.GONE);
                webview.setVisibility(View.VISIBLE);
               // Commons.hideProgress();
                System.out.println("===res==="+response_faq.toString());

                for (int j = 0; j < response_faq.getData().size(); j++) {
                    String content = response_faq.getData().get(0).getPost_content();

                    String font_size = "12";
                    String finalHtml = "<html><head>"
                            + "<style type=\"text/css\">li{color: #fff} span {color: #000}"
                            + "</style></head>"
                            + "<body style='font-size:" + font_size + "px" + "'>" + "<font color='black'>"
                            + content + "</font>"
                            + "</body></html>";

                    webview.loadDataWithBaseURL(null, finalHtml, "text/html", "UTF-8", null);
                }
               /* webview.loadData(
                        Base64.encodeToString(
                                content.getBytes(StandardCharsets.UTF_8),
                                Base64.DEFAULT), // encode in Base64 encoded
                        "text/html; charset=utf-8", // utf-8 html content (personal recommendation)
                        "base64");*/
               // webview.loadData(Base64.encodeToString(ht), "text/html; charset=UTF-8", null);
            }
        });



      /*  webview.requestFocus();
        webview.getSettings().

                setJavaScriptEnabled(true);

        WebSettings webSettings = webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webview.loadUrl("https://alliedmotors.com/faq-page/");
        webview.setWebViewClient(new

                                         WebViewClient() {
                                             @Override
                                             public boolean shouldOverrideUrlLoading (WebView view, String url){
                                                 view.loadUrl(url);
                                                 Commons.hideProgress();
                                                 return true;
                                             }

                                             @Override
                                             public void onPageStarted (WebView view, String url, Bitmap favicon){
                                                 super.onPageStarted(view, url, favicon);
                                             }

                                             @Override
                                             public void onPageFinished (WebView view, String url){
                                                 super.onPageFinished(view, url);

                                             }

                                         });
        Commons.hideProgress();

        webview.setWebChromeClient(new WebChromeClient() {

        });
*/
    }
}