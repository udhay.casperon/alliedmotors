package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.RHDCar.CarDatasItem;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.Car_Detail__Constarint_Activity1;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.mohammedalaa.gifloading.LoadingView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RHDCarList_Fullcar_Adapter extends RecyclerView.Adapter<RHDCarList_Fullcar_Adapter.ViewHolder> {

    ArrayList<CarDatasItem> caritems = new ArrayList<>();


    SharedPref sharedPref;
    private Context mcontext;


    public RHDCarList_Fullcar_Adapter(Context context, ArrayList<CarDatasItem> listdata) {
        this.mcontext = context;
        this.caritems = listdata;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.festival_car_rv, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String imgurl = caritems.get(position).getCarImage();

        String promoimage=caritems.get(position).getPromo_logo();
        if( promoimage == null || promoimage.equals("") || promoimage=="") {
            Picasso.get().load(imgurl)
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });            holder.promo_logo.setVisibility(View.GONE);
        }
        else
        {
            holder.promo_logo.setVisibility(View.GONE);  // Remove this line and uncomment next line to view PromoLogo
            //Picasso.get().load(caritems.get(position).getPromo_logo()).into(holder.promo_logo);
            Picasso.get().load(imgurl)
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        }
        Picasso.get().load(caritems.get(position).getCurrencyFlagImage()).into(holder.img_country_flag);
        Picasso.get().load(caritems.get(position).getMakeIcon()).into(holder.img_carlogo);
        holder.tv_makername.setText(caritems.get(position).getMakename());
        holder.tv_model_name.setText(caritems.get(position).getModelName());
        holder.tv_model_year.setText(caritems.get(position).getModelYear());
        //holder.tv_vehicle_type.setText(caritems.get(position).getVehiceType());
        String vehicle_price=caritems.get(position).getPrice();


        if(vehicle_price.isEmpty()||vehicle_price.equals(null)||vehicle_price.equals(""))
        {
            if(sharedPref.getString(Constants.JWT_TOKEN)==null)
            {
                holder.tv_veh_price.setText("Login for Price");
                holder.cv_car.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent tologin=new Intent(mcontext, LoginActivity.class);
                        mcontext.startActivity(tologin);
                    }
                });
            }
            else {
                holder.tv_veh_price.setText("Price on Request");
                holder.cv_car.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent toCarDetail =new Intent(mcontext.getApplicationContext(), Car_Detail__Constarint_Activity1.class);
                        toCarDetail.putExtra("carid",caritems.get(position).getMainId());
                        mcontext.startActivity(toCarDetail);
                    }
                });
            }
        }
        else
        {
            if(sharedPref.getString(Constants.JWT_TOKEN)==null)
            {
                holder.tv_veh_price.setText("Login for Price");
                holder.cv_car.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent tologin=new Intent(mcontext, LoginActivity.class);
                        mcontext.startActivity(tologin);
                    }
                });
            }
            else {
                holder.tv_veh_price.setText("From * "+caritems.get(position).getCurrencyName()+" "+caritems.get(position).getPrice());
                holder.cv_car.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent toCarDetail =new Intent(mcontext.getApplicationContext(), Car_Detail__Constarint_Activity1.class);
                        toCarDetail.putExtra("carid",caritems.get(position).getMainId());
                        mcontext.startActivity(toCarDetail);
                    }
                });
            }
        }



        String vehicle_type = caritems.get(position).getVehiceType();
        if(vehicle_type.contains("(")) {
            Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(vehicle_type);
            while (m.find()) {
                System.out.println("===ss===" + m.group(1));
                holder.tv_vehicle_type.setText(m.group(1));
            }
        }
        else
        {
            holder.tv_vehicle_type.setText(AppInstalled.toTitleCase(vehicle_type.toLowerCase()));
        }
    }

    @Override
    public int getItemCount() {
        return caritems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressbar;
        LoadingView loader;
        CardView cv_car;
        public ImageView imageView,img_carlogo,img_country_flag,promo_logo;
        CustomRegularTextview tv_makername,tv_model_year,tv_vehicle_type;
        CustomTextViewSemiBold tv_model_name,tv_veh_price;
        public ViewHolder(View itemView) {

            super(itemView);
            this.progressbar =  itemView.findViewById(R.id.progressbar);
            this.promo_logo =  itemView.findViewById(R.id.promo_logo);
            this.cv_car =  itemView.findViewById(R.id.cv_car);
            this.imageView =  itemView.findViewById(R.id.imageview1);
            this.img_carlogo =  itemView.findViewById(R.id.img_carlogo);
            this.img_country_flag =  itemView.findViewById(R.id.img_country_flag);
            this.tv_makername =  itemView.findViewById(R.id.tv_makername);
            this.tv_model_name =  itemView.findViewById(R.id.tv_model_name);
            this.tv_model_year =  itemView.findViewById(R.id.tv_model_year);
            this.tv_vehicle_type =  itemView.findViewById(R.id.tv_vehicle_type);
            this.tv_veh_price =  itemView.findViewById(R.id.tv_veh_price);


        }
    }
}

