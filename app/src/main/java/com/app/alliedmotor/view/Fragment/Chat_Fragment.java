package com.app.alliedmotor.view.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.MyOrderLatest.OrderDataItem;
import com.app.alliedmotor.model.MyOrderLatest.Response_MyOrderLatest;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.view.Adapter.ChatOrder_Adapter;
import com.app.alliedmotor.viewmodel.MyOrderviewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Chat_Fragment extends Fragment {


    ImageView back_setting;
    CardView cv_generalchat;
    RecyclerView rv_orderchats;
    View root;
    private Context context;



    LinearLayoutManager linearLayoutManager;
    MyOrderviewModel myOrderviewModel;
    ArrayList<OrderDataItem> ordersItemArrayList = new ArrayList<>();
    ChatOrder_Adapter chatOrder_adapter;

    SharedPref sharedPref;
    LinearLayout loading_ll, main_ll, ll_empty;
    String st_contact="";

   /* private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                _hasLoadedOnce = true;
            }
        }
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        if (root == null) {

            root = inflater.inflate(R.layout.fragment_chat_linear, container,false);
            myOrderviewModel = ViewModelProviders.of(this).get(MyOrderviewModel.class);
            context = getActivity();
            sharedPref = new SharedPref(context);

            findbyview();
            Apicall_YourOrder();
            back_setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
        Func_AppInfo();


        Func_LoginorNot();

        cv_generalchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Func_AppInfo();
                //openWhatsApp();
                openWhatsApp1();

            }
        });


        return  root;
    }

    private void Func_LoginorNot() {
        if (sharedPref.getString(Constants.JWT_TOKEN) == null || sharedPref.getString(Constants.JWT_TOKEN).equals(null) || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
            st_contact=sharedPref.getString(Constants.MOBILENO);

            } else if(sharedPref.getString(Constants.JWT_TOKEN)!=null){

            st_contact=sharedPref.getString(Constants.tv_whatsapp_value);
        }
    }



    private void openWhatsApp1() {
        try {
            Uri uri = Uri.parse("smsto:" + st_contact);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.putExtra("sms_body", "Welcome");
            i.setPackage("com.whatsapp");
            startActivity(i);
        }
        catch(Exception e)

        {
            e.printStackTrace();
            Commons.Alert_custom(context, "Please Install WhatsApp in your phone");
        }
    }

    public void openWhatsApp(){
        //  PackageManager pm=getPackageManager();
        try {
            String toNumber = st_contact; // Replace with mobile phone number without +Sign or leading zeros, but with country code.
            String url = "https://api.whatsapp.com/send?phone=" + toNumber;
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        catch (Exception e){
            e.printStackTrace();
            Commons.Alert_custom(context,"Please Install WhatsApp in your phone");
        }
    }


    private void findbyview() {
        back_setting=root.findViewById(R.id.back_setting);
        cv_generalchat=root.findViewById(R.id.cv_generalchat);
        rv_orderchats=root.findViewById(R.id.rv_orderchats);

        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_orderchats.setLayoutManager(linearLayoutManager);
        rv_orderchats.setHasFixedSize(true);
    }


    private void Apicall_YourOrder() {
        HashMap<String, String> header = new HashMap<>();
        header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));

        Log.i("req-ur order-->", String.valueOf(header));

        myOrderviewModel.getmyorderdata(header).observe(getActivity(), new Observer<Response_MyOrderLatest>() {
            @Override
            public void onChanged(Response_MyOrderLatest myOrder_response) {
                System.out.println("===res=my chats=" + myOrder_response.toString());
                if (myOrder_response.getStatus().equals("1")) {

                        ordersItemArrayList = myOrder_response.getData().getOrderData();
                        Collections.sort(ordersItemArrayList, new Comparator<OrderDataItem>() {
                            @Override
                            public int compare(OrderDataItem lhs, OrderDataItem rhs) {
                                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                                return Integer.parseInt(lhs.getTicketId()) > Integer.parseInt(rhs.getTicketId()) ? -1 : (Integer.parseInt(lhs.getTicketId()) < Integer.parseInt(rhs.getTicketId()) ) ? 1 : 0;
                            }
                        });
                        chatOrder_adapter = new ChatOrder_Adapter(context, ordersItemArrayList);
                        rv_orderchats.setAdapter(chatOrder_adapter);

                } else if (myOrder_response.getStatus().equals("0")) {
                    Log.i("-->res onstaus0==", myOrder_response.toString());
                    Commons.hideProgress();
                  //  Toast.makeText(context, myOrder_response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");

        }

        myOrderviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                    if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("")) {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                    }
                    else {
                        sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());

                        String count = response_appInfo.getData().getUnreadNotificationCount();
                        if(count.equals("0")) {


                        }
                        else
                        {
                            sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                            sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptionsContactPhoneNumber());

                        }
                    }

                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }




}
