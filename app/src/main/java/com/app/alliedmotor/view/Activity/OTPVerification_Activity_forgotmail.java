package com.app.alliedmotor.view.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.ForgotEmail.Response_ForgotEmail;
import com.app.alliedmotor.model.Response_OtpRequest;
import com.app.alliedmotor.model.Response_VerfiyOTP.Response_VerifyOTP;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.viewmodel.ForgotEmailviewModel;
import com.app.alliedmotor.viewmodel.VerifyOTPviewModel;
import com.poovam.pinedittextfield.LinePinField;

import java.util.HashMap;

public class OTPVerification_Activity_forgotmail extends AppCompatActivity implements View.OnClickListener{



    CustomRegularButton bt_submit;
  ImageView back_setting;
    String st_otp,st_reotp;
    Context context;
    VerifyOTPviewModel verifyOTPviewModel;
    CustomRegularTextview tv_resent_otp;
    CustomRegularTextview tv_otpviewcontent;
    ForgotEmailviewModel forgotEmailviewModel;
    String res_otp,otp_type,r_contentmessage,r_otpdevmode,r_otptoken;
    SharedPref sharedPref;

    LinePinField otppinfield;
    String r_fname="",r_lname="",r_emai="",e_mobile="",r_usertype="",r_company="",r_userrole="",r_password="",r_userletter="",r_otptype="";
  String r_pagename="verifyphonenumber";


    Runnable autoCompleteRunnable;
    Handler autoCompleteHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_verification_forgotemail);
        forgotEmailviewModel = ViewModelProviders.of(this).get(ForgotEmailviewModel.class);
        verifyOTPviewModel = ViewModelProviders.of(this).get(VerifyOTPviewModel.class);

        context=this;
        findbyviews();
        clicklistener();
        sharedPref=new SharedPref(context);



        Intent getIntent=getIntent();

        if(getIntent!=null) {

            r_emai = getIntent.getStringExtra("useremail");
            e_mobile = getIntent.getStringExtra("mobilenumber");
            r_otptype = getIntent.getStringExtra("otptype");
            r_contentmessage = getIntent.getStringExtra("contentmessage");
            r_otpdevmode = getIntent.getStringExtra("otpdevmode");
            r_otptoken = getIntent.getStringExtra("otptoken");
            r_pagename=getIntent.getStringExtra("pagename");
        }
        tv_otpviewcontent.setText(r_contentmessage);
        Commons.Alert_custom(context,r_contentmessage);


        if(r_otpdevmode.equals("1"))
        {
            System.out.println("--Development---");
            otppinfield.setText(r_otptoken);
        }
        else if(r_otpdevmode.equals("0"))
        {
            System.out.println("--Production---");
           // new OTPReceiver_forgotemail().setEditText_otp(otppinfield);
        }

        autoCompleteHandler = new Handler();
        autoCompleteRunnable = new Runnable() {
            public void run() {
                Func_OtpResent();
            }
        };

        requestPermissions();
      //  new OTPReceiver_forgotemail().setEditText_otp(otppinfield);



    }


    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(OTPVerification_Activity_forgotmail.this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(OTPVerification_Activity_forgotmail.this,new String[]{
                    Manifest.permission.RECEIVE_SMS
            },100);
        }
    }


    private void findbyviews() {
        bt_submit=findViewById(R.id.bt_submit);
        back_setting=findViewById(R.id.back_setting);
        otppinfield=findViewById(R.id.otppinfield);
        tv_otpviewcontent=findViewById(R.id.tv_otpviewcontent);
        tv_resent_otp=findViewById(R.id.tv_resent_otp);

    }

    private void clicklistener() {
        bt_submit.setOnClickListener(this);
        tv_resent_otp.setOnClickListener(this);
        back_setting.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back_setting:
                Intent tologin=new Intent(OTPVerification_Activity_forgotmail.this,ForgotPassword.class);
                startActivity(tologin);
                finish();
                break;
            case R.id.tv_resent_otp:
                autoCompleteHandler.removeCallbacks(autoCompleteRunnable);
                autoCompleteHandler.postDelayed(autoCompleteRunnable, 500);
                break;
            case R.id.bt_submit:
                Validtion_OTP();
                break;

        }
    }



    private void Func_OtpResent() {

        Commons.showProgress(context);
        HashMap<String, String> request_forgotemail= new HashMap<>();
        request_forgotemail.put("mobile_number",e_mobile);
        System.out.println("==req-forgot-usermail---"+request_forgotemail);
        forgotEmailviewModel.getforgotemaildata(request_forgotemail).observe(OTPVerification_Activity_forgotmail.this, new Observer<Response_ForgotEmail>() {
            @Override
            public void onChanged(Response_ForgotEmail response_forgotEmail) {
                Commons.hideProgress();
                System.out.println("===respo---forgot pwd=="+response_forgotEmail.toString());
                if(response_forgotEmail.getStatus().equals("1"))
                {
                    otppinfield.getText().clear();
                    if(response_forgotEmail.getData().getDevMode()==1)
                    {
                        System.out.println("===localserver===");
                        r_otptoken=String.valueOf(response_forgotEmail.getData().getCode());
                        r_otpdevmode=String.valueOf(response_forgotEmail.getData().getDevMode());
                        System.out.println("--Development---");
                        otppinfield.setText(r_otptoken);

                    }
                    else
                    {
                        System.out.println("==production===");
                        r_otptoken=String.valueOf(response_forgotEmail.getData().getCode());
                       // new OTPReceiver_forgotemail().setEditText_otp(otppinfield);
                        otppinfield.setText(r_otptoken);
                       // Commons.Alert_custom(context,response_forgotEmail.getData().getCode().toString());
                    }
                }

            }
        });

    }

    private void Validtion_OTP() {
         st_otp=otppinfield.getText().toString().trim();

         if(st_otp.isEmpty())
         {
             Commons.Alert_custom(context,"Please Enter OTP");
         }
         else if(!r_otptoken.equals(st_otp))
         {
             Commons.Alert_custom(context,"Entered Otp wrong");
         }
        else
        {
            Call_API_VerifyOTP_Mail();
        }
    }

    private void Call_API_VerifyOTP_Mail() {
        HashMap<String, String> header= new HashMap<>();
        header.put("Auth","");

        HashMap<String, String> request= new HashMap<>();
        request.put("received_otp",r_otptoken);
        request.put("enter_otp",st_otp);
        request.put("otp_type",r_otptype);
      forgotEmailviewModel.getVerifyOTpdata(header,request).observe(OTPVerification_Activity_forgotmail.this, new Observer<Response_VerifyOTP>() {
                  @Override
                  public void onChanged(Response_VerifyOTP verifyOTP) {

                    if(verifyOTP.status.equals("1"))
                    {
                        Toast.makeText(context, r_emai, Toast.LENGTH_SHORT).show();
                        Intent tologin=new Intent(OTPVerification_Activity_forgotmail.this,LoginActivity.class);
                        startActivity(tologin);
                        finish();
                    }
                    else if(verifyOTP.status.equals("0")){
                       // Toast.makeText(context, verifyOTP.longMessage, Toast.LENGTH_SHORT).show();

                    }
                  }
              });

          }


    private void Func_alertfialog(String content) {

        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.custom_alert_layout_ios_singlebutton);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
        CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
        txttitle.setText("Allied Motors");
        txtInstructionDialog.setText(content);
        txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
        CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_okay);
        CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_cancel);
        btokay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent tologin=new Intent(OTPVerification_Activity_forgotmail.this,LoginActivity.class);
               startActivity(tologin);
               finish();
                alertDialog.dismiss();
            }
        });


        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing())
            alertDialog.show();

        }
}
