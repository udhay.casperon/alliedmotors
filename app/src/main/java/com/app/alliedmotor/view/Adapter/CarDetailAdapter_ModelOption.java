package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.CarDetail.ModelOptionListItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;

import java.util.ArrayList;

public class CarDetailAdapter_ModelOption extends RecyclerView.Adapter<CarDetailAdapter_ModelOption.ViewHolder> {

    ArrayList<ModelOptionListItem> features;

    SharedPref sharedPref;
    private Context mcontext;
    int current_position=-1;
    int previousposition=0;
    String name_model="";


    ModelOptionSelected listener;

    public interface ModelOptionSelected
    {
        void onmodelOptionSelected(String data,String modeloption_name);

    }



    public CarDetailAdapter_ModelOption(Context context, ArrayList<ModelOptionListItem> features,ModelOptionSelected listener,String namemodel) {
        this.mcontext = context;
        this.features = features;
        this.listener=listener;
        this.name_model=namemodel;
        for (int k = 0; k < features.size(); k++) {
            if (name_model.equals(features.get(k).getModelOptions())) {
                current_position = k;
            }
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rv_filter, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
       /* if(previousposition!=0)
        {
            current_position=previousposition;
        }*/
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_value.setText((features.get(position).getModelOptions()));
        holder.ll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onmodelOptionSelected(features.get(position).getMoId(),features.get(position).getModelOptions());
                current_position=position;
                notifyDataSetChanged();
            }
        });

        if(current_position==position)
        {
            holder.ll_filter.setBackgroundResource(R.drawable.edittext_roundshadow);
            holder.tv_value.setTextColor(mcontext.getResources().getColor(R.color.white));
        }
        else {
            holder.ll_filter.setBackgroundResource(R.drawable.edittext_roundshadow_outline_grey);
            holder.tv_value.setTextColor(mcontext.getResources().getColor(R.color.black));
        }
    }

    @Override
    public int getItemCount() {
      return features.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewSemiBold tv_value;
        LinearLayout ll_filter;

        public ViewHolder(View itemView) {

            super(itemView);

            tv_value=itemView.findViewById(R.id.tv_value);
            ll_filter=itemView.findViewById(R.id.ll_filter);

        }
    }
}

