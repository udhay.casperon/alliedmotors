package com.app.alliedmotor.view.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.alliedmotor.R;
import com.app.alliedmotor.database.MyDataBase;
import com.app.alliedmotor.model.AppInfo.Response_AppInfo;
import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.model.Login12.Service_LoginDetail;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.PasswordValidator;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.viewmodel.LoginviewModel;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    String FCM_token="";
    private Context context;
    private static final int REQUEST_CODE_GIS_SAVE_PASSWORD = 2;
    private LoginviewModel loginviewModel;
    String st_userlogin,st_userpass,st_devicetype;
    AutoCompleteTextView et_userpass;
    AutoCompleteTextView et_username;
    CustomRegularButton bt_signin;
    CustomRegularTextview tv_signup,tv_forgot_password,tv_guestuser,tv_forgotmail;
    PasswordValidator passwordValidator;
   static CheckBox checkbox_rememberpassowrd;
    SharedPref sharedPref;
    SpinKitView spin_kit;
    private static int TIME_LOADER = 2000;
  String JWTToken_st="";
    MyDataBase myDataBase;

    private ArrayList<String> allnamefromdb=new ArrayList<>();
    private ArrayList<String> allpasswordfromdb=new ArrayList<>();
    String password_fromdb="";
    String res_emailaddress="";
    ArrayAdapter<String>autocomplete_useremail,autocomplete_userpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginviewModel = ViewModelProviders.of(this).get(LoginviewModel.class);
        context=this;
        sharedPref=new SharedPref(context);
        myDataBase=new MyDataBase(context);



        Func_AppInfo();
        st_devicetype="Android";
        passwordValidator = new PasswordValidator();
        boolean valid = passwordValidator.validate("mkyong1A@");
        findbyviews();
        clicklistener();



        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                         FCM_token = task.getResult();

                        System.out.println("------token--------"+FCM_token);

                        // Log and toast
//                        String msg = getString(R.string.msg_token_fmt, token);
//                        Log.d(TAG, msg);
//                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });





        allnamefromdb = myDataBase.getUsername();
        if(allnamefromdb.size()!=0) {
            autocomplete_useremail=new ArrayAdapter<>(LoginActivity.this,android.R.layout.simple_list_item_1,allnamefromdb);
            et_username.setAdapter(autocomplete_useremail);
            et_username.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(et_username.isPopupShowing())
                    {
                        password_fromdb=myDataBase.getpassword(et_username.getText().toString().trim());
                        et_userpass.setText(password_fromdb);
                    }
                    et_username.setText(autocomplete_useremail.getItem(position));
                    checkbox_rememberpassowrd.setChecked(true);
                }
            });
        }




        if(getIntent()!=null) {
            Intent fromIntnet = getIntent();
            res_emailaddress = fromIntnet.getStringExtra("emailaddress");
            et_username.setText(res_emailaddress);
        }
    }


    private void Func_AppInfo() {
        HashMap<String,String>header=new HashMap<>();
        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals(""))
        {
            header.put("Auth","");
            header.put("langname","en");
        }
        else
        {
            header.put("Auth",sharedPref.getString(Constants.JWT_TOKEN));
            header.put("langname","en");
        }

        loginviewModel.getAppInfodata(header).observe(this, new Observer<Response_AppInfo>() {
            @Override
            public void onChanged(Response_AppInfo response_appInfo) {
                //  Commons.hideProgress();
                if(response_appInfo.getStatus().equals("1"))
                {
                    System.out.println("resp===app info==="+response_appInfo.toString());

                    sharedPref.setString(Constants.PREF_USERNAME, response_appInfo.getData().getUserName());
                    sharedPref.setString(Constants.PREF_USERMAIL, response_appInfo.getData().getUserEmail());
                    sharedPref.setString(Constants.MOBILENO,response_appInfo.getData().getOptions_whatsapp_2_number());
                    sharedPref.setString(Constants.PREF_UNREAD_NOTIFICATION,response_appInfo.getData().getUnreadNotificationCount());
                }
                else if(response_appInfo.getStatus().equals("0"))
                {
                    System.out.println("res==="+response_appInfo.toString());
                }
            }
        });

    }



    private void findbyviews() {
        spin_kit=findViewById(R.id.spin_kit);
        et_username=findViewById(R.id.et_username);
        et_userpass=findViewById(R.id.et_userpass);
        bt_signin=findViewById(R.id.bt_signin);
        tv_signup=findViewById(R.id.tv_signup);
        checkbox_rememberpassowrd=findViewById(R.id.checkbox_rememberpassowrd);
        tv_forgot_password=findViewById(R.id.tv_forgot_password);
        tv_guestuser=findViewById(R.id.tv_guestuser);
        tv_forgotmail=findViewById(R.id.tv_forgotmail);
      //  loading_ll=findViewById(R.id.loading_ll);
    }

    private void clicklistener() {

        bt_signin.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        checkbox_rememberpassowrd.setOnClickListener(this);
        tv_forgot_password.setOnClickListener(this);
        tv_guestuser.setOnClickListener(this);
        tv_forgotmail.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.bt_signin:
                Validation();
                break;
            case R.id.tv_signup:
                Intent signupIntent=new Intent(LoginActivity.this,Signup_Activity.class);
                startActivity(signupIntent);
                finish();
                break;

            case R.id.tv_forgot_password:
                Intent forgotpwdIntent=new Intent(LoginActivity.this,ForgotPassword.class);
                startActivity(forgotpwdIntent);
                finish();
                break;

            case R.id.tv_forgotmail:
                Intent forgotmailIntent=new Intent(LoginActivity.this,ForgotEmail.class);
                startActivity(forgotmailIntent);
                finish();
                break;
            case R.id.tv_guestuser:
                Intent guestuserIntent=new Intent(LoginActivity.this, HomeActivity_searchimage_BottomButton.class);
                sharedPref.setString(Constants.JWT_TOKEN,"");
                startActivity(guestuserIntent);
                finish();
                break;
        }

    }

    private void Validation() {

        st_userpass=et_userpass.getText().toString().trim();
        st_userlogin=et_username.getText().toString().trim();

        if(st_userlogin.isEmpty()&& st_userpass.isEmpty())
        {
            Commons.Alert_custom(context,"Please give the All details");
            Commons.hideProgress();
        }
        else if(st_userlogin.isEmpty())
        {
            Commons.Alert_custom(context,"Please enter valid user Email");
            Commons.hideProgress();
        }
        else if(st_userpass.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter password");
            Commons.hideProgress();
        }
        else if(!passwordValidator.validate(st_userpass))
        {
            Commons.Alert_custom(context,"Minimum of 8 characters in length, Should contain atleast one uppercase,lowercase,number and special Character");
            Commons.hideProgress();
        }
        else
        {
            Method_loginservice();
        }
    }

    private void Method_loginservice() {
        Commons.showProgress(context);
        HashMap<String, String> request_login = new HashMap<>();
        request_login.put("user_login",st_userlogin);
        request_login.put("user_pass",st_userpass);
        request_login.put("device_type",st_devicetype);
        request_login.put("device_id",FCM_token);
        System.out.println("==req---login---"+request_login);

        String loginurl = Constants.BASE_URL + "v1/user-login";

        new Service_LoginDetail(loginurl,request_login, new ResponseInterface.LoginInterface() {
            @Override
            public void onloginsuccessSuccess(LoginResponse loginResponse) {
                Commons.hideProgress();

                if (loginResponse.getStatus().equals("1")) {
                    JWTToken_st=loginResponse.getCommonArr().getToken();
                    if(checkbox_rememberpassowrd.isChecked())
                    {
                        boolean recordexxit = myDataBase.recordExist_useremail(st_userlogin);
                        String password_fromdb=myDataBase.getpassword(st_userpass);
                        if (recordexxit && password_fromdb.equals(st_userpass)) {
                            func_tohome();
                        }
                       else if (recordexxit && !password_fromdb.equals(st_userpass)) {
                            dialog_updatepassword();
                        }
                        else {
                            dialog_savepassword();
                        }

                    }
                    else if(!checkbox_rememberpassowrd.isChecked()) {
                        sharedPref.setString(Constants.JWT_TOKEN, JWTToken_st);
                        System.out.println("--token===" + JWTToken_st);
                        sharedPref.setString(Constants.PREF_LOGIN_STATUS, "1");
                        System.out.println("--token==loginstatus=" + sharedPref.getString(Constants.PREF_LOGIN_STATUS));
                        func_tohome();
                    }

                }
                else if (loginResponse.getStatus().equals("0"))
                {

                    System.out.println("===res==failure=" + loginResponse.toString());
                    Commons.Alert_custom(context,loginResponse.getLong_message());
                    et_userpass.getText().clear();
                }

            }


        });

/*

       loginviewModel.getLoginData(request_login).observe(LoginActivity.this, new Observer<LoginResponse>() {
            @Override
            public void onChanged(LoginResponse response_login) {
                Commons.hideProgress();

                if (response_login.getStatus().equals("1")) {
                    Log.e("login->",request_login.toString());
                    System.out.println("===res===" + response_login.toString());
                        if(checkbox_rememberpassowrd.isChecked())
                        {
                            myDataBase.Insert_loginsample(st_userlogin,st_userpass);
                        }
                    sharedPref.setString(Constants.JWT_TOKEN, response_login.getCommonArr().getToken());
                    System.out.println("--token===" + sharedPref.getString(Constants.JWT_TOKEN));

                    System.out.println("--token==loginstatus=" + sharedPref.getString(Constants.PREF_LOGIN_STATUS));

                        Intent tomain = new Intent(LoginActivity.this, HomeActivity_searchimage_BottomButton.class);
                        sharedPref.setString(Constants.PREF_LOGIN_STATUS, "1");
                        tomain.putExtra("useremail",st_userlogin);
                        tomain.putExtra("userpassword",st_userpass);
                        System.out.println("--token==loginstatus=ss==" + sharedPref.getString(Constants.PREF_LOGIN_STATUS));
                        spin_kit.setVisibility(View.GONE);
                        startActivity(tomain);
                        finish();
                }
                else if (response_login.getStatus().equals("0"))
                {

                    System.out.println("===res==failure=" + response_login.toString());
                    Commons.Alert_custom(context,response_login.getLong_message());
                    et_userpass.getText().clear();
                }

            }
        });
*/

    }

    private void func_tohome() {

        Intent tomain = new Intent(LoginActivity.this, HomeActivity_searchimage_BottomButton.class);
        sharedPref.setString(Constants.PREF_LOGIN_STATUS, "1");
        sharedPref.setString(Constants.JWT_TOKEN, JWTToken_st);
        startActivity(tomain);
        finish();
    }


    //update password
    private void dialog_updatepassword() {

        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.bottomsheet_changepassword);
            alertDialog.setCancelable(true);
            Window window = alertDialog.getWindow();
            window.setGravity(Gravity.BOTTOM);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Your Password has been changed, do you want to save the password?");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myDataBase.update(st_userlogin,st_userpass);
                    func_tohome();
                    alertDialog.dismiss();
                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    func_tohome();
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();


        }
    }

   //Save PAssword dislaog

    private void dialog_savepassword() {

        {
            final Dialog alertDialog;
            alertDialog = new Dialog(context);
            alertDialog.setContentView(R.layout.bottomsheet_changepassword);
            alertDialog.setCancelable(true);
            Window window = alertDialog.getWindow();
            window.setGravity(Gravity.BOTTOM);
            alertDialog.setCanceledOnTouchOutside(true);
            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CustomTextViewRegular txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            CustomTextViewSemiBold txttitle = alertDialog.findViewById(R.id.txttitle);
            txttitle.setText("Allied Motors");
            txtInstructionDialog.setText("Allied Motors want to save your password");
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            CustomRegularTextview btokay = alertDialog.findViewById(R.id.bt_button1);
            CustomRegularTextview bt_cancel = alertDialog.findViewById(R.id.bt_button2);
            btokay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                  /*  boolean recordexxit = myDataBase.recordExist_useremail(st_userlogin);
                    if (recordexxit) {
                        // Toast.makeText(getBaseContext(), "exist", Toast.LENGTH_SHORT).show();
                        myDataBase.update(st_userlogin,st_userpass);
                    }
                    else {*/
                        myDataBase.Insert_loginsample(st_userlogin, st_userpass);
                        func_tohome();
                        alertDialog.dismiss();

                }
            });


            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    func_tohome();
                    alertDialog.dismiss();
                }
            });

            if (!alertDialog.isShowing())
                alertDialog.show();


        }
    }


}