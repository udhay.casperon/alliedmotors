package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;

import com.app.alliedmotor.model.NewCar.MakeListItem;
import com.app.alliedmotor.model.Pre_OwnedCar.CarDetailsItem;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Activity.PreOwnedCar_Detail_Activity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PreOwned_Adapter extends RecyclerView.Adapter<PreOwned_Adapter.ViewHolder> {

    ArrayList<CarDetailsItem> caritems;


    SharedPref sharedPref;
    private Context mcontext;
    PreownedCar_Listener listener;

    public interface PreownedCar_Listener  {
        void onBrandSelected(String car_preId);
    }


    public PreOwned_Adapter(Context context, ArrayList<CarDetailsItem> listdata,PreownedCar_Listener listener) {
        this.mcontext = context;
        this.caritems = listdata;
        this.listener=listener;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.preowned_rv_linear_constarint, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String imgurl = caritems.get(position).getMainImg();


        String promoimage=caritems.get(position).getPromoLogo();
        if( promoimage == null || promoimage.equals("") || promoimage=="") {
            Picasso.get().load(imgurl)
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
            holder.promo_logo.setVisibility(View.GONE);
        }
        else
        {
            holder.promo_logo.setVisibility(View.GONE);  // Remove this line and uncomment next line to view PromoLogo
            //Picasso.get().load(caritems.get(position).getPromoLogo()).into(holder.promo_logo);
            Picasso.get().load(imgurl)
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        }

        Picasso.get().load(caritems.get(position).getMakeIcon()).into(holder.img_carlogo);
        String modelname = caritems.get(position).getModelName();
        holder.tv_model_name.setText(modelname);
        holder.tv_makername.setText(caritems.get(position).getMakename());
        holder.tv_model_year.setText(caritems.get(position).getModelYear());
        holder.tv_transmission.setText(caritems.get(position).getTransmission());
        holder.tv_vehicle_type.setText(caritems.get(position).getVehiceType());
        holder.tv_milage_type.setText("Mileage: "+caritems.get(position).getMileage());

        String vehicle_price = caritems.get(position).getPrice();

        if(sharedPref.getString(Constants.JWT_TOKEN)==null || sharedPref.getString(Constants.JWT_TOKEN).equals("") || sharedPref.getString(Constants.JWT_TOKEN).equals(null))
        {

            holder.tv_veh_price.setText("Login for Price");
                holder.tv_veh_price.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent tologin = new Intent(mcontext, LoginActivity.class);
                        mcontext.startActivity(tologin);
                    }
                });
        }
        else if(sharedPref.getString(Constants.JWT_TOKEN)!=null)
        {

            if(vehicle_price.isEmpty()||vehicle_price.equals(null)||vehicle_price.equals("")) {
                holder.tv_veh_price.setText("Price on Request");
            }
            else
            {
                holder.tv_veh_price.setText("From * "+caritems.get(position).getCurrency_name()+" "+caritems.get(position).getPrice());
            }
        }



        holder.cv_preowned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBrandSelected(caritems.get(position).getPreId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return caritems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressbar;
        CardView cv_preowned;
        public ImageView imageView,img_carlogo,img_country_flag,ic_share,ic_favourite,promo_logo;
        CustomRegularTextview tv_makername,tv_model_year,tv_vehicle_type,tv_milage_type,tv_transmission;
        CustomTextViewSemiBold tv_model_name,tv_veh_price;
        public ViewHolder(View itemView) {

            super(itemView);
            this.progressbar =  itemView.findViewById(R.id.progressbar);
            this.cv_preowned =  itemView.findViewById(R.id.cv_preowned);

            this.promo_logo =  itemView.findViewById(R.id.promo_logo);

            this.imageView =  itemView.findViewById(R.id.imageview1);
           /* this.ic_share =  itemView.findViewById(R.id.ic_share);
            this.ic_favourite =  itemView.findViewById(R.id.ic_favourite);*/
            this.tv_milage_type =  itemView.findViewById(R.id.tv_milage_type);

            this.tv_transmission =  itemView.findViewById(R.id.tv_transmission);
            this.img_carlogo =  itemView.findViewById(R.id.img_carlogo);
            this.img_country_flag =  itemView.findViewById(R.id.img_country_flag);
            this.tv_makername =  itemView.findViewById(R.id.tv_makername);
            this.tv_model_name =  itemView.findViewById(R.id.tv_model_name);
            this.tv_model_year =  itemView.findViewById(R.id.tv_model_year);
            this.tv_vehicle_type =  itemView.findViewById(R.id.tv_vehicle_type);
            this.tv_veh_price =  itemView.findViewById(R.id.tv_veh_price);



        }
    }
}

