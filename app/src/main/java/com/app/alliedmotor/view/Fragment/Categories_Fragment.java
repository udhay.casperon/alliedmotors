package com.app.alliedmotor.view.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.HomePage.CategoriesItem;
import com.app.alliedmotor.model.HomePage.Response_Homepage;
import com.app.alliedmotor.view.Activity.HomeActivity_searchimage_BottomButton;
import com.app.alliedmotor.view.Adapter.HomePage_Categories_Adapter;
import com.app.alliedmotor.view.Adapter.ImageBanner_Adapter_Static;
import com.app.alliedmotor.viewmodel.HomepageviewModel;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Categories_Fragment extends Fragment {

    View root;
    RecyclerView rv_category;
    Dialog Category_dialog;
    ArrayList<CategoriesItem>categoriesItems=new ArrayList<>();
    HomePage_Categories_Adapter homePageCategories_adapter;
    HomepageviewModel homepageviewModel;
    LinearLayout loading_ll,main_ll;
    RelativeLayout linearLayout;
    Context context;
    ViewPager photos_viewpager;
    TabLayout tabindiactor;

    Timer timer;
    private int NUM_PAGES;
    final long DELAY_MS = 1000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    private static int currentPage = 0;
    PagerAdapter imageBanner_adapter;
    RecyclerView grid_categories;
    RelativeLayout rl_container1,bottomsheet_ll;

    private static final Integer[] IMAGES= {R.drawable.banner1,R.drawable.banner2,R.drawable.banner3,R.drawable.banner4,R.drawable.banner5,R.drawable.banner6};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

RelativeLayout ll_photopager,rl_newcontent1;
LinearLayout ll_photopager1;

    /*private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                _hasLoadedOnce = true;
            }
        }
    }*/


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        if (root == null) {
            root = inflater.inflate(R.layout.fragment_category, container,false);
            homepageviewModel = ViewModelProviders.of(this).get(HomepageviewModel.class);
            context=getActivity();

            linearLayout = root.findViewById(R.id.bottomsheet_ll);
            loading_ll=root.findViewById(R.id.loading_ll);
            main_ll=root.findViewById(R.id.main_ll);
            rv_category = root.findViewById(R.id.rv_category);
            rl_container1=root.findViewById(R.id.rl_container1);
            rl_newcontent1=root.findViewById(R.id.rl_newcontent1);

            bottomsheet_ll=root.findViewById(R.id.bottomsheet_ll);
            ll_photopager=root.findViewById(R.id.ll_photopager);

            ll_photopager1=root.findViewById(R.id.ll_photopager1);

            for(int i=0;i<IMAGES.length;i++)
                ImagesArray.add(IMAGES[i]);

            photos_viewpager = root.findViewById(R.id.photos_viewpager);
            tabindiactor = root.findViewById(R.id.tabindiactor);
            grid_categories = root.findViewById(R.id.grid_categories);

            GridLayoutManager gridLayoutManager1 = new GridLayoutManager(context, 4);
            gridLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
            grid_categories.setLayoutManager(gridLayoutManager1);
            grid_categories.setHasFixedSize(true);


            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 4);
            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            // set Horizontal Orientation
            rv_category.setLayoutManager(gridLayoutManager);
            rv_category.setHasFixedSize(true);
            loading_ll.setVisibility(View.VISIBLE);
            func();

            homepageviewModel.getHomepagedata().observe(getActivity(), new Observer<Response_Homepage>() {
                @Override
                public void onChanged(Response_Homepage response_homepage) {
                    loading_ll.setVisibility(View.GONE);
                    main_ll.setVisibility(View.VISIBLE);
                    rv_category.setVisibility(View.VISIBLE);
                    if(response_homepage.getStatus().equals("1")) {
                        categoriesItems = response_homepage.getData().getCategories();
                        homePageCategories_adapter = new HomePage_Categories_Adapter(context, categoriesItems);
                        rv_category.setAdapter(homePageCategories_adapter);
                        grid_categories.setAdapter(homePageCategories_adapter);

                    }
                    else if(response_homepage.getStatus().equals("0"))
                    {
                        Log.i("fav-status0==>",response_homepage.toString());
                    }

                }
            });



            rl_newcontent1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tocart = new Intent(context, HomeActivity_searchimage_BottomButton.class);
                    tocart.putExtra("tabposition", "categories");
                    startActivity(tocart);
                    getActivity().finish();
                }
            });

            ll_photopager1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tocart = new Intent(context, HomeActivity_searchimage_BottomButton.class);
                    tocart.putExtra("tabposition", "categories");
                    startActivity(tocart);
                    getActivity().finish();
                }
            });





            rl_container1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent tocart = new Intent(context, HomeActivity_searchimage_BottomButton.class);
                    tocart.putExtra("tabposition", "categories");
                    startActivity(tocart);
                    getActivity().finish();


                }
            });
        }
        else {
            ((ViewGroup) root.getParent()).removeView(root);
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("====>catOnResume call---->");

    }


    @Override
    public void onPause() {
        super.onPause();
        System.out.println("====>catpausecall---->");
    }

    public void  func()
    {
        //NUM_PAGES = bannerDetailsItems.size();
        NUM_PAGES = IMAGES.length;
        imageBanner_adapter = new ImageBanner_Adapter_Static(context, ImagesArray);
        photos_viewpager.setAdapter(imageBanner_adapter);
        photos_viewpager.setPageMargin(50);
        photos_viewpager.setClipToPadding(false);
        photos_viewpager.setPadding(5, 5, 5, 5);
        tabindiactor.setupWithViewPager(photos_viewpager, true);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                photos_viewpager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }




}
