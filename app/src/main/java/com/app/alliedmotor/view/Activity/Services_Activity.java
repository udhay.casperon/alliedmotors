package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.Commons;

public class Services_Activity extends AppCompatActivity {


    ImageView back;
    WebView webview;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);


        context = this;
        back = findViewById(R.id.back);
        webview = findViewById(R.id.webview);
        Commons.showProgress(context);
        webview.requestFocus();
        webview.getSettings().

                setJavaScriptEnabled(true);

        WebSettings webSettings = webview.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webview.loadUrl("https://alliedmotors.com/service/");
        webview.setWebViewClient(new

                                         WebViewClient() {
                                             @Override
                                             public boolean shouldOverrideUrlLoading (WebView view, String url){
                                                 view.loadUrl(url);
                                                 return true;
                                             }

                                             @Override
                                             public void onPageStarted (WebView view, String url, Bitmap favicon){
                                                 super.onPageStarted(view, url, favicon);
                                             }

                                             @Override
                                             public void onPageFinished (WebView view, String url){
                                                 super.onPageFinished(view, url);

                                             }

                                         });
        Commons.hideProgress();

       /* newseventsviewModel.getnewsEventsdata().observe(this, new Observer<Response_NewsEvents>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onChanged(Response_NewsEvents response_newsEvents) {
                Commons.hideProgress();
                System.out.println("===res=pp=="+response_newsEvents.toString());

                    content = response_newsEvents.getData().get(0).getPostContent();


                String font_size="14";
                String finalHtml = "<html><head>"
                        + "<style type=\"text/css\">li{color: #fff} span {color: #000}"
                        + "</style></head>"
                        +"<body style='font-size:"+font_size+"px"+"'>"+"<font color='black'>"
                        + content+"</font>"
                        + "</body></html>";
                webview.loadDataWithBaseURL(null,finalHtml,"text/html", "UTF-8",null);
            }
        });
*/


        back.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View v){
                onBackPressed();
                finish();
            }
        });
}
}