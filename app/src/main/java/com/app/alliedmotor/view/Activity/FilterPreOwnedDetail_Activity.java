package com.app.alliedmotor.view.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Pojo_PreOwnedFilter;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.viewmodel.PreownedCardetail_FilterViewModel;
import com.app.alliedmotor.viewmodel.PreownedviewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

public class FilterPreOwnedDetail_Activity extends AppCompatActivity {


    CustomRegularEditText et_make,et_model,et_fuel,et_transmission,et_year,et_maxyear,et_mileage,et_body_condition,et_dealer_warranty,et_exteriorcolor,et_interior_color,et_descrption, et_engine,et_varient,et_door;
    ConstraintLayout ll_make,ll_model,ll_fueltype,ll_transmission,ll_select_year,ll_select_maxyear;
    ConstraintLayout ll_mileage,ll_body_condition,ll_dealer_warranty,ll_exteriorcolor,ll_interior_color,ll_descrption;
    LinearLayout ll_varienttype,ll_door,ll_enginetype;
    String temp_makelist="",temp_model="",temp_fuel="",temp_tranmission="",temp_year="",temp_maxyear="",temp_mileage="",temp_extcolor="",temp_intcolor="";
    String temp_engine="",tempvarient="",tempdoors="";
    int makelistsize,modellistsize;
    String[] str_makedid;
    String[] strmodellist;
    String ip_makeid,ipmodelid;
    Context context;
    SharedPref sharedPref;
    PreownedviewModel preownedviewModel;
    ArrayList<Response_CarMakeList.DataItem> carmakelist=new ArrayList<>();
    ArrayList<Response_CarModelList.DataItem>carmodellist=new ArrayList<>();
    PreownedCardetail_FilterViewModel preownedCardetail_filterViewModel;
    int index=0;
    ImageView img_home_back;
    com.app.alliedmotor.utility.widgets.CustomRegularButton bt_go,bt_advanced;
    LinearLayout ll_advanced;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_owned_detail_);
        preownedCardetail_filterViewModel = ViewModelProviders.of(this).get(PreownedCardetail_FilterViewModel.class);
        preownedviewModel = ViewModelProviders.of(this).get(PreownedviewModel.class);

        context=this;
        sharedPref=new SharedPref(context);
        findviews();
        WebServiceCall_MakeList();


        temp_engine=et_engine.getText().toString().trim();
        tempvarient=et_varient.getText().toString().trim();
        tempdoors=et_door.getText().toString().trim();

        ll_make.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_NumberPicker_MakeList();
            }
        });

        ll_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(temp_makelist.isEmpty())
                {
                    Commons.Alert_custom(context,"Please select the make options");
                }
                else {
                    Dialog_NumberPicker_ModelList();
                }
            }
        });
        ll_select_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_Year();
            }
        });
        ll_select_maxyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_Year_max();
            }
        });
       /* ll_mileage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_Mileage();
            }
        });
        ll_transmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_Transmission();
            }
        });
        ll_fueltype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_Fuel();
            }
        });
        ll_exteriorcolor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_ExteriorSell();
            }
        });
        ll_interior_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_InteriorSell();
            }
        });*/

        img_home_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
            }
        });

        bt_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().postSticky(new Pojo_PreOwnedFilter(ip_makeid,ipmodelid,temp_year,temp_maxyear,temp_mileage,temp_engine,temp_tranmission,temp_fuel,tempvarient,tempdoors,temp_extcolor,temp_intcolor));

                Intent toPrevious = new Intent(FilterPreOwnedDetail_Activity.this, NewCar_Tablayout_Activity_static.class);
                toPrevious.putExtra("tab_position", "3");
                startActivity(toPrevious);
                finish();

            }
        });


       /* bt_advanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ll_advanced.isShown()) {
                    bt_advanced.setText("Advanced Options");
                    ll_advanced.setVisibility(View.GONE);
                }
                else {

                    bt_advanced.setText("-");
                    ll_advanced.setVisibility(View.VISIBLE);
                }
            }
        });*/
    }

    private void findviews() {

        img_home_back=findViewById(R.id.img_home_back);
      //  bt_advanced=findViewById(R.id.bt_advanced);
        bt_go=findViewById(R.id.bt_go);
        ll_advanced=findViewById(R.id.ll_advanced);


        et_make=findViewById(R.id.et_make);
        et_model=findViewById(R.id.et_model);
        et_year=findViewById(R.id.et_year);
        et_maxyear=findViewById(R.id.et_maxyear);
        et_mileage=findViewById(R.id.et_mileage);
        et_engine=findViewById(R.id.et_engine);
        et_varient=findViewById(R.id.et_varient);
        et_transmission=findViewById(R.id.et_transmission);
        et_fuel=findViewById(R.id.et_fuel);
        et_door=findViewById(R.id.et_door);
        et_exteriorcolor=findViewById(R.id.et_exteriorcolor);
        et_interior_color=findViewById(R.id.et_interior_color);




        ll_make=findViewById(R.id.ll_make);
        ll_model=findViewById(R.id.ll_model);
        ll_fueltype=findViewById(R.id.ll_fueltype);
        ll_transmission=findViewById(R.id.ll_transmission);
        ll_select_year=findViewById(R.id.ll_select_year);
        ll_select_maxyear=findViewById(R.id.ll_select_maxyear);
        ll_mileage=findViewById(R.id.ll_mileage);
        ll_varienttype=findViewById(R.id.ll_varienttype);
        ll_enginetype=findViewById(R.id.ll_enginetype);
        ll_door=findViewById(R.id.ll_door);
        ll_exteriorcolor=findViewById(R.id.ll_exteriorcolor);
        ll_interior_color=findViewById(R.id.ll_interior_color);

    }


    private void WebServiceCall_MakeList() {
        HashMap<String,String>request=new HashMap<>();
        request.put("isPreowned","yes");


        preownedCardetail_filterViewModel.getcarmakelist(request).observe(this, new Observer<Response_CarMakeList>() {
            @Override
            public void onChanged(Response_CarMakeList response_carMakeList) {
                //Commons.hideProgress();

                if(response_carMakeList.status.equals("1")) {
                    System.out.println("===res=preowned=makelist=" + response_carMakeList.data.toString());
                    makelistsize=response_carMakeList.data.size();
                    carmakelist=response_carMakeList.data;
                    Log.i("res-=preownr maklist0->",response_carMakeList.message);

                }
                else if(response_carMakeList.status.equals("0"))
                {
                    Log.i("r=preownr maklist->",response_carMakeList.message);
                    System.out.println("===r=preownr maklist=" + response_carMakeList.data);
                    Toast.makeText(context,response_carMakeList.message,Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void WebServiceCall_ModelList() {

        Commons.showProgress(context);
        HashMap<String,String> request=new HashMap<>();
        request.put("make_id",ip_makeid);
        request.put("isPreowned","yes");
        System.out.println("url-modellist->"+request);
        Log.e("--modellist--->",request.toString());
        preownedCardetail_filterViewModel.getCarModeldata(request).observe(this, new Observer<Response_CarModelList>() {
            @Override
            public void onChanged(Response_CarModelList response_carModelList) {

                Commons.hideProgress();
                System.out.println("===res=car model list=" + response_carModelList.message);
                if(response_carModelList.status.equals("1"))
                {
                    carmodellist=response_carModelList.data;
                    modellistsize=carmodellist.size();
                }
                else if(response_carModelList.status.equals("0"))
                {
                    Toast.makeText(context,response_carModelList.message,Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    private void Dialog_NumberPicker_MakeList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(makelistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmakelist.size()];
        str_makedid=new String[carmakelist.size()];


        for (int i=0;i<carmakelist.size();i++)
        {
            str[i]=carmakelist.get(i).makename;
            str_makedid[i]=carmakelist.get(i).mkid;
        }
        np.setDisplayedValues(str);
        temp_makelist=str[index];
        ip_makeid=str_makedid[index];
        button_addquote.setOnClickListener(v -> {
            et_make.setText(temp_makelist);
            //  PreOwned__Fragment1.res_make=temp_makelist;
            dialog.dismiss();
            WebServiceCall_ModelList();
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_makelist=str[newVal];
                ip_makeid=str_makedid[newVal];
                // Toast.makeText(getActivity(), temp_makelist, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_NumberPicker_ModelList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(modellistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmodellist.size()];
        strmodellist=new String[carmodellist.size()];
        for (int i=0;i<carmodellist.size();i++)
        {
            str[i]=carmodellist.get(i).modelName;
            strmodellist[i]=carmodellist.get(i).modid;
        }
        np.setDisplayedValues(str);
        temp_model=str[index];
        ipmodelid=strmodellist[index];

        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_model.setText(temp_model);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_model=str[newVal];
                ipmodelid=strmodellist[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Fuel() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] fuel_array = getResources().getStringArray(R.array.Fuel);
        np.setMaxValue(fuel_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(fuel_array);
        temp_fuel=fuel_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_fuel.setText(temp_fuel);
                // PreOwned__Fragment1.res_fueltype=temp_fuel;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_fuel=fuel_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Transmission() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Transmission);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_tranmission=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_transmission.setText(temp_tranmission);
                //   PreOwned__Fragment1.res_transmission=temp_tranmission;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_tranmission=transmission_array[newVal];
                // Toast.makeText(getActivity(), temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Year() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Min_Year_Preowned);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_year=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_year.setText(temp_year);
                // PreOwned__Fragment1.res_minyear=temp_year;
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_year=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Year_max() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Year);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_maxyear=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_maxyear.setText(temp_maxyear);
                //  PreOwned__Fragment1.res_maxyear=temp_maxyear;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_maxyear=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Mileage() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Mileage);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_mileage=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_mileage.setText(temp_mileage);
                //  PreOwned__Fragment1.res_mileage=temp_mileage;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_mileage=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_ExteriorSell() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.exterior_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_extcolor=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_exteriorcolor.setText(temp_extcolor);
                // PreOwned__Fragment1.res_extcolor=temp_extcolor;

                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_extcolor=transmission_array[newVal];
            }
        });
        dialog.show();
    }

    private void Dialog_InteriorSell() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);

        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.interior_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_intcolor=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_interior_color.setText(temp_intcolor);
                //   PreOwned__Fragment1.res_intcolor=temp_intcolor;
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_intcolor=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }



}