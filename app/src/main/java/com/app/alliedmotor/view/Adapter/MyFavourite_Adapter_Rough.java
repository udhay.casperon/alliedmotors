package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.MyFavourite.DataItem;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyFavourite_Adapter_Rough extends RecyclerView.Adapter<MyFavourite_Adapter_Rough.ViewHolder> {

    ArrayList<DataItem> dataItems = new ArrayList<>();

    I_MyFavouriterough listener;
    SharedPref sharedPref;
    private Context mcontext;
    private int selectedPosition = -1;
    private SparseBooleanArray mSelectedItemsIds;

    public MyFavourite_Adapter_Rough(Context context, ArrayList<DataItem> dataItems, I_MyFavouriterough listener) {
        this.mcontext = context;
        this.dataItems = dataItems;
        this.listener=listener;
        mSelectedItemsIds = new SparseBooleanArray();


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.my_favourite_car_rv_linear_final, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String imgurl = dataItems.get(position).getCarImage();
        String temp_Ecolor=dataItems.get(position).getExteriorColor();
        String temp_Icolor=dataItems.get(position).getInteriorColor();

        Picasso.get().load(imgurl).into(holder.ic_car);
        holder.tv_carname.setText(AppInstalled.toTitleCase(dataItems.get(position).getCarType()));
        holder.tv_cartitle.setText(AppInstalled.toTitleCase(dataItems.get(position).getCarTitle()));
        holder.tv_modelyear.setText(AppInstalled.toTitleCase(dataItems.get(position).getModelYear()));
        holder.tv_varientcode.setText(AppInstalled.toTitleCase(dataItems.get(position).getVarientCode()));
        holder.tv_cartrans.setText(AppInstalled.toTitleCase(dataItems.get(position).getCarTransmission()));


        if(temp_Ecolor!=null)
        {

            holder.tv_tick1_bg.setCardBackgroundColor(Color.parseColor(dataItems.get(position).getExteriorColor()));

        }
        else {
            holder.ll_selection1.setVisibility(View.GONE);
            //holder.tv_tick1_bg.setVisibility(View.GONE);
        }
        if(temp_Icolor!=null)
        {
            holder.tv_tick2_bg.setCardBackgroundColor(Color.parseColor(temp_Icolor));
        }
        else {
            holder.ll_selection2.setVisibility(View.GONE);
            //holder.tv_tick2_bg.setVisibility(View.GONE);
        }


        holder.checkbox_selection.setChecked(mSelectedItemsIds.get(position));

        final int pos = position;


        holder.checkbox_selection.setChecked(dataItems.get(position).isSelected());

        holder.checkbox_selection.setTag(dataItems.get(position));

        holder.checkbox_selection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                DataItem model = (DataItem) cb.getTag();

                model.setSelected(cb.isChecked());
                dataItems.get(pos).setSelected(cb.isChecked());
                listener.on_CarSelected(dataItems.get(position).getFav_id(),position);
            }
        });


    }

    public void removeItem(int position) {
        dataItems.remove(position);
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public interface I_MyFavouriterough {
        void on_CarSelected(String dataItem, int position);
        void on_Selection(DataItem dataItem, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        ImageView ic_car;
        CustomTextViewSemiBold tv_carname;
        CustomTextViewSemiBold tv_cartitle;
        CustomTextViewRegular tv_modelyear,tv_varientcode,tv_cartrans;
        CheckBox checkbox_selection;
        LinearLayout ll_colorvarient;
        CardView tv_tick1_bg,tv_tick2_bg;
        LinearLayout ll_selection1,ll_selection2;


        public ViewHolder(View itemView) {

            super(itemView);

            ll_selection1=itemView.findViewById(R.id.ll_selection1);
            ll_selection2=itemView.findViewById(R.id.ll_selection2);

            ll_colorvarient=itemView.findViewById(R.id.ll_colorvarient);
            tv_tick1_bg=itemView.findViewById(R.id.tv_tick1_bg);
            tv_tick2_bg=itemView.findViewById(R.id.tv_tick2_bg);


            tv_carname=itemView.findViewById(R.id.tv_carname);
            ic_car=itemView.findViewById(R.id.ic_car);
            tv_cartitle=itemView.findViewById(R.id.tv_cartitle);
            tv_modelyear=itemView.findViewById(R.id.tv_modelyear);
            tv_varientcode=itemView.findViewById(R.id.tv_varientcode);
            tv_cartrans=itemView.findViewById(R.id.tv_cartrans);
            checkbox_selection=itemView.findViewById(R.id.checkbox_selection);

        }
    }




}

