package com.app.alliedmotor.view.Activity;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.app.alliedmotor.R;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.view.Adapter.CarDetailImageBanner_Adapter1;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ImageViewer_Activity extends AppCompatActivity {

    File path,dirfile,finalpath;

    ImageView img_share,back_image;
    CustomRegularTextview imageno;
    android.graphics.Matrix matrix = new android.graphics.Matrix();
    ViewPager rv_imageviewer;
    private int NUM_PAGES;
    ArrayList<String> img_list;
    private Context context;

    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
   String curr_path="";
    CarDetailImageBanner_Adapter1 carDetailImageBanner_adapter;
    String sharingurl="";
    int image_position=0;
    Bitmap bmp;

    String imgageurl="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer_);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        context=this;

        back_image=findViewById(R.id.back_image);
        imageno=findViewById(R.id.tv_imageno);
        img_share=findViewById(R.id.dialog_share);
        rv_imageviewer=findViewById(R.id.rv_imageviewer);

        Intent fromIntent=getIntent();
        img_list=fromIntent.getStringArrayListExtra("car_images");
        sharingurl=fromIntent.getStringExtra("sharingurl");
        image_position=fromIntent.getIntExtra("position",0);
        System.out.println("--imagto imageviewer actiivty==="+img_list);

        NUM_PAGES = img_list.size();
        carDetailImageBanner_adapter = new CarDetailImageBanner_Adapter1(context, img_list);
        rv_imageviewer.setAdapter(carDetailImageBanner_adapter);
        rv_imageviewer.setCurrentItem(Integer.valueOf(image_position));

        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());








        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

       // String mediaPath = MediaStore.Images.Media.insertImage(context.getContentResolver(), result, finalpath.getName(), null); imageInternalUri = Uri.parse(mediaPath);


        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Download_file();

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                sharingIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                sharingIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(finalpath));
                sharingIntent.setType("image/jpg");
                startActivity(sharingIntent);
            }
        });

        rv_imageviewer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                rv_imageviewer.getCurrentItem();
                imageno.setText((rv_imageviewer.getCurrentItem() + 1) + " of " + NUM_PAGES);
                imgageurl = img_list.get(rv_imageviewer.getCurrentItem());
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void Download_file() {

        path=new File("/alliedmotors/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date())+".jpg");
        dirfile=new File(path, Environment.DIRECTORY_DOWNLOADS);
        finalpath=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), String.valueOf(path));
        DownloadManager downloadManager= (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(imgageurl));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setTitle("Downloading");
        request.setDescription("Please wait...");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, String.valueOf(path));
        downloadManager.enqueue(request);

    }
    private void sharecontent() {
        Bitmap bitmap =getBitmapFromView(rv_imageviewer);
        int j=rv_imageviewer.getCurrentItem();
        try {

            File file = new File(img_list.get(j));
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_TEXT, "share");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share image via"));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            bgDrawable.draw(canvas);
        }   else{
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        mScaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }


    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector){
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(0.1f,
                    Math.min(mScaleFactor, 5.0f));
            rv_imageviewer.setScaleX(mScaleFactor);
            rv_imageviewer.setScaleY(mScaleFactor);
            return true;
        }
    }


    @Override
    public void onConfigurationChanged(@NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            //Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }


    }










