package com.app.alliedmotor.view.Fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.CarDetail12.Service_PreOwnedCardetail;
import com.app.alliedmotor.model.FileUpload.Response_FileUpload;
import com.app.alliedmotor.model.HomePage.DataItem;
import com.app.alliedmotor.model.Login.LoginResponse;
import com.app.alliedmotor.model.Login12.Service_FileUpload;
import com.app.alliedmotor.model.Login12.Service_LoginDetail;
import com.app.alliedmotor.model.Pojo_PreOwnedFilter;
import com.app.alliedmotor.model.Pre_OwnedCar.CarDetailsItem;
import com.app.alliedmotor.model.Pre_OwnedCar.Response_PreOwned;
import com.app.alliedmotor.model.Response_CarMakeList;
import com.app.alliedmotor.model.Response_CarModelList;
import com.app.alliedmotor.model.Response_SellPreOwned;
import com.app.alliedmotor.utility.Commons;
import com.app.alliedmotor.utility.Constants;
import com.app.alliedmotor.utility.CountryDialCode;
import com.app.alliedmotor.utility.GPSTracker;
import com.app.alliedmotor.utility.ResponseInterface;
import com.app.alliedmotor.utility.ServiceInterface;
import com.app.alliedmotor.utility.WebServiceData;
import com.app.alliedmotor.utility.WebServiceData_sample;
import com.app.alliedmotor.utility.widgets.CustomRegularButton;
import com.app.alliedmotor.utility.widgets.CustomRegularEditText;
import com.app.alliedmotor.utility.widgets.CustomRegularEditTextBold;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.utility.widgets.CustomTextViewRegular;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.HomeActivity_searchimage_BottomButton;
import com.app.alliedmotor.view.Activity.LoginActivity;
import com.app.alliedmotor.view.Activity.PreOwnedCar_Detail_Activity;
import com.app.alliedmotor.view.Adapter.PreOwned_Adapter;
import com.app.alliedmotor.viewmodel.PreownedviewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class PreOwned__Fragment1 extends Fragment implements View.OnClickListener {


    private static final int PERMISSION_REQUEST_CODE = 200;
    MultipartBody.Part img = null;
    WebServiceData retrofitInstance = new WebServiceData();
    private static final String TAG = "102";
    private static final int REQUEST_CODE =1 ;
     RecyclerView rv_preowned;
    CardView cv1_buy,cv2_sell;
    LinearLayoutManager linearLayoutManager1;
    PreownedviewModel preownedviewModel;
    ArrayList<CarDetailsItem>carDetailsItems=new ArrayList<>();
    PreOwned_Adapter preOwned_adapter;
    View root;
    private Context context;
    LinearLayout loading_ll,main_ll,ll_buy;
    RelativeLayout rl_sell;
    CustomRegularEditTextBold et_make,et_model,et_fuel,et_transmission,et_year,et_mileage,et_body_condition,et_dealer_warranty,et_exteriorcolor,et_interior_color,et_descrption;
    ConstraintLayout ll_make,ll_model,ll_fueltype,ll_transmission,ll_select_year,ll_mileage,ll_body_condition,ll_dealer_warranty,ll_exteriorcolor,ll_interior_color;
    LinearLayout ll_descrption;
    String temp_makelist="",temp_model,temp_fuel,temp_tranmission,temp_year,temp_mileage,temp_bodycondition,temp_dealerwarranty,temp_extcolor,temp_intcolor, temp_descrption="";
    ArrayList<Response_CarMakeList.DataItem>carmakelist=new ArrayList<>();
    int makelistsize,modellistsize;
    String[] str_makedid;
    String[] strmodellist;
    String ip_makeid,ipmodelid;
    int int_makid,int_modelid;
    ArrayList<Response_CarModelList.DataItem>carmodellist=new ArrayList<>();
    CustomRegularButton bt_back1,bt_next1,bt_back2,bt_next2,bt_back3,bt_next3;
    LinearLayout ll_upload,ll_personaldetails,ll_sell_cardetails;
    int tempresponse_data=0,tempresponse_data1=0,temp_response_data2=0;
    Array[]image_Array;
    String buttontype="";
    static  PreOwned__Fragment1 preOwned__fragment1;
    int index=0;


    MultipartBody.Part img_mutlipart = null;
    CustomRegularTextview tv_upload1,tv_upload2,tv_upload3,tv_upload4,tv_upload5,tv_upload6,tv_upload7,tv_upload8,tv_upload9,tv_upload10,tv_upload11,tv_upload12,tv_upload13,tv_upload14,tv_upload15,tv_upload16,tv_upload17,tv_upload18,tv_upload19,tv_upload20;

    public static PreOwned__Fragment1 getInstance() {
        if (preOwned__fragment1 == null)
            preOwned__fragment1 = new PreOwned__Fragment1();
        return preOwned__fragment1;
    }






    public  String res_make="",res_model="",res_minyear="",res_maxyear="",res_mileage="",res_engine="",res_varient="",res_transmission="",res_fueltype="",res_door="",res_extcolor="",res_intcolor="";


    private final int COMPRESS = 100;

    ImageView img_search;
    private File imageRoot;
    Uri imageUri;
    CustomRegularTextview tv_frontview,tv_driverside,tv_passengerside,tv_rearview,tv_exteriorroof,tv_driverside1,tv_frontpassenger,tv_frontdashboard,tv_rearpassenger,tv_interiorroof,tv_boot,tv_enginebay,tv_other1,tv_other2,tv_other3,tv_other4,tv_other5,tv_other6,tv_other7,tv_other8;
    String myFilePath = null;
    private int myRequestCode = 1;
    private boolean iskitkat = false;
    File imgfile;

    LinearLayout ll_frontview,ll_driverside,ll_passengerside,ll_rearview,ll_extrior_roof,ll_driverside1,ll_frontpassenger;
    LinearLayout ll_frontdashboard,ll_rearpassenger,ll_interiorroof,ll_boot,ll_enginebay;
    LinearLayout ll_other1,ll_other2,ll_other3,ll_other4,ll_other5,ll_other6,ll_other7,ll_other8;

    CardView bt_upload3,bt_upload4,bt_upload5,bt_upload6,bt_upload7,bt_upload8,bt_upload9,bt_upload10,bt_upload11,bt_upload12,bt_upload13,bt_upload14,bt_upload15,bt_upload16,bt_upload17,bt_upload18,bt_upload19,bt_upload20;

    CardView bt_upload2;
    CustomRegularEditText et_name,et_mobile,et_email;
    CustomRegularEditTextBold et_remarks;
    String st_name,st_mobile,st_email,st_remarks;
  String Imagepath="",documenttype = "";
 CustomRegularButton CustomRegularButton;

ArrayList<String> arrayList = null;
    ArrayList<String> storearrayList = null;
    String path1="";
    Dialog FilterDialog;
    private LinearLayout myCountryCodeLAY;
    private ImageView myFlagIMG;
    private TextView myCountryCodeTXT;
    private CountryPicker myPicker;
    private String myFlagStr = "",st_full_mobile;
    private GPSTracker myGPSTracker;

    String t_makeid,t_modid,t_fuel,t_transmission,t_year,t_mileage,t_bodycondtion,t_dealerwarranty,t_extcolor,t_intcolor;

    CircularProgressBar loader2,loader3,loader4,loader5,loader6,loader7,loader8,loader9,loader10,loader11,loader12,loader13,loader14,loader15,loader16,loader17,loader18,loader19,loader20;
  CircularProgressBar loader1;
  CardView bt_upload1;

  CustomRegularTextview tv_nodata;

  LinearLayout ll_nodatafound;
  RelativeLayout rl_loader1,rl_loader2,rl_loader3,rl_loader4,rl_loader5,rl_loader6,rl_loader7,rl_loader8,rl_loader9,rl_loader10;
  RelativeLayout rl_loader11,rl_loader12,rl_loader13,rl_loader14,rl_loader15,rl_loader16,rl_loader17,rl_loader18,rl_loader19,rl_loader20;

    private boolean _hasLoadedOnce= false;
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                res_make="";res_model="";res_minyear="";res_maxyear="";res_mileage="";res_engine="";res_transmission="";res_fueltype="";res_varient="";res_door="";res_extcolor="";res_intcolor="";

                WebServiceCall(res_make="",res_model="",res_minyear="",res_maxyear="",res_mileage="",res_engine="",res_transmission="",res_fueltype="",res_varient="",res_door="",res_extcolor="",res_intcolor="");
                _hasLoadedOnce = true;

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        preOwned__fragment1 = this;

        if(root==null) {
            root = inflater.inflate(R.layout.fragment_preowned_ios, container, false);
            preownedviewModel = ViewModelProviders.of(this).get(PreownedviewModel.class);

            context = getActivity();
            arrayList = new ArrayList<>();
            storearrayList = new ArrayList<>();
            storearrayList.clear();
            String appDirectoryName = getString(R.string.app_name);
            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            } else if (!imageRoot.isDirectory()) {
                imageRoot.delete();
                imageRoot.mkdir();
            }
            findbyviews();
            clicklistener();
            WebServiceCall(res_make,res_model,res_minyear,res_maxyear,res_mileage,res_engine,res_transmission,res_fueltype,res_varient,res_door,res_extcolor,res_intcolor);

            WebServiceCall_MakeList();
        }
        return root;
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity().getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }




    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }




    private void clicklistener() {
        cv1_buy.setOnClickListener(this);
        cv2_sell.setOnClickListener(this);
        ll_make.setOnClickListener(this);
        ll_model.setOnClickListener(this);
        ll_fueltype.setOnClickListener(this);
           ll_transmission.setOnClickListener(this);
           ll_select_year.setOnClickListener(this);
           ll_mileage.setOnClickListener(this);
           ll_body_condition.setOnClickListener(this);
           ll_dealer_warranty.setOnClickListener(this);
           ll_exteriorcolor.setOnClickListener(this);
           ll_interior_color.setOnClickListener(this);
           ll_descrption.setOnClickListener(this);
           bt_back1.setOnClickListener(this);
           bt_next1.setOnClickListener(this);
           bt_back2.setOnClickListener(this);
           bt_next2.setOnClickListener(this);
           bt_back3.setOnClickListener(this);
           bt_next3.setOnClickListener(this);

           bt_upload1.setOnClickListener(this);
           bt_upload2.setOnClickListener(this);
           bt_upload3.setOnClickListener(this);
           bt_upload4.setOnClickListener(this);
           bt_upload5.setOnClickListener(this);
           bt_upload6.setOnClickListener(this);
           bt_upload7.setOnClickListener(this);
           bt_upload8.setOnClickListener(this);
           bt_upload9.setOnClickListener(this);
           bt_upload10.setOnClickListener(this);
           bt_upload11.setOnClickListener(this);
           bt_upload12.setOnClickListener(this);
           bt_upload13.setOnClickListener(this);
           bt_upload14.setOnClickListener(this);
           bt_upload15.setOnClickListener(this);
           bt_upload16.setOnClickListener(this);
           bt_upload17.setOnClickListener(this);
           bt_upload18.setOnClickListener(this);
           bt_upload19.setOnClickListener(this);
           bt_upload20.setOnClickListener(this);
        myCountryCodeLAY.setOnClickListener(this);



        rl_loader1.setOnClickListener(this);
        rl_loader2.setOnClickListener(this);
        rl_loader3.setOnClickListener(this);
        rl_loader4.setOnClickListener(this);
        rl_loader5.setOnClickListener(this);
        rl_loader6.setOnClickListener(this);
        rl_loader7.setOnClickListener(this);
        rl_loader8.setOnClickListener(this);
        rl_loader9.setOnClickListener(this);
        rl_loader10.setOnClickListener(this);
        rl_loader11.setOnClickListener(this);
        rl_loader12.setOnClickListener(this);
        rl_loader13.setOnClickListener(this);
        rl_loader14.setOnClickListener(this);
        rl_loader15.setOnClickListener(this);
        rl_loader16.setOnClickListener(this);
        rl_loader17.setOnClickListener(this);
        rl_loader18.setOnClickListener(this);
        rl_loader19.setOnClickListener(this);
        rl_loader20.setOnClickListener(this);



    }

    private void findbyviews() {

        rl_loader1=root.findViewById(R.id.rl_loader1);
        rl_loader2=root.findViewById(R.id.rl_loader2);
        rl_loader3=root.findViewById(R.id.rl_loader3);
        rl_loader4=root.findViewById(R.id.rl_loader4);
        rl_loader5=root.findViewById(R.id.rl_loader5);
        rl_loader6=root.findViewById(R.id.rl_loader6);
        rl_loader7=root.findViewById(R.id.rl_loader7);
        rl_loader8=root.findViewById(R.id.rl_loader8);
        rl_loader9=root.findViewById(R.id.rl_loader9);
        rl_loader10=root.findViewById(R.id.rl_loader10);
        rl_loader11=root.findViewById(R.id.rl_loader11);
        rl_loader12=root.findViewById(R.id.rl_loader12);
        rl_loader13=root.findViewById(R.id.rl_loader13);
        rl_loader14=root.findViewById(R.id.rl_loader14);
        rl_loader15=root.findViewById(R.id.rl_loader15);
        rl_loader16=root.findViewById(R.id.rl_loader16);
        rl_loader17=root.findViewById(R.id.rl_loader17);
        rl_loader18=root.findViewById(R.id.rl_loader18);
        rl_loader19=root.findViewById(R.id.rl_loader19);
        rl_loader20=root.findViewById(R.id.rl_loader20);










        ll_nodatafound=root.findViewById(R.id.ll_nodatafound);

        loader1=root.findViewById(R.id.loader1);
        loader2=root.findViewById(R.id.loader2);
        loader3=root.findViewById(R.id.loader3);
        loader4=root.findViewById(R.id.loader4);
        loader5=root.findViewById(R.id.loader5);
        loader6=root.findViewById(R.id.loader6);
        loader7=root.findViewById(R.id.loader7);
        loader8=root.findViewById(R.id.loader8);
        loader9=root.findViewById(R.id.loader9);
        loader10=root.findViewById(R.id.loader10);
        loader11=root.findViewById(R.id.loader11);
        loader12=root.findViewById(R.id.loader12);
        loader13=root.findViewById(R.id.loader13);
        loader14=root.findViewById(R.id.loader14);
        loader15=root.findViewById(R.id.loader15);
        loader16=root.findViewById(R.id.loader16);
        loader17=root.findViewById(R.id.loader17);
        loader18=root.findViewById(R.id.loader18);
        loader19=root.findViewById(R.id.loader19);
        loader20=root.findViewById(R.id.loader20);







        myGPSTracker = new GPSTracker(context);
        myCountryCodeLAY = root.findViewById(R.id.activity_signup_mobile_no_LAY1);
        myFlagIMG = root. findViewById(R.id.activity_signup_country_flag_IMG1);
        myCountryCodeTXT = root.findViewById(R.id.activity_signup_country_code_TXT1);


        et_name=root.findViewById(R.id.et_name);
        et_mobile=root.findViewById(R.id.et_mobile);
        et_email=root.findViewById(R.id.et_email);
        et_remarks=root.findViewById(R.id.et_remarks);



        tv_driverside=root.findViewById(R.id.tv_driverside);
        tv_frontview=root.findViewById(R.id.tv_frontview);
        tv_passengerside=root.findViewById(R.id.tv_passengerside);
        tv_rearview=root.findViewById(R.id.tv_rearview);
        tv_exteriorroof=root.findViewById(R.id.tv_exteriorroof);
        tv_driverside1=root.findViewById(R.id.tv_driverside1);
        tv_frontpassenger=root.findViewById(R.id.tv_frontpassenger);
        tv_frontdashboard=root.findViewById(R.id.tv_frontdashboard);
        tv_rearpassenger=root.findViewById(R.id.tv_rearpassenger);
        tv_interiorroof=root.findViewById(R.id.tv_interiorroof);
        tv_boot=root.findViewById(R.id.tv_boot);
        tv_enginebay=root.findViewById(R.id.tv_enginebay);
        tv_other1=root.findViewById(R.id.tv_other1);
        tv_other2=root.findViewById(R.id.tv_other2);
        tv_other3=root.findViewById(R.id.tv_other3);
        tv_other4=root.findViewById(R.id.tv_other4);
        tv_other5=root.findViewById(R.id.tv_other5);
        tv_other6=root.findViewById(R.id.tv_other6);
        tv_other7=root.findViewById(R.id.tv_other7);
        tv_other8=root.findViewById(R.id.tv_other8);


        ll_sell_cardetails=root.findViewById(R.id.ll_sell_cardetails);
        ll_upload=root.findViewById(R.id.ll_upload);
        ll_personaldetails=root.findViewById(R.id.ll_personaldetails);
        bt_back1=root.findViewById(R.id.bt_back1);
        bt_next1=root.findViewById(R.id.bt_next1);
        bt_back2=root.findViewById(R.id.bt_back2);
        bt_next2=root.findViewById(R.id.bt_next2);
        bt_back3=root.findViewById(R.id.bt_back3);
        bt_next3=root.findViewById(R.id.bt_next3);

        ll_buy=root.findViewById(R.id.ll_buy);
        rl_sell=root.findViewById(R.id.rl_sell);
        cv1_buy=root.findViewById(R.id.cv1_buy);
        cv2_sell=root.findViewById(R.id.cv2_sell);

        rv_preowned = root.findViewById(R.id.rv_preowned);

        bt_upload1 = root.findViewById(R.id.bt_upload1);
        bt_upload2 = root.findViewById(R.id.bt_upload2);
        bt_upload3 = root.findViewById(R.id.bt_upload3);
        bt_upload4 = root.findViewById(R.id.bt_upload4);
        bt_upload5 = root.findViewById(R.id.bt_upload5);
        bt_upload6 = root.findViewById(R.id.bt_upload6);
        bt_upload7 = root.findViewById(R.id.bt_upload7);
        bt_upload8 = root.findViewById(R.id.bt_upload8);
        bt_upload9 = root.findViewById(R.id.bt_upload9);
        bt_upload10 = root.findViewById(R.id.bt_upload10);
        bt_upload11 = root.findViewById(R.id.bt_upload11);
        bt_upload12 = root.findViewById(R.id.bt_upload12);
        bt_upload13 = root.findViewById(R.id.bt_upload13);
        bt_upload14 = root.findViewById(R.id.bt_upload14);
        bt_upload15 = root.findViewById(R.id.bt_upload15);
        bt_upload16 = root.findViewById(R.id.bt_upload16);
        bt_upload17 = root.findViewById(R.id.bt_upload17);
        bt_upload18 = root.findViewById(R.id.bt_upload18);
        bt_upload19 = root.findViewById(R.id.bt_upload19);
        bt_upload20 = root.findViewById(R.id.bt_upload20);




       // loading_ll=root.findViewById(R.id.loading_ll);
        main_ll=root.findViewById(R.id.main_ll);
        linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_preowned.setLayoutManager(linearLayoutManager1);
        rv_preowned.setHasFixedSize(true);

        et_make=root.findViewById(R.id.et_make);
        et_model=root.findViewById(R.id.et_model);
        et_fuel=root.findViewById(R.id.et_fuel);
        et_transmission=root.findViewById(R.id.et_transmission);
        et_year=root.findViewById(R.id.et_year);
        et_mileage=root.findViewById(R.id.et_mileage);
        ll_make=root.findViewById(R.id.ll_make);
        ll_model=root.findViewById(R.id.ll_model);
        ll_fueltype=root.findViewById(R.id.ll_fueltype);
        ll_transmission=root.findViewById(R.id.ll_transmission);
        ll_select_year=root.findViewById(R.id.ll_select_year);
        ll_mileage=root.findViewById(R.id.ll_mileage);

        et_body_condition=root.findViewById(R.id.et_body_condition);
        et_dealer_warranty=root.findViewById(R.id.et_dealer_warranty);
        et_exteriorcolor=root.findViewById(R.id.et_exteriorcolor);
        et_interior_color=root.findViewById(R.id.et_interior_color);
        et_descrption=root.findViewById(R.id.et_descrption);
        ll_body_condition=root.findViewById(R.id.ll_body_condition);
        ll_dealer_warranty=root.findViewById(R.id.ll_dealer_warranty);
        ll_exteriorcolor=root.findViewById(R.id.ll_exteriorcolor);
        ll_interior_color=root.findViewById(R.id.ll_interior_color);
        ll_descrption=root.findViewById(R.id.ll_descrption);



        ll_frontview=root.findViewById(R.id.ll_frontview);
        ll_driverside=root.findViewById(R.id.ll_driverside);
        ll_passengerside=root.findViewById(R.id.ll_passengerside);
        ll_rearview=root.findViewById(R.id.ll_rearview);

        ll_extrior_roof=root.findViewById(R.id.ll_extrior_roof);
        ll_driverside1=root.findViewById(R.id.ll_driverside1);
        ll_frontpassenger=root.findViewById(R.id.ll_frontpassenger);
        ll_frontdashboard=root.findViewById(R.id.ll_frontdashboard);

        ll_rearpassenger=root.findViewById(R.id.ll_rearpassenger);
        ll_interiorroof=root.findViewById(R.id.ll_interiorroof);
        ll_boot=root.findViewById(R.id.ll_boot);
        ll_enginebay=root.findViewById(R.id.ll_enginebay);

        ll_other1=root.findViewById(R.id.ll_other1);
        ll_other2=root.findViewById(R.id.ll_other2);
        ll_other3=root.findViewById(R.id.ll_other3);
        ll_other4=root.findViewById(R.id.ll_other4);

        ll_other5=root.findViewById(R.id.ll_other5);
        ll_other6=root.findViewById(R.id.ll_other6);
        ll_other7=root.findViewById(R.id.ll_other7);
        ll_other8=root.findViewById(R.id.ll_other8);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.rl_loader1:
                bt_upload1.setVisibility(View.VISIBLE);
                rl_loader1.setVisibility(View.GONE);
                tv_frontview.setText("");
                break;
            case R.id.rl_loader2:
                bt_upload2.setVisibility(View.VISIBLE);
                rl_loader2.setVisibility(View.GONE);
                tv_driverside.setText("");
                break;

            case R.id.rl_loader3:
                bt_upload3.setVisibility(View.VISIBLE);
                rl_loader3.setVisibility(View.GONE);
                tv_passengerside.setText("");
                break;
            case R.id.rl_loader4:
                bt_upload4.setVisibility(View.VISIBLE);
                rl_loader4.setVisibility(View.GONE);
                tv_rearview.setText("");
                break;
            case R.id.rl_loader5:
                bt_upload5.setVisibility(View.VISIBLE);
                rl_loader5.setVisibility(View.GONE);
                tv_exteriorroof.setText("");
                break;
            case R.id.rl_loader6:
                bt_upload6.setVisibility(View.VISIBLE);
                rl_loader6.setVisibility(View.GONE);
                tv_driverside1.setText("");
                break;
            case R.id.rl_loader7:
                bt_upload7.setVisibility(View.VISIBLE);
                rl_loader7.setVisibility(View.GONE);
                tv_frontpassenger.setText("");
                break;
            case R.id.rl_loader8:
                bt_upload8.setVisibility(View.VISIBLE);
                rl_loader8.setVisibility(View.GONE);
                tv_frontdashboard.setText("");
                break;
            case R.id.rl_loader9:
                bt_upload9.setVisibility(View.VISIBLE);
                rl_loader9.setVisibility(View.GONE);
                tv_rearpassenger.setText("");
                break;
            case R.id.rl_loader10:
                bt_upload10.setVisibility(View.VISIBLE);
                rl_loader10.setVisibility(View.GONE);
                tv_interiorroof.setText("");
                break;
            case R.id.rl_loader11:
                bt_upload11.setVisibility(View.VISIBLE);
                rl_loader11.setVisibility(View.GONE);
                tv_boot.setText("");
                break;
            case R.id.rl_loader12:
                bt_upload12.setVisibility(View.VISIBLE);
                rl_loader12.setVisibility(View.GONE);
                tv_enginebay.setText("");
                break;
            case R.id.rl_loader13:
                bt_upload13.setVisibility(View.VISIBLE);
                rl_loader13.setVisibility(View.GONE);
                tv_other1.setText("");
                break;
            case R.id.rl_loader14:
                bt_upload14.setVisibility(View.VISIBLE);
                rl_loader14.setVisibility(View.GONE);
                tv_other2.setText("");
                break;
            case R.id.rl_loader15:
                bt_upload15.setVisibility(View.VISIBLE);
                rl_loader15.setVisibility(View.GONE);
                tv_other3.setText("");
                break;
            case R.id.rl_loader16:
                bt_upload16.setVisibility(View.VISIBLE);
                rl_loader16.setVisibility(View.GONE);
                tv_other4.setText("");
                break;
            case R.id.rl_loader17:
                bt_upload17.setVisibility(View.VISIBLE);
                rl_loader17.setVisibility(View.GONE);
                tv_other5.setText("");
                break;
            case R.id.rl_loader18:
                bt_upload18.setVisibility(View.VISIBLE);
                rl_loader18.setVisibility(View.GONE);
                tv_other6.setText("");
                break;
            case R.id.rl_loader19:
                bt_upload19.setVisibility(View.VISIBLE);
                rl_loader19.setVisibility(View.GONE);
                tv_other7.setText("");
                break;
            case R.id.rl_loader20:
                bt_upload20.setVisibility(View.VISIBLE);
                rl_loader20.setVisibility(View.GONE);
                tv_other8.setText("");
                break;


            case R.id.activity_signup_mobile_no_LAY1:
                myPicker.show(getChildFragmentManager(), "COUNTRY_PICKER");
                break;
                case R.id.bt_upload1:
                    isStoragePermissionGranted();
                    if (checkPermission()) {
                    } else {
                        requestPermission();
                    }
                    buttontype="1";
                    documenttype = "frontview";
                    Select_Image(context);
                break;
            case R.id.bt_upload2:
                isStoragePermissionGranted();
                if (checkPermission()) {
                } else {
                    requestPermission();
                }
                buttontype="2";
                documenttype = "driverside";
                Select_Image(context);
                break;
            case R.id.bt_upload3:
                isStoragePermissionGranted();
                if (checkPermission()) {
                } else {
                    requestPermission();
                }
                buttontype="3";
                documenttype = "passengerside";
                Select_Image(context);

                break;
            case R.id.bt_upload4:
                isStoragePermissionGranted();
                if (checkPermission()) {
                } else {
                    requestPermission();
                }
                buttontype="4";
                documenttype = "rearview";
                Select_Image(context);
                break;
            case R.id.bt_upload5:
                isStoragePermissionGranted();
                if (checkPermission()) {
                } else {
                    requestPermission();
                }
                buttontype="5";
                documenttype = "exteriorroof";
                Select_Image(context);
              break;
            case R.id.bt_upload6:
                buttontype="6";
                documenttype = "driverside1";
                Select_Image(context);
                break;

            case R.id.bt_upload7:
                buttontype="7";
                documenttype = "frontpassenger";
                Select_Image(context);
                break;
            case R.id.bt_upload8:
                buttontype="8";
                documenttype = "frontdashboard";
                Select_Image(context);
              break;
            case R.id.bt_upload9:
                buttontype="9";
                documenttype = "rearpassenger";
                Select_Image(context);

                break;
            case R.id.bt_upload10:
                buttontype="10";
                documenttype = "interiorroof";
                Select_Image(context);
                break;
            case R.id.bt_upload11:
                buttontype="11";
                documenttype = "boot";
                Select_Image(context);
                break;
            case R.id.bt_upload12:
                buttontype="12";
                documenttype = "enginebay";
                Select_Image(context);
                break;
            case R.id.bt_upload13:
                buttontype="13";
                documenttype = "other1";
                Select_Image(context);
                break;
            case R.id.bt_upload14:
                buttontype="14";
                documenttype = "other2";
                Select_Image(context);
                break;
            case R.id.bt_upload15:
                buttontype="15";
                documenttype = "other3";
                Select_Image(context);
                break;
            case R.id.bt_upload16:
                buttontype="16";
                documenttype = "other4";
                Select_Image(context);
                break;
            case R.id.bt_upload17:
                buttontype="17";
                documenttype = "other5";
                Select_Image(context);
                break;
            case R.id.bt_upload18:
                buttontype="18";
                documenttype = "other6";
                Select_Image(context);

                break;
            case R.id.bt_upload19:
                buttontype="19";
                documenttype = "other7";
                Select_Image(context);
                break;
            case R.id.bt_upload20:
                buttontype="20";
                documenttype = "other8";
                Select_Image(context);
                break;

            case R.id.cv1_buy:
                ll_buy.setVisibility(View.VISIBLE);
                cv1_buy.setCardBackgroundColor(getResources().getColor(R.color.dark_blue));
                cv2_sell.setCardBackgroundColor(getResources().getColor(R.color.dark_gray));
                rl_sell.setVisibility(View.GONE);

                break;
            case R.id.cv2_sell:
                rl_sell.setVisibility(View.VISIBLE);
                ll_buy.setVisibility(View.GONE);
                cv2_sell.setCardBackgroundColor(getResources().getColor(R.color.dark_blue));
                cv1_buy.setCardBackgroundColor(getResources().getColor(R.color.dark_gray));
                break;
            case R.id.ll_make:
                Dialog_NumberPicker_MakeList();
                break;
            case R.id.ll_model:
                if(temp_makelist.isEmpty())
                {
                    Commons.Alert_custom(context,"Please select the make options");
                }
                else {
                    Dialog_NumberPicker_ModelList();
                }
                break;

            case R.id.ll_fueltype:
                Dialog_Fuel();
                break;

            case R.id.ll_transmission:
                Dialog_Transmission();
                break;
            case R.id.ll_select_year:
                Dialog_Year();
                break;
            case R.id.ll_mileage:
                Dialog_Mileage();
                break;
            case R.id.ll_body_condition:
                Dialog_Bodycondition_sell();
                break;
            case R.id.ll_dealer_warranty:
                Dialog_DealerWarranty();
                break;
            case R.id.ll_exteriorcolor:
                Dialog_ExteriorSell();
                break;
            case R.id.ll_interior_color:
                Dialog_InteriorSell();
                break;
            case R.id.ll_descrption:
                et_descrption.requestFocus();
                break;
            case R.id.bt_back1:
                break;
            case R.id.bt_next1:
                Validation_Sell1();


                break;
            case R.id.bt_back2:
                ll_sell_cardetails.setVisibility(View.VISIBLE);
                ll_upload.setVisibility(View.GONE);

                break;
            case R.id.bt_next2:
                 ApiCall_FileUpload_Validation();
                break;
            case R.id.bt_back3:
                    ll_upload.setVisibility(View.VISIBLE);
                    ll_personaldetails.setVisibility(View.GONE);
                break;
            case R.id.bt_next3:
                classAndWidgetInitialize();
                step3_Validation();
                break;

        }
    }





    private void Validation_Sell1() {

   t_makeid=et_make.getText().toString().trim();
        t_modid=et_model.getText().toString().trim();
        t_fuel=et_fuel.getText().toString().trim();
        t_transmission=et_transmission.getText().toString().trim();
        t_year=et_year.getText().toString().trim();
        t_mileage=et_mileage.getText().toString().trim();
        t_bodycondtion=et_body_condition.getText().toString().trim();
        t_dealerwarranty=et_dealer_warranty.getText().toString().trim();
        t_extcolor=et_exteriorcolor.getText().toString().trim();
        t_intcolor=et_interior_color.getText().toString().trim();


        if(t_makeid.isEmpty() && t_modid.isEmpty() && t_fuel.isEmpty() && t_transmission.isEmpty() && t_year.isEmpty() && t_mileage.isEmpty() && t_bodycondtion.isEmpty() && t_dealerwarranty.isEmpty() && t_extcolor.isEmpty()&& t_intcolor.isEmpty() )
        {
            Commons.Alert_custom(context,"Please fill all details");
        }
        else if(t_makeid.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Make details");
        }
        else if(t_modid.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Model details");
        }
        else if(t_fuel.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter fuel details");
        }
        else if(t_transmission.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Transmission details");
        }
        else if(t_year.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Year details");
        }
        else if(t_mileage.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Mileage details");
        }
        else if(t_bodycondtion.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Body Condition details");
        }
        else if(t_dealerwarranty.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Dealer Warranty details");
        }
        else if(t_extcolor.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Exterior color details");
        }
        else if(t_intcolor.isEmpty())
        {
            Commons.Alert_custom(context,"Please Enter Interior color details");
        }
        else {
            ApiCall_Sell1();
        }




    }


    private void classAndWidgetInitialize() {

        myPicker = CountryPicker.newInstance("Select Country");
        myPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flag) {
                myPicker.dismiss();
                myCountryCodeTXT.setText(dialCode);
                myFlagIMG.setVisibility(View.VISIBLE);
                myFlagStr = "" + flag;
                myFlagIMG.setImageResource(flag);
                String  exampleNumber= String.valueOf(PhoneNumberUtil.getInstance().getExampleNumber(code).getNationalNumber());
                et_mobile.setFilters( new InputFilter[] {new InputFilter.LengthFilter(exampleNumber.length())});  //mypicker.setlistener

            }
        });
        setCountryDataValues();

    }


    private void setCountryDataValues() {
        if (myGPSTracker.canGetLocation() && myGPSTracker.isgpsenabled()) {
            double MyCurrent_lat = myGPSTracker.getLatitude();
            double MyCurrent_long = myGPSTracker.getLongitude();
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    String aCountryCodeStr = addresses.get(0).getCountryCode();
                    if (aCountryCodeStr.length() > 0 && !aCountryCodeStr.equals(null) && !aCountryCodeStr.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(aCountryCodeStr);
                        myCountryCodeTXT.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + aCountryCodeStr.toLowerCase(Locale.ENGLISH);
                        myFlagIMG.setImageResource(getResId(drawableName));
                        myFlagStr = "" + getResId(drawableName);
                        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(Str_countyCode));
                        String  exampleNumber= String.valueOf(phoneNumberUtil.getExampleNumber(isoCode).getNationalNumber());
                        int phoneLength=exampleNumber.length();
                        et_mobile.setFilters( new InputFilter[] {
                                new InputFilter.LengthFilter(phoneLength)});
                        et_mobile.setEms(phoneLength);
                        System.out.println("==d==="+et_mobile.getText().toString().trim());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getResId(String drawableName) {
        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
        }
        return -1;
    }

    public void WebServiceCall(String t_res,String t_model,String t_minyear,String t_maxyear,String t_mileages,String t_engine,String t_trans,
                                String t_fueltype,String t_varient,String t_door,String t_exxolr,String t_intcolors) {

            ll_buy.setVisibility(View.VISIBLE);
            rl_sell.setVisibility(View.GONE);
        cv1_buy.setCardBackgroundColor(getResources().getColor(R.color.dark_blue));
        cv2_sell.setCardBackgroundColor(getResources().getColor(R.color.dark_gray));
        if(t_model==null)
        {
            t_model="";
        }

        HashMap<String, String> request_cardetail = new HashMap<>();
        request_cardetail.put("make_id", t_res);
        request_cardetail.put("model_id", t_model);
        request_cardetail.put("min_year", t_minyear);
        request_cardetail.put("max_year", t_maxyear);
        request_cardetail.put("mileage", t_mileages);
        request_cardetail.put("engine", t_engine);
        request_cardetail.put("transmission", t_trans);
        request_cardetail.put("fuel_type", t_fueltype);
        request_cardetail.put("varient", t_varient);
        request_cardetail.put("doors", t_door);
        request_cardetail.put("ext_color", t_exxolr);
        request_cardetail.put("int_color", t_intcolors);
        String tempLive_Url="https://alliedmotors.com/alliedmotors/v1/preowned-car-details/";



       /* new Service_PreOwnedCardetail(tempLive_Url, request_cardetail, new ResponseInterface.PReOwnedCarListingInterface() {
            @Override
            public void onPreownedSuccess(Response_PreOwned response_preOwned) {
                    Commons.hideProgress();
                if(response_preOwned.getStatus().equals("1"))
                {
                    System.out.println("===res=preowned==" + response_preOwned.toString());
                    carDetailsItems = response_preOwned.getData().getCarDetails();

                    Log.i("res-0=preownedcar->", response_preOwned.toString());
                    //Toast.makeText(context,response_preOwned.getMessage(),Toast.LENGTH_SHORT).show();
                    if (carDetailsItems.size() == 0) {
                        rv_preowned.setVisibility(View.GONE);
                        ll_nodatafound.setVisibility(View.VISIBLE);
                    } else {
                        rv_preowned.setVisibility(View.VISIBLE);
                        ll_nodatafound.setVisibility(View.GONE);
                        FuncAdapter();
                    }

                } else if (response_preOwned.getStatus().equals("0")) {
                    Log.i("res-0=preownedcar->", response_preOwned.toString());
                    System.out.println("===res=preowned==" + response_preOwned.toString());
                    Toast.makeText(context, response_preOwned.getMessage(), Toast.LENGTH_SHORT).show();
                }
                }

        });
*/

     if (preownedviewModel == null) {

            preownedviewModel = ViewModelProviders.of(this).get(PreownedviewModel.class);
            System.out.println("--preowned===>"+preownedviewModel);
        }
      preownedviewModel.getpreowneddata(tempLive_Url, request_cardetail).observe(getActivity(), new Observer<Response_PreOwned>() {
                @Override
                public void onChanged(Response_PreOwned response_preOwned) {
                    Commons.hideProgress();
                    main_ll.setVisibility(View.VISIBLE);
                    if (response_preOwned.getStatus().equals("1")) {
                        System.out.println("===res=preowned==" + response_preOwned.toString());
                        carDetailsItems = response_preOwned.getData().getCarDetails();

                        Log.i("res-0=preownedcar->", response_preOwned.toString());
                        if (carDetailsItems.size() == 0) {
                            rv_preowned.setVisibility(View.GONE);
                            ll_nodatafound.setVisibility(View.VISIBLE);
                        } else {
                            rv_preowned.setVisibility(View.VISIBLE);
                            ll_nodatafound.setVisibility(View.GONE);
                            FuncAdapter();
                        }

                    } else if (response_preOwned.getStatus().equals("0")) {
                        Log.i("res-0=preownedcar->", response_preOwned.toString());
                        System.out.println("===res=preowned==" + response_preOwned.toString());
                        Toast.makeText(context, response_preOwned.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            });

    }
    private void FuncAdapter() {
        preOwned_adapter = new PreOwned_Adapter(context, carDetailsItems, car_preId -> {
            Intent toIntent=new Intent(context, PreOwnedCar_Detail_Activity.class);
            toIntent.putExtra("carid",car_preId);
            startActivityForResult(toIntent,108);
        });
        rv_preowned.setAdapter(preOwned_adapter);
    }




    private void Func_FileUpload(String prefixfile) {


        if (buttontype.equals("1"))
        {
            bt_upload1.setVisibility(View.GONE);
            loader1.setVisibility(View.VISIBLE);
            rl_loader1.setVisibility(View.VISIBLE);
            loader1.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("2")) {
            bt_upload2.setVisibility(View.GONE);
            rl_loader2.setVisibility(View.VISIBLE);
            loader2.setVisibility(View.VISIBLE);
            loader2.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("3")) {
            bt_upload3.setVisibility(View.GONE);
            rl_loader3.setVisibility(View.VISIBLE);
            loader3.setVisibility(View.VISIBLE);
            loader3.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("4")) {
            bt_upload4.setVisibility(View.GONE);
            rl_loader4.setVisibility(View.VISIBLE);
            loader4.setProgressWithAnimation(100, 500);
            loader4.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("5")) {
            bt_upload5.setVisibility(View.GONE);
            rl_loader5.setVisibility(View.VISIBLE);
            loader5.setVisibility(View.VISIBLE);
            loader5.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("6")) {
            bt_upload6.setVisibility(View.GONE);
            rl_loader6.setVisibility(View.VISIBLE);
            loader6.setProgressWithAnimation(100, 500);
            loader6.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("7")) {
            bt_upload7.setVisibility(View.GONE);
            rl_loader7.setVisibility(View.VISIBLE);
            loader7.setVisibility(View.VISIBLE);
            loader7.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("8")) {
            bt_upload8.setVisibility(View.GONE);
            rl_loader8.setVisibility(View.VISIBLE);
            loader8.setVisibility(View.VISIBLE);
            loader8.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("9")) {
            bt_upload9.setVisibility(View.GONE);
            rl_loader9.setVisibility(View.VISIBLE);
            loader9.setProgressWithAnimation(100, 500);
            loader9.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("10")) {
            bt_upload10.setVisibility(View.GONE);
            rl_loader10.setVisibility(View.VISIBLE);
            loader10.setProgressWithAnimation(100, 500);
            loader10.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("11")) {
            bt_upload11.setVisibility(View.GONE);
            rl_loader11.setVisibility(View.VISIBLE);
            loader11.setVisibility(View.VISIBLE);
            loader11.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("12")) {
            bt_upload12.setVisibility(View.GONE);
            rl_loader12.setVisibility(View.VISIBLE);
            loader12.setVisibility(View.VISIBLE);
            loader12.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("13")) {
            bt_upload13.setVisibility(View.GONE);
            rl_loader13.setVisibility(View.VISIBLE);
            loader13.setProgressWithAnimation(100, 500);
            loader13.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("14")) {
            bt_upload14.setVisibility(View.GONE);
            rl_loader14.setVisibility(View.VISIBLE);
            loader14.setProgressWithAnimation(100, 500);
            loader14.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("15")) {
            bt_upload15.setVisibility(View.GONE);
            rl_loader15.setVisibility(View.VISIBLE);
            loader15.setProgressWithAnimation(100, 500);
            loader15.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("16")) {
            bt_upload16.setVisibility(View.GONE);
            rl_loader16.setVisibility(View.VISIBLE);
            loader16.setProgressWithAnimation(100, 500);
            loader16.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("17")) {
            bt_upload17.setVisibility(View.GONE);
            rl_loader17.setVisibility(View.VISIBLE);
            loader17.setVisibility(View.VISIBLE);
            loader17.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("18")) {
            bt_upload18.setVisibility(View.GONE);
            rl_loader18.setVisibility(View.VISIBLE);
            loader18.setVisibility(View.VISIBLE);
            loader18.setProgressWithAnimation(100, 500);
        } else if (buttontype.equals("19")) {
            bt_upload19.setVisibility(View.GONE);
            rl_loader19.setVisibility(View.VISIBLE);
            loader19.setProgressWithAnimation(100, 500);
            loader19.setVisibility(View.VISIBLE);
        } else if (buttontype.equals("20")) {
            bt_upload20.setVisibility(View.GONE);
            rl_loader20.setVisibility(View.VISIBLE);
            loader20.setProgressWithAnimation(100, 500);
            loader20.setVisibility(View.VISIBLE);
        }


        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (buttontype.equals("1"))
                {
                    bt_upload1.setVisibility(View.GONE);
                    rl_loader1.setVisibility(View.VISIBLE);
                    loader1.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("2")) {
                    bt_upload2.setVisibility(View.GONE);
                    rl_loader2.setVisibility(View.VISIBLE);

                    loader2.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("3")) {
                    bt_upload3.setVisibility(View.GONE);
                    rl_loader3.setVisibility(View.VISIBLE);

                    loader3.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("4")) {
                    bt_upload4.setVisibility(View.GONE);
                    rl_loader4.setVisibility(View.VISIBLE);

                    loader4.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("5")) {
                    bt_upload5.setVisibility(View.GONE);
                    rl_loader5.setVisibility(View.VISIBLE);

                    loader5.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("6")) {
                    bt_upload6.setVisibility(View.GONE);
                    rl_loader6.setVisibility(View.VISIBLE);

                    loader6.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("7")) {
                    bt_upload7.setVisibility(View.GONE);
                    rl_loader7.setVisibility(View.VISIBLE);

                    loader7.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("8")) {
                    bt_upload8.setVisibility(View.GONE);
                    rl_loader8.setVisibility(View.VISIBLE);

                    loader8.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("9")) {
                    bt_upload9.setVisibility(View.GONE);
                    rl_loader9.setVisibility(View.VISIBLE);

                    loader9.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("10")) {
                    bt_upload10.setVisibility(View.GONE);
                    rl_loader10.setVisibility(View.VISIBLE);

                    loader10.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("11")) {
                    bt_upload11.setVisibility(View.GONE);
                    rl_loader11.setVisibility(View.VISIBLE);

                    loader11.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("12")) {
                    bt_upload12.setVisibility(View.GONE);
                    rl_loader12.setVisibility(View.VISIBLE);

                    loader12.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("13")) {
                    bt_upload13.setVisibility(View.GONE);
                    rl_loader13.setVisibility(View.VISIBLE);

                    loader13.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("14")) {
                    bt_upload14.setVisibility(View.GONE);
                    rl_loader14.setVisibility(View.VISIBLE);

                    loader14.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("15")) {
                    bt_upload15.setVisibility(View.GONE);
                    rl_loader15.setVisibility(View.VISIBLE);

                    loader15.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("16")) {
                    bt_upload16.setVisibility(View.GONE);
                    rl_loader16.setVisibility(View.VISIBLE);

                    loader16.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("17")) {
                    bt_upload17.setVisibility(View.GONE);
                    rl_loader17.setVisibility(View.VISIBLE);

                    loader17.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("18")) {
                    bt_upload18.setVisibility(View.GONE);
                    rl_loader18.setVisibility(View.VISIBLE);

                    loader18.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("19")) {
                    bt_upload19.setVisibility(View.GONE);
                    rl_loader19.setVisibility(View.VISIBLE);

                    loader19.setProgressWithAnimation(100, 500);
                } else if (buttontype.equals("20")) {
                    bt_upload20.setVisibility(View.GONE);
                    rl_loader20.setVisibility(View.VISIBLE);

                    loader20.setProgressWithAnimation(100, 500);
                }

            }
        }, 500);*/

        HashMap<String, String> request_imageupload = new HashMap<>();
        request_imageupload.put("image", myFilePath);
        request_imageupload.put("file_prefix", prefixfile);


        String st_transactionimg =myFilePath;
        String fileprefix        =prefixfile;
        RequestBody Filename_Prefix = RequestBody.create(MediaType.parse("text/plain"), fileprefix);
        if (st_transactionimg != null)
        {
            File fileToUpload = new File(st_transactionimg);
            if (fileToUpload.exists())
            {
                Log.d("exist", "exist");
            }
            else
            {
                Log.d("not exist", "not exist");
            }
            //File file = new File(st_transactionimg);
            RequestBody requestfile = RequestBody.create(MediaType.parse("image/png"), fileToUpload);
            img = MultipartBody.Part.createFormData("image", fileToUpload.getName(), requestfile);
        }


        retrofitInstance.retrofit().ServiceCall_fileUpload(Filename_Prefix,img).enqueue(new Callback<Response_FileUpload>() {
            @Override
            public void onResponse(Call<Response_FileUpload> call, Response<Response_FileUpload> response) {

                if(response.body()!=null) {

                      if(response.body().status.equals("1"))
                      {
                          arrayList.add(response.body().data.fullPath);
                          String fullpath = response.body().data.fullPath;
                          path1 = fullpath.substring(fullpath.lastIndexOf("/"));
                          System.out.println("===fullpath====" + path1);
                          imagepath(path1, documenttype);
                          documenttype = "";
                          System.out.println("===arraylist---" + arrayList);
                      }
                      else
                      {
                          Toast.makeText(getActivity(), response.body().message, Toast.LENGTH_SHORT).show();
                      }

                    Log.e("AliedmoonSue>",String.valueOf(response.body()));
                    System.out.println("--res=sell pre=" + response.body().toString());


                }
            }

            @Override
            public void onFailure(Call<Response_FileUpload> call, Throwable t) {

                Log.e("AliedmoOnpre=->",String.valueOf(t));
            }
        });

/*

        preownedviewModel.FileImageUpload(request_imageupload).observe(getActivity(), new Observer<Response_FileUpload>() {
            @Override
            public void onChanged(Response_FileUpload response_fileUpload) {
                if (response_fileUpload.status.equals("1")) {
                    if (buttontype.equals("1")) {
                        bt_upload1.setVisibility(View.VISIBLE);
                        rl_loader1.setVisibility(View.GONE);

                    } else if (buttontype.equals("2")) {
                        bt_upload2.setVisibility(View.VISIBLE);
                        rl_loader2.setVisibility(View.GONE);

                    } else if (buttontype.equals("3")) {
                        bt_upload3.setVisibility(View.VISIBLE);
                        rl_loader3.setVisibility(View.GONE);

                    } else if (buttontype.equals("4")) {
                        bt_upload4.setVisibility(View.VISIBLE);
                        rl_loader4.setVisibility(View.GONE);

                    } else if (buttontype.equals("5")) {
                        bt_upload5.setVisibility(View.VISIBLE);
                        rl_loader5.setVisibility(View.GONE);

                    } else if (buttontype.equals("6")) {
                        bt_upload6.setVisibility(View.VISIBLE);
                        rl_loader6.setVisibility(View.GONE);

                    } else if (buttontype.equals("7")) {
                        bt_upload7.setVisibility(View.VISIBLE);
                        rl_loader7.setVisibility(View.GONE);

                    } else if (buttontype.equals("8")) {
                        bt_upload8.setVisibility(View.VISIBLE);
                        rl_loader8.setVisibility(View.GONE);

                    } else if (buttontype.equals("9")) {
                        bt_upload9.setVisibility(View.VISIBLE);
                        rl_loader9.setVisibility(View.GONE);

                    } else if (buttontype.equals("10")) {
                        bt_upload10.setVisibility(View.VISIBLE);
                        rl_loader10.setVisibility(View.GONE);

                    } else if (buttontype.equals("11")) {
                        bt_upload11.setVisibility(View.VISIBLE);
                        rl_loader11.setVisibility(View.GONE);

                    } else if (buttontype.equals("12")) {
                        bt_upload12.setVisibility(View.VISIBLE);
                        rl_loader12.setVisibility(View.GONE);

                    } else if (buttontype.equals("13")) {
                        bt_upload13.setVisibility(View.VISIBLE);
                        rl_loader13.setVisibility(View.GONE);

                    } else if (buttontype.equals("14")) {
                        bt_upload14.setVisibility(View.VISIBLE);
                        rl_loader14.setVisibility(View.GONE);

                    } else if (buttontype.equals("15")) {
                        bt_upload15.setVisibility(View.VISIBLE);
                        rl_loader15.setVisibility(View.GONE);

                    } else if (buttontype.equals("16")) {
                        bt_upload16.setVisibility(View.VISIBLE);
                        rl_loader16.setVisibility(View.GONE);

                    } else if (buttontype.equals("17")) {
                        bt_upload17.setVisibility(View.VISIBLE);
                        rl_loader17.setVisibility(View.GONE);

                    } else if (buttontype.equals("18")) {
                        bt_upload18.setVisibility(View.VISIBLE);
                        rl_loader18.setVisibility(View.GONE);

                    } else if (buttontype.equals("19")) {
                        bt_upload19.setVisibility(View.VISIBLE);
                        rl_loader19.setVisibility(View.GONE);

                    } else if (buttontype.equals("20")) {
                        bt_upload20.setVisibility(View.VISIBLE);
                        rl_loader20.setVisibility(View.GONE);

                    }
                    System.out.println("--response--->" + response_fileUpload.message);
                    System.out.println("--response--dd->" + response_fileUpload.data.fullPath);
                    arrayList.add(response_fileUpload.data.fullPath);
                    String fullpath = response_fileUpload.data.fullPath;
                    path1 = fullpath.substring(fullpath.lastIndexOf("/"));
                    System.out.println("===fullpath====" + path1);
                    imagepath(path1, documenttype);
                    documenttype = "";
                    System.out.println("===arraylist---" + arrayList);
                }

                else if(response_fileUpload.status.equals("0")) {
                    if (buttontype.equals("1")) {
                        bt_upload1.setVisibility(View.VISIBLE);
                        rl_loader1.setVisibility(View.GONE);

                    } else if (buttontype.equals("2")) {
                        bt_upload2.setVisibility(View.VISIBLE);
                        rl_loader2.setVisibility(View.GONE);

                    } else if (buttontype.equals("3")) {
                        bt_upload3.setVisibility(View.VISIBLE);
                        rl_loader3.setVisibility(View.GONE);

                    } else if (buttontype.equals("4")) {
                        bt_upload4.setVisibility(View.VISIBLE);
                        rl_loader4.setVisibility(View.GONE);

                    } else if (buttontype.equals("5")) {
                        bt_upload5.setVisibility(View.VISIBLE);
                        rl_loader5.setVisibility(View.GONE);

                    } else if (buttontype.equals("6")) {
                        bt_upload6.setVisibility(View.VISIBLE);
                        rl_loader6.setVisibility(View.GONE);

                    } else if (buttontype.equals("7")) {
                        bt_upload7.setVisibility(View.VISIBLE);
                        rl_loader7.setVisibility(View.GONE);

                    } else if (buttontype.equals("8")) {
                        bt_upload8.setVisibility(View.VISIBLE);
                        rl_loader8.setVisibility(View.GONE);

                    } else if (buttontype.equals("9")) {
                        bt_upload9.setVisibility(View.VISIBLE);
                        rl_loader9.setVisibility(View.GONE);

                    } else if (buttontype.equals("10")) {
                        bt_upload10.setVisibility(View.VISIBLE);
                        rl_loader10.setVisibility(View.GONE);

                    } else if (buttontype.equals("11")) {
                        bt_upload11.setVisibility(View.VISIBLE);
                        rl_loader11.setVisibility(View.GONE);

                    } else if (buttontype.equals("12")) {
                        bt_upload12.setVisibility(View.VISIBLE);
                        rl_loader12.setVisibility(View.GONE);

                    } else if (buttontype.equals("13")) {
                        bt_upload13.setVisibility(View.VISIBLE);
                        rl_loader13.setVisibility(View.GONE);

                    } else if (buttontype.equals("14")) {
                        bt_upload14.setVisibility(View.VISIBLE);
                        rl_loader14.setVisibility(View.GONE);

                    } else if (buttontype.equals("15")) {
                        bt_upload15.setVisibility(View.VISIBLE);
                        rl_loader15.setVisibility(View.GONE);

                    } else if (buttontype.equals("16")) {
                        bt_upload16.setVisibility(View.VISIBLE);
                        rl_loader16.setVisibility(View.GONE);

                    } else if (buttontype.equals("17")) {
                        bt_upload17.setVisibility(View.VISIBLE);
                        rl_loader17.setVisibility(View.GONE);

                    } else if (buttontype.equals("18")) {
                        bt_upload18.setVisibility(View.VISIBLE);
                        rl_loader18.setVisibility(View.GONE);

                    } else if (buttontype.equals("19")) {
                        bt_upload19.setVisibility(View.VISIBLE);
                        rl_loader19.setVisibility(View.GONE);

                    } else if (buttontype.equals("20")) {
                        bt_upload20.setVisibility(View.VISIBLE);
                        rl_loader20.setVisibility(View.GONE);

                    }

                    Toast.makeText(context, response_fileUpload.message, Toast.LENGTH_SHORT).show();
                    System.out.println("---res========" + response_fileUpload.message);


                }    }
        });
*/


    }

    private void imagepath(String filePath, String documenttype) {
        switch (documenttype){

            case "frontview":
                bt_upload1.setVisibility(View.VISIBLE);
                rl_loader1.setVisibility(View.GONE);
                loader1.setVisibility(View.GONE);
                tv_frontview.setText(filePath);
                break;
            case "driverside":
                bt_upload2.setVisibility(View.VISIBLE);
                rl_loader2.setVisibility(View.GONE);
                loader2.setVisibility(View.GONE);
                tv_driverside.setText(filePath);
                break;
            case "passengerside":
                bt_upload3.setVisibility(View.VISIBLE);
                rl_loader3.setVisibility(View.GONE);
                loader3.setVisibility(View.GONE);
                tv_passengerside.setText(filePath);
                break;
            case "rearview":
                loader4.setVisibility(View.GONE);
                bt_upload4.setVisibility(View.VISIBLE);
                rl_loader4.setVisibility(View.GONE);
                tv_rearview.setText(filePath);
                break;
            case "exteriorroof":
                loader5.setVisibility(View.GONE);
                bt_upload5.setVisibility(View.VISIBLE);
                rl_loader5.setVisibility(View.GONE);
                tv_exteriorroof.setText(filePath);
                break;
            case "driverside1":
                loader6.setVisibility(View.GONE);
                bt_upload6.setVisibility(View.VISIBLE);
                rl_loader6.setVisibility(View.GONE);
                tv_driverside1.setText(filePath);
                break;
            case "frontpassenger":
                loader7.setVisibility(View.GONE);
                bt_upload7.setVisibility(View.VISIBLE);
                rl_loader7.setVisibility(View.GONE);
                tv_frontpassenger.setText(filePath);
                break;
            case "frontdashboard":
                loader8.setVisibility(View.GONE);
                bt_upload8.setVisibility(View.VISIBLE);
                rl_loader8.setVisibility(View.GONE);
                tv_frontdashboard.setText(filePath);
                break;
            case "rearpassenger":
                bt_upload9.setVisibility(View.VISIBLE);
                rl_loader9.setVisibility(View.GONE);
                tv_rearpassenger.setText(filePath);
                loader9.setVisibility(View.GONE);
                break;
            case "interiorroof":
                bt_upload10.setVisibility(View.VISIBLE);
                rl_loader10.setVisibility(View.GONE);
                tv_interiorroof.setText(filePath);
                loader10.setVisibility(View.GONE);
                break;
            case "boot":

                bt_upload11.setVisibility(View.VISIBLE);
                rl_loader11.setVisibility(View.GONE);
                tv_boot.setText(filePath);
                loader11.setVisibility(View.GONE);
                break;
            case "enginebay":
                loader12.setVisibility(View.GONE);
                bt_upload12.setVisibility(View.VISIBLE);
                rl_loader12.setVisibility(View.GONE);
                tv_enginebay.setText(filePath);
                loader12.setVisibility(View.GONE);
                break;
            case "other1":
                bt_upload13.setVisibility(View.VISIBLE);
                rl_loader13.setVisibility(View.GONE);
                tv_other1.setText(filePath);
                loader13.setVisibility(View.GONE);
                break;
            case "other2":
                bt_upload14.setVisibility(View.VISIBLE);
                rl_loader14.setVisibility(View.GONE);
                tv_other2.setText(filePath);
                loader14.setVisibility(View.GONE);
                break;
            case "other3":
                bt_upload15.setVisibility(View.VISIBLE);
                rl_loader15.setVisibility(View.GONE);
                tv_other3.setText(filePath);
                loader15.setVisibility(View.GONE);
                break;
            case "other4":
                bt_upload16.setVisibility(View.VISIBLE);
                rl_loader16.setVisibility(View.GONE);
                tv_other4.setText(filePath);
                loader16.setVisibility(View.GONE);
                break;
            case "other5":
                bt_upload17.setVisibility(View.VISIBLE);
                rl_loader17.setVisibility(View.GONE);
                tv_other5.setText(filePath);
                loader17.setVisibility(View.GONE);
                break;
            case "other6":
                bt_upload18.setVisibility(View.VISIBLE);
                rl_loader18.setVisibility(View.GONE);
                tv_other6.setText(filePath);
                loader18.setVisibility(View.GONE);
                break;
            case "other7":
                bt_upload19.setVisibility(View.VISIBLE);
                rl_loader19.setVisibility(View.GONE);
                tv_other7.setText(filePath);
                loader19.setVisibility(View.GONE);
                break;
            case "other8":
                bt_upload20.setVisibility(View.VISIBLE);
                rl_loader20.setVisibility(View.GONE);
                tv_other8.setText(filePath);
                loader20.setVisibility(View.GONE);
                break;
        }
    }

    private void ApiCall_FileUpload_Validation() {

        if(tv_frontview.getText().toString().trim().isEmpty())
        {
            ll_frontview.requestFocus();
            Commons.Alert_custom(context,"Please fill all mandatory details");
        }
        else if(tv_driverside.getText().toString().trim().isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
            ll_driverside.requestFocus();
        }
        else if(tv_passengerside.getText().toString().trim().isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
            ll_passengerside.requestFocus();
        }
        else if(tv_rearview.getText().toString().trim().isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
            ll_rearview.requestFocus();
        }
        else if(tv_driverside1.getText().toString().trim().isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
            ll_driverside1.requestFocus();
        }
        else if(tv_frontpassenger.getText().toString().trim().isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
            ll_frontpassenger.requestFocus();
        }
        else if(tv_frontdashboard.getText().toString().trim().isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
            ll_frontdashboard.requestFocus();
        }
        else if(tv_rearpassenger.getText().toString().trim().isEmpty())
        {
            Commons.Alert_custom(context,"Please fill all mandatory details");
            ll_rearpassenger.requestFocus();
        }
        else
        {
            Apicall_Sell2();
        }


    }

    private void ApiCall_Sell1() {
        Commons.showProgress(context);

        String stepno="1";
        HashMap<String,String> request=new HashMap<>();
        request.put("step_id",stepno);
        request.put("make_id",ip_makeid);
        request.put("model_id",ipmodelid);
        request.put("year",temp_year);
        request.put("transmission",temp_tranmission);
        request.put("mileage",temp_mileage);
        request.put("fuel",temp_fuel);
        request.put("accident_histroy","No");
        request.put("service_histroy","No");
        request.put("body_condition",temp_bodycondition);
        request.put("dealer_warranty",temp_dealerwarranty);
        request.put("exterior_color",temp_extcolor);
        request.put("interior_color",temp_intcolor);
        request.put("description",temp_descrption);

        System.out.println("url-step1->"+request);
        Log.e("--modellist-step1-->",request.toString());

        preownedviewModel.sellpreowned(request).observe(getActivity(), new Observer<Response_SellPreOwned>() {
            @Override
            public void onChanged(Response_SellPreOwned response_sellPreOwned) {

                Commons.hideProgress();
                System.out.println("===res=sell preowned=" + response_sellPreOwned.message);
                if(response_sellPreOwned.status.equals("1"))
                {
                    Commons.Alert_custom(context,"Car Details saved successfully");
                    tempresponse_data=response_sellPreOwned.data.tempId;
                    ll_upload.setVisibility(View.VISIBLE);
                    ll_personaldetails.setVisibility(View.GONE);
                    ll_sell_cardetails.setVisibility(View.GONE);
                }
                else
                {
                    Commons.Alert_custom(context,response_sellPreOwned.longMessage);
                }
            }
        });

    }


    private void Apicall_Sell2()
    {
        Commons.showProgress(context);
            String stepno="2";
        HashMap<String, String> request = new HashMap<>();
        for(int i=0;i<arrayList.size();i++) {
            request.put("sell_images[" + i + "]", arrayList.get(i));
        }
        request.put("step_id",stepno);
        request.put("temp_sell_enq_id",String.valueOf(tempresponse_data));

        preownedviewModel.sellpreowned(request).observe(getActivity(), new Observer<Response_SellPreOwned>() {
            @Override
            public void onChanged(Response_SellPreOwned response_sellPreOwned) {

                Commons.hideProgress();
                if(response_sellPreOwned.status.equals("1"))
                {
                    System.out.println("Stp2---"+response_sellPreOwned.data);
                    System.out.println("Stp2---"+response_sellPreOwned.message);
                    System.out.println("Stp2---"+response_sellPreOwned.longMessage);
                    tempresponse_data1=response_sellPreOwned.data.tempId;
                    System.out.println("Stp3333--"+response_sellPreOwned.longMessage);
                    Commons.Alert_custom(context,"Image upload saved successfully");
                    //Commons.Alert_custom(context,response_sellPreOwned.longMessage);
                    ll_sell_cardetails.setVisibility(View.GONE);
                    ll_upload.setVisibility(View.GONE);
                    ll_personaldetails.setVisibility(View.VISIBLE);
                }
                else if(response_sellPreOwned.status.equals("0"))
                {
                    Commons.Alert_custom(context,response_sellPreOwned.longMessage);
                    System.out.println("--failure--->"+response_sellPreOwned.longMessage);
                    System.out.println("--failure--->"+response_sellPreOwned.message);
                }
            }
            });
    }


    public void step3_Validation()
    {

        st_name=et_name.getText().toString().trim();
        st_email=et_email.getText().toString().trim();
        st_mobile=et_mobile.getText().toString().trim();
        st_remarks=et_remarks.getText().toString().trim();

        if(st_name.isEmpty() && st_mobile.isEmpty() && st_email.isEmpty() && st_remarks.isEmpty())
        {
            Toast.makeText(context,"Please Provide all details",Toast.LENGTH_SHORT).show();
        }
        else if(st_name.isEmpty())
        {
            Commons.Alert_custom(context,"Please Provide name");
            et_name.requestFocus();
        }

        else if(st_mobile.isEmpty())
        {
            Commons.Alert_custom(context,"Please Provide mobile number");
            et_mobile.requestFocus();
        }

        else if(st_email.isEmpty())
        {
            Commons.Alert_custom(context,"Please Provide email");
            et_email.requestFocus();
        }
        else if (!Commons.isValidMail(st_email))
        {
            Commons.Alert_custom(context,"Please Provide valid email-id");
            et_email.requestFocus();
        }
        else if(st_remarks.isEmpty())
        {
            Commons.Alert_custom(context,"Please Provide remarks");
            et_remarks.requestFocus();
        }
        else
        {
            Apicall_Sell3();
        }

    }


    private void Apicall_Sell3()
    {
        Commons.showProgress(context);
        String stepid="3";
        HashMap<String,String> request=new HashMap<>();
        request.put("step_id",stepid);
        request.put("temp_sell_enq_id",String.valueOf(tempresponse_data1));
        request.put("name",st_name);
        request.put("contact",st_mobile);
        request.put("email",st_email);
        request.put("remarks",st_remarks);
        System.out.println("url-modellist->"+request);
        Log.e("--modellist--->",request.toString());
        preownedviewModel.sellpreowned(request).observe(getActivity(), new Observer<Response_SellPreOwned>() {
            @Override
            public void onChanged(Response_SellPreOwned response_sellPreOwned) {
                Commons.hideProgress();

                if(response_sellPreOwned.status.equals("1"))
                {
                    System.out.println("Stp3---"+response_sellPreOwned.data);
                    System.out.println("Stp3---"+response_sellPreOwned.message);
                    System.out.println("Stp3--"+response_sellPreOwned.longMessage);
                    temp_response_data2=response_sellPreOwned.data.tempId;
                    System.out.println("Stp3333--"+response_sellPreOwned.longMessage);
                    Commons.Alert_custom(context,response_sellPreOwned.longMessage);
                    ll_sell_cardetails.setVisibility(View.GONE);
                    ll_upload.setVisibility(View.GONE);
                    ll_personaldetails.setVisibility(View.GONE);
                    getActivity().onBackPressed();

                }
                else if(response_sellPreOwned.status.equals("0"))
                {
                    Commons.Alert_custom(context,response_sellPreOwned.longMessage);
                    System.out.println("--failure3--->"+response_sellPreOwned.longMessage);
                    System.out.println("--failure-3-->"+response_sellPreOwned.message);
                }
            }
        });


    }


    /*------------------------ Sell ---------------------------- */
    private void WebServiceCall_ModelList() {

        Commons.showProgress(context);
        HashMap<String,String> request=new HashMap<>();
        request.put("make_id",ip_makeid);
        request.put("isPreowned","no");
        System.out.println("url-modellist->"+request);
        Log.e("--modellist--->",request.toString());
        preownedviewModel.getCarModeldata(request).observe(getActivity(), new Observer<Response_CarModelList>() {
            @Override
            public void onChanged(Response_CarModelList response_carModelList) {

                Commons.hideProgress();
                System.out.println("===res=car model list=" + response_carModelList.message);
                if(response_carModelList.status.equals("1"))
                {
                    carmodellist=response_carModelList.data;
                    modellistsize=carmodellist.size();
                }
                else if(response_carModelList.status.equals("0"))
                {
                    Toast.makeText(context,response_carModelList.message,Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    private void WebServiceCall_MakeList() {
        HashMap<String,String>request=new HashMap<>();
        request.put("isPreowned","no");


        preownedviewModel.getcarmakelist(request).observe(getActivity(), new Observer<Response_CarMakeList>() {
            @Override
            public void onChanged(Response_CarMakeList response_carMakeList) {
                main_ll.setVisibility(View.VISIBLE);
                if(response_carMakeList.status.equals("1")) {
                    System.out.println("===res=preowned=makelist=" + response_carMakeList.data.toString());
                    makelistsize=response_carMakeList.data.size();
                    carmakelist=response_carMakeList.data;
                    Log.i("res-=preownr maklist0->",response_carMakeList.message);

                }
                else if(response_carMakeList.status.equals("0"))
                {
                    Log.i("r=preownr maklist->",response_carMakeList.message);
                    System.out.println("===r=preownr maklist=" + response_carMakeList.data);
                    Toast.makeText(context,response_carMakeList.message,Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void Dialog_NumberPicker_MakeList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(makelistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmakelist.size()];
        str_makedid=new String[carmakelist.size()];
        for (int i=0;i<carmakelist.size();i++)
        {
            str[i]=carmakelist.get(i).makename;
            str_makedid[i]=carmakelist.get(i).mkid;
        }
        np.setDisplayedValues(str);

        temp_makelist=str[index];
        ip_makeid=str_makedid[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_make.setText(temp_makelist);
                dialog.dismiss();
                WebServiceCall_ModelList();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_makelist=str[newVal];
                ip_makeid=str_makedid[newVal];
            }
        });
        dialog.show();
    }

    private void Dialog_NumberPicker_ModelList() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        np.setMaxValue(modellistsize-1); // max value 100
        np.setMinValue(0);   // min value 0
        //
        np.setWrapSelectorWheel(false);
        String str[]=new String[carmodellist.size()];
        strmodellist=new String[carmodellist.size()];
        for (int i=0;i<carmodellist.size();i++)
        {
            str[i]=carmodellist.get(i).modelName;
            strmodellist[i]=carmodellist.get(i).modid;
        }
        np.setDisplayedValues(str);
        temp_model=str[index];
        ipmodelid=strmodellist[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_model.setText(temp_model);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_model=str[newVal];
                ipmodelid=strmodellist[newVal];
            }
        });
        dialog.show();
    }

    private void Dialog_Fuel() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
       // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] fuel_array = getResources().getStringArray(R.array.Fuel);
        np.setMaxValue(fuel_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(fuel_array);
        temp_fuel=fuel_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_fuel.setText(temp_fuel);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_fuel=fuel_array[newVal];
            }
        });
        dialog.show();
    }

    private void Dialog_Transmission() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);

        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Transmission);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_tranmission=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_transmission.setText(temp_tranmission);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_tranmission=transmission_array[newVal];
            }
        });
        dialog.show();
    }

    private void Dialog_Year() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Year);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_year=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_year.setText(temp_year);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_year=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Mileage() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.Mileage);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_mileage=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_mileage.setText(temp_mileage);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_mileage=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_Bodycondition_sell() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.body_condition_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_bodycondition=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_body_condition.setText(temp_bodycondition);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_bodycondition=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_DealerWarranty() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.dealer_warranty_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_dealerwarranty=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_dealer_warranty.setText(temp_dealerwarranty);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_dealerwarranty=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void Dialog_ExteriorSell() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);
        // min value 0
        //
        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.exterior_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_extcolor=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_exteriorcolor.setText(temp_extcolor);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                temp_extcolor=transmission_array[newVal];
            }
        });
        dialog.show();
    }

    private void Dialog_InteriorSell() {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        dialog.setContentView(R.layout.bottomsheet_numberpicker);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //Width and Height
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberpicker);
        CustomRegularTextview button_addquote;
        button_addquote=dialog.findViewById(R.id.dialog_addquote);

        np.setWrapSelectorWheel(false);
        String[] transmission_array = getResources().getStringArray(R.array.interior_sell);
        np.setMaxValue(transmission_array.length-1); // max value 100
        np.setMinValue(0);
        np.setDisplayedValues(transmission_array);
        temp_intcolor=transmission_array[index];
        button_addquote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_interior_color.setText(temp_intcolor);
                dialog.dismiss();
            }
        });


        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // temp_makelist = "PreOwned--Selected" + str[oldVal] + " to " + str[newVal];
                temp_intcolor=transmission_array[newVal];
                // Toast.makeText(context, temp_model, Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }




    public  void Select_Image(Context context)
    {
        final Dialog alertDialog;
        alertDialog = new Dialog(context);
        alertDialog.setContentView(R.layout.alert_label_editor);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        if(alertDialog.getWindow() != null) {
            alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomTextViewRegular tv_camera = alertDialog.findViewById(R.id.tv_camera);
        CustomTextViewRegular tv_gallery = alertDialog.findViewById(R.id.tv_gallery);
        CustomTextViewRegular tv_cancel = alertDialog.findViewById(R.id.tv_cancel);


        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
                alertDialog.dismiss();
            }
        });

        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
                alertDialog.dismiss();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });



        if (!alertDialog.isShowing())
            alertDialog.show();
    }


    private void selectImage(Context context) {
        final CharSequence[] options = {"Camera", "Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater=this.getLayoutInflater();

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Camera")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                        Uri tempUri = getImageUri(context, photo);
                        // CALL THIS METHOD TO GET THE ACTUAL PATH
                        File finalFile = new File(getRealPathFromURI(tempUri));
                        myFilePath=finalFile.getPath();
                        Func_Condition();

                    }

                    break;
                    // case 1 for Gallery
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor =context.getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                myFilePath=picturePath;

                                cursor.close();
                                com();

                            }
                        }
                    }
                    break;
            }
        }






    }

    private void com()
    {
        Compressor.getDefault(getActivity())
                .compressToFileAsObservable(new File(myFilePath))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<File>() {
                    @Override
                    public void call(File file) {
                        myFilePath = file.toString();
                        Func_Condition();
                        System.out.println("myFilePath--"+myFilePath);

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }

    private void Func_Condition() {

        if (buttontype.equals("1")) {

            Func_FileUpload(temp_makelist+"_"+temp_model + "_frontview");

        } else if (buttontype.equals("2")) {

            Func_FileUpload(temp_makelist+"_"+temp_model+ "_driverside");

        }
        else if (buttontype.equals("3")) {
            Func_FileUpload(temp_makelist+"_"+temp_model + "_passengerside");
        }
        else if (buttontype.equals("4")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_rearview");
        }
        else if (buttontype.equals("5")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_exteriorroof");
        }
        else if (buttontype.equals("6")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_driverside1");
        }
        else if (buttontype.equals("7")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_frontpassenger");
        }
        else if (buttontype.equals("8")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_dashboard");
        }
        else if (buttontype.equals("9")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_rearpassenger");
        }
        else if (buttontype.equals("10")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_interiorroof");
        }
        else if (buttontype.equals("11")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_boot");
        }
        else if (buttontype.equals("12")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_enginebay");
        }
        else if (buttontype.equals("13")) {

            Func_FileUpload(temp_makelist+"_"+temp_model+"_other1");
        }
        else if (buttontype.equals("14")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_other2");

        }
        else if (buttontype.equals("15")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_other3");

        }
        else if (buttontype.equals("16")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_other4");

        }
        else if (buttontype.equals("17")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_other5");

        }
        else if (buttontype.equals("18")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_other6");

        }
        else if (buttontype.equals("19")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_other7");

        } else if (buttontype.equals("20")) {
            Func_FileUpload(temp_makelist+"_"+temp_model+"_other8");
        }

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_" + Calendar.getInstance().getTime(), null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (context.getContentResolver() != null) {
            Cursor cursor =context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("OnResume"," Context : "+getActivity());

        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        //WebServiceCall(res_make,res_model,res_minyear,res_maxyear,res_mileage,res_engine,res_transmission,res_fueltype,res_varient,res_door,res_extcolor,res_intcolor);

    }

    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Pojo_PreOwnedFilter pojo_preOwnedFilter) {
        //Toast.makeText(context, "event"+pojo_preOwnedFilter.getRes_mileage1(), Toast.LENGTH_SHORT).show();
        res_make=pojo_preOwnedFilter.getRes_make1();
        res_model=pojo_preOwnedFilter.getRes_model1();
        res_minyear=pojo_preOwnedFilter.getRes_minyear1();
        res_maxyear=pojo_preOwnedFilter.getRes_maxyear1();
        res_mileage=pojo_preOwnedFilter.getRes_mileage1();
        res_engine=pojo_preOwnedFilter.getRes_engine1();
        res_varient=pojo_preOwnedFilter.getRes_varient1();
        res_transmission=pojo_preOwnedFilter.getRes_transmission1();
        res_fueltype=pojo_preOwnedFilter.getRes_fueltype1();
        res_door=pojo_preOwnedFilter.getRes_door1();
        res_extcolor=pojo_preOwnedFilter.getRes_extcolor1();
        res_intcolor=pojo_preOwnedFilter.getRes_intcolor1();
        WebServiceCall(res_make,res_model,res_minyear,res_maxyear,res_mileage,res_engine,res_transmission,res_fueltype,res_varient,res_door,res_extcolor,res_intcolor);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e("OnPause"," Context : "+getActivity() ) ;
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        preownedviewModel = ViewModelProviders.of(this).get(PreownedviewModel.class);
    }





    @Override
    public void onDestroy() {
        super.onDestroy();
        preOwned__fragment1 = null;



    }

    @Override
    public void onStop() {
        super.onStop();
        preOwned__fragment1 = null;
    }


    
    
    
    

}
