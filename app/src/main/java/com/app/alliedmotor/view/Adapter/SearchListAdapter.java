package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.Response_SearchbyKeywords;
import com.app.alliedmotor.utility.AppInstalled;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomTextViewSemiBold;
import com.app.alliedmotor.view.Activity.Car_Detail__Constarint_Activity1;

import java.util.ArrayList;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {

    ArrayList<Response_SearchbyKeywords.CarsItem> carsItems;


    SharedPref sharedPref;
    private Context mcontext;

    public SearchListAdapter(Context context, ArrayList<Response_SearchbyKeywords.CarsItem> carsItems) {
        this.mcontext = context;
        this.carsItems = carsItems;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.adapter_data, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(carsItems.get(position).carTitle!=null) {
            holder.tv_points.setText(AppInstalled.toTitleCase(carsItems.get(position).carTitle));
        }
        else {
            holder.tv_points.setVisibility(View.GONE);
        }
        holder.ll_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toIntnet=new Intent(mcontext, Car_Detail__Constarint_Activity1.class);
                toIntnet.putExtra("carid",carsItems.get(position).mainId);
                toIntnet.putExtra("searchname",carsItems.get(position).carTitle);
                mcontext.startActivity(toIntnet);
            }
        });

    }

    @Override
    public int getItemCount() {
        return carsItems.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextViewSemiBold tv_points;
       LinearLayout ll_1;

        public ViewHolder(View itemView) {

            super(itemView);

            tv_points=itemView.findViewById(R.id.tv_points);
            ll_1=itemView.findViewById(R.id.ll_1);

        }
    }
}

