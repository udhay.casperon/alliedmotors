package com.app.alliedmotor.view.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.alliedmotor.R;
import com.app.alliedmotor.model.HomePage.CategoriesItem;
import com.app.alliedmotor.utility.SharedPref;
import com.app.alliedmotor.utility.widgets.CustomRegularTextview;
import com.app.alliedmotor.view.Activity.ContactUs_Activity;

import com.app.alliedmotor.view.Activity.NewCar_Tablayout_Activity_static;
import com.app.alliedmotor.view.Activity.NewsEvents_Activity;
import com.app.alliedmotor.view.Activity.Services_Activity;
import com.app.alliedmotor.view.Activity.SpareParts_Activity;

import java.util.ArrayList;

public class HomePage_Categories_Adapter extends RecyclerView.Adapter<HomePage_Categories_Adapter.ViewHolder> {

    ArrayList<CategoriesItem> categoriesItems = new ArrayList<>();

    HomePage_Categories_Adapter.Categoires listener;
    SharedPref sharedPref;
    private Context mcontext;


    public HomePage_Categories_Adapter(Context context, ArrayList<CategoriesItem> listdata) {
        this.mcontext = context;
        this.categoriesItems = listdata;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.grid_categories_constraint, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        sharedPref = new SharedPref(mcontext);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {



            if(categoriesItems.get(position).getPostTitle().equals("New Cars"))
            {
                holder.cat_name.setText("New\nCars");
                holder.imageView.setImageResource(R.drawable.new_car);
                holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.new_car_bg));
                  holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext, NewCar_Tablayout_Activity_static.class);
                    toIntent.putExtra("tab_position","0");


                    Log.e("-->",String.valueOf(position));
                    mcontext.startActivity(toIntent);
                }
            });



            }
        if(categoriesItems.get(position).getPostTitle().equals("Luxury Cars"))
        {
            holder.cat_name.setText("Luxury\nCars");
            holder.imageView.setImageResource(R.drawable.luxury_car);
            holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.luxury_card_bg));

              holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext,NewCar_Tablayout_Activity_static.class);
                    toIntent.putExtra("tab_position","1");
                    Log.e("-->",String.valueOf(position));
                    mcontext.startActivity(toIntent);
                }
            });

        }
        if(categoriesItems.get(position).getPostTitle().equals("RHD Cars"))
        {
            holder.cat_name.setText("RHD\nCars");
            holder.imageView.setImageResource(R.drawable.rhd_car);
            holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.rhd_car_bg));
              holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext,NewCar_Tablayout_Activity_static.class);
                    toIntent.putExtra("tab_position","2");
                    Log.e("-->",String.valueOf(position));
                    mcontext.startActivity(toIntent);
                }
            });

        }
        if(categoriesItems.get(position).getPostTitle().equals("Pre Owned Cars"))
        {
            holder.cat_name.setText("Pre Owned\nCars");
            holder.imageView.setImageResource(R.drawable.pre_owned_car);
            holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.pre_owned_car_bg));

            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext,NewCar_Tablayout_Activity_static.class);
                    toIntent.putExtra("tab_position","3");
                    Log.e("-->",String.valueOf(position));
                    mcontext.startActivity(toIntent);
                }
            });
        }

        if(categoriesItems.get(position).getPostTitle().equals("Spare Parts"))
        {
            holder.cat_name.setText("Spare\nParts");
            holder.imageView.setImageResource(R.drawable.spare_parts);
            holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.spare_parts_bg));

            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext, SpareParts_Activity.class);
                    mcontext.startActivity(toIntent);
                }
                });

        }
        if(categoriesItems.get(position).getPostTitle().equals("Services"))
        {
            holder.cat_name.setText("Services");
            holder.imageView.setImageResource(R.drawable.services);
            holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.services_bg));
            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext, Services_Activity.class);
                    mcontext.startActivity(toIntent);
                }
            });

        }
        if(categoriesItems.get(position).getPostTitle().equals("News & Events"))
        {
            holder.cat_name.setText("News &\nEvents");
            holder.imageView.setImageResource(R.drawable.news_events);
            holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.news_bg));

            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext,NewsEvents_Activity.class);
                    mcontext.startActivity(toIntent);
                }
            });
        }

        if(categoriesItems.get(position).getPostTitle().equals("ContactUs"))
        {
            holder.cat_name.setText("Contact us");
            holder.imageView.setImageResource(R.drawable.icon_contactus);
            holder.cardview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.contactus_bg));
            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent toIntent=new Intent(mcontext, ContactUs_Activity.class);
                    mcontext.startActivity(toIntent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return categoriesItems.size();
    }

    public interface Categoires {
        void onCategorySelected(CategoriesItem data, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ImageView imageView;
        CardView cardview;
        CustomRegularTextview cat_name;

        public ViewHolder(View itemView) {

            super(itemView);

            this.imageView =  itemView.findViewById(R.id.imageview1);
            this.cardview =  itemView.findViewById(R.id.ll_imagecontainr);
            this.cat_name =  itemView.findViewById(R.id.cat_name);
        }
    }
}

